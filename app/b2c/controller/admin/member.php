<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */

class b2c_ctl_admin_member extends desktop_controller{

    var $workground = 'b2c_ctl_admin_member';
    var $pagelimit = 10;
    var $listpage = 30;
    var $member_model ;
    public function __construct($app)
    {
        parent::__construct($app);
        $this->member_model = $this->app->model('members');
        header("cache-control: no-store, no-cache, must-revalidate");
    }

    function index(){
        //增加会员相关权限判断@lujy
        if($this->has_permission('addmember')){
            $custom_actions[] = array('label'=>app::get('b2c')->_('添加会员'),'href'=>'index.php?app=b2c&ctl=admin_member&act=add_page','target'=>'dialog::{title:\''.app::get('b2c')->_('添加会员').'\',width:460,height:460}');
        }
        if($this->has_permission('send_email')){
            $custom_actions[] =  array('label'=>app::get('b2c')->_('群发邮件'),'submit'=>'index.php?app=b2c&ctl=admin_member&act=send_email','target'=>'dialog::{title:\''.app::get('b2c')->_('群发邮件').'\',width:700,height:400}');
        }
        if($this->has_permission('send_msg')){
            $custom_actions[] =  array('label'=>app::get('b2c')->_('群发站内信'),'submit'=>'index.php?app=b2c&ctl=admin_member&act=send_msg','target'=>'dialog::{title:\''.app::get('b2c')->_('群发站内信').'\',width:500,height:350}');
        }
        if($this->has_permission('send_sms')){
            $custom_actions[] =  array('label'=>app::get('b2c')->_('群发短信'),'submit'=>'index.php?app=b2c&ctl=admin_member&act=send_sms','target'=>'dialog::{title:\''.app::get('b2c')->_('群发短信').'\',width:500,height:590}');
        }

        $actions_base['title'] = app::get('b2c')->_('会员列表');
        $actions_base['actions'] = $custom_actions;
        $actions_base['allow_detail_popup'] = true;
        $actions_base['use_buildin_set_tag'] = true;
        $actions_base['use_buildin_export'] = true;
        $actions_base['use_buildin_filter'] = true;
        $actions_base['use_view_tab'] = true;
        //标记为后端管理会员列表页面 为了mdl层重写getList不走直接打积分查询接口的方法
        $actions_base['base_filter'] = array("bg_mg_member_list_page"=>true);

        $this->finder('b2c_mdl_members',$actions_base);
    }

    public function check_pending_list(){
        $actions_base['title'] = app::get('b2c')->_('待审核列表');
        $actions_base['actions'] = array();
        $actions_base['allow_detail_popup'] = true;
        $actions_base['use_buildin_export'] = true;
        $actions_base['use_buildin_filter'] = true;
        $actions_base['use_view_tab'] = true;//array("member_type|than" => 0,"check_status|neq" => '1');
        $actions_base['base_filter'] = array("filter_sql" => "member_type > 0 and check_status != 1");
        $this->finder('b2c_mdl_members',$actions_base);
    }

    public function group_member_list(){
        $actions_base['title'] = app::get('b2c')->_('企业会员列表');
        $actions_base['actions'] = array();
        $actions_base['allow_detail_popup'] = true;
        $actions_base['use_buildin_export'] = true;
        $actions_base['use_buildin_filter'] = true;
        $actions_base['use_view_tab'] = true;
        $actions_base['base_filter'] = array("member_type|than" => 0,"check_status" => 1);
        $this->finder('b2c_mdl_members',$actions_base);
    }

    public function  retail_member_list(){
        $actions_base['title'] = app::get('b2c')->_('分销会员列表');
        $actions_base['actions'] = array();
        $actions_base['allow_detail_popup'] = true;
        $actions_base['use_buildin_export'] = true;
        $actions_base['use_buildin_filter'] = true;
        $actions_base['use_view_tab'] = true;
        $actions_base['base_filter'] = array('filter_sql' => "(`member_type` > 0 and is_white = 1 and `check_status` = 1) or f_level > 0");
        $this->finder('b2c_mdl_members',$actions_base);
    }

    public function check_page(){
        $member_id = $_GET['member_id'];
        $this->pagedata['member_id'] = $member_id;
        $this->page('admin/member/check.html');
    }

    public function check(){
        $this->begin('index.php?app=b2c&ctl=admin_member&act=check_pending_list');
        $pk['member_id'] = $_POST['member_id'];
        $member = $this->member_model->dump($pk['member_id']);
        if(!$member){
            $this->end(false,'用户不存在！');
        }
        $data['reason'] = isset($_POST['reason']) && $_POST['reason'] ? trim($_POST['reason']) : '';
        if($_POST['invalid_name'] == 'on'){
            $data['check_status'] = '1';
            $data['state'] = 1;
            if($_POST['is_white'] == 'A'){
                $data['is_white'] = '1';
                $data['f_level'] = '1';
            }else{
                $data['is_white'] = '0';
            }
        }elseif($_POST['invalid_name'] == 'off'){
            $data['check_status'] = '2';
            if($data['reason']){
                b2c_send::sendEmail($member['email'],'审核未通过',$data['reason']);
            }
        }else{
            $data['check_status'] = '0';
        }
        $res = $this->member_model->update($data,$pk);
        if($res){
            $this->end(true);
        }else{
            $this->end(false);
        }
    }

    /*
     * 设置返佣比例
     */
    public function return_set(){
        $member_id = $_GET['member_id'];
        $member = $this->member_model->dump($member_id);
        $this->pagedata['member_id'] = $member_id;
        $this->pagedata['scale'] = $member['scale'];
        $this->page('admin/member/return_set.html');
    }

    public function return_set_do(){
        $this->begin();
        $pk['member_id'] = $_POST['member_id'];
        $member = $this->member_model->dump($pk['member_id']);
        if(!$member){
            $this->end(false,'用户不存在！');
        }
        $scale = intval($_POST['set']) ? intval($_POST['set']) : 0;
        if($scale > 100 || $scale < 0){
            $this->end(false,'返佣比例只能在0-100之间！');
        }
        $data['scale'] = $scale;
        $res = $this->member_model->update($data,$pk);
        if($res){
            $this->end(true,'设置成功!');
        }else{
            $this->end(false,'设置失败!');
        }
    }

    /*
     * 设置分成比例
     */
    public function percentage_set(){
        $member_id = $_GET['member_id'];
        $member = $this->member_model->dump($member_id);
        $this->pagedata['member_id'] = $member_id;
        $this->pagedata['percentage'] = $member['percentage'];
        $this->page('admin/member/percentage_set.html');
    }

    public function save_percentage(){
        $this->begin();
        $pk['member_id'] = $_POST['member_id'];
        $member = $this->member_model->dump($pk['member_id']);
        if(!$member){
            $this->end(false,'用户不存在！');
        }
        $percentage = intval($_POST['percentage']) ? intval($_POST['percentage']) : 0;
        if($percentage > 100 || $percentage < 0){
            $this->end(false,'返佣比例只能在0-100之间！');
        }
        $data['percentage'] = $percentage;
        $res = $this->member_model->update($data,$pk);
        if($res){
            $this->end(true,'设置成功!');
        }else{
            $this->end(false,'设置失败!');
        }
    }

    /*
     * 绑定下线
     */
    public function bind_page(){
        $member_id = $_GET['member_id'];
        $member = $this->member_model->dump($member_id);
        $this->pagedata['member_id'] = $member['member_id'];
        $this->page('admin/member/bind_page.html');
    }

    public function bind(){
        $this->begin();
        $member_id = $_POST['member_id'];
        $email = $_POST['email'] ? trim($_POST['email']) : '';
        $account = app::get('pam')->model('members')->getRow('member_id',array('login_account' => $email));
        if(!$account){
            $this->end(false,'此账号不存在!');
        }
        $memberInfo = app::get('b2c')->model('members')->getRow('member_id,member_type,f_level,is_white,foreign_id',array('member_id' => $account['member_id']));
        $recommendMemberInfo = app::get('b2c')->model('members')->getRow('member_id,member_type,f_level,is_white,foreign_id',array('member_id' => $_POST['member_id']));
        if($memberInfo['member_type'] > 0){
            $this->end(false,'下线不能绑定企业用户!');
        }
        if($memberInfo['foreign_id']){
            $this->end(false,'此账号已绑定上线!');
        }
        if($recommendMemberInfo['is_white'] == '1'){
            $res = app::get('b2c')->model('members')->update(['foreign_id' => $member_id,'f_level' => 2],['member_id' => $account['member_id']]);
        }
        if($recommendMemberInfo['is_white'] == '2'){
            $res = app::get('b2c')->model('members')->update(['foreign_id' => $member_id,'f_level' => 3],['member_id' => $account['member_id']]);
        }
        if($recommendMemberInfo['is_white'] == '3'){
            $this->end(false,'三级下线不能绑定下线!');
        }

        if($res){
            $this->end(true,'绑定成功!');
        }
        $this->end(false,'绑定失败!');
    }

    public function unbind_page(){
        $member_id = $_GET['member_id'];
        $this->pagedata['member_id'] = $member_id;
        $this->page('admin/member/unbind_page.html');
    }

    public function unbind(){
        $this->begin();
        $member_id = $_POST['member_id'];
        $memberInfo = app::get('b2c')->model('members')->getRow('member_id,f_level,is_white,foreign_id',array('member_id' => $member_id));
        if(!$memberInfo){
            $this->end(false,'用户不存在!');
        }
        $res = app::get('b2c')->model('members')->update(['foreign_id' => 0],['member_id' => $member_id]);
        if($res){
            $this->end(true,'解绑成功!');
        }
        $this->end(false,'解绑失败!');
    }

    //设置返佣分成期限
    public function return_date_set(){
        $this->begin('index.php?app=b2c&ctl=admin_member&act=return_date_set');
        $date_num = $_POST['date_nums'] ? $_POST['date_nums'] : 0;
        $info = app::get('b2c')->model('return_date_set')->getRow('*');
        if($date_num){
            if($info){
                $res = app::get('b2c')->model('return_date_set')->update(['date_nums' => $date_num],['id' => $info['id']]);
            }else{
                $data = ['date_nums' => $date_num];
                $res = app::get('b2c')->model('return_date_set')->save($data);
            }
            if($res){
                $this->end(true,'操作成功!');
            }
            $this->end(false,'操作失败!');
        }
        $this->pagedata['date_nums'] = $info['date_nums'];
        $this->page('admin/member/return_date_set.html');
    }

    function _views(){
        $mdl_member = $this->app->model('members');
        //今日新增会员
        $today_filter = array(
            '_regtime_search'=>'between',
            'regtime_from'=>date('Y-m-d'),
            'regtime_to'=>date('Y-m-d'),
            'regtime' => date('Y-m-d'),
            '_DTIME_'=>
                array(
                    'H'=>array('regtime_from'=>'00','regtime_to'=>date('H')),
                    'M'=>array('regtime_from'=>'00','regtime_to'=>date('i'))
                )
        );
        $today_reg = $mdl_member->count($today_filter);
        $sub_menu[0] = array('label'=>app::get('b2c')->_('今日新增会员'),'optional'=>true,'filter'=>$today_filter,'addon'=>$today_reg,'href'=>'index.php?app=b2c&ctl=admin_member&act=index&view=0&view_from=dashboard');

        //昨日新增
        $date = strtotime('yesterday');
        $yesterday_filter = array(
            '_regtime_search'=>'between',
            'regtime_from'=>date('Y-m-d',$date),
            'regtime_to'=>date('Y-m-d'),
            'regtime' => date('Y-m-d',$date),
            '_DTIME_'=>
                array(
                    'H'=>array('regtime_from'=>'00','regtime_to'=>date('H',$date)),
                    'M'=>array('regtime_from'=>'00','regtime_to'=>date('i',$date))
                )
        );
        $yesterday_reg = $mdl_member->count($yesterday_filter);
        $sub_menu[1] = array('label'=>app::get('b2c')->_('昨日新增会员'),'optional'=>true,'filter'=>$yesterday_filter,'addon'=>$yesterday_reg,'href'=>'index.php?app=b2c&ctl=admin_member&act=index&view=1&view_from=dashboard');

        //TAB扩展
        foreach(kernel::servicelist('desktop_member_view_extend') as $service){
            if(method_exists($service,'getViews')) {
                $service->getViews($sub_menu);
            }
        }

        foreach($sub_menu as $k=>$v){
            if($v['optional']==false){
                $show_menu[$k] = $v;
                if(is_array($v['filter'])){
                    $v['filter'] = array_merge(array(),$v['filter']);
                }else{
                    $v['filter'] = array();
                }
                $show_menu[$k]['filter'] = $v['filter']?$v['filter']:null;
                if($k==$_GET['view']){
                    $show_menu[$k]['addon'] = $mdl_member->count($v['filter']);
                }
                $show_menu[$k]['href'] = 'index.php?app=b2c&ctl=admin_member&act=index&view='.($k).(isset($_GET['optional_view'])?'&optional_view='.$_GET['optional_view'].'&view_from=dashboard':'');
            }elseif(($_GET['view_from']=='dashboard')&&$k==$_GET['view']){
                $show_menu[$k] = $v;
            }
        }
        return $show_menu;
    }

    function add_page(){
        $member_lv=$this->app->model("member_lv");
        foreach($member_lv->getMLevel() as $row){
            $options[$row['member_lv_id']] = $row['name'];
        }
        $a_mem['lv']['options'] = is_array($options) ? $options : array(app::get('b2c')->_('请添加会员等级')) ;
        $attr = kernel::single('b2c_user_passport')->get_signup_attr();
        $this->pagedata['attr'] = $attr;
        $this->pagedata['mem'] = $a_mem;
        $this->display('admin/member/new.html');
    }

    function namecheck(){
        $userPassport = kernel::single('b2c_user_passport');
        if( !$userPassport->check_signup_account($_POST['pam_account']['login_name'],$message) ){
            echo json_encode(array('error'=>$message));exit;
        }
        echo json_encode(array('success'=>app::get('b2c')->_('该登录账号可用')));exit;
    }

    function add(){
        $this->begin();
        $userPassport = kernel::single('b2c_user_passport');
        $member_model = app::get('b2c')->model('members');

        if( !$userPassport->check_signup_account($_POST['pam_account']['login_name'],$message) ){
            $this->end(false, $message);
        }

        if(!$userPassport->check_passport($_POST['pam_account']['login_password'],$_POST['pam_account']['psw_confirm'],$message)){
            $this->end(false, $message);
        }

        $saveData = $_POST;
        $saveData = $userPassport->pre_signup_process($saveData);
        if( !$member_id = $userPassport->save_members($saveData,$msg) ){
            $this->end(false, app::get('b2c')->_('添加失败！'));
        }
        //增加会员同步 2012-5-15
        if( $member_rpc_object = kernel::service("b2c_member_rpc_sync") ) {
            $member_rpc_object->createActive($member_id);
        }
        $this->end(true, app::get('b2c')->_('添加成功！'));
    }

    function regitem(){
        $this->display('member/member_regitem.html');
    }

    function send_email(){
        if($_POST['isSelectedAll'] == '_ALL_'){
            $view_filter = $this->get_view_filter('b2c_ctl_admin_member','b2c_mdl_members');
            $aMember = array();
            $obj_member = app::get('b2c')->model('members');
            $_POST = array_merge($_POST,$view_filter);
            unset($_POST['isSelectedAll']);

            $obj_member->filter_use_like = true;
            $aData = $obj_member->getList('member_id',$_POST);
            foreach((array)$aData as $key => $val){
                $aMember[] = $val['member_id'];
            }
        }
        else{
            $aMember = $_POST['member_id'];
        }
        $aEmail = array();
        foreach( $aMember as $mid){
            $current_email = $this->get_email($mid);
            if ($current_email){
                $aEmail[] = $current_email;
            }
        }
        $this->pagedata['aEmail'] = json_encode($aEmail);
        $this->page('admin/messenger/write_email.html');
    }

    function send_msg(){
        if($_POST['isSelectedAll'] == '_ALL_'){
            $aMember = array();
            $view_filter = $this->get_view_filter('b2c_ctl_admin_member','b2c_mdl_members');
            $obj_member = app::get('b2c')->model('members');
            $_POST = array_merge($_POST,$view_filter);
            unset($_POST['isSelectedAll']);

            $obj_member->filter_use_like = true;
            $aData = $obj_member->getList('member_id',$_POST);
            foreach((array)$aData as $key => $val){
                $aMember[] = $val['member_id'];
            }
        }
        else{
            $aMember = $_POST['member_id'];
        }
        $this->pagedata['aMember'] = json_encode($aMember);
        $this->page('admin/messenger/write_msg.html');
    }

    function send_sms(){
        $obj_member = app::get('b2c')->model('members');
        $params = kernel::single('base_component_request')->get_post();
        $response = kernel::single('base_component_response');
        if($params['isSelectedAll'] == '_ALL_'){
            $aMember = array();
            $view_filter = $this->get_view_filter('b2c_ctl_admin_member','b2c_mdl_members');
            $_POST = array_merge($_POST,$view_filter);
            unset($_POST['isSelectedAll']);

            $obj_member->filter_use_like = true;
            $aData = $obj_member->getList('member_id',$_POST);
            foreach($aData as $val){
                $aMember[] = $val['member_id'];
            }
        }
        else{
            $aMember = $params['member_id'];
        }
        foreach( $aMember as $mid){
            $row = app::get('pam')->model('members')->getList('login_account',array('login_type'=>'mobile','member_id' => $mid));
            if($row[0]['login_account']){
                $mobile_number[] = $row[0]['login_account'];
            }else{
                $noMobile[] = $mid;
            }
        }

        if($noMobile) {
            $account = kernel::single('pam_mdl_members')->getList('login_account',array('login_type'=>'local','member_id'=>$noMobile));
            $wechat_obj = kernel::single('weixin_wechat');
            foreach($account as &$value){
                $value['login_account'] = $wechat_obj->emoji_decode($value['login_account']);
            }
            $this->pagedata['noMobile'] = $account;
        }

        $setSmsSign = app::get('b2c')->getConf('setSmsSign');
        $setSmsSign = '【'.$setSmsSign['sign'].'】';
        $setSmsSignLen = mb_strlen(urldecode(trim($setSmsSign)),'utf-8')+4;
        $this->pagedata['setSmsSignLen'] = $setSmsSignLen;
        $this->pagedata['mobile_number'] = json_encode($mobile_number);
        $this->page('admin/messenger/write_sms.html');
    }
    //判断短信内容
    function checkReg($params){
        $arr = array(
            '【', '】',
        );

        if ((strstr($params, $arr[0]) && (strstr($params, $arr[1]))) != false)
        {
            return 'false';
        }

        return $params;

    }
    function sms_queue(){
        $this->begin();
        $member_obj = $this->app->model('members');
        $mobile_number = json_decode($_POST['mobile_number']);
        if(!$mobile_number) $this->end(false,app::get('b2c')->_('所选会员都没有填写手机号码'));
        $mobile_number = array_unique($mobile_number);

        $mobile_number = array_chunk($mobile_number,200,false);
        $_POST['sendType'] = 'fan-out';
        //判断短信内容
        $content=$this->checkReg($_POST['content']);
        if($content=='false')
        {
            $this->end(false,app::get('b2c')->_('不能含有非法字符'));
        }
        $_POST['content'].="退订回N";
        foreach($mobile_number as $m){
            $params = array(
                'mobile_number' => implode(',',(array)$m),
                'data' => $_POST);

            if(!system_queue::instance()->publish('b2c_tasks_sendsms', 'b2c_tasks_sendsms', $params)){
                $this->end(false,app::get('b2c')->_('操作失败！'));
            }
        }
        $this->end(true,app::get('b2c')->_('操作成功！'));
    }

    function msg_queue(){
        $this->begin();
        $member_obj = $this->app->model('members');
        $aMember = json_decode($_POST['arrMember']);
        unset($_POST['arrMember']);
        foreach($aMember as $key=>$val){
            $login_name = kernel::single('b2c_user_object')->get_member_name(null,$val);
            $params = array(
                'member_id'=>$val,
                'data' =>$_POST,
                'name' => $login_name,
            );

            if(!system_queue::instance()->publish('b2c_tasks_sendmsg', 'b2c_tasks_sendmsg', $params)){
                $this->end(false,app::get('b2c')->_('操作失败！'));
            }
        }
        $this->end(true,app::get('b2c')->_('操作成功！'));
    }

    function insert_queue(){
        #$this->begin('index.php?app=b2c&ctl=admin_member&act=index');
        $this->begin();
        $aEmail = json_decode($_POST['aEmail']);
        $service = kernel::service("b2c.messenger.email_content");
        if(is_object($service))
        {
            if(method_exists($service,'get_content'))
                $_POST['content'] = $service->get_content($_POST['content']);
        }
        $content = trim($_POST['content'],'&nbsp;');
        if(empty($content)){
            $this->end(false,app::get('b2c')->_('邮件内容不能为空！'));
        }
        foreach($aEmail as $key=>$val){
            $params = array(
                'acceptor'=>$val,
                'body' =>$_POST['content'],
                'title' =>$_POST['title'],
            );
            if(!system_queue::instance()->publish('b2c_tasks_sendemail', 'b2c_tasks_sendemail', $params)){
                $this->end(false,app::get('b2c')->_('操作失败！'));
            }
        }
        $this->end(true,app::get('b2c')->_('操作成功！'));
    }

    function get_email($member_id){
        $obj_member = app::get('pam')->model('members');
        $sdf = $obj_member->getList('login_account',array('login_type'=>'email','member_id'=>$member_id));
        return $sdf[0]['login_account'];
    }

    function chkpassword($member_id=null){
        $member = $this->app->model('members');
        //modified by zengxinwen
        $_POST=utils::_filter_input($_POST);
        if($_POST){
            $userPassport = kernel::single('b2c_user_passport');
            $this->begin();
            $member_id = $_POST['member_id'];
            if ( !$userPassport->check_passport($_POST['newPassword'],$_POST['confirmPassword'],$msg) ){
                $this->end(false,$msg);
            }

            if ( !$userPassport->reset_passport($member_id,trim($_POST['newPassword'])) ){
                $msg=app::get('b2c')->_('密码修改失败！');
                $this->end(false,$msg);
            }

            $arr_colunms = $userPassport->userObject->get_pam_data('*',$member_id);
            $aData['email'] = $arr_colunms['email'];
            $aData['uname'] = $arr_colunms['local'] ? $arr_colunms['local'] : $arr_colunms['mobile'];
            $aData['passwd'] = $data['passwd'];

            //发送邮件或者短信
            $obj_account=$this->app->model('member_account');
            $obj_account->fireEvent('chgpass',$aData,$member_id);
            kernel::single('pam_lock')->flush_lock($member_id);
            $msg=app::get('b2c')->_('密码修改成功！');
            $this->end(true,$msg);
        }
        $this->pagedata['member_id'] = $member_id;
        $this->display('admin/member/chkpass.html');
    }

    function detail_order($member_id=null){
        if(!$member_id) return null;
        $nPage = $_GET['detail_order'] ? $_GET['detail_order'] : 1;
        $app = app::get('b2c');
        $member = $app->model('members');
        $orders = $member->getOrderByMemId($member_id,$this->pagelimit*($nPage-1),$this->pagelimit);
        $order =  $app->model('orders');
        if($member_id){
            $row = $order->getList('order_id',array('member_id' => $member_id));
            $count = count($row);
        }
        foreach($orders as $key=>$order1){
            $orders[$key]['status'] = $order->trasform_status('status',$orders[$key]['status']);
            $orders[$key]['pay_status'] = $order->trasform_status('pay_status',$orders[$key]['pay_status'] );
            $orders[$key]['ship_status'] = $order->trasform_status('ship_status', $orders[$key]['ship_status']);
        }

        $render = $app->render();
        $render->pagedata['orders'] = $orders;
        if($_GET['page']) unset($_GET['page']);
        $_GET['page'] = 'detail_order';
        $render->pagedata['pager']= $this->pagination($nPage,$count,$_GET);
        return $render->fetch('admin/member/order.html');
    }

    function detail_recommend($member_id=null){
        if(!$member_id) return null;
        $nPage = $_GET['detail_recommend'] ? $_GET['detail_recommend'] : 1;
        $app = app::get('b2c');

        $memberModel = $app->model('members');
        $members = $memberModel->getList('member_id,name,email,order_num',['foreign_id' => $member_id],$this->pagelimit*($nPage-1),$this->pagelimit);
        $count = $memberModel->getList('member_id',['foreign_id' => $member_id]);

        $render = $app->render();
        $render->pagedata['members'] = $members;
        if($_GET['page']) unset($_GET['page']);
        $_GET['page'] = 'detail_recommend';
        $render->pagedata['pager']= $this->pagination($nPage,count($count),$_GET);
        return $render->fetch('admin/member/page_recommend.html');
    }

    public function detail_return_order($member_id=null){
        if(!$member_id) return null;
        $nPage = $_GET['detail_return_order'] ? $_GET['detail_return_order'] : 1;
        $app = app::get('b2c');
        $member = $app->model('members');
        $memberInfo = $member->getRow('member_id,scale,member_type,is_white,check_status',['member_id' => $member_id]);
        if($memberInfo['member_type'] > 0 && $memberInfo['is_white'] == '1' && $memberInfo['check_status'] == '1'){
            $member = $app->model('members');
            $memberInfo = $member->getRow('member_id,scale',['member_id' => $member_id]);
            $orders = $member->getReturnOrderByMemberId($member_id,$this->pagelimit*($nPage-1),$this->pagelimit);
            $orderModel =  $app->model('orders');
            $count = $orderModel->getList('order_id',array('member_id' => $member_id,'order_status'=> b2c_mdl_orders::ORDER_STATUS_FINISHED));
            if($orders){
                foreach($orders as $key =>$order){
                    if(!$order['return_status']){
                        //$orders[$key]['operate'] = '<a href="index.php?app=b2c&ctl=admin_member&act=returnSet&order_id='.$order['order_id'].'&member_id='.$member_id.'&is_return=1" onclick="if(confirm(\'确定返佣?\')==false)return false;">返佣</a>|<a href="index.php?app=b2c&ctl=admin_member&act=returnSet&order_id='.$order['order_id'].'&member_id='.$member_id.'" onclick="if(confirm(\'确定不设置返佣?\')==false)return false;">不返佣</a>';
                        $orders[$key]['operate'] = '<span onclick="new Dialog(\'index.php?app=b2c&ctl=admin_member&act=returnSet&member_id='.$member_id.'&order_id='.$order['order_id'].'&is_return=1\',{width:200,height:70,title:\'确认返佣\'})" class="lnk">返佣</span>|
                        <span onclick="new Dialog(\'index.php?app=b2c&ctl=admin_member&act=returnSet&member_id='.$member_id.'&order_id='.$order['order_id'].'\',{width:200,height:70,title:\'确认不返佣\'})" class="lnk">不返佣</span>';
                    }
                    if($order['return_status'] == '1'){
                        $orders[$key]['operate'] = '已返佣';
                    }
                    if($order['return_status'] == '2'){
                        $orders[$key]['operate'] = '不返佣';
                    }
                    $orders[$key]['scale'] = bcmul($order['total_amount'],bcdiv($memberInfo['scale'],100,2),2);
                    $orders[$key]['status'] = b2c_mdl_orders::getOrderStatusTxtForUser($orders[$key]['order_status']);
                    $orders[$key]['pay_status'] = $orderModel->trasform_status('pay_status',$orders[$key]['pay_status'] );
                    $orders[$key]['return_status'] = $orderModel->trasform_status('return_status',$orders[$key]['return_status']);
                }
            }
        }else{
            $orders = [];
            $count = [];
        }
        $render = $app->render();
        $render->pagedata['orders'] = $orders;
        if($_GET['page']) unset($_GET['page']);
        $_GET['page'] = 'detail_return_order';
        $render->pagedata['pager']= $this->pagination($nPage,count($count),$_GET);
        return $render->fetch('admin/member/return_order.html');
    }

    //返佣操作
    public function returnSet(){
        $order_id = $_GET['order_id'] ? $_GET['order_id'] : 0;
        $member_id = $_GET['member_id'] ? $_GET['member_id'] : 0;
        $is_return = $_GET['is_return'] ? trim($_GET['is_return']) : '';
        if($_POST){
            $this->begin();
            $order_id = $_POST['order_id'] ? $_POST['order_id'] : 0;
            $member_id = $_POST['member_id'] ? $_POST['member_id'] : 0;
            $is_return = $_POST['is_return'] ? trim($_POST['is_return']) : '';

            $memberInfo = app::get('b2c')->model('members')->getRow('member_id,scale',['member_id' => $member_id]);
            $orderInfo = app::get('b2c')->model('orders')->getRow('order_id,total_amount,createtime',['order_id' => $order_id,'member_id' => $member_id]);
            if(!$memberInfo || !$orderInfo){
                $this->end(false,'操作失败！');
            }
            if(!$memberInfo['scale']){
                $this->end(false,'未设置返佣比例！');
            }
            if($orderInfo['return_status']){
                $this->end(false,'此订单已设置返佣或不返佣！');
            }
            $date_nums = app::get('b2c')->model('return_date_set')->getRow('*');
            if($date_nums && (time() - $orderInfo['createtime']) > $date_nums['date_nums'] * 24 * 60 * 60){
                $this->end(false,'已过设置返佣期限！');
            }
            $money = bcmul($orderInfo['total_amount'],bcdiv($memberInfo['scale'],100,2),2);
            if($is_return && $is_return == '1'){
                $data = ['member_id' => $member_id,'order_id' => $order_id,'return_money' => $money,'return_status' => 1,'createtime' => time()];
                $res = app::get('b2c')->model('return_log')->save($data);
                app::get('b2c')->model('orders')->update(['return_status' => '1'],['order_id' => $order_id]);
            }else{
                $data = ['member_id' => $member_id,'order_id' => $order_id,'return_money' => $money,'return_status' => 0,'createtime' => time()];
                $res = app::get('b2c')->model('return_log')->save($data);
                app::get('b2c')->model('orders')->update(['return_status' => '2'],['order_id' => $order_id]);
            }
            if($res){
                $this->end(true,'操作成功！');
            }
            $this->end(false,'操作失败！');
        }
        $this->pagedata['order_id'] = $order_id;
        $this->pagedata['member_id'] = $member_id;
        $this->pagedata['is_return'] = $is_return;
        $this->display('admin/member/confirm_return.html');
    }

    public function detail_percentage_order($member_id=null){
        if(!$member_id) return null;
        $nPage = $_GET['detail_percentage_order'] ? $_GET['detail_percentage_order'] : 1;
        $app = app::get('b2c');
        $member = $app->model('members');
        $orders = $member->getPercentageOrderByMemberId($member_id,$this->pagelimit*($nPage-1),$this->pagelimit);
        $orderModel =  $app->model('orders');
        $count = $orderModel->getList('order_id',array('percentage_member' => $member_id,'order_status'=> b2c_mdl_orders::ORDER_STATUS_FINISHED));
        if($orders){
            foreach($orders as $key =>$order){
                if(!$order['percentage_status']){
                    //$orders[$key]['operate'] = '<a href="index.php?app=b2c&ctl=admin_member&act=returnSet&order_id='.$order['order_id'].'&member_id='.$member_id.'&is_return=1" onclick="if(confirm(\'确定返佣?\')==false)return false;">返佣</a>|<a href="index.php?app=b2c&ctl=admin_member&act=returnSet&order_id='.$order['order_id'].'&member_id='.$member_id.'" onclick="if(confirm(\'确定不设置返佣?\')==false)return false;">不返佣</a>';
                    $orders[$key]['operate'] = '<span onclick="new Dialog(\'index.php?app=b2c&ctl=admin_member&act=percentage&member_id='.$member_id.'&order_id='.$order['order_id'].'&is_return=1\',{width:200,height:70,title:\'确认分成\'})" class="lnk">分成</span>|
                    <span onclick="new Dialog(\'index.php?app=b2c&ctl=admin_member&act=percentage&member_id='.$member_id.'&order_id='.$order['order_id'].'\',{width:200,height:70,title:\'确认不分成\'})" class="lnk">不分成</span>';
                }
                if($order['percentage_status'] == '1'){
                    $orders[$key]['operate'] = '已分成';
                }
                if($order['percentage_status'] == '2'){
                    $orders[$key]['operate'] = '不分成';
                }
                $orders[$key]['order_status'] = b2c_mdl_orders::getOrderStatusTxtForUser($orders[$key]['order_status']);
                $orders[$key]['pay_status'] = $orderModel->trasform_status('pay_status',$orders[$key]['pay_status'] );
                $orders[$key]['percentage_status'] = $orderModel->trasform_status('percentage_status',$orders[$key]['percentage_status']);
            }
        }
        $render = $app->render();
        $render->pagedata['orders'] = $orders;
        if($_GET['page']) unset($_GET['page']);
        $_GET['page'] = 'detail_percentage_order';
        $render->pagedata['pager']= $this->pagination($nPage,count($count),$_GET);
        return $render->fetch('admin/member/percentage_order.html');
    }

    //分成操作
    public function percentage(){
        $order_id = $_GET['order_id'] ? $_GET['order_id'] : 0;
        $member_id = $_GET['member_id'] ? $_GET['member_id'] : 0;
        $is_return = $_GET['is_return'] ? trim($_GET['is_return']) : '';
        if($_POST){
            $this->begin();
            $order_id = $_POST['order_id'] ? $_POST['order_id'] : 0;
            $member_id = $_POST['member_id'] ? $_POST['member_id'] : 0;
            $is_return = $_POST['is_return'] ? trim($_POST['is_return']) : '';
            $memberInfo = app::get('b2c')->model('members')->getRow('member_id,percentage',['member_id' => $member_id]);
            $orderInfo = app::get('b2c')->model('orders')->getRow('order_id,member_id,order_status,total_amount,percentage_member,createtime',['order_id' => $order_id]);
            if(!$memberInfo || !$orderInfo){
                $this->end(false,'操作失败！');
            }
            if(!$memberInfo['percentage']){
                $this->end(false,'未设置分成比例！');
            }
            if($orderInfo['percentage_status']){
                $this->end(false,'此订单已设置分成或不分成！');
            }
            $date_nums = app::get('b2c')->model('return_date_set')->getRow('*');
            if($date_nums && (time() - $orderInfo['createtime']) > $date_nums['date_nums'] * 24 * 60 * 60){
                $this->end(false,'已过设置分成期限！');
            }
            $money = bcmul($orderInfo['total_amount'],bcdiv($memberInfo['percentage'],100,2),2);
            if($is_return && $is_return == '1'){
                $data = ['member_id' => $member_id,'order_id' => $order_id,'percentage_money' => $money,'percentage_status' => 1,'createtime' => time()];
                $res = app::get('b2c')->model('percentage_log')->save($data);
                app::get('b2c')->model('orders')->update(['percentage_status' => '1'],['order_id' => $order_id]);
            }else{
                $data = ['member_id' => $member_id,'order_id' => $order_id,'percentage_money' => $money,'percentage_status' => 0,'createtime' => time()];
                $res = app::get('b2c')->model('percentage_log')->save($data);
                app::get('b2c')->model('orders')->update(['percentage_status' => '2'],['order_id' => $order_id]);
            }
            if($res){
                $this->end(true,'操作成功！');
            }
            $this->end(false,'操作失败！');
        }
        $this->pagedata['order_id'] = $order_id;
        $this->pagedata['member_id'] = $member_id;
        $this->pagedata['is_return'] = $is_return;
        $this->display('admin/member/confirm_percentage.html');
    }

    public function return_order_list(){
        $type = $_GET['type'];
        $time_from = $_GET['time_from'] ? strtotime($_GET['time_from']) : 0;
        $time_to = $_GET['time_to'] ? strtotime($_GET['time_to']) : 0;
        $page = $_GET['page'] ? $_GET['page'] : 1;
        $pageSize = $_GET['pageSize'] ? $_GET['pageSize'] : $this->listpage;
        $this->begin();
        if($time_from && $time_to && $time_from >= $time_to){
            $this->end(false,'结束时间不能大于开始时间！');
        }
        $where = '';
        $link = '';
        if(!$type){
            $where .= " and o.return_status = 0";
            $link = "index.php?app=b2c&ctl=admin_member&act=return_order_list&page=%d";
        }
        if($type == '1'){
            $where .= " and o.return_status = 1";
            $link = "index.php?app=b2c&ctl=admin_member&act=return_order_list&type=1&page=%d";
        }
        if($type == '2'){
            $where .= " and o.return_status = 2";
            $link = "index.php?app=b2c&ctl=admin_member&act=return_order_list&type=2&page=%d";
        }
        if($time_from){
            $where .= " and o.createtime >= $time_from";
        }
        if($time_to){
            $where .= " and o.createtime <= $time_to";
        }

        /**
         * @var b2c_mdl_members $membersModel
         */
        $membersModel = $this->app->model('members');
        $count = $membersModel->getAllReturnOrdersCount($where);
        $orders = $membersModel->getAllReturnOrders($where,($page - 1) * $pageSize,$pageSize);
        if($orders){
            $orderModel = $this->app->model('orders');
            foreach($orders as $key =>$order){
                if(!$order['return_status']){
                    //$orders[$key]['operate'] = '<a href="index.php?app=b2c&ctl=admin_member&act=returnSet&order_id='.$order['order_id'].'&member_id='.$member_id.'&is_return=1" onclick="if(confirm(\'确定返佣?\')==false)return false;">返佣</a>|<a href="index.php?app=b2c&ctl=admin_member&act=returnSet&order_id='.$order['order_id'].'&member_id='.$member_id.'" onclick="if(confirm(\'确定不设置返佣?\')==false)return false;">不返佣</a>';
                    $orders[$key]['operate'] = '<span onclick="new Dialog(\'index.php?app=b2c&ctl=admin_member&act=returnSet&member_id='.$order['member_id'].'&order_id='.$order['order_id'].'&is_return=1\',{width:200,height:70,title:\'确认返佣\'})" class="lnk">返佣</span>|
                        <span onclick="new Dialog(\'index.php?app=b2c&ctl=admin_member&act=returnSet&member_id='.$order['member_id'].'&order_id='.$order['order_id'].'\',{width:200,height:70,title:\'确认不返佣\'})" class="lnk">不返佣</span>';
                }
                if($order['return_status'] == '1'){
                    $orders[$key]['operate'] = '已返佣';
                }
                if($order['return_status'] == '2'){
                    $orders[$key]['operate'] = '不返佣';
                }
                $orders[$key]['scale'] = bcmul($order['total_amount'],bcdiv($order['scale'],100,2),2);
                $orders[$key]['order_status'] = b2c_mdl_orders::getOrderStatusTxtForUser($orders[$key]['order_status']);
                $orders[$key]['pay_status'] = $orderModel->trasform_status('pay_status',$orders[$key]['pay_status'] );
                $orders[$key]['return_status'] = $orderModel->trasform_status('return_status',$orders[$key]['return_status']);
            }
        }
        if($time_from){
            $this->pagedata['time_from'] = $time_from;
        }
        if($time_to){
            $this->pagedata['time_to'] = $time_to;
        }
        $this->pagedata['type'] = $type;
        $this->pagedata['orders'] = $orders;
        $pagination = new base_component_ui($this->app);
        $this->pagedata['pager'] = $pagination->pager(['current' => $page,'total' =>ceil($count/$pageSize),'link' => $link]);
        $this->page('admin/member/return_order_list.html');
    }

    public function percentage_order_list(){
        $type = $_GET['type'];
        $time_from = $_GET['time_from'] ? strtotime($_GET['time_from']) : 0;
        $time_to = $_GET['time_to'] ? strtotime($_GET['time_to']) : 0;
        $page = $_GET['page'] ? $_GET['page'] : 1;
        $pageSize = $_GET['pageSize'] ? $_GET['pageSize'] : $this->listpage;
        $this->begin();
        if($time_from && $time_to && $time_from >= $time_to){
            $this->end(false,'结束时间不能大于开始时间！');
        }
        $where = '';
        $link = '';
        if(!$type){
            $where .= " and percentage_status = 0";
            $link = "index.php?app=b2c&ctl=admin_member&act=percentage_order_list&page=%d";
        }
        if($type == '1'){
            $where .= " and percentage_status = 1";
            $link = "index.php?app=b2c&ctl=admin_member&act=percentage_order_list&type=1&page=%d";
        }
        if($type == '2'){
            $where .= " and percentage_status = 2";
            $link = "index.php?app=b2c&ctl=admin_member&act=percentage_order_list&type=2&page=%d";
        }
        if($time_from){
            $where .= " and createtime >= $time_from";
        }
        if($time_to){
            $where .= " and createtime <= $time_to";
        }

        /**
         * @var b2c_mdl_members $membersModel
         */
        $membersModel = $this->app->model('members');
        $count = $membersModel->getAllPercentageOrderCount($where);
        $orders = $membersModel->getAllPercentageOrder($where,($page - 1) * $pageSize,$pageSize);
        if($orders){
            $orderModel = $this->app->model('orders');
            foreach($orders as $key =>$order){
                $memberInfo = $membersModel->getRow('member_id,percentage,email',['member_id' => $order['percentage_member']]);
                $orderMemberInfo = $this->app->model('members')->getRow('email',['member_id' => $order['member_id']]);
                if(!$order['percentage_status']){
                    //$orders[$key]['operate'] = '<a href="index.php?app=b2c&ctl=admin_member&act=returnSet&order_id='.$order['order_id'].'&member_id='.$member_id.'&is_return=1" onclick="if(confirm(\'确定返佣?\')==false)return false;">返佣</a>|<a href="index.php?app=b2c&ctl=admin_member&act=returnSet&order_id='.$order['order_id'].'&member_id='.$member_id.'" onclick="if(confirm(\'确定不设置返佣?\')==false)return false;">不返佣</a>';
                    $orders[$key]['operate'] = '<span onclick="new Dialog(\'index.php?app=b2c&ctl=admin_member&act=percentage&member_id='.$order['percentage_member'].'&order_id='.$order['order_id'].'&is_return=1\',{width:200,height:70,title:\'确认返佣\'})" class="lnk">分成</span>|
                        <span onclick="new Dialog(\'index.php?app=b2c&ctl=admin_member&act=percentage&member_id='.$order['percentage_member'].'&order_id='.$order['order_id'].'\',{width:200,height:70,title:\'确认不返佣\'})" class="lnk">不分成</span>';
                }
                if($order['percentage_status'] == '1'){
                    $orders[$key]['operate'] = '已分成';
                }
                if($order['percentage_status'] == '2'){
                    $orders[$key]['operate'] = '不分成';
                }
                $orders[$key]['member_email'] = $orderMemberInfo['email'];
                $orders[$key]['percentage_member_email'] = $memberInfo['email'];
                $orders[$key]['percentage'] = bcmul($order['total_amount'],bcdiv($memberInfo['percentage'],100,2),2);
                $orders[$key]['order_status'] = b2c_mdl_orders::getOrderStatusTxtForUser($orders[$key]['order_status']);
                $orders[$key]['pay_status'] = $orderModel->trasform_status('pay_status',$orders[$key]['pay_status'] );
                $orders[$key]['percentage_status'] = $orderModel->trasform_status('percentage_status',$orders[$key]['return_status']);
            }
        }
        if($time_from){
            $this->pagedata['time_from'] = $time_from;
        }
        if($time_to){
            $this->pagedata['time_to'] = $time_to;
        }
        $this->pagedata['type'] = $type;
        $this->pagedata['orders'] = $orders;
        $pagination = new base_component_ui($this->app);
        $this->pagedata['pager'] = $pagination->pager(['current' => $page,'total' => ceil($count/$pageSize),'link' => $link]);
        $this->page('admin/member/percentage_order_list.html');
    }

    //返佣日志列表
    public function return_log_list(){
        $time_from = $_GET['time_from'] ? strtotime($_GET['time_from']) : 0;
        $time_to = $_GET['time_to'] ? strtotime($_GET['time_to']) : 0;
        $page = $_GET['page'] ? $_GET['page'] : 1;
        $pageSize = $_GET['pageSize'] ? $_GET['pageSize'] : $this->listpage;
        $this->begin();
        if($time_from && $time_to && $time_from >= $time_to){
            $this->end(false,'结束时间不能大于开始时间！');
        }
        $where = '';
        if($time_from){
            $where .= " and o.createtime >= $time_from";
        }
        if($time_to){
            $where .= " and o.createtime <= $time_to";
        }
        /**
         * @var b2c_mdl_return_log $logModel
         */
        $logModel = $this->app->model('return_log');
        $count = $logModel->getLogCount($where);
        $logs = $logModel->getLogList($where,($page - 1) * $pageSize,$pageSize);
        if($logs){
            foreach($logs as $key =>$log){
                $orderInfo = $this->app->model('orders')->getRow('createtime',['order_id' => $log['order_id']]);
                $memberInfo = $this->app->model('members')->getRow('email',['member_id' => $log['member_id']]);
                if(!$log['return_status']){
                    $logs[$key]['return_status'] = '不返佣';
                }
                if($log['return_status'] == '1'){
                    $logs[$key]['return_status'] = '已返佣';
                }
                $logs[$key]['order_createtime'] = $orderInfo['createtime'];
                $logs[$key]['member'] = $memberInfo['email'];
            }
            $sumMoney = $logModel->getSumMoney();
        }
        if($time_from){
            $this->pagedata['time_from'] = $time_from;
        }
        if($time_to){
            $this->pagedata['time_to'] = $time_to;
        }
        $link = "index.php?app=b2c&ctl=admin_member&act=return_log_list&page=%d";
        $this->pagedata['sumMoney'] = $sumMoney;
        $this->pagedata['logs'] = $logs;
        $pagination = new base_component_ui($this->app);
        $this->pagedata['pager'] = $pagination->pager(['current' => $page,'total' =>ceil($count/$pageSize),'link' => $link]);
        $this->page('admin/member/return_log_list.html');
    }

    public function percentage_log_list(){
        $time_from = $_GET['time_from'] ? strtotime($_GET['time_from']) : 0;
        $time_to = $_GET['time_to'] ? strtotime($_GET['time_to']) : 0;
        $page = $_GET['page'] ? $_GET['page'] : 1;
        $pageSize = $_GET['pageSize'] ? $_GET['pageSize'] : $this->listpage;
        $this->begin();
        if($time_from && $time_to && $time_from >= $time_to){
            $this->end(false,'结束时间不能大于开始时间！');
        }
        $where = '';
        if($time_from){
            $where .= " createtime >= $time_from";
        }
        if($time_to){
            $where .= " createtime <= $time_to";
        }
        /**
         * @var b2c_mdl_percentage_log $logModel
         */
        $logModel = $this->app->model('percentage_log');
        $count = $logModel->getLogCount($where);
        $logs = $logModel->getLogList($where,($page - 1) * $pageSize,$pageSize);
        if($logs){
            foreach($logs as $key =>$log){
                $orderInfo = $this->app->model('orders')->getRow('member_id,createtime',['order_id' => $log['order_id']]);
                $memberInfo = $this->app->model('members')->getRow('email',['member_id' => $log['member_id']]);
                $percentageMemberInfo = $this->app->model('members')->getRow('email',['member_id' => $orderInfo['member_id']]);
                if(!$log['percentage_status']){
                    $logs[$key]['return_status'] = '不分成';
                }
                if($log['return_status'] == '1'){
                    $logs[$key]['percentage_status'] = '已分成';
                }
                $logs[$key]['order_createtime'] = $orderInfo['createtime'];
                $logs[$key]['member'] = $memberInfo['email'];
                $logs[$key]['percentage_member'] = $percentageMemberInfo['email'];
            }
            $sumMoney = $logModel->getSumMoney();
        }
        if($time_from){
            $this->pagedata['time_from'] = $time_from;
        }
        if($time_to){
            $this->pagedata['time_to'] = $time_to;
        }
        $link = "index.php?app=b2c&ctl=admin_member&act=percentage_log_list&page=%d";
        $this->pagedata['sumMoney'] = $sumMoney;
        $this->pagedata['logs'] = $logs;
        $pagination = new base_component_ui($this->app);
        $this->pagedata['pager'] = $pagination->pager(['current' => $page,'total' =>ceil($count/$pageSize),'link' => $link]);
        $this->page('admin/member/percentage_log_list.html');
    }

    public function pagination($current,$count,$get){ //本控制器公共分页函数
        $app = app::get('b2c');
        $render = $app->render();
        $ui = new base_component_ui($this->app);
        //unset($get['singlepage']);
        $link = 'index.php?app=b2c&ctl=admin_member&act=ajax_html&id='.$get['id'].'&finder_act='.$get['page'].'&'.$get['page'].'=%d';
        $this->pagedata['pager'] = $ui->pager(array(
            'current'=>$current,
            'total'=>ceil($count/$this->pagelimit),
            'link' =>$link,
        ));
        return $this->pagedata['pager'];
    }

    public function ajax_html()
    {
        $finder_act = $_GET['finder_act'];
        $html = $this->$finder_act($_GET['id']);
        echo $html;
    }

    public function detail_point($member_id=null)
    {
        if(!$member_id) return null;
        $nPage = $_GET['detail_point'] ? $_GET['detail_point'] : 1;
        $mem_point = $this->app->model('member_point');
        $data = $this->member_model->dump($member_id,'*',array('score/event'=>array('*',null,array($this->pagelimit*($nPage-1),$this->pagelimit))));
        $nodes_obj = $this->app->model('shop');
        $nodes = $nodes_obj->count( array('node_type'=>'ecos.taocrm','status'=>'bind'));

        if($nodes > 0){
            $getlog_params = array('member_id'=>$member_id,'page'=>$nPage,'page_size'=>10);
            $pointlog = kernel::single('b2c_member_point_contact_crm')->getPointLog($getlog_params);

            $count = $pointlog['total'];
            $data['score']['event'] = $pointlog['historys'];
            foreach($data['score']['event'] as $key=>$val){
                $data['score']['event'][$key]['operator_name'] = '';
            }
        }else{
            $row = $mem_point->getList('id',array('member_id' => $member_id,'status'=>'false'));
            $count = count($row);
            //获取日志操作管理员名称@lujy--start--
            foreach($data['score']['event'] as $key=>$val){
                if( $val['status'] == 'false' ){
                    $operatorInfo = app::get('pam')->model('account')->getList('login_name',array('account_id' => $val['operator']));
                    $data['score']['event'][$key]['operator_name'] = $operatorInfo['0']['login_name'];
                }else
                {
                    unset($data['score']['event'][$key]);
                }
            }
        }
        $this->pagedata['member'] = $data;
        $this->pagedata['event'] = $data['score']['event'];
        if($_GET['page']) unset($_GET['page']);
        $_GET['page'] = 'detail_point';
        $this->pagination($nPage,$count,$_GET);
        echo $this->fetch('admin/member/page_point_list.html');
    }

    function detail_coupon($member_id){
        $app = app::get('b2c');
        if(!$member_id) return null;
        $nPage = $_GET['detail_coupon'] ? $_GET['detail_coupon'] : 1;
        $oCoupon = kernel::single('b2c_coupon_mem');
        $aData = $oCoupon->get_list_m($member_id,$nPage);
        if($member_id){
            $row = $oCoupon->get_list_m($member_id);
            $count = count($row);
        }
        if ($aData) {
            foreach ($aData as $k => $item) {
                if ($item['coupons_info']['cpns_status'] !=1) {
                    $aData[$k]['coupons_info']['cpns_status'] = false;
                    $aData[$k]['memc_status'] = app::get('b2c')->_('此种优惠券已取消');
                    continue;
                }

                $curTime = time();
                if ($curTime>=$item['time']['from_time'] && $curTime<$item['time']['to_time']) {
                    if ($item['memc_used_times']<$this->app->getConf('coupon.mc.use_times')){
                        if ($item['coupons_info']['cpns_status']){
                            $aData[$k]['memc_status'] = app::get('b2c')->_('可使用');
                        }else{
                            $aData[$k]['memc_status'] = app::get('b2c')->_('本优惠券已作废');
                        }
                    }else{
                        $aData[$k]['coupons_info']['cpns_status'] = false;
                        if($item['disabled'] == 'busy'){
                            $aData[$k]['memc_status'] = app::get('b2c')->_('使用中');
                        }else{
                            $aData[$k]['memc_status'] = app::get('b2c')->_('本优惠券次数已用完');
                        }
                    }
                }else{
                    $aData[$k]['coupons_info']['cpns_status'] = false;
                    $aData[$k]['memc_status'] = app::get('b2c')->_('还未开始或已过期');
                }
            }
        }
        $this->pagedata['coupons'] = $aData;
        if($_GET['page']) unset($_GET['page']);
        $_GET['page'] = 'detail_coupon';
        $this->pagination($nPage,$count,$_GET);
        echo $this->fetch('admin/member/coupon_list.html');
    }

    public function detail_msg($member_id=null)
    {
        if(!$member_id) return null;
        $member_id = intval($member_id);
        $nPage = $_GET['detail_msg'] ? $_GET['detail_msg'] : 1;
        $this->db = kernel::database();
        $_count_row = $this->db->select('select * from sdb_b2c_member_comments where has_sent="true" and object_type="msg" and (to_id ='.$this->db->quote($member_id).' or author_id='.$this->db->quote($member_id).')');
        $row = $this->db->select('select * from sdb_b2c_member_comments where has_sent="true" and object_type="msg" and (to_id ='.$this->db->quote($member_id).' or author_id='.$this->db->quote($member_id).') limit '.$this->pagelimit*($nPage-1).','.$this->pagelimit);
        $count = count($_count_row);
        $this->pagedata['msgs'] =  $row;
        if($_GET['page']) unset($_GET['page']);
        $_GET['page'] = 'detail_msg';
        $this->pagination($nPage,$count,$_GET);
        echo $this->fetch('admin/member/member_msg.html');
    }

    public function license(){
        if($_POST['license']){
            $this->begin();
            app::get('b2c')->setConf('b2c.register.setting_member_license',$_POST['license']);
            $this->end(true, app::get('wap')->_('当前配置修改成功！'));
        }
        $this->pagedata['license'] = app::get('b2c')->getConf('b2c.register.setting_member_license');
        $this->page('admin/member/member_license.html');
    }

    public function privacy(){
        if($_POST['privacy']){
            $this->begin();
            app::get('b2c')->setConf('b2c.register.setting_member_privacy',$_POST['privacy']);
            $this->end(true, app::get('b2c')->_('当前配置修改成功！'));
        }
        $this->pagedata['privacy'] = app::get('b2c')->getConf('b2c.register.setting_member_privacy');
        $this->page('admin/member/member_privacy.html');
    }

    //异步打积分查询接口获取当前行会员的积分
    function getMemberPoint(){
        $member_id = $_POST['member_id'];
        if(!$member_id) {
            $result = array('status'=>'fail','msg'=>'参数为空!');
            echo json_encode($result);exit;
        }
        //走统一获取积分的方法
        $mem_point = $this->app->model('member_point');
        $point = $mem_point->get_total_count($member_id);
        $result = array('status'=>'succ','point'=>$point);
        echo json_encode($result);exit;
    }

}
