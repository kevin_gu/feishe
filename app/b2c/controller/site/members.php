<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */

class b2c_ctl_site_members extends b2c_frontpage
{
    public $action_view;
    public $action;
    public $header;
    public $pagesize;
    public $path = [];

    function __construct(&$app)
    {
        parent::__construct($app);
        $shopname = app::get('site')->getConf('site.name');
        if (isset($shopname)) {
            $this->title = app::get('b2c')->_('会员中心') . '_' . $shopname;
            $this->keywords = app::get('b2c')->_('会员中心_') . '_' . $shopname;
            $this->description = app::get('b2c')->_('会员中心_') . '_' . $shopname;
        }
        $this->header .= '<meta name="robots" content="noindex,noarchive,nofollow" />';
        $this->_response->set_header('Cache-Control', 'no-store');
        $this->verify_member();
        $this->pagesize = 10;
        $this->action = $this->_request->get_act_name();
        if (!$this->action) $this->action = 'index';
        $this->action_view = $this->action . ".php";
        $this->member = $this->get_current_member();
        $this->head_css = ['user-center.css'];
        /** end **/
    }

    /*
     *会员中心左侧菜单栏
     * */
    private function get_cpmenu()
    {
        $arr_bases = array(
            array(
                'label' => $this->t('订单管理'),
                'mid' => 0,
                'items' => array(
                    array('label' => $this->t('我的订单'), 'app' => 'b2c', 'ctl' => 'site_members', 'link' => 'orders'),
                    array('label' => $this->t('售后订单'), 'app' => 'b2c', 'ctl' => 'site_members', 'link' => 'refundlist'),
                    array('label' => $this->t('更多报价单'), 'app' => 'b2c', 'ctl' => 'site_members', 'link' => 'more_orders'),
                )
            ),
            array(
                'label' => $this->t('我的账户'),
                'mid' => 1,
                'items' => array(
                    array('label' => $this->t('商品收藏'), 'app' => 'b2c', 'ctl' => 'site_members', 'link' => 'favorite'),
                    array('label' => $this->t('我的发票'), 'app' => 'b2c', 'ctl' => 'site_members', 'link' => 'invoice'),
                    array('label' => $this->t('成为企业客户'), 'app' => 'b2c', 'ctl' => 'site_members', 'link' => 'company'),
                ),
            ),
            array(
                'label' => $this->t('个人信息管理'),
                'mid' => 4,
                'items' => array(
                    array('label' => $this->t('个人信息'), 'app' => 'b2c', 'ctl' => 'site_members', 'link' => 'setting'),
                    array('label' => $this->t('站内信'), 'app' => 'b2c', 'ctl' => 'site_members', 'link' => 'inbox'),
                    array('label' => $this->t('安全中心'), 'app' => 'b2c', 'ctl' => 'site_members', 'link' => 'security'),
                    array('label' => $this->t('收货地址'), 'app' => 'b2c', 'ctl' => 'site_members', 'link' => 'receiver'),
                ),
            ),
        );
        return $arr_bases;
    }

    /*
     *会员中心页面统一输出
     * */
    protected function output($app_id = 'b2c')
    {
        $this->pagedata['member'] = $this->member;
        $this->pagedata['cpmenu'] = $this->get_cpmenu();
        $this->pagedata['current'] = $this->action;
        if (!isset($this->pagedata['hiddenLeftMenu'])) {
            $this->pagedata['hiddenLeftMenu'] = false;
        }
        if ($this->pagedata['_PAGE_']) {
            $this->pagedata['_PAGE_'] = 'site/members/' . $this->pagedata['_PAGE_'];
        } else {
            $this->pagedata['_PAGE_'] = 'site/members/' . $this->action_view;
        }

        foreach (kernel::servicelist('member_index') as $service) {
            if (is_object($service)) {
                if (method_exists($service, 'get_member_html')) {
                    $aData[] = $service->get_member_html();
                }
            }
        }
        $this->pagedata['app_id'] = $app_id;
        $this->pagedata['_MAIN_'] = 'site/members/main.php';
        $this->pagedata['get_member_html'] = $aData;
        $this->set_tmpl('member');
        $this->render('site/members/main.php', $this->pagedata);
    }


    /*
     *本控制器公共分页函数
     * */
    function pagination($current, $totalPage, $act, $arg = '', $app_id = 'b2c', $ctl = 'site_members')
    {
        if (!$arg)
            $this->pagedata['pager'] = array(
                'current' => $current,
                'total' => $totalPage,
                'link' => $this->gen_url(array('app' => $app_id, 'ctl' => $ctl, 'act' => $act, 'args' => array(($tmp = time())))),
                'token' => $tmp,
            );
        else {
            $arg = array_merge($arg, array(($tmp = time())));
            $this->pagedata['pager'] = array(
                'current' => $current,
                'total' => $totalPage,
                'link' => $this->gen_url(array('app' => $app_id, 'ctl' => $ctl, 'act' => $act, 'args' => $arg)),
                'token' => $tmp,
            );
        }
    }

    function get_start($nPage, $count)
    {
        $maxPage = ceil($count / $this->pagesize);
        if ($nPage > $maxPage) $nPage = $maxPage;
        $start = ($nPage - 1) * $this->pagesize;
        $start = $start < 0 ? 0 : $start;
        $aPage['start'] = $start;
        $aPage['maxPage'] = $maxPage;
        return $aPage;
    }

    /*
     *会员中心首页
     * */
    public function index()
    {
        $userObject = kernel::single('b2c_user_object');
        //面包屑
        $this->path[] = array('title' => app::get('b2c')->_('会员中心'), 'link' => $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'index', 'full' => 1)));
        $GLOBALS['runtime']['path'] = $this->path;

        #会员基本信息,已在construct()内获取过。

        #获取会员等级
        $obj_mem_lv = $this->app->model('member_lv');
        $levels = $obj_mem_lv->getList('name,disabled', array('member_lv_id' => $this->member['member_lv']));
        if ($levels[0]['disabled'] == 'false') {
            $this->member['levelname'] = $levels[0]['name'];
        }
        $oMem_lv = $this->app->model('member_lv');
        $this->pagedata['switch_lv'] = $oMem_lv->get_member_lv_switch($this->member['member_lv']);

        //交易提醒
        $msgAlert = $this->msgAlert();
        $this->member = array_merge($this->member, $msgAlert);

        //订单列表
        $oRder = $this->app->model('orders');//--11sql
        $aData = $oRder->fetchByMember($this->app->member_id, $nPage = 1, array(), 5); //--141sql优化点
        //获取预售信息主要是时间219-231
        $prepare_order = kernel::service('prepare_order');
        if ($prepare_order) {
            $pre_order = $prepare_order->get_prepare_info($aData['data']);
            foreach ($aData['data'] as $key => $value) {
                if ($value['promotion_type'] == 'prepare') {
                    $aData['data'][$key]['prepare'] = $pre_order[$value['order_id']];
                }

            }
        }
        foreach ($aData['data'] as $key => $value) {
            $aData['data'][$key]['url'] = $this->gen_url(array('app' => 'b2c', 'ctl' => "site_member", 'act' => "receive", 'arg0' => $value['order_id']));;
            $order_payed = kernel::single('b2c_order_pay')->check_payed($value['order_id']);
            if ($order_payed == $value['total_amount']) {
                $aData['data'][$key]['is_pay'] = 1;
            } else {
                $aData['data'][$key]['is_pay'] = 0;
            }
        }
        $prepare_order = kernel::service('prepare_order');
        if ($prepare_order) {
            $pre_order = $prepare_order->get_prepare_info($aData['data']);
            foreach ($aData['data'] as $key => $value) {
                if ($value['promotion_type'] == 'prepare') {
                    //判断是否在预售期，查看订金金额是否已经支付
                    $prepare = $aData['data'][$key]['prepare'] = $pre_order[$value['order_id']];
                    $order_payed = kernel::single('b2c_order_pay')->check_payed($value['order_id']);
                    if ($order_payed > 0) {

                        if ($prepare['begin_time'] < time() && time() < $prepare['end_time']) {
                            if ($order_payed == $prepare['preparesell_price'] && $value['pay_status'] == '0') {
                                $aData['data'][$key]['is_pay'] = 1;
                            }
                        }
                        if (time() > $prepare['begin_time_final'] && $order_payed >= $prepare['promotion_price'] && ($value['pay_status'] == '0' || $value['pay_status'] == '3')) {
                            $aData['data'][$key]['is_pay'] = 1;
                        }
                    }
                }
            }
        }
        $this->get_order_details($aData, 'member_latest_orders');//--177sql 优化点
        $this->pagedata['orders'] = $aData['data'];

        //收藏列表
        $obj_member = $this->app->model('member_goods');
        $aData_fav = $obj_member->get_favorite($this->app->member_id, $this->member['member_lv'], $page = 1, $num = 4);//201sql
        $this->pagedata['favorite'] = $aData_fav['data'];
        #默认图片
        $imageDefault = app::get('image')->getConf('image.set');
        $this->pagedata['defaultImage'] = $imageDefault['S']['default_image'];

        $member_signin_obj = $this->app->model('member_signin');
        $this->pagedata['signin_status'] = $member_signin_obj->exists_signin($this->app->member_id, date('Y-m-d'));
        $this->pagedata['site_checkout_login_point_open'] = $this->app->getConf('site.checkout.login_point.open');
        $this->pagedata['site_login_point_num'] = $this->app->getConf('site.login_point.num');
        $mdl_b2c_refund_apply = $this->app->model('refund_apply');
        $this->pagedata["field_refunds_reason"] = $mdl_b2c_refund_apply->get_field_refunds_reason(); //退款申请理由列表
        //输出
        $this->pagedata['member'] = $this->member;
        $this->set_tmpl('member');
        $this->output();
    }

    /*
     *会员中心首页交易提醒 (未付款订单,到货通知，未读的评论咨询回复)
     * */
    private function msgAlert()
    {
        //获取待付款订单数
        $oRder = $this->app->model('orders');//--11sql
        $un_pay_orders = $oRder->count(array('member_id' => $this->member['member_id'], 'pay_status' => 0, 'status' => 'active', 'promotion_type' => 'normal'));
        $member['un_pay_orders'] = $un_pay_orders;
        //获取预售订单数
        $prepare_pay_orders = $oRder->count(array('member_id' => $this->member['member_id'], 'promotion_type' => 'prepare'));
        $member['prepare_pay_orders'] = $prepare_pay_orders;
        //到货通知
        $member_goods = $this->app->model('member_goods');
        $member['sto_goods_num'] = $member_goods->get_goods($this->app->member_id);

        //评论咨询回复
        $mem_msg = $this->app->model('member_comments');
        $object_type = array('discuss', 'ask');
        $aData = $mem_msg->getList('*', array('to_id' => $this->app->member_id, 'object_type' => $object_type, 'mem_read_status' => 'false', 'display' => 'true'));
        $un_readAskMsg = 0;
        $un_readDiscussMsg = 0;
        foreach ($aData as $val) {
            if ($val['object_type'] == 'ask') {
                $un_readAskMsg += 1;
            } else {
                $un_readDiscussMsg += 1;
            }
        }
        $member['un_readAskMsg'] = $un_readAskMsg;
        $member['un_readDiscussMsg'] = $un_readDiscussMsg;
        return $member;
    }


    function setting()
    {
        $this->path[] = array('title' => app::get('b2c')->_('会员中心'), 'link' => $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'index', 'full' => 1)));
        $this->path[] = array('title' => app::get('b2c')->_('个人信息'), 'link' => '#');
        $GLOBALS['runtime']['path'] = $this->path;
        $memberInfo = $this->app->model('members')->getRow('*', ['member_id' => $this->member['member_id']]);
        if($memberInfo['b_month'] && $memberInfo['b_month'] < 10){
            $memberInfo['b_month'] = '0'.$memberInfo['b_month'];
        }
        if($memberInfo['b_day'] && $memberInfo['b_day'] < 10){
            $memberInfo['b_day'] = '0'.$memberInfo['b_day'];
        }
        if($memberInfo['member_type'] > 0 && $memberInfo['check_status'] == '1'){
            switch ($memberInfo['cp_ind']){
                case '1':
                    $memberInfo['cp_ind_name'] = '地产';break;
                case '2':
                    $memberInfo['cp_ind_name'] = '酒店';break;
                case '3':
                    $memberInfo['cp_ind_name'] = '建筑设计';break;
                case '4':
                    $memberInfo['cp_ind_name'] = '室内设计';break;
                case '5':
                    $memberInfo['cp_ind_name'] = '产品设计';break;
                case '6':
                    $memberInfo['cp_ind_name'] = '卖场及代理';break;
                case '7':
                    $memberInfo['cp_ind_name'] = '项目管控';break;
                default:
                    $memberInfo['cp_ind_name'] = '地产';
            }
            if($memberInfo['cp_dim'] == '1' ){
                $memberInfo['cp_dim_name'] = '20人以下';
            }
            if($memberInfo['cp_dim'] == '2' ){
                $memberInfo['cp_dim_name'] = '20-50人';
            }
            if($memberInfo['cp_dim'] == '3' ){
                $memberInfo['cp_dim_name'] = '50-100人';
            }
            if($memberInfo['cp_dim'] == '4' ){
                $memberInfo['cp_dim_name'] = '100人以上';
            }
        }
        $this->pagedata['memberInfo'] = $memberInfo;
        $this->output();
    }

    function save_setting()
    {
        $member_id = $this->member['member_id'];
        if (!$member_id) {
            $this->splash('failed', '', app::get('b2c')->_('操作失败！'), true);
        }
        $info['uname'] = $_POST['uname'] ? $_POST['uname'] : '';
        $info['sex'] = isset($_POST['sex']) ? $_POST['sex'] : 0;
        $info['b_year'] = $_POST['b_year'] ? intval($_POST['b_year']) : 0;
        $info['b_month'] = $_POST['b_month'] ? intval($_POST['b_month']) : 0;
        $info['b_day'] = $_POST['b_day'] ? intval($_POST['b_day']) : '';
        $info['head_url'] = $_POST['head_url'] ? $_POST['head_url'] : '';
        if (!$info['uname'] || !$info['b_year'] || !$info['b_month'] || !$info['b_day'] || !$info['head_url']) {
            $this->splash('failed', '', app::get('b2c')->_($this->t('请完善资料')), true);
        }
        $memberModel = app::get('b2c')->model('members');
        $res = $memberModel->update($info, ['member_id' => $member_id]);
        if ($res) {
            $this->splash('success', '', app::get('b2c')->_($this->t('操作成功！')), true);
        }
        $this->splash('failed', '', app::get('b2c')->_($this->t('操作失败！')), true);
    }

    //修改用户名埋点
    //public function change_member_name(){
    //    $member_id    = $this->member['member_id'];
    //    $member_name  = $_POST['local_name'];
    //    $userPassport = kernel::single('b2c_user_passport');
    //    $url = $this->gen_url(array('app'=>'b2c', 'ctl'=>'site_member', 'act'=>'setting'));
    //    if($userPassport->change_member_name($member_id, $member_name, $msg))
    //        $this->splash('success', $url, app::get('b2c')->_('提交成功'), true);
    //    $this->splash('failed', null, $msg, true);
    //  //$this->splash('failed', null,$userPassport->change_member_name($member_id, $member_name, $msg), true);
    //}


    /**
     * Member order list datasource
     * @params int equal to 1
     * @return null
     */
    public function orders($pay_status = 'all', $nPage = 1)
    {
        $size = 2;
        $this->path[] = array('title' => app::get('b2c')->_('会员中心'), 'link' => $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'index', 'full' => 1)));
        $this->path[] = array('title' => app::get('b2c')->_('我的订单'), 'link' => '#');
        $GLOBALS['runtime']['path'] = $this->path;
        /**
         * @var $order b2c_mdl_orders
         */
        $order = $this->app->model('orders');
        if ($pay_status == 'all') {
            $aData = $order->fetchByMemberId($this->app->member_id, $nPage, [], $size);
        } else {
            if ($pay_status == 'wait_pay') {
                $order_status = array('order_current_status' => [b2c_mdl_orders::ORDER_STATUS_WAIT_DOWN_PAYMENT, b2c_mdl_orders::ORDER_STATUS_WAIT_FINAL_PAYMENT]);
            } else if ($pay_status == 'wait_offer') {
                $order_status = array('order_current_status' => [b2c_mdl_orders::ORDER_STATUS_WAIT_OFFER, b2c_mdl_orders::ORDER_STATUS_WAIT_ASSIGN, b2c_mdl_orders::ORDER_STATUS_WAIT_MODIFY_OFFER]);
            } else if($pay_status == 'wait_shipped') {
                $order_status = array('order_current_status' => [b2c_mdl_orders::ORDER_STATUS_WAIT_SHIPPED, b2c_mdl_orders::ORDER_STATUS_WAIT_PRODUCT, b2c_mdl_orders::ORDER_STATUS_PRODUCTION]);
            }else {
                $order_status = array('order_current_status' => $pay_status);
            }
            $aData = $order->fetchByMemberId($this->app->member_id, $nPage - 1, $order_status, $size);
            //echo '<pre>';print_r($aData);exit();
        }
        $this->get_order_details($aData, 'member_orders');
        $oImage = app::get('image')->model('image');
        $oGoods = app::get('b2c')->model('goods');
        $imageDefault = app::get('image')->getConf('image.set');
        foreach ($aData['data'] as $k => &$v) {
            $order_payed = kernel::single('b2c_order_pay')->check_payed($v['order_id']);
            if ($order_payed == $v['total_amount']) {
                $v['is_pay'] = 1;
            } else {
                $v['is_pay'] = 0;
            }

            foreach ($v['goods_items'] as $k2 => &$v2) {
                $spec_desc_goods = $oGoods->getList('spec_desc', array('goods_id' => $v2['product']['goods_id']));
                // 此处是获取购买的有规格的货品的缩略图，无规格的商品没有spec_desc，没有货品，跳过该商品。
                if (!is_array($v2['product']['products']['spec_desc']['spec_private_value_id'])) {
                    continue;
                }
                $select_spec_private_value_id = reset($v2['product']['products']['spec_desc']['spec_private_value_id']);
                if (!is_array($spec_desc_goods[0]['spec_desc'])) {
                    continue;
                }
                $spec_desc_goods = reset($spec_desc_goods[0]['spec_desc']);
                if ($spec_desc_goods[$select_spec_private_value_id]['spec_goods_images']) {
                    list($default_product_image) = explode(',', $spec_desc_goods[$select_spec_private_value_id]['spec_goods_images']);
                    $v2['product']['thumbnail_pic'] = $default_product_image;
                } else {
                    if (!$v2['product']['thumbnail_pic'] && !$oImage->getList("image_id", array('image_id' => $v['image_default_id']))) {
                        $v2['product']['thumbnail_pic'] = $imageDefault['S']['default_image'];
                    }
                }
            }
        }
        // echo '<pre>';print_r( $aData['data']);exit();
        //获取预售信息主要是时间390-398
        $prepare_order = kernel::service('prepare_order');
        if ($prepare_order) {
            $pre_order = $prepare_order->get_prepare_info($aData['data']);
            foreach ($aData['data'] as $key => $value) {
                if ($value['promotion_type'] == 'prepare') {
                    //判断是否在预售期，查看订金金额是否已经支付
                    $prepare = $aData['data'][$key]['prepare'] = $pre_order[$value['order_id']];
                    $order_payed = kernel::single('b2c_order_pay')->check_payed($value['order_id']);
                    if ($order_payed > 0) {

                        if ($prepare['begin_time'] < time() && time() < $prepare['end_time']) {
                            if ($order_payed == $prepare['preparesell_price'] && $value['pay_status'] == '0') {
                                $aData['data'][$key]['is_pay'] = 1;
                            }
                        }
                        if (time() > $prepare['begin_time_final'] && $order_payed >= $prepare['promotion_price'] && ($value['pay_status'] == '0' || $value['pay_status'] == '3')) {
                            $aData['data'][$key]['is_pay'] = 1;
                        }
                    }
                }
            }
        }
        $obj_return_policy = kernel::service("aftersales.return_policy");
        (!isset($obj_return_policy) || !is_object($obj_return_policy)) ? $is_obj_return_policy = 0 : $is_obj_return_policy = 1;
        foreach ($aData['data'] as $key => $value) {
            $aData['data'][$key]['url'] = $this->gen_url(array('app' => 'b2c', 'ctl' => "site_member", 'act' => "receive", 'arg0' => $value['order_id']));;
            if ($is_obj_return_policy) {
                $aData['data'][$key]['is_aftersales'] = $obj_return_policy->is_order_aftersales($value['order_id'], $this->app->member_id);
            }
            $aData['data'][$key]['offer'] = $order->getOrderLatestOffer($value['order_id']);
        }
        $this->pagedata['orders'] = $aData['data'];
        $arr_args = array($pay_status);
        $this->pagination($nPage, $aData['pager']['total'], 'orders', $arr_args);
        $this->pagedata["pager_pay_status"] = $pay_status;
        $mdl_b2c_refund_apply = $this->app->model('refund_apply');
        $this->pagedata["field_refunds_reason"] = $mdl_b2c_refund_apply->get_field_refunds_reason(); //退款申请理由列表
        $this->pagedata['res_url'] = $this->app->res_url;
        $this->pagedata['is_orders'] = "true";
        $this->output();
    }

    /**
     * Member order list datasource
     * @params int equal to 1
     * @return null
     */
    public function archive_orders($pay_status = 'all', $nPage = 1)
    {
        $this->path[] = array('title' => app::get('b2c')->_('会员中心'), 'link' => $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'index', 'full' => 1)));
        $this->path[] = array('title' => app::get('b2c')->_('我的订单'), 'link' => '#');
        $GLOBALS['runtime']['path'] = $this->path;
        $order = $this->app->model('archive_orders');
        $aData = $order->fetchByMember($this->app->member_id, $nPage - 1);

        $this->get_order_details($aData, 'member_orders');
        $oImage = app::get('image')->model('image');
        $imageDefault = app::get('image')->getConf('image.set');
        foreach ($aData['data'] as $k => $v) {
            foreach ($v['goods_items'] as $k2 => $v2) {
                if (!$v2['product']['thumbnail_pic'] && !$oImage->getList("image_id", array('image_id' => $v['image_default_id']))) {
                    $aData['data'][$k]['goods_items'][$k2]['product']['thumbnail_pic'] = $imageDefault['S']['default_image'];
                }
            }
        }
        foreach ($aData['data'] as $key => $value) {
            $aData['data'][$key]['url'] = $this->gen_url(array('app' => 'b2c', 'ctl' => "site_member", 'act' => "receive", 'arg0' => $value['order_id']));;
        }
        $this->pagedata['orders'] = $aData['data'];

        $arr_args = array('all');
        $this->pagination($nPage, $aData['pager']['total'], 'archive_orders', $arr_args);
        $this->pagedata['res_url'] = $this->app->res_url;
        $this->pagedata['is_orders'] = "true";
        $this->pagedata['archive'] = true;
        $this->action_view = 'orders.html';
        $this->output();
    }


    /**
     * 得到订单列表详细
     * @param array 订单详细信息
     * @param string tpl
     * @return null
     */
    protected function get_order_details(&$aData, $tml = 'member_orders')
    {
        if (isset($aData['data']) && $aData['data']) {
            $objMath = kernel::single('ectools_math');
            // 所有的goods type 处理的服务的初始化.
            $arr_service_goods_type_obj = array();
            $arr_service_goods_type = kernel::servicelist('order_goodstype_operation');
            foreach ($arr_service_goods_type as $obj_service_goods_type) {
                $goods_types = $obj_service_goods_type->get_goods_type();
                $arr_service_goods_type_obj[$goods_types] = $obj_service_goods_type;
            }

            foreach ($aData['data'] as &$arr_data_item) {
                $this->get_order_detail_item($arr_data_item, $tml);
            }
        }
    }

    /**
     * 得到订单列表详细
     * @param array 订单详细信息
     * @param string 模版名称
     * @return null
     */
    protected function get_order_detail_item(&$arr_data_item, $tpl = 'member_order_detail')
    {
        if (isset($arr_data_item) && $arr_data_item) {
            $objMath = kernel::single('ectools_math');
            // 所有的goods type 处理的服务的初始化.
            $arr_service_goods_type_obj = array();
            $arr_service_goods_type = kernel::servicelist('order_goodstype_operation');
            foreach ($arr_service_goods_type as $obj_service_goods_type) {
                $goods_types = $obj_service_goods_type->get_goods_type();
                $arr_service_goods_type_obj[$goods_types] = $obj_service_goods_type;
            }


            $arr_data_item['goods_items'] = array();
            $obj_specification = $this->app->model('specification');
            $obj_spec_values = $this->app->model('spec_values');
            $obj_goods = $this->app->model('goods');
            if (isset($arr_data_item['order_objects']) && $arr_data_item['order_objects']) {
                foreach ($arr_data_item['order_objects'] as $k => $arr_objects) {
                    $index = 0;
                    $index_adj = 0;
                    $index_gift = 0;
                    $image_set = app::get('image')->getConf('image.set');
                    if ($arr_objects['obj_type'] == 'goods') {
                        foreach ($arr_objects['order_items'] as $arr_items) {
                            if (!$arr_items['products']) {
                                $o = $this->app->model('order_items');
                                $tmp = $o->getList('*', array('item_id' => $arr_items['item_id']));
                                $arr_items['products']['product_id'] = $tmp[0]['product_id'];
                            }

                            if ($arr_items['item_type'] == 'product') {
                                if ($arr_data_item['goods_items'][$k]['product'])
                                    $arr_data_item['goods_items'][$k]['product']['quantity'] = $objMath->number_plus(array($arr_items['quantity'], $arr_data_item['goods_items'][$k]['product']['quantity']));
                                else
                                    $arr_data_item['goods_items'][$k]['product']['quantity'] = $arr_items['quantity'];

                                $arr_data_item['goods_items'][$k]['product'] = $arr_items;
                                $arr_data_item['goods_items'][$k]['product']['name'] = $arr_items['name'];
                                $arr_data_item['goods_items'][$k]['product']['goods_id'] = $arr_items['goods_id'];
                                $arr_data_item['goods_items'][$k]['product']['price'] = $arr_items['price'];
                                $arr_data_item['goods_items'][$k]['product']['score'] = intval($arr_items['score'] * $arr_data_item['goods_items'][$k]['product']['quantity']);
                                $arr_data_item['goods_items'][$k]['product']['amount'] = $arr_items['amount'];
                                $arr_goods_list = $obj_goods->getList('image_default_id,tax_number', array('goods_id' => $arr_items['goods_id']));
                                $arr_goods = $arr_goods_list[0];
                                if (!$arr_goods['image_default_id']) {
                                    $arr_goods['image_default_id'] = $image_set['S']['default_image'];
                                }
                                $arr_data_item['goods_items'][$k]['product']['thumbnail_pic'] = $arr_goods['image_default_id'];
                                $arr_data_item['goods_items'][$k]['product']['link_url'] = $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_product', 'act' => 'index', 'arg0' => $arr_items['products']['product_id']));;
                                $arr_data_item['goods_items'][$k]['product']['tax_number'] = $arr_goods['tax_number'];
                                if ($arr_items['addon']) {
                                    $arrAddon = $arr_addon = unserialize($arr_items['addon']);
                                    if ($arr_addon['product_attr'])
                                        unset($arr_addon['product_attr']);
                                    $arr_data_item['goods_items'][$k]['product']['minfo'] = $arr_addon;
                                } else {
                                    unset($arrAddon, $arr_addon);
                                }
                                if ($arrAddon['product_attr']) {
                                    foreach ($arrAddon['product_attr'] as $arr_product_attr) {
                                        $arr_data_item['goods_items'][$k]['product']['attr'] .= $arr_product_attr['label'] . $this->app->_(":") . $arr_product_attr['value'] . $this->app->_(" ");
                                    }
                                }

                                if (isset($arr_data_item['goods_items'][$k]['product']['attr']) && $arr_data_item['goods_items'][$k]['product']['attr']) {
                                    if (strpos($arr_data_item['goods_items'][$k]['product']['attr'], " ") !== false) {
                                        $arr_data_item['goods_items'][$k]['product']['attr'] = substr($arr_data_item['goods_items'][$k]['product']['attr'], 0, strrpos($arr_data_item['goods_items'][$k]['product']['attr'], $this->app->_(" ")));
                                    }
                                }
                            } elseif ($arr_items['item_type'] == 'adjunct') {
                                $str_service_goods_type_obj = $arr_service_goods_type_obj[$arr_items['item_type']];
                                $str_service_goods_type_obj->get_order_object(array('goods_id' => $arr_items['goods_id'], 'product_id' => $arr_items['products']['product_id']), $arrGoods, $tpl);


                                if ($arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj])
                                    $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['quantity'] = $objMath->number_plus(array($arr_items['quantity'], $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['quantity']));
                                else
                                    $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['quantity'] = $arr_items['quantity'];

                                if (!$arrGoods['image_default_id']) {
                                    $arrGoods['image_default_id'] = $image_set['S']['default_image'];
                                }
                                $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj] = $arr_items;
                                $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['name'] = $arr_items['name'];
                                $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['score'] = intval($arr_items['score'] * $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['quantity']);
                                $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['goods_id'] = $arr_items['goods_id'];
                                $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['price'] = $arr_items['price'];
                                $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['thumbnail_pic'] = $arrGoods['image_default_id'];
                                $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['link_url'] = $arrGoods['link_url'];
                                $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['amount'] = $arr_items['amount'];

                                if ($arr_items['addon']) {
                                    $arr_addon = unserialize($arr_items['addon']);

                                    if ($arr_addon['product_attr']) {
                                        foreach ($arr_addon['product_attr'] as $arr_product_attr) {
                                            $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['attr'] .= $arr_product_attr['label'] . $this->app->_(":") . $arr_product_attr['value'] . $this->app->_(" ");
                                        }
                                    }
                                }

                                if (isset($arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['attr']) && $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['attr']) {
                                    if (strpos($arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['attr'], $this->app->_(" ")) !== false) {
                                        $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['attr'] = substr($arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['attr'], 0, strrpos($arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_adj]['attr'], $this->app->_(" ")));
                                    }
                                }

                                $index_adj++;
                            } else {
                                // product gift.
                                if ($arr_service_goods_type_obj[$arr_objects['obj_type']]) {
                                    $str_service_goods_type_obj = $arr_service_goods_type_obj[$arr_items['item_type']];
                                    $str_service_goods_type_obj->get_order_object(array('goods_id' => $arr_items['goods_id'], 'product_id' => $arr_items['products']['product_id']), $arrGoods, $tpl);

                                    if ($arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift])
                                        $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift]['quantity'] = $objMath->number_plus(array($arr_items['quantity'], $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['quantity']));
                                    else
                                        $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift]['quantity'] = $arr_items['quantity'];

                                    if (!$arrGoods['image_default_id']) {
                                        $arrGoods['image_default_id'] = $image_set['S']['default_image'];
                                    }
                                    $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift] = $arr_items;
                                    $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift]['name'] = $arr_items['name'];
                                    $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift]['goods_id'] = $arr_items['goods_id'];
                                    $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift]['price'] = $arr_items['price'];
                                    $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift]['thumbnail_pic'] = $arrGoods['image_default_id'];
                                    $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift]['score'] = intval($arr_items['score'] * $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift]['quantity']);
                                    $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift]['link_url'] = $arrGoods['link_url'];
                                    $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift]['amount'] = $arr_items['amount'];

                                    if ($arr_items['addon']) {
                                        $arr_addon = unserialize($arr_items['addon']);

                                        if ($arr_addon['product_attr']) {
                                            foreach ($arr_addon['product_attr'] as $arr_product_attr) {
                                                $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift]['attr'] .= $arr_product_attr['label'] . $this->app->_(":") . $arr_product_attr['value'] . $this->app->_(" ");
                                            }
                                        }
                                    }

                                    if (isset($arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift]['attr']) && $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift]['attr']) {
                                        if (strpos($arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift]['attr'], $this->app->_(" ")) !== false) {
                                            $arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift]['attr'] = substr($arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift]['attr'], 0, strrpos($arr_data_item['goods_items'][$k][$arr_items['item_type'] . '_items'][$index_gift]['attr'], $this->app->_(" ")));
                                        }
                                    }
                                }
                                $index_gift++;
                            }
                        }
                    } else {
                        if ($arr_objects['obj_type'] == 'gift') {
                            //积分兑换赠品
                            $gift_key = '';
                            if ($arr_objects['obj_alias'] == app::get('b2c')->_('商品区块')) {
                                $gift_key = 'gift';//积分兑换赠品
                            } else {
                                $gift_key = 'order'; //订单送赠品，包含优惠券送赠品
                            }
                            if ($arr_service_goods_type_obj[$arr_objects['obj_type']]) {
                                foreach ($arr_objects['order_items'] as $arr_items) {
                                    if (!$arr_items['products']) {
                                        $o = $this->app->model('order_items');
                                        $tmp = $o->getList('*', array('item_id' => $arr_items['item_id']));
                                        $arr_items['products']['product_id'] = $tmp[0]['product_id'];
                                    }

                                    $str_service_goods_type_obj = $arr_service_goods_type_obj[$arr_objects['obj_type']];
                                    $str_service_goods_type_obj->get_order_object(array('goods_id' => $arr_items['goods_id'], 'product_id' => $arr_items['products']['product_id']), $arrGoods, $tpl);

                                    if (!isset($arr_items['products']['product_id']) || !$arr_items['products']['product_id'])
                                        $arr_items['products']['product_id'] = $arr_items['goods_id'];

                                    if ($arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']])
                                        $arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['quantity'] = $objMath->number_plus(array($arr_items['quantity'], $arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['quantity']));
                                    else
                                        $arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['quantity'] = $arr_items['quantity'];

                                    if (!$arrGoods['image_default_id']) {
                                        $arrGoods['image_default_id'] = $image_set['S']['default_image'];
                                    }

                                    $arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['name'] = $arr_items['name'];
                                    $arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['goods_id'] = $arr_items['goods_id'];
                                    $arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['price'] = $arr_items['price'];
                                    $arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['thumbnail_pic'] = $arrGoods['image_default_id'];
                                    $arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['point'] = intval($arr_items['score'] * $arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['quantity']);
                                    $arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['nums'] = $arr_items['quantity'];
                                    $arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['link_url'] = $arrGoods['link_url'];
                                    $arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['amount'] = $arr_items['amount'];

                                    if ($arr_items['addon']) {
                                        $arr_addon = unserialize($arr_items['addon']);

                                        if ($arr_addon['product_attr']) {
                                            foreach ($arr_addon['product_attr'] as $arr_product_attr) {
                                                $arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['attr'] .= $arr_product_attr['label'] . $this->app->_(":") . $arr_product_attr['value'] . $this->app->_(" ");
                                            }
                                        }
                                    }

                                    if (isset($arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['attr']) && $arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['attr']) {
                                        if (strpos($arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['attr'], $this->app->_(" ")) !== false) {
                                            $arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['attr'] = substr($arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['attr'], 0, strrpos($arr_data_item[$gift_key][$arr_items['item_type'] . '_items'][$arr_items['products']['product_id']]['attr'], $this->app->_(" ")));
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($arr_service_goods_type_obj[$arr_objects['obj_type']]) {

                                $str_service_goods_type_obj = $arr_service_goods_type_obj[$arr_objects['obj_type']];
                                $arr_data_item['extends_items'][] = $str_service_goods_type_obj->get_order_object($arr_objects, $arr_Goods, $tpl);
                            }
                        }
                    }
                }
            }

        }
    }

    /**
     * Generate the order detail
     * @params string order_id
     * @return null
     */
    public function orderdetail($order_id = 0, $archive = false)
    {
        $this->path[] = array('title' => app::get('b2c')->_('会员中心'), 'link' => $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'index', 'full' => 1)));
        $this->path[] = array('title' => app::get('b2c')->_('我的订单'), 'link' => $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'orders', 'full' => 1)));
        $this->path[] = array('title' => app::get('b2c')->_('订单详情'), 'link' => '#');
        $GLOBALS['runtime']['path'] = $this->path;

        if (!isset($order_id) || !$order_id) {
            $this->begin(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'index'));
            $this->end(false, app::get('b2c')->_('订单编号不能为空！'));
        }

        $objOrder = $archive ? $this->app->model('archive_orders') : $this->app->model('orders');
        $subsdf = array('order_objects' => array('*', array('order_items' => array('*', array(':products' => '*')))), 'order_pmt' => array('*'));
        $sdf_order = $objOrder->dump(array('order_id' => $order_id), '*', $subsdf);

        $objMath = kernel::single("ectools_math");

        if (!$sdf_order || $this->app->member_id != $sdf_order['member_id']) {
            kernel::single('site_router')->http_status(404);
            return;
        }

        $order_payed = kernel::single('b2c_order_pay')->check_payed($sdf_order['order_id']);
        if ($order_payed == $sdf_order['total_amount']) {
            $sdf_order['is_pay'] = 1;
        } else {
            $sdf_order['is_pay'] = 0;
        }

        if ($sdf_order['member_id']) {
            $member = $this->app->model('members');
            $aMember = $member->dump($sdf_order['member_id'], 'email');
            $sdf_order['receiver']['email'] = $aMember['contact']['email'];
        }

        // 处理收货人地区
        $arr_consignee_area = array();
        $arr_consignee_regions = array();
        if (strpos($sdf_order['consignee']['area'], ':') !== false) {
            $arr_consignee_area = explode(':', $sdf_order['consignee']['area']);
            if ($arr_consignee_area[1]) {
                if (strpos($arr_consignee_area[1], '/') !== false) {
                    $arr_consignee_regions = explode('/', $arr_consignee_area[1]);
                }
            }

            $sdf_order['consignee']['area'] = (is_array($arr_consignee_regions) && $arr_consignee_regions) ? $arr_consignee_regions[0] . $arr_consignee_regions[1] . $arr_consignee_regions[2] : $sdf_order['consignee']['area'];
        }
        $this->pagedata['site_checkout_receivermore_open'] = $this->app->getConf('site.checkout.receivermore.open');
        // 订单的相关信息的修改
        $obj_other_info = kernel::servicelist('b2c.order_other_infomation');
        if ($obj_other_info) {
            foreach ($obj_other_info as $obj) {
                $this->pagedata['discount_html'] = $obj->gen_point_discount($sdf_order);
            }
        }
        //判断预售 promotion_type
        if ($sdf_order['promotion_type'] == 'prepare') {
            $prepare_order = kernel::service('prepare_order');
            if ($prepare_order) {
                $prepare = $prepare_order->get_order_info($sdf_order['order_id']);
                $order_payed = kernel::single('b2c_order_pay')->check_payed($sdf_order['order_id']);
                if ($order_payed > 0) {
                    if ($prepare['begin_time'] < time() && time() < $prepare['end_time']) {
                        if ($order_payed == $prepare['preparesell_price']) {
                            $sdf_order['is_pay'] = 1;
                        }
                    }
                }
            }
        }
        $this->pagedata['order'] = $sdf_order;
        //显示是否有必填项
        $minfo = $objOrder->minfo($sdf_order);
        if (!empty($minfo)) {
            $this->pagedata['is_minfo'] = 1;
            $this->pagedata['minfo'] = $minfo;
        } else {
            $this->pagedata['is_minfo'] = 0;
        }

        $order_items = array();
        $gift_items = array();
        $this->get_order_detail_item($sdf_order, 'member_order_detail');
        $this->pagedata['order'] = $sdf_order;
        $mdl_b2c_refund_apply = $this->app->model('refund_apply');
        $this->pagedata["field_refunds_reason"] = $mdl_b2c_refund_apply->get_field_refunds_reason(); //退款申请理由列表
        /** 将商品促销单独剥离出来 **/
        if ($this->pagedata['order']['order_pmt']) {
            foreach ($this->pagedata['order']['order_pmt'] as $key => $arr_pmt) {
                if ($arr_pmt['pmt_type'] == 'goods') {
                    $this->pagedata['order']['goods_pmt'][$arr_pmt['product_id']][$key] = $this->pagedata['order']['order_pmt'][$key];
                    unset($this->pagedata['order']['order_pmt'][$key]);
                }
            }
        }
        /** end **/

        // 得到订单留言.
        $oMsg = kernel::single("b2c_message_order");
        $arrOrderMsg = $oMsg->getList('*', array('order_id' => $order_id, 'object_type' => 'order'), $offset = 0, $limit = -1, 'time DESC');

        $this->pagedata['ordermsg'] = $arrOrderMsg;
        $this->pagedata['res_url'] = $this->app->res_url;

        //我已付款
        $timeHours = array();
        for ($i = 0; $i < 24; $i++) {
            $v = ($i < 10) ? '0' . $i : $i;
            $timeHours[$v] = $v;
        }
        $timeMins = array();
        for ($i = 0; $i < 60; $i++) {
            $v = ($i < 10) ? '0' . $i : $i;
            $timeMins[$v] = $v;
        }
        $this->pagedata['timeHours'] = $timeHours;
        $this->pagedata['timeMins'] = $timeMins;

        // 生成订单日志明细
        //$oLogs =$this->app->model('order_log');
        //$arr_order_logs = $oLogs->getList('*', array('rel_id' => $order_id));
        $arr_order_logs = $objOrder->getOrderLogList($order_id);
        $this->pagedata['orderlogs'] = $arr_order_logs['data'];

        // 取到支付单信息
        $obj_payments = app::get('ectools')->model('payments');
        $this->pagedata['paymentlists'] = $obj_payments->get_payments_by_order_id($order_id);

        // 支付方式的解析变化
        $obj_payments_cfgs = app::get('ectools')->model('payment_cfgs');
        $arr_payments_cfg = $obj_payments_cfgs->getPaymentInfo($this->pagedata['order']['payinfo']['pay_app_id']);
        $this->pagedata['order']['payment'] = $arr_payments_cfg;

        // 添加html埋点
        foreach (kernel::servicelist('b2c.order_add_html') as $services) {
            if (is_object($services)) {
                if (method_exists($services, 'fetchHtml')) {
                    $services->fetchHtml($this, $order_id, 'site/invoice_detail.html');
                }
            }
        }
        $this->pagedata['controller'] = "orders";
        $this->pagedata['archive'] = $archive;
        // 预售订单信息
        $prepare_order = kernel::service('prepare_order');
        if ($prepare_order) {
            $pre_order = $prepare_order->get_order_info($order_id);
            if (!empty($pre_order)) {
                $pre_order['prepare_type'] = 'prepare';
                $this->pagedata['prepare'] = $pre_order;
            }
        }
        $this->pagedata['offer'] = $this->getOrderLatestOffer($order_id);
        $this->pagedata['hiddenLeftMenu'] = true;
        //echo '<pre>';print_r($prepare_order);exit();
        $this->output();
    }

    /*
     *会员中心 商品收藏
     * */
    /*
     *会员中心 商品收藏
     * */
    function favorite($nPage=1){
        $this->path[] = array('title'=>app::get('b2c')->_('会员中心'),'link'=>$this->gen_url(array('app'=>'b2c', 'ctl'=>'site_member', 'act'=>'index','full'=>1)));
        $this->path[] = array('title'=>app::get('b2c')->_('商品收藏'),'link'=>'#');
        $GLOBALS['runtime']['path'] = $this->path;
        $membersData = kernel::single('b2c_user_object')->get_members_data(array('members'=>'member_lv_id'));
        $aData = kernel::single('b2c_member_fav')->get_favorite($this->app->member_id,$membersData['members']['member_lv_id'],$nPage,$this->pagesize);
        $total = count(kernel::single('b2c_member_fav')->get_favorite($this->app->member_id,$membersData['members']['member_lv_id'],1,100000)['data']);
        $pagetotal= $total ? ceil($total/$this->pagesize) : 1;


        $imageDefault = app::get('image')->getConf('image.set');
        foreach ((array)$aData['data'] as $key => $value) {
            $aData['data'][$key]['url'] = $this->gen_url(array('app'=>'b2c','ctl'=>"site_member",'act'=>"receive",'arg0'=>$value['order_id']));;
        }
        $aProduct = $aData['data'];
        foreach((array)$aProduct as $k=>$v){
            if($v['nostore_sell']){
                $aProduct[$k]['store'] = 999999;
                $aProduct[$k]['product_id'] = $v['spec_desc_info'][0]['product_id'];
            }else{
                foreach((array)$v['spec_desc_info'] as $value){
                    $aProduct[$k]['product_id'] = $value['product_id'];
                    $aProduct[$k]['spec_info'] = $value['spec_info'];
                    $aProduct[$k]['price'] = $value['price'];
                    if(is_null($value['store']) ){
                        $aProduct[$k]['store'] = 999999;
                        break;
                    }elseif( ($value['store']-$value['freez']) > 0 ){
                        $aProduct[$k]['store'] = $value['store']-$value['freez'];
                        break;
                    }else{
                        $aProduct[$k]['store'] = false;
                    }
                }
            }
        }
        $this->pagedata['browser'] = $this->get_browser();
        $this->pagedata['favorite'] = $aProduct;
        $this->pagination($nPage,$aData['page'],'favorite');
        $this->pagedata['defaultImage'] = $imageDefault['S']['default_image'];
        $setting['buytarget'] = $this->app->getConf('site.buy.target');
        $this->pagedata['setting'] = $setting;
        $this->pagedata['current_page'] = $nPage;
        /** 接触收藏的页面地址 **/
        $this->pagedata['fav_ajax_del_goods_url'] = $this->gen_url(array('app'=>'b2c','ctl'=>'site_member','act'=>'ajax_del_fav','args'=>array('goods')));
        $img_model = app::get('image')->model('image');

        $pager =[
            'current'=>$nPage,
            'total'=>$pagetotal,
        ];

       foreach ([
                    'favorite'=>i18n_mdl_words::translateArray($aProduct),
                    'img_model'=>$img_model,
                    'pager'=>$pager
                ] as $k=>$v){
           $this->pagedata[$k] = $v;
       }
       $this->output();
    }

    /*
     *删除商品收藏
     * */
    function ajax_del_fav($gid = null, $object_type = 'goods')
    {
        if (!$gid) {
            $this->splash('error', null, app::get('b2c')->_('参数错误！'), true);
        }
        if (!kernel::single('b2c_member_fav')->del_fav($this->app->member_id, $object_type, $gid, $maxPage)) {
            $this->splash('error', null, app::get('b2c')->_('移除失败！'));
        } else {
            $this->set_cookie('S[GFAV]' . '[' . $this->app->member_id . ']', $this->get_member_fav($this->app->member_id), false);

            $current_page = $_POST['current'];
            if ($current_page > $maxPage) {
                $current_page = $maxPage;
                $reload_url = $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'favorite', 'args' => array($current_page)));
                $this->splash('success', $url, app::get('b2c')->_('成功移除！'), true);
            }
            $aData = kernel::single('b2c_member_fav')->get_favorite($this->app->member_id, $this->member['member_lv'], $current_page, $this->pagesize);
            foreach ((array)$aData['data'] as $key => $value) {
                $aData['data'][$key]['url'] = $this->gen_url(array('app' => 'b2c', 'ctl' => "site_member", 'act' => "receive", 'arg0' => $value['order_id']));;
            }
            $aProduct = $aData['data'];

            $oImage = app::get('image')->model('image');
            $imageDefault = app::get('image')->getConf('image.set');
            foreach ((array)$aProduct as $k => $v) {
                if (!$oImage->getList("image_id", array('image_id' => $v['image_default_id']))) {
                    $aProduct[$k]['image_default_id'] = $imageDefault['S']['default_image'];
                }
            }
            $this->pagedata['favorite'] = $aProduct;
            $this->pagedata['defaultImage'] = $imageDefault['S']['default_image'];
            $this->splash('success', $url, app::get('b2c')->_('成功移除！'), array('request' => true));
        }
    }

    function ajax_fav()
    {
        $object_type = $_POST['type'];
        $nGid = $_POST['gid'];
        if (!kernel::single('b2c_member_fav')->add_fav($this->app->member_id, $object_type, $nGid)) {
            $this->splash('failed', app::get('b2c')->_('商品收藏添加失败！'), '', '', true);
        } else {
            $this->set_cookie('S[GFAV]' . '[' . $this->app->member_id . ']', $this->get_member_fav($this->app->member_id), false);
            $this->splash('success', $url, app::get('b2c')->_('商品收藏添加成功'), array('request' => true));
        }
    }

    /*
    * 普通用户申请成为企业用户
    */
    public function company()
    {
        $member_id = $this->member['member_id'];
        $this->path[] = array('title' => app::get('b2c')->_('会员中心'), 'link' => $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'index', 'full' => 1)));
        $this->path[] = array('title' => app::get('b2c')->_('我的账户'));
        $this->path[] = array('title' => app::get('b2c')->_('成为企业客户'), 'link' => '#');
        $GLOBALS['runtime']['path'] = $this->path;
        $memberModel = app::get('b2c')->model('members');
        $memberData = $memberModel->dump(['member_id' => $member_id], 'member_type,check_status');

        $this->pagedata['memberData'] = $memberData;
        $this->output();
    }


    public function be_company()
    {
        $member_id = $this->member['member_id'];
        $register['cp_name'] = $_POST['cpName'] ? $_POST['cpName'] : '';
        $register['cp_id'] = $_POST['cpId'] ? $_POST['cpId'] : '';
        $register['cp_addr'] = $_POST['cpAddr'] ? $_POST['cpAddr'] : '';
        $register['cp_ind'] = $_POST['cpInd'] ? $_POST['cpInd'] : '';
        $register['cp_dim'] = $_POST['cpDim'] ? $_POST['cpDim'] : '';
        $register['cp_contact'] = $_POST['cpConact'] ? $_POST['cpConact'] : '';
        $register['cp_web'] = $_POST['cpWeb'] ? $_POST['cpWeb'] : '';
        $register['pst_photo'] = $_POST['pstPhoto'] ? $_POST['pstPhoto'] : '';
        $register['member_type'] = $_POST['member_type'] ? $_POST['member_type'] : 1;
        $register['check_status'] = 0;
        $register['is_update'] = 1;
        $memberModel = app::get('b2c')->model('members');
        $memberData = $memberModel->dump(['member_id' => $member_id], '*');
        if (!$memberData) {
            $this->splash('failed', '', app::get('b2c')->_($this->t('用户不存在')), true);
        }
        if ($memberData['member_type']) {
            $this->splash('failed', '', app::get('b2c')->_($this->t('用户已经是企业用户')), true);
        }
        $res = $memberModel->update($register, ['member_id' => $member_id]);
        if ($res) {
            $this->splash('success', '', app::get('b2c')->_($this->t('提交成功,请等待审核')), true);
        }
        $this->splash('failed', '', app::get('b2c')->_($this->t('申请失败')), true);
    }


    /*
     *获取未读信息数目
     * */
    function get_unreadmsg_num()
    {
        $oMsg = kernel::single('b2c_message_msg');
        $num = $oMsg->count(array('to_id' => $this->app->member_id, 'has_sent' => 'true', 'for_comment_id' => 'all', 'inbox' => 'true', 'mem_read_status' => 'false'));
        $data['inbox_num'] = $num ? $num : 0;
        echo json_encode($data);
    }

    /*
     *获取收件箱未读信息数量
     * */
    function get_msg_num()
    {
        $oMsg = kernel::single('b2c_message_msg');
        $row = $oMsg->getList('*', array('to_id' => $this->app->member_id, 'has_sent' => 'true', 'for_comment_id' => 'all', 'inbox' => 'true', 'mem_read_status' => 'false'));
        $this->pagedata['inbox_num'] = count($row) ? count($row) : 0;
        $row = $oMsg->getList('*', array('has_sent' => 'false', 'author_id' => $this->app->member_id));
        $this->pagedata['outbox_num'] = count($row) ? count($row) : 0;
    }

    function inbox($nPage=1) {
        $this->get_msg_num();
        $oMsg = kernel::single('b2c_message_msg');
        $row = $oMsg->getList('*',array('to_id' => $this->app->member_id,'has_sent' => 'true','for_comment_id' => 'all','inbox' => 'true','mem_read_status' => 'false'));
        $this->pagedata['inbox_num'] = count($row)?count($row):0;

        $row = $oMsg->getList('*',array('to_id' => $this->app->member_id,'has_sent' => 'true','for_comment_id' => 'all','inbox' => 'true'));
        $aData['data'] = $row;
        $aData['total'] = count($row);
        $count = count($row);
        $this->pagesize = 2;
        $aPage = $this->get_start($nPage,$count);
        $params['data'] = $oMsg->getList('*',array('to_id' => $this->app->member_id,'has_sent' => 'true','for_comment_id' => 'all','inbox' =>'true'),$aPage['start'],$this->pagesize);
        $params['page'] = $aPage['maxPage'];
        $this->pagedata['message'] = $params['data'];
        $this->pagedata['total_msg'] = $aData['total'];

        $pagetotal= $aData['total'] ? ceil($aData['total']/$this->pagesize) : 1;

        $pager =[
            'current'=>$nPage,
            'total'=>$pagetotal,
        ];
        $data = [
            'pager'=>$pager,
            'list'=>$params['data'],
            'count'=>$aData['total'],
            'count_unread'=>$this->pagedata['inbox_num']
        ];
        foreach($data as $k=>$v){
            $this->pagedata[$k]=$v;
        }
        $this->output();
    }

    //草稿箱
    function outbox($nPage=1) {
        $this->get_msg_num();
        $oMsg = kernel::single('b2c_message_msg');
        $row = $oMsg->getList('*',array('has_sent' => 'false','author_id' => $this->app->member_id));
        $aData['data'] = $row;
        $aData['total'] = count($row);
        $count = count($row);
        $this->pagesize = 2;
        $aPage = $this->get_start($nPage,$count);
        $params['data'] = $oMsg->getList('*',array('has_sent' => 'false','author_id' => $this->app->member_id),$aPage['start'],$this->pagesize);
        $params['page'] = $aPage['maxPage'];
        $this->pagedata['message'] = $params['data'];
        $this->pagedata['total_msg'] = $aData['total'];

        $pagetotal= $aData['total'] ? ceil($aData['total']/$this->pagesize) : 1;

        $pager =[
            'current'=>$nPage,
            'total'=>$pagetotal,
        ];

       foreach([
                   'pager'=>$pager,
                   'list'=>$params['data'],
                   'count'=>$aData['total'],
               ] as $k=>$v){
           $this->pagedata[$k] = $v;
       }
       $this->output();
    }

    //已发送

    //已发送
    function track($nPage=1) {
        $this->get_msg_num();
        $oMsg = kernel::single('b2c_message_msg');
        $row = $oMsg->getList('*',array('author_id' => $this->app->member_id,'has_sent' => 'true','track' => 'true'));
        $aData['data'] = $row;
        $aData['total'] = count($row);
        $count = count($row);
        $this->pagesize = 2;
        $aPage = $this->get_start($nPage,$count);
        $params['data'] = $oMsg->getList('*',array('author_id' => $this->app->member_id,'has_sent' => 'true','track' => 'true'),$aPage['start'],$this->pagesize);
        $params['page'] = $aPage['maxPage'];
        $this->pagedata['message'] = $params['data'];
        $this->pagedata['total_msg'] = $aData['total'];

        $pagetotal= $aData['total'] ? ceil($aData['total']/$this->pagesize) : 1;

        $pager =[
            'current'=>$nPage,
            'total'=>$pagetotal,
        ];

        foreach ([
                     'pager'=>$pager,
                     'list'=>$params['data'],
                     'count'=>$aData['total'],
                 ] as $k=>$v){
            $this->pagedata[$k] = $v;
        }
        $this->output();
    }


    function view_msg()
    {
        $nMsgId = $_POST['comment_id'];
        $objMsg = kernel::single('b2c_message_msg');
        $aMsg = $objMsg->getList('comment', array('comment_id' => $nMsgId, 'for_comment_id' => 'all', 'to_id' => $this->app->member_id));
        if ($aMsg[0] && ($aMsg[0]['author_id'] != $this->app->member_id && $aMsg[0]['to_id'] != $this->app->member_id)) {
            header('Content-Type:text/html; charset=utf-8');
            echo app::get('b2c')->_('对不起，您没有权限查看这条信息！');
            exit;
        }
        $objMsg->setReaded($nMsgId);
        $objAjax = kernel::single('b2c_view_ajax');
        echo $objAjax->get_html(htmlspecialchars_decode($aMsg[0]['comment']), 'b2c_ctl_site_member', 'view_msg');
        exit;

    }

    function deleteMsg()
    {
        if (!empty($_POST['delete'])) {
            $objMsg = kernel::single('b2c_message_msg');
            if ($objMsg->check_msg($_POST['delete'], $this->app->member_id)) {
                $objMsg->delete(array('object_type' => 'msg', 'comment_id' => $_POST['delete']));
                $this->splash('success', 'reload', app::get('b2c')->_('删除成功'), $_POST['response_json']);
            } else {
                $this->splash('failed', null, app::get('b2c')->_('参数提交错误'), $_POST['response_json']);
            }
        } else {
            $this->splash('failed', null, app::get('b2c')->_('没有选中任何记录'), $_POST['response_json']);
        }
    }

    function send($nMsgId=false,$type='') {
        $this->get_msg_num();
        if($nMsgId){
            $objMsg = kernel::single('b2c_message_msg');
            $init =  $objMsg->dump($nMsgId);
            if($type == 'reply'){
                $objMsg->setReaded($nMsgId);
                $init['to_uname'] = $init['author'];
                $init['subject'] = "Re:".$init['title'];
                $init['comment'] = '';
                $this->pagedata['is_reply'] = true;
            }
            else{
                $init['subject'] = $init['title'];
            }
            $this->pagedata['init'] = $init;
            $this->pagedata['comment_id'] = $nMsgId;
        }
        $this->pagedata['controller'] = "inbox";


        $this->output();
    }

    function message($nMsgId=false, $status='send') { //给管理员发信件
        $this->get_msg_num();
        if($nMsgId){
            $objMsg = kernel::single('b2c_message_msg');
            $init =  $objMsg->dump($nMsgId);
            if($init['author_id'] == $this->app->member_id)
            {
                $this->pagedata['init'] = $init;
                $this->pagedata['msg_id'] = $nMsgId;
            }
        }
        if($status === 'reply'){
            $this->pagedata['reply'] = 1;
        }
        $this->pagedata['controller'] = "inbox";
        $this->output();
    }


    /*
     *发送站内信
     * */
    function send_msg()
    {
        $member_id = $_SESSION['member'];
        //判断当前时间与session时间是否在5秒内
        if (isset($_SESSION[$member_id]['last_send']) and (time() - $_SESSION[$member_id]['last_send']) <= 5) {
            return false;
        }

        if (!isset($_POST['msg_to']) || $_POST['msg_to'] == '管理员') {
            $_POST['to_type'] = 'admin';
            $_POST['msg_to'] = 0;
        } else {
            $userObject = kernel::single('b2c_user_object');
            $to_id = $userObject->get_id_by_uname($_POST['msg_to']);
            if (!$to_id) {
                $this->splash('failed', null, app::get('b2c')->_('收件人不存在'), $_POST['response_json']);
            }
            $_POST['to_id'] = $to_id;
        }
        if ($_POST['subject'] && $_POST['comment']) {
            $objMessage = kernel::single('b2c_message_msg');
            $_POST['has_sent'] = $_POST['has_sent'] == 'false' ? 'false' : 'true';
            $_POST['member_id'] = $this->app->member_id;
            $_POST['uname'] = $this->member['uname'];
            $_POST['contact'] = $this->member['email'];
            $_POST['ip'] = $_SERVER["REMOTE_ADDR"];
            $_POST['subject'] = strip_tags($_POST['subject']);
            $_POST['comment'] = strip_tags($_POST['comment']);
            if ($_POST['comment_id']) {
                //$data['comment_id'] = $_POST['comment_id'];
                $_POST['comment_id'] = '';//防止用户修改comment_id
            }

            //设置session时间
            $_SESSION[$member_id]['last_send'] = time();

            if ($objMessage->send($_POST)) {
                if ($_POST['has_sent'] == 'false') {
                    $this->splash('success', 'reload', app::get('b2c')->_('保存到草稿箱成功'), $_POST['response_json']);
                    //发送成功后释放session
                    unset($_SESSION[$member_id]['last_send']);
                } else {
                    $this->splash('success', 'reload', app::get('b2c')->_('发送成功'), $_POST['response_json']);
                    unset($_SESSION[$member_id]['last_send']);
                }
            } else {
                $this->splash('failed', null, app::get('b2c')->_('发送失败'), $_POST['response_json']);
                unset($_SESSION[$member_id]['last_send']);
            }
        } else {
            $this->splash('failed', null, app::get('b2c')->_('必填项不能为空'), $_POST['response_json']);
        }
    }

    /*
     *会员中心 修改密码页面
     * */
    public function security(){
        $this->path[] = array('title' => app::get('b2c')->_('会员中心'), 'link' => $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_members', 'act' => 'index', 'full' => 1)));
        $this->path[] = array('title' => app::get('b2c')->_('修改密码'), 'link' => '#');
        $GLOBALS['runtime']['path'] = $this->path;
        $this->output();
    }

    /*
     *保存修改密码
     * */
    function save_security(){
        $old_pwd = $_POST['old_pwd'] ? $_POST['old_pwd'] : '';
        $pwd = $_POST['pwd'] ? $_POST['pwd'] : '';
        $rePwd = $_POST['re_pwd'] ? $_POST['re_pwd'] : '';
        $userPassport = kernel::single('b2c_user_passport');
        if( !$userPassport->check_passport($pwd,$rePwd,$msg) ){
            $this->splash('failed',null,$msg,true);
        }
        $member_id = $this->member['member_id'];
        //验证原密码
        $pamMembersModel = app::get('pam')->model('members');
        $pamData = $pamMembersModel->getList('login_password,password_account,createtime',array('member_id'=>$member_id));
        $use_pass_data['login_name'] = $pamData[0]['password_account'];
        $use_pass_data['createtime'] = $pamData[0]['createtime'];
        $login_password = pam_encrypt::get_encrypted_password(trim($old_pwd),'member',$use_pass_data);
        if($login_password !== $pamData[0]['login_password']){
            $msg = app::get('b2c')->_('输入的旧密码与原密码不符！');
            $this->splash('failed',null,$msg,true);
        }

        if( !$userPassport->reset_passport($member_id,$pwd) ){
            $msg = app::get('b2c')->_('密码重置失败,请重试');
            $this->splash('failed','',$msg,true);
        }
        kernel::single('pam_lock')->flush_lock($member_id);
        $this->splash('success','',app::get('b2c')->_('修改成功！'),true);
    }

    /*
     *会员中心收货地址
     * */
    function receiver()
    {
        $this->path[] = array('title' => app::get('b2c')->_('会员中心'), 'link' => $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'index', 'full' => 1)));
        $this->path[] = array('title' => app::get('b2c')->_('收货地址'), 'link' => '#');
        $GLOBALS['runtime']['path'] = $this->path;
        /**
         * $objMem = $this->app->model('members');
         * $this->pagedata['browser'] = $this->get_browser();
         * $this->pagedata['receiver'] = $objMem->getMemberAddr($this->app->member_id);
         * $this->pagedata['is_allow'] = (count($this->pagedata['receiver'])<10 ? 1 : 0);
         * $this->pagedata['num'] = count($this->pagedata['receiver']);
         * $this->pagedata['res_url'] = $this->app->res_url;
         */
        /**
         * @var b2c_mdl_member_addrs $obj_member_addrs
         */
        $obj_member_addrs = $this->app->model('member_addrs');
        $this->pagedata['member_addr_list'] = $obj_member_addrs->getOrderAddrList($this->getMemberId());
        $this->output();
    }


    /*
     * 设置和取消默认地址，$disabled 2为设置默认1为取消默认
     */
    function set_default($addrId = null, $disabled = 2)
    {
        // $addrId = $_POST['addr_id'];
        if (!$addrId) $this->splash('failed', null, app::get('b2c')->_('参数错误'), true);
        $url = $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'receiver'));
        /**
         * @var $obj_member b2c_mdl_members
         */
        $obj_member = $this->app->model('members');
        $member_id = $this->app->member_id;
        if ($obj_member->check_addr($addrId, $member_id)) {
            if ($obj_member->set_to_def($addrId, $member_id, $message, $disabled)) {
                $this->splash('success', $url, $message, true);
            } else {
                $this->splash('error', null, $message, true);
            }
        } else {
            $this->splash('error', null, app::get('b2c')->_('参数错误'), true);
        }
    }

    /*
     *添加、修改收货地址
     * */
    function modify_receiver($addrId = null)
    {
        // $addrId = $_POST['addr_id'];
        if (!$addrId) {
            echo app::get('b2c')->_("参数错误");
            exit;
        }
        $obj_member = $this->app->model('members');
        if ($obj_member->check_addr($addrId, $this->app->member_id)) {
            if ($aRet = $obj_member->getAddrById($addrId)) {
                $aRet['defOpt'] = array('0' => app::get('b2c')->_('否'), '1' => app::get('b2c')->_('是'));
                $this->pagedata = $aRet;
            } else {
                echo app::get('b2c')->_("修改的收货地址不存在");
                exit;
            }
            echo $this->fetch('site/member/modify_receiver.html');
            exit;
        } else {
            echo app::get('b2c')->_("参数错误");
            exit;
        }
    }

    /*
     *保存收货地址
     * */
    function save_rec()
    {
        if (!$_POST['def_addr']) {
            $_POST['def_addr'] = 0;
        }
        $save_data = kernel::single('b2c_member_addrs')->purchase_save_addr($_POST, $this->app->member_id, $msg);
        if (!$save_data) {
            $this->splash('error', null, $msg, true);
        }
        $this->splash('success', $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'receiver')), app::get('b2c')->_('保存成功'), true);
    }

    /*
     *删除收货地址
     * */
    function del_rec($addrId = null)
    {
        // $addrId = $_POST['addr_id'];
        if (!$addrId) $this->splash('failed', null, app::get('b2c')->_('参数错误'), true);
        $url = $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'receiver'));
        $obj_member = $this->app->model('members');
        if ($obj_member->check_addr($addrId, $this->app->member_id)) {
            if ($obj_member->del_rec($addrId, $message, $this->app->member_id)) {
                $this->splash('success', $url, $message, true);
            } else {
                $this->splash('failed', $url, $message, true);
            }
        } else {
            $this->splash('failed', $url, app::get('b2c')->_('参数错误'), true);
        }
    }

    function exchange($cpnsId = null)
    {
        //积分设置的用途
        $site_point_usage = app::get('b2c')->getConf('site.point_usage');
        if ($site_point_usage != '1') {
            $this->splash('failed', $url, app::get('b2c')->_('积分只用于抵扣，不能兑换'), true);
        }
        if (!$cpnsId) $this->splash('failed', $url, app::get('b2c')->_('参数错误'), true);
        $oExchangeCoupon = kernel::single('b2c_coupon_mem');
        $memberId = intval($this->app->member_id);//会员id号
        if ($memberId) {
//             $membersData = $this->get_current_member();
            $cur_coupon_nums = $this->app->model('member_coupon')->count(array('cpns_id' => $cpnsId, 'member_id' => $memberId, 'source' => 1));
            $coupons = $this->app->model('coupons');
            $cur_coupon = $coupons->dump($cpnsId);
            if ($cur_coupon['cpns_max_num'] > 0) {  //兼容老数据处理老数据还是无限制兑换
                if ($cur_coupon_nums >= $cur_coupon['cpns_max_num']) {
                    $this->splash('failed', $url, app::get('b2c')->_('您的兑换次数已达上限！'), true);
                }
            }

            $obj_point_common = kernel::single('pointprofessional_point_common');
            $real_point_get = $obj_point_common->check_used_point_get($memberId);
            if (!$real_point_get["rs"]) {
                $this->splash('failed', $url, app::get('b2c')->_('获取可用积分失败！'), true);
            }

            if ($real_point_get['real_point'] < $cur_coupon['cpns_point']) {
                $this->splash('failed', $url, app::get('b2c')->_('您的积分不足！'), true);
            }

            if ($oExchangeCoupon->exchange($cpnsId, $memberId, $real_point_get['real_point'], $params)) {
                $cpns_point = $params['cpns_point'];
                $member_point = $this->app->model('member_point');
                if ($obj_point_common->point_change_action($this->member['member_id'], -$cpns_point, $msg, 'exchange_coupon', 2, $memberId, $memberId, 'exchange', false)) {
                    $change_nums = $cur_coupon['cpns_max_num'] - $cur_coupon_nums - 1;
                    $url = $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'coupon'));
                    if ($cur_coupon['cpns_max_num'] > 0) {
                        $this->splash('success', $url, app::get('b2c')->_('兑换成功,您还可以兑换' . $change_nums . '张'), true);
                    } else {
                        $this->splash('success', $url, app::get('b2c')->_('兑换成功'), true);
                    }
                } else {
                    $oExchangeCoupon->exchange_delete($params);
                    $this->splash('failed', $url, $msg, array('request' => true));
                }
            }
        } else {
            $this->splash('failed', $url, app::get('b2c')->_('没有登录'), true);
        }
        $this->splash('failed', $url, app::get('b2c')->_('积分不足或兑换购物券无效'), true);
    }

    function download_ddvanceLog()
    {
        $charset = kernel::single('base_charset');
        $obj_member = $this->app->model('member_advance');
        $aData = $obj_member->get_list_bymemId($this->app->member_id);
        header('Pragma: no-cache, no-store');
        header("Expires: Wed, 26 Feb 1997 08:21:57 GMT");
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=advance_" . date("Ymd") . ".csv");
        $out = app::get('b2c')->_("事件,存入金额,支出金额,当前余额,时间\n");
        foreach ($aData as $v) {
            $out .= $v['message'] . "," . $v['import_money'] . "," . $v['explode_money'] . "," . $v['member_advance'] . "," . date("Y-m-d H:i", $v['mtime']) . "\n";
        }
        echo $charset->utf2local($out, 'zh');
        exit;
    }

    /**
     * 添加留言
     * @params string order_id
     * @params string message type
     */
    public function addMsgData()
    {
        $timeHours = array();
        for ($i = 0; $i < 24; $i++) {
            $v = ($i < 10) ? '0' . $i : $i;
            $timeHours[$v] = $v;
        }
        $timeMins = array();
        for ($i = 0; $i < 60; $i++) {
            $v = ($i < 10) ? '0' . $i : $i;
            $timeMins[$v] = $v;
        }
        $this->pagedata['orderId'] = $order_id;
        $this->pagedata['msgType'] = $msgType;
        $this->pagedata['timeHours'] = $timeHours;
        $this->pagedata['timeMins'] = $timeMins;

        $this->output();
    }

    /**
     * 订单留言提交
     * @params null
     * @return null
     */
    public function toadd_order_msg()
    {
        if (!$_POST['msg']['orderid']) {
            $this->splash(false, app::get('b2c')->_('参数错误'), true);
        }
        $this->begin(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'orderdetail', 'arg0' => $_POST['msg']['orderid']));
        $obj_filter = kernel::single('b2c_site_filter');
        $_POST = $obj_filter->check_input($_POST);

        $_POST['to_type'] = 'admin';
        $_POST['author_id'] = $this->app->member_id;
        $_POST['author'] = $this->member['uname'];
        $is_save = true;
        $obj_order_message = kernel::single("b2c_order_message");
        if ($obj_order_message->create($_POST))
            $this->end(true, app::get('b2c')->_('留言成功'), null, false, true);
        else
            $this->end(false, app::get('b2c')->_('留言失败'), null, false, true);
    }

    /*
        过滤POST来的数据,基于安全考虑,会把POST数组中带HTML标签的字符过滤掉
    */
    function check_input($data)
    {
        $aData = $this->arrContentReplace($data);
        return $aData;
    }

    function arrContentReplace($array)
    {
        if (is_array($array)) {
            foreach ($array as $key => $v) {
                $array[$key] = $this->arrContentReplace($array[$key]);
            }
        } else {
            $array = strip_tags($array);
        }
        return $array;
    }

    function set_read($comment_id = null, $object_type = 'ask')
    {
        if (!$comment_id) return;
        $comment = kernel::single('b2c_message_disask');
        $comment->type = $object_type;
        $reply_data = $comment->getList('comment_id', array('for_comment_id' => $comment_id));
        foreach ($reply_data as $v) {
            $comment->setReaded($v['comment_id']);
        }

    }


    /*
     * 获取评论咨询的数据
     *
     * */
    public function getComment($nPage = 1, $type = 'discuss')
    {
        //获取评论咨询基本数据
        $comment = kernel::single('b2c_message_disask');
        $aData = $comment->get_member_disask($this->app->member_id, $nPage, $type, $this->pagesize);
        $gids = array();
        $productGids = array();
        $comment_ids = array();
        foreach ((array)$aData['data'] as $k => $v) {
            $comment_ids[] = $v['comment_id'];
            if ($v['type_id'] && !in_array($v['type_id'], $gids)) {
                $gids[] = $v['type_id'];
            }
            if (!$v['product_id'] && !in_array($v['type_id'], $productGids)) {
                $productGids[] = $v['type_id'];
            }

            if ($v['items']) {//统计回复未读的数量
                $unReadNum = 0;
                foreach ($v['items'] as $val) {
                    if ($val['mem_read_status'] == 'false') {
                        $unReadNum += 1;
                    }
                }
            }
            $aData['data'][$k]['unReadNum'] = $unReadNum;
        }

        //获取货品ID
        $productData = $productGids ? $this->app->model('products')->getList('goods_id,product_id', array('goods_id' => $productGids, 'is_default' => 'true')) : array();
        foreach ((array)$productData as $p_row) {
            $productList[$p_row['goods_id']] = $p_row['product_id'];
        }
        $this->pagedata['productList'] = $productList;

        //评论咨询商品信息
        $goodsData = $gids ? $this->app->model('goods')->getList('goods_id,name,price,thumbnail_pic,udfimg,image_default_id', array('goods_id' => $gids)) : null;
        if ($goodsData) {
            foreach ($goodsData as $row) {
                $goodsList[$row['goods_id']] = $row;
            }
        }
        $this->pagedata['goodsList'] = $goodsList;

        //评论咨询私有的数据
        if ($type == 'discuss') {
            $this->pagedata['point_status'] = app::get('b2c')->getConf('goods.point.status') ? app::get('b2c')->getConf('goods.point.status') : 'on';
            if ($this->pagedata['point_status'] == 'on') {//如果开启评分则获取评论评分
                $objPoint = $this->app->model('comment_goods_point');
                $goods_point = $objPoint->get_comment_point_arr($comment_ids);
                $this->pagedata['goods_point'] = $goods_point;
            }
        } else {
            $gask_type = unserialize($this->app->getConf('gask_type'));//咨询类型
            foreach ((array)$gask_type as $row) {
                $gask_type_list[$row['type_id']] = $row['name'];
            }
            $this->pagedata['gask_type'] = $gask_type_list;
        }
        return $aData;
    }

    function comment($nPage = 1)
    {
        if ($this->app->getConf('comment.switch.discuss') == 'off') {
            kernel::single('site_router')->http_status(404);
            return;
        }
        //面包屑
        $this->path[] = array('title' => app::get('b2c')->_('会员中心'), 'link' => $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'index', 'full' => 1)));
        $this->path[] = array('title' => app::get('b2c')->_('评论与咨询'), 'link' => '#');
        $GLOBALS['runtime']['path'] = $this->path;

        $comment = $this->getComment($nPage, 'discuss');
        $this->pagedata['commentList'] = $comment['data'];
        $this->pagination($nPage, $comment['page'], 'comment');
        $this->output();
    }

    function ask($nPage = 1)
    {
        if ($this->app->getConf('comment.switch.ask') == 'off') {
            kernel::single('site_router')->http_status(404);
            return;
        }
        //面包屑
        $this->path[] = array('title' => app::get('b2c')->_('会员中心'), 'link' => $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'index', 'full' => 1)));
        $this->path[] = array('title' => app::get('b2c')->_('评论与咨询'), 'link' => '#');
        $GLOBALS['runtime']['path'] = $this->path;

        $this->pagedata['controller'] = "comment";
        $comment = $this->getComment($nPage, 'ask');
        $this->pagedata['commentList'] = $comment['data'];
        $this->pagedata['commentType'] = 'ask';
        $this->pagination($nPage, $comment['page'], 'ask');
        $this->action_view = 'comment.html';
        $this->output();
    }

    /*
     *未评论商品
     **/
    public function nodiscuss($nPage = 1)
    {
        if ($this->app->getConf('comment.switch.discuss') == 'off') {
            kernel::single('site_router')->http_status(404);
            return;
        }
        //面包屑
        $this->path[] = array('title' => app::get('b2c')->_('会员中心'), 'link' => $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'index', 'full' => 1)));
        $this->path[] = array('title' => app::get('b2c')->_('未评论商品'), 'link' => '#');
        $GLOBALS['runtime']['path'] = $this->path;

        //获取会员已发货的商品日志
        $sell_logs = $this->app->model('sell_logs')->getList('order_id,product_id,goods_id', array('member_id' => $this->app->member_id));
        //获取会员已评论的商品
        $comments = $this->app->model('member_comments')->getList('order_id,product_id', array('author_id' => $this->app->member_id, 'object_type' => 'discuss', 'for_comment_id' => '0'));
        $data = array();
        if ($comments) {
            foreach ((array)$comments as $row) {
                if ($row['order_id'] && $row['product_id']) {
                    $data[$row['order_id']][$row['product_id']] = $row['product_id'];
                }
            }
        }

        //统一获取相关订单的order_id数组
        $order_ids = array();
        foreach ($sell_logs as $row_sell_log) {
            if (!in_array($row_sell_log["order_id"], $order_ids)) {
                $order_ids[] = $row_sell_log["order_id"];
            }
        }
        //获取没有确认收货的订单
        $unreceived_order_ids = array();
        $arr_unreceived_order_ids = $this->app->model('orders')->getList("order_id", array("received_status" => "0", "order_id|in" => $order_ids));
        if (!empty($arr_unreceived_order_ids)) {
            foreach ($arr_unreceived_order_ids as $var_order_id) {
                $unreceived_order_ids[] = $var_order_id["order_id"];
            }
        }

        foreach ((array)$sell_logs as $key => $log_row) {
            //评论过的商品不能再进行展示评论  订单必须是确认收货后才能进行展示评论 必须同时满足这两个条件
            if (($data && $data[$log_row['order_id']][$log_row['product_id']] == $log_row['product_id']) || (!empty($unreceived_order_ids) && in_array($log_row["order_id"], $unreceived_order_ids))) {
                unset($sell_logs[$key]);
            } else {
                $filter['order_id'][] = $log_row['order_id'];
                $filter['product_id'][] = $log_row['product_id'];
                $filter['item_type|noequal'] = 'gift';
            }
        }

        $orderItemModel = app::get('b2c')->model('order_items');
        $limit = $this->pagesize;
        $start = ($nPage - 1) * $limit;
        $i = 0;
        if (empty($filter)) {
            $nogift = array();
        } else {
            $nogift = $orderItemModel->getList('order_id,product_id', $filter);
        }

        if ($nogift) {
            foreach ($nogift as $row) {
                $tmp_nogift_order_id[] = $row['order_id'];
                $tmp_nogift_product_id[] = $row['product_id'];
            }
        }
        foreach ((array)$sell_logs as $key => $log_row) {
            if (in_array($log_row['order_id'], $tmp_nogift_order_id) && in_array($log_row['product_id'], $tmp_nogift_product_id)) {//剔除赠品,赠品不需要评论
                if ($i >= $start && $i < ($nPage * $limit)) {
                    /*从订单表orders中获取时间做为评论页面的购买时间@djh*/
                    $orderLogModel = app::get('b2c')->model('orders');
                    $orderLog = $orderLogModel->getRow('createtime', array('order_id' => $log_row['order_id']));
                    $log_row['createtime'] = $orderLog['createtime'];
                    unset($orderLog);
                    /*从订单表orders中获取时间做为评论页面的购买时间@djh*/

                    $sell_logs_data[] = $log_row;
                    $gids[] = $log_row['goods_id'];
                }
                if ($nogift) {
                    $i += 1;
                }
            }
        }
        $totalPage = ceil($i / $limit);
        if ($nPage > $totalPage) $nPage = $totalPage;


        $this->pagedata['list'] = $sell_logs_data;
        $this->pagination($nPage, $totalPage, 'nodiscuss');

        if ($gids) {
            //获取商品信息
            $goodsData = $this->app->model('goods')->getList('goods_id,name,image_default_id', array('goods_id' => $gids));
            $goodsList = array();
            foreach ((array)$goodsData as $goods_row) {
                $goodsList[$goods_row['goods_id']]['name'] = $goods_row['name'];
                $goodsList[$goods_row['goods_id']]['image_default_id'] = $goods_row['image_default_id'];
            }
            $this->pagedata['goods'] = $goodsList;

            $this->pagedata['point_status'] = app::get('b2c')->getConf('goods.point.status') ? app::get('b2c')->getConf('goods.point.status') : 'on';
            $this->pagedata['verifyCode'] = $this->app->getConf('comment.verifyCode');
            if ($this->pagedata['point_status'] == 'on') {
                //评分类型
                $comment_goods_type = $this->app->model('comment_goods_type');
                $this->pagedata['comment_goods_type'] = $comment_goods_type->getList('*');
                if (!$this->pagedata['comment_goods_type']) {
                    $sdf['type_id'] = 1;
                    $sdf['name'] = app::get('b2c')->_('商品评分');
                    $addon['is_total_point'] = 'on';
                    $sdf['addon'] = serialize($addon);
                    $comment_goods_type->insert($sdf);
                    $this->pagedata['comment_goods_type'] = $comment_goods_type->getList('*');
                }
            }

            $this->pagedata['submit_comment_notice'] = $this->app->getConf('comment.submit_comment_notice.discuss');
        }
        $this->output();
    }

    ##缺货登记
    function notify($nPage = 1)
    {
        $this->path[] = array('title' => app::get('b2c')->_('会员中心'), 'link' => $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'index', 'full' => 1)));
        $this->path[] = array('title' => app::get('b2c')->_('到货通知'), 'link' => '#');
        $GLOBALS['runtime']['path'] = $this->path;
        foreach ($aData['data'] as $key => $value) {
            $aData['data'][$key]['url'] = $this->gen_url(array('app' => 'b2c', 'ctl' => "site_member", 'act' => "receive", 'arg0' => $value['order_id']));;
        }
        $membersData = kernel::single('b2c_user_object')->get_members_data(array('members' => 'member_lv_id'));
        $oMem = $this->app->model('member_goods');
        $aData = $oMem->get_gnotify($this->app->member_id, $membersData['members']['member_lv_id'], $nPage);
        $this->pagedata['browser'] = $this->get_browser();
        $this->pagedata['notify'] = $aData['data'];
        $this->pagination($nPage, $aData['page'], 'notify');
        $setting['buytarget'] = $this->app->getConf('site.buy.target');
        $imageDefault = app::get('image')->getConf('image.set');
        $this->pagedata['defaultImage'] = $imageDefault['S']['default_image'];
        $this->pagedata['setting'] = $setting;
        $this->pagedata['member_id'] = $this->app->member_id;
        $this->output();
    }

    ##删除缺货登记
    function del_notify($pid = null)
    {
        $member_id = $this->app->member_id;
        if (!$pid || !$member_id) $this->splash('failed', 'back', app::get('b2c')->_('参数错误'), true);
        $url = $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'notify'));
        $member_goods = $this->app->model('member_goods');
        if ($member_goods->getList('gnotify_id', array('product_id' => $pid, 'member_id' => $this->app->member_id))) {
            if ($member_goods->delete(array('product_id' => $pid, 'member_id' => $this->app->member_id))) {
                $this->splash('success', $url, app::get('b2c')->_('移除成功'), true);
            } else {
                $this->splash('failed', null, app::get('b2c')->_('没有选中任何记录'), true);
            }
        } else {
            $this->splash('failed', null, app::get('b2c')->_('没有选中任何记录'), true);
        }

    }

    function coupon($nPage = 1)
    {
        $this->path[] = array('title' => app::get('b2c')->_('会员中心'), 'link' => $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'index', 'full' => 1)));
        $this->path[] = array('title' => app::get('b2c')->_('我的优惠券'), 'link' => '#');
        $GLOBALS['runtime']['path'] = $this->path;
        $oCoupon = kernel::single('b2c_coupon_mem');
        $aData = $oCoupon->get_list_m($this->app->member_id, $nPage);
        if ($aData) {
            foreach ($aData as $k => $item) {
                if ($item['coupons_info']['cpns_status'] != 1) {
                    $aData[$k]['coupons_info']['cpns_status'] = false;
                    $aData[$k]['memc_status'] = app::get('b2c')->_('此种优惠券已取消');
                    continue;
                }

                $member_lvs = explode(',', $item['time']['member_lv_ids']);
                if (!in_array($this->member['member_lv'], (array)$member_lvs)) {
                    $aData[$k]['coupons_info']['cpns_status'] = false;
                    $aData[$k]['memc_status'] = app::get('b2c')->_('本级别不准使用');
                    continue;
                }

                $curTime = time();
                if ($curTime >= $item['time']['from_time'] && $curTime < $item['time']['to_time']) {
                    if ($item['memc_used_times'] < $this->app->getConf('coupon.mc.use_times')) {
                        if ($item['coupons_info']['cpns_status']) {
                            $aData[$k]['memc_status'] = app::get('b2c')->_('可使用');
                        } else {
                            $aData[$k]['memc_status'] = app::get('b2c')->_('本优惠券已作废');
                        }
                    } else {
                        $aData[$k]['coupons_info']['cpns_status'] = false;
                        if ($item['disabled'] == 'busy') {
                            $aData[$k]['memc_status'] = app::get('b2c')->_('使用中');
                        } else {
                            $aData[$k]['memc_status'] = app::get('b2c')->_('本优惠券次数已用完');
                        }
                    }
                } else {
                    $aData[$k]['coupons_info']['cpns_status'] = false;
                    $aData[$k]['memc_status'] = app::get('b2c')->_('还未开始或已过期');
                }
            }
        }

        $total = $oCoupon->get_list_m($this->app->member_id);
        $this->pagination($nPage, ceil(count($total) / $this->pagesize), 'coupon');
        $this->pagedata['browser'] = $this->get_browser();
        $this->pagedata['coupons'] = $aData;
        $this->output();
    }

    /*
     * 积分兑换优惠卷
     * */
    function coupon_exchange($page = 1)
    {
        $this->path[] = array('title' => app::get('b2c')->_('会员中心'), 'link' => $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'index', 'full' => 1)));
        $this->path[] = array('title' => app::get('b2c')->_('积分兑换优惠卷'), 'link' => '#');
        $GLOBALS['runtime']['path'] = $this->path;

        $pageLimit = $this->pagesize;
        $oExchangeCoupon = kernel::single('b2c_coupon_mem');
        $filter = array('ifvalid' => 1);
        $site_point_usage = $this->app->getConf('site.point_usage');
        $this->pagedata['browser'] = $this->get_browser();
        $this->pagedata['site_point_usage'] = ($site_get_policy_method != '1' && $site_point_usage == '1') ? 'true' : 'false';
        if ($aExchange = $oExchangeCoupon->get_list()) {
            foreach ($aExchange as $k => $item) {
                $member_lvs = explode(',', $item['time']['member_lv_ids']);
                if (!in_array($this->member['member_lv'], (array)$member_lvs)) {
                    unset($aExchange[$k]);
                    continue;
                }
            }
            $this->pagedata['couponList'] = $aExchange;
        }
        $this->output();
    }

    /**
     * 重新购买
     * @param int $order_id [description]
     * @param boolean $archive [description]
     */
    public function reAddCart($order_id, $archive = false)
    {
        $url = $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_cart'));
        if (!$order_id) {
            $this->splash('error', $url);
        }
        $orderItemObj = $archive ? app::get('b2c')->model('archive_order_items') : app::get('b2c')->model('order_items');
        $ordersData = $orderItemObj->getList('*', array('order_id' => $order_id));
        foreach ($ordersData as $row) {
            $product_id[] = $row['product_id'];
            if ($row['item_type'] == 'product') {
                $cartData[$row['obj_id']]['goods']['goods_id'] = $row['goods_id'];
                $cartData[$row['obj_id']]['goods']['product_id'] = $row['product_id'];
                $cartData[$row['obj_id']]['goods']['num'] = 1;
                $cartData[$row['obj_id']][0] = 'goods';
            } elseif ($row['item_type'] == 'adjunct') {
                $cartData[$row['obj_id']]['goods']['adjunct'][0][$row['product_id']] = 1;
            }
        }
        //预售商品进入再次购买的判断
        $prepare = kernel::service('prepare_goods');
        if ($prepare) {
            $pre = $prepare->get_product_buyagain($product_id);

            foreach ($pre as $key => $value) {
                $prep[] = $value['promotion_type'];
            }
            //echo '<pre>';print_r($prep);exit();
            if (!empty($prep)) {
                $msg = app::get('b2c')->_('预售商品不能加入购物车！');
                if (in_array('prepare', $prep)) {
                    $this->splash('error', $url, $msg);
                }
            }
        }
        $obj_cart_object = kernel::single('b2c_cart_objects');
        $obj_goods = kernel::single('b2c_cart_object_goods');
        foreach ($cartData as $goods) {
            $obj_ident = $obj_cart_object->add_object($obj_goods, $goods, $msg);
        }

        $this->splash('error', $url);

    }

    public function edit_pass(){

    }

    function verify($verifyType = 'verifymobile')
    {
        unset($_SESSION['vcodeVerifykey']['activation']);
        $member_id = $this->app->member_id;
        $pamMemberData = app::get('pam')->model('members')->getList('*', array('member_id' => $member_id));
        foreach ($pamMemberData as $row) {
            if ($row['login_type'] == 'mobile' && $row['disabled'] == 'false') {
                $data['mobile'] = $row['login_account'];
                $mobile = mb_substr($row['login_account'], 0, 3) . '****' . mb_substr($row['login_account'], 7, 4);
                $show['mobile'] = $mobile;
                $verify['mobile'] = true;
            }
            if ($row['login_type'] == 'email' && $row['disabled'] == 'false') {
                $data['email'] = $row['login_account'];
                $email_array = explode('@', $row['login_account']);
                $email = mb_substr($email_array[0], 0, 1) . '***@' . $email_array[1];
                $show['email'] = $email;
                $verify['email'] = true;
            }
        }
        $MemberErrDate = app::get('b2c')->model('members_error')->getRow('*', array('member_id' => $member_id, 'type' => 'possword'));
        $datetime = date('Y-m-d', time());
        if ($datetime == date('Y-m-d', $MemberErrDate['etime']) && $MemberErrDate['error_num'] >= 3) {
            $this->pagedata['show_varycode'] = true;
        }
        $this->pagedata['site_sms_valide'] = $this->app->getConf('site.sms_valide');
        $this->pagedata['verifyType'] = $verifyType;
        $this->pagedata['verify'] = $verify;
        $this->pagedata['data'] = $data;
        $this->pagedata['show'] = $show;
        $this->output();
    }

    public function verify_vcode()
    {
        $send_type = $_POST['send_type'];
        if ($_POST['verifyType'] == 'cancelpaypassword') {
            $act_name = 'verify3';
            if (app::get('b2c')->model('members')->update(array('pay_password' => null), array('member_id' => $this->app->member_id))) {
                app::get('b2c')->model('members_error')->update(array('error_num' => '0'), array('member_id' => $this->app->member_id, 'type' => 'check'));
            }
        } else {
            $act_name = 'verify2';
        }
        if (!empty($_POST['show_varycode']) && !base_vcode::verify('b2c', $_POST['verifycode'])) {
            $msg = app::get('b2c')->_('验证码错误');
            $this->splash('failed', null, $msg, true);
            exit;
        }
        if (isset($_POST['password'])) {
            $pamMembersModel = app::get('pam')->model('members');
            $pamData = $pamMembersModel->getList('login_password,password_account,createtime', array('member_id' => $this->app->member_id));
            $use_pass_data['login_name'] = $pamData[0]['password_account'];
            $use_pass_data['createtime'] = $pamData[0]['createtime'];
            $login_password = pam_encrypt::get_encrypted_password(trim($_POST['password']), 'member', $use_pass_data);
            if ($login_password !== $pamData[0]['login_password']) {
                $msg = app::get('b2c')->_('您输入的密码与账号不匹配');
                $MemberErrDate = app::get('b2c')->model('members_error')->getRow('*', array('member_id' => $this->app->member_id, 'type' => 'possword'));
                if (!$MemberErrDate) {
                    $datetime = time();
                    $error_msg = array('member_id' => $this->app->member_id, 'etime' => $datetime, 'error_num' => 1, 'type' => 'possword');
                    app::get('b2c')->model('members_error')->save($error_msg);
                } else {
                    $datetime = date('Y-m-d', time());
                    if ($datetime == date('Y-m-d', $MemberErrDate['etime'])) {
                        $error_num = $MemberErrDate['error_num'] + 1;
                    } else {
                        $error_num = 1;
                    }
                    app::get('b2c')->model('members_error')->update(array('error_num' => $error_num, 'etime' => time()), array('member_id' => $this->app->member_id, 'type' => 'possword'));
                }
                if ($error_num == 3) {
                    $url = $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'verify', 'arg0' => $_POST['verifyType']));
                    $this->splash('success', $url);
                    exit;
                }
                $this->splash('failed', null, $msg, true);
                exit;
            } else {
                app::get('b2c')->model('members_error')->update(array('error_num' => '0'), array('member_id' => $this->app->member_id, 'type' => 'possword'));
                $_SESSION['vcodeVerifykey']['activation'] = 'true';
                $url = $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => $act_name, 'arg0' => $_POST['verifyType']));
                $this->splash('success', $url);
            }
        }
        $userVcode = kernel::single('b2c_user_vcode');
        if (!$userVcode->verify($_POST['vcode'][$send_type], $_POST[$send_type], 'activation')) {
            $msg = app::get('b2c')->_('验证码错误');
            $this->splash('failed', null, $msg, true);
            exit;
        }

        $_SESSION['vcodeVerifykey']['activation'] = 'true';
        $url = $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => $act_name, 'arg0' => $_POST['verifyType']));
        $this->splash('success', $url);
    }

    function verify2($verifyType)
    {
        if (!$_SESSION['vcodeVerifykey']['activation']) {
            $url = $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'verify', 'arg0' => $verifyType));
            $this->redirect($url);
        }
        $userPassport = kernel::single('b2c_user_passport');
        $member_id = $userPassport->userObject->get_member_id();
        $arr_colunms = $userPassport->userObject->get_pam_data('login_account', $member_id);
        $this->pagedata['site_sms_valide'] = $this->app->getConf('site.sms_valide');
        $this->pagedata['verifyType'] = $verifyType;
        $this->pagedata['data'] = $arr_colunms;
        $this->output();
    }

    function verify_vcode2($verifyType)
    {
        unset($_SESSION['vcodeVerifykey']['activation']);
        if ($verifyType == 'setpaypassword' || $verifyType == 'verifypaypassword') {
            $pay_password = $_POST['pay_password'];
            $re_pay_password = $_POST['re_pay_password'];
            if ($pay_password !== $re_pay_password) {
                $msg = app::get('b2c')->_('您输入的密码不一致');
                $this->splash('failed', null, $msg, true);
                exit;
            }
            if (mb_strlen($pay_password) < 6 || mb_strlen($re_pay_password) < 6) {
                $msg = app::get('b2c')->_('请输入正确格式的密码');
                $this->splash('failed', null, $msg, true);
                exit;
            }
            $password = pam_encrypt::get_encrypted_password(trim($pay_password), pam_account::get_account_type($this->app->app_id), $use_pass_data);
            if (app::get('b2c')->model('members')->update(array('pay_password' => $password), array('member_id' => $this->app->member_id))) {
                app::get('b2c')->model('members_error')->update(array('error_num' => '0'), array('member_id' => $this->app->member_id, 'type' => 'check'));
            }
            $url = $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'verify3', 'arg0' => $verifyType));
            $this->splash('success', $url);
        } else {
            $send_type = $_POST['send_type'];
            $userVcode = kernel::single('b2c_user_vcode');
            if (!$userVcode->verify($_POST['vcode'], $_POST['uname'], $send_type)) {
                $msg = app::get('b2c')->_('验证码错误');
                $this->splash('failed', null, $msg, true);
                exit;
            }

            $userPassport = kernel::single('b2c_user_passport');
            $accountType = $userPassport->get_login_account_type($_POST['uname']);
            if ($_POST['send_type'] == 'reset') {
                if (!$userPassport->set_new_account($this->app->member_id, trim($_POST['uname']), $msg)) {
                    $msg = $msg ? $msg : app::get('b2c')->_('修改信息失败');
                    $this->splash('failed', null, $msg, true);
                    exit;
                }
            } else {
                if (!app::get('pam')->model('members')->update(array('login_account' => $_POST['uname'], 'disabled' => 'false'), array('member_id' => $this->app->member_id, 'login_type' => $accountType))) {
                    $msg = app::get('b2c')->_('重置信息失败');
                    $this->splash('failed', null, $msg, true);
                    exit;
                }
            }
            //增加会员同步 2012-05-15
            if ($member_rpc_object = kernel::service("b2c_member_rpc_sync")) {
                $member_rpc_object->modifyActive($this->app->member_id);
            }
            $url = $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'verify3', 'arg0' => $verifyType));
            $this->splash('success', $url);
        }
    }

    function verify3($verifyType)
    {
        $this->pagedata['verifyType'] = $verifyType;
        $this->output();
    }

    function cancel($order_id)
    {
        $this->pagedata['cancel_order_id'] = $order_id;
        $this->page('site/member/order_cancel_reason.html');

    }

    function docancel()
    {
        $arrMember = kernel::single('b2c_user_object')->get_current_member(); //member_id,uname
        //开启事务处理
        $db = kernel::database();
        $transaction_status = $db->beginTransaction();

        $order_cancel_reason = $_POST['order_cancel_reason'];
        if ($order_cancel_reason['reason_type'] == 7 && !$order_cancel_reason['reason_desc']) {
            $this->splash('error', '', '请输入详细原因', true);
        }
        if (strlen($order_cancel_reason['reason_desc']) > 150) {
            $this->splash('error', '', '详细原因过长，请输入50个字以内', true);
        }
        if ($order_cancel_reason['reason_type'] != 7 && strlen($order_cancel_reason['reason_desc']) > 0) {
            $order_cancel_reason['reason_desc'] = '';
        }
        $order_cancel_reason = utils::_filter_input($order_cancel_reason);
        $order_cancel_reason['cancel_time'] = time();
        $mdl_order = app::get('b2c')->model('orders');
        $sdf_order_member_id = $mdl_order->getRow('member_id', array('order_id' => $order_cancel_reason['order_id']));
        if ($sdf_order_member_id['member_id'] != $arrMember['member_id']) {
            $db->rollback();
            $this->splash('error', '', "请勿取消别人的订单", true);
            return;
        }

        $mdl_order_cancel_reason = app::get('b2c')->model('order_cancel_reason');
        $result = $mdl_order_cancel_reason->save($order_cancel_reason);
        if (!$result) {
            $db->rollback();
            $this->splash('error', '', "订单取消原因记录失败", true);
        }
        $obj_checkorder = kernel::service('b2c_order_apps', array('content_path' => 'b2c_order_checkorder'));
        if (!$obj_checkorder->check_order_cancel($order_cancel_reason['order_id'], '', $message)) {
            $db->rollback();
            $this->splash('error', '', $message, true);
        }

        $sdf['order_id'] = $order_cancel_reason['order_id'];
        $sdf['op_id'] = $arrMember['member_id'];
        $sdf['opname'] = $arrMember['uname'];
        $sdf['account_type'] = 'member';

        $order_payed = kernel::single('b2c_order_pay')->check_payed($sdf['order_id']);
        if ($order_payed > 0) {
            $this->splash('error', '', "支付过的订单，无法取消订单", true);
        }

        $b2c_order_cancel = kernel::single("b2c_order_cancel");
        if ($b2c_order_cancel->generate($sdf, $this, $message)) {
            if ($order_object = kernel::service('b2c_order_rpc_async')) {
                $order_object->modifyActive($sdf['order_id']);
            }
            $url = $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'index'));
            $obj_coupon = kernel::single("b2c_coupon_order");
            if ($obj_coupon) {
                $obj_coupon->use_c($sdf['order_id'], 'cancel');
            }
            $db->commit($transaction_status);
            $this->splash('success', $url, "订单取消成功", true);
        } else {
            $db->rollback();
            $this->splash('error', '', "订单取消失败", true);
        }
    }

    public function signin()
    {
        $site_checkout_login_point_open = $this->app->getConf('site.checkout.login_point.open');
        $site_login_point_num = $this->app->getConf('site.login_point.num');
        if ($site_checkout_login_point_open == 'false') {
            $msg = '未开启签到送积分功能';
            $this->splash('error', '', $msg, true);
        }

        $signin_obj = $this->app->model('member_signin');
        $member_id = $this->app->member_id;
        $signin_date = date('Y-m-d');

        if ($signin_obj->exists_signin($member_id, $signin_date)) {
            $msg = '您今天已经签到过';
            $this->splash('error', '', $msg, true);
        }
        $data = array(
            'member_id' => $member_id,
            'signin_date' => $signin_date,
            'signin_time' => time(),
            'point' => $site_login_point_num
        );
        $result = kernel::single('b2c_member_signin')->sign($data);
        if ($result) {
            $msg = '签到成功，获得' . $site_login_point_num . '积分';
            $this->splash('success', '', $msg, true);
        } else {
            $msg = '签到失败';
            $this->splash('error', '', $msg, true);
        }
    }

    function receive($order_id)
    {
        $arrMember = kernel::single('b2c_user_object')->get_current_member();
        $mdl_order = app::get('b2c')->model('orders');
        $sdf_order_member_id = $mdl_order->getRow('member_id', array('order_id' => $order_id));
        $sdf_order_member_id['member_id'] = (int)$sdf_order_member_id['member_id'];
        if ($sdf_order_member_id['member_id'] != $arrMember['member_id']) {
            $this->splash('error', null, '请勿操作别人的收货', true);
            exit;
        } else {
            $arr_updates = array('order_id' => $order_id, 'received_status' => '1', 'received_time' => time());
            $mdl_order->save($arr_updates);
            $delivery_mdl = app::get('b2c')->model('order_delivery_time');
            $delivery_mdl->delete(array('order_id' => $order_id));
            $orderLog = $this->app->model("order_log");
            $log_text = serialize($log_text);
            $sdf_order_log = array(
                'rel_id' => $order_id,
                'op_id' => $arrMember['member_id'],
                'op_name' => (!$arrMember['member_id']) ? app::get('b2c')->_('顾客') : $arrMember['uname'],
                'alttime' => time(),
                'bill_type' => 'order',
                'behavior' => 'receive',
                'result' => 'SUCCESS',
                'log_text' => '用户已确认收货！',
            );
            if ($orderLog->save($sdf_order_log)) {
                $this->splash('success', null, '已完成收货', true);
                exit;
            } else {
                $this->splash('error', null, '收货失败', true);
                exit;
            }
        }
    }

    //售前申请退款操作
    public function do_refund_apply()
    {
        $order_id = $_POST["order_id"];
        if (!$order_id) {
            $this->splash("error", null, "订单id不存在", true);
            exit;
        }
        $refund_apply_reason = $_POST["refund_apply_reason"];
        if (!$refund_apply_reason) {
            $this->splash("error", null, "请选择退款理由", true);
        }
        //检查订单是否符合要求：未发货 已支付
        $mdl_order = $this->app->model('orders');
        $order_info = $mdl_order->dump(array("order_id" => $order_id), "pay_status,ship_status,member_id,final_amount");
        if (empty($order_info)) {
            $this->splash("error", null, "此订单不存在", true);
        }
        if ($order_info["cur_amount"] == 0) {
            $this->splash("error", null, "0元订单不能申请退款申请", true);
        }
        //是否有售后申请记录
        $mdl_aftersales_return_product = app::get('aftersales')->model('return_product');
        $rs_aftersales_return_product = $mdl_aftersales_return_product->dump(array("order_id" => $order_id));
        //判断是否有未操作过的此订单的退款申请记录
        /**
         * @var b2c_mdl_refund_apply
         */
        $mdl_b2c_refund_apply = app::get('b2c')->model('refund_apply');
        $rs_refund_apply = $mdl_b2c_refund_apply->dump(array("order_id" => $order_id, "status" => "0"));
        //满足已支付 未发货 没有售后退换货记录的 做售前退款操作
        if ($order_info["pay_status"] == "1" && $order_info["ship_status"] == "0" && empty($rs_aftersales_return_product) && empty($rs_refund_apply)) {
            $refund_apply_bn = date("YmdHis", time()) . str_pad(rand(1, 999), 4, '0', STR_PAD_LEFT);
            $current_time = time();
            $request_arr = array(
                "order_id" => $order_id,
                "member_id" => $order_info["member_id"],
                "refund_apply_bn" => $refund_apply_bn,
                "refunds_reason" => $refund_apply_reason,
                "money" => $order_info["cur_amount"],
                "current_time" => $current_time,
            );
            //如绑定把退款申请单打到oms
            $obj_apiv = kernel::single('b2c_apiv_exchanges_request');
            $obj_apiv->rpc_caller_request($request_arr, 'orderrefund');
            //生成售前退款记录
            $insert_arr = array(
                "refund_apply_bn" => $refund_apply_bn,
                "order_id" => $order_id,
                "member_id" => $order_info["member_id"],
                "money" => $order_info["cur_amount"],
                "refunds_reason" => $refund_apply_reason,
                "create_time" => $current_time,
                "last_modified" => $current_time,
            );
            if ($mdl_b2c_refund_apply->insert($insert_arr)) {
                //更新订单支付状态为退款申请中
                $mdl_order->update(array('pay_status' => '6'), array('order_id' => $order_id));
                $msg = "退款申请成功";
                $mdl_b2c_refund_apply->saveOrderLog($refund_apply_bn, $msg);
                //成功后跳转的url
                if ($_POST["order_detail"]) {
                    //订单详细页
                    $reload_url = $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'orderdetail', 'args' => array($order_id)));
                } else {
                    //我的订单页：获取也得支付方式和当前页两个参数
                    $current_page = $_POST["refund_apply_pager_current"] ? $_POST["refund_apply_pager_current"] : 1;
                    $current_pay_status = $_POST["refund_apply_pager_pay_status"] ? $_POST["refund_apply_pager_pay_status"] : "all";
                    $reload_url = $this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member', 'act' => 'orders', 'args' => array($current_pay_status, $current_page)));
                }
                $this->splash("success", $reload_url, $msg, true);
            } else {
                $msg = "退款申请失败";
                $mdl_b2c_refund_apply->saveOrderLog($refund_apply_bn, $msg);
                $this->splash("error", null, $msg, true);
            }
        } else {
            $this->splash("error", null, "不满足售前退款的条件", true);
        }
    }

    /*
     * 申请退款列表
     * @params int $nPage 页码
     * */
    public function refundlist($status = 'all', $nPage = 1)
    {

        $filter = array();
        $filter["member_id"] = $this->getMemberId();
        if ($status == 'wait') {
            $filter['status'] = 2;
        } else if ($status == 'ing') {
            $filter['status'] = [3];
        } else if ($status == 'done') {
            $filter['status'] = [4, 5];
        }
        $this->begin($this->gen_url(array('app' => 'b2c', 'ctl' => 'site_member')));
        $statusOptions = array(
            '1' => $this->t('未操作'),
            '2' => $this->t('审核中'),
            '3' => $this->t('接受申请'),
            '4' => $this->t('完成'),
            '5' => $this->t('拒绝'),
            '6' => $this->t('已收货'),
            '7' => $this->t('已质检'),
            '8' => $this->t('补差价'),
            '9' => $this->t('已拒绝退款'),
        );
        /**
         * @var $obj_return_policy aftersales_data_return_policy
         */
        $obj_return_policy = kernel::service("aftersales.return_policy");
        $aData = $obj_return_policy->get_return_product_list('*', $filter, $nPage);
        foreach ($aData['data'] as $key => $val) {
            $aData['data'][$key]['product_data'] = unserialize($val['product_data']);
            $aData['data'][$key]['comment'] = unserialize($val['comment']);
        }
        $this->pagedata['orders'] = $aData['data'];
        $imageDefault = app::get('image')->getConf('image.set');
        $this->pagedata['defaultImage'] = $imageDefault['S']['default_image'];
        $arrPager = $this->get_start($nPage, $aData['total']);
        $this->pagination($nPage, $arrPager['maxPage'], 'refundlist', [$status]);
        $this->pagedata['statusOptions'] = $statusOptions;
        $this->pagedata['status'] = $status;
        $this->output();
    }

    /*
     * 申请退款详细页
     * @params int $refund_apply_bn 退款申请单号
     * */
    function refund_detail($return_bn)
    {
        /**
         * @var $obj_return_policy aftersales_data_return_policy
         */
        $obj_return_policy = kernel::service("aftersales.return_policy");
        $data = $obj_return_policy->get_return_product_by_return_id($return_bn);
        $this->pagedata['order'] = $data;
        $this->pagedata['hiddenLeftMenu'] = true;
        $this->output();
    }

    public function coupon_receive()
    {
        if (isset($_POST['cpns_id'])) {
            $cpnsId = $_POST['cpns_id'];

            $concurrent_cpns = kernel::single("base_concurrent_file");
            $cpns_group_id = 'cpns_group_' . intval($cpnsId % 1000);
            $concurrent_cpns->status($cpns_group_id);
            if (!$concurrent_cpns->check_flock()) {
                $concurrent_cpns->close_lock();
                echo json_encode(array('status' => 'fail', 'msg' => "网络异常，请重试"));
                exit();
            }

            $oExchangeCoupon = kernel::single('b2c_coupon_mem');
            $memberId = intval($this->app->member_id);//会员id号
            $obj_widget_coupons = kernel::single('wap_widgets_coupons');
            if ($memberId) {
                $msgArr = array(
                    '2' => 'cpns_id为空',
                    '3' => '优惠券已经领光',
                    '4' => '会员等级不符',
                    '5' => '活动未开始',
                    '6' => '活动已结束',
                );
                if ($obj_widget_coupons->getReceiveStatus($cpnsId)) {
                    $concurrent_cpns->close_lock();
                    echo json_encode(array('status' => 'fail', 'msg' => "不可重复领取"));
                    exit();
                }

                $verify_status = $obj_widget_coupons->verify($cpnsId);
                if ($verify_status != 1) {
                    $concurrent_cpns->close_lock();
                    echo json_encode(array('status' => 'fail', 'msg' => $msgArr[$verify_status]));
                    exit();
                }

                $coupons = $this->app->model('coupons');
                $cur_coupon = $coupons->dump($cpnsId);

                if ($oExchangeCoupon->obtain($cpnsId, $memberId, $params)) {
                    $concurrent_cpns->unlock();
                    echo json_encode(array('status' => 'success', 'msg' => "领取成功"));
                    exit();
                } else {
                    $concurrent_cpns->close_lock();
                    echo json_encode(array('status' => 'fail', 'msg' => "领取失败"));
                    exit();
                }
            } else {
                $concurrent_cpns->close_lock();
                echo json_encode(array('status' => 'fail', 'msg' => "没有登录"));
                exit();
            }
        }

        echo json_encode(array('status' => 'fail', 'msg' => "参数异常"));
        exit();
    }

    /**
     * 获取订单的最后报价单
     * @param $order_id
     * @return array|null
     */
    protected function getOrderLatestOffer($order_id)
    {
        /**
         * @var $mdl_orders b2c_mdl_orders
         */
        $mdl_orders = $this->app->model('orders');
        $offer = $mdl_orders->getOrderLatestOffer($order_id);
        return $offer;
    }

    protected function getOrder($order_id)
    {
        /**
         * @var $mdl_orders b2c_mdl_orders
         */
        $mdl_orders = $this->app->model('orders');
        $order = $mdl_orders->dump($order_id);
        return $order;
    }

    /**
     * 查看当前用户的报价单
     * @param $order_id
     * @param $id
     */
    public function offer_self_detail($order_id, $id)
    {
        $order = $this->getOrder($order_id);
        $offer = null;
        if ($order && $order['member_id'] == $this->getMemberId()) {
            $offer = $this->getOrderLatestOffer($order_id);
        }
        $this->pagedata['order'] = $order;
        $this->pagedata['offer'] = $offer;
        $this->pagedata['hiddenLeftMenu'] = true;
        $this->output();
    }

    /**
     * 查看别人分享的报价单
     * @param $share_id
     */
    public function offer_share_detail($share_id)
    {

        $mdl_offer_share = b2c_mdl_order_share::getInstance();
        $share_info = $mdl_offer_share->ensureMemberOffer($share_id, $this->getMemberId());
        $offer = null;
        $order = null;
        $share_info['owner'] = '会员' . $share_info['owner_id'];
        if ($share_info) {
            $order_id = $share_info['order_id'];
            $offer_id = $share_info['order_index'];
            $order = $this->getOrder($order_id);
            $mdl_order = b2c_mdl_orders::getInstance();
            $offer = $mdl_order->getOrderOffer($offer_id);
        }
        $this->pagedata['order'] = $order;
        $this->pagedata['offer'] = $offer;
        $this->pagedata['share_info'] = $share_info;
        $this->pagedata['hiddenLeftMenu'] = true;
        $this->output();
    }

    /**
     * 别人分享的报价单列表
     * @param $page
     */
    public function more_orders($page)
    {
        $mdl_offer_share = b2c_mdl_order_share::getInstance();
        $aData = $mdl_offer_share->getMemberOffers($this->getMemberId(), $page, 2);
        $this->pagedata['offers'] = $aData['data'];
        $this->pagination($page, $aData['pager']['total'], 'more_orders');
        $this->output();
    }


    /**
     * 分享报价单
     */
    public function offer_share()
    {
        $order_id = $_POST['order_id'];
        $offer_id = $_POST['offer_id'];
        $offer_name = $_POST['offer_name'];
        $members = $_POST['member_id'];
        if (!$order_id || !$offer_id || !$members) {
            $this->json_error($this->t('参数错误'));
        }
        $ownerId = $this->getMemberId();
        $mdl_offer = b2c_mdl_order_offer::getInstance();
        $offer = $mdl_offer->getRow('*', ['id' => $offer_id]);
        if (!$offer || $offer['order_id'] != $order_id) {
            $this->json_error($this->t('分享报价单失败'));
        }
        $b2c_order = b2c_mdl_orders::getInstance();
        if (!$b2c_order->ensureMemberOrder($order_id, $ownerId)) {
            $this->json_error($this->t('分享报价单失败') . '!');
        }
        $members = explode(',', $members);
        $db = kernel::database();
        $db->beginTransaction();
        if ($offer_name) {
            $offer_data = ['name' => $offer_name];
            $mdl_offer->update($offer_data, ['id' => $offer_id]);
        }
        $mdl_order_share = b2c_mdl_order_share::getInstance();
        foreach ($members as $member_id) {
            $member_id = intval($member_id);
            if ($member_id < 1) {
                continue;
            }
            $mdl_order_share->saveShare($ownerId, $order_id, $offer_id, $member_id);
        }
        $db->commit();
        $this->json_success('分享成功');
    }

    /**
     * 发票管理
     * @param $type
     */
    public function invoice($type = 'normal')
    {

        $memberId = $this->getMemberId();
        /**
         * @var $mdl_tax_company b2c_mdl_tax_company
         */
        $mdl_tax_company = $this->app->model('tax_company');
        /**
         * @var b2c_mdl_member_addrs $obj_member_addrs
         */
        $obj_member_addrs = $this->app->model('member_addrs');
        if ($type == 'added') {
            $this->pagedata['invoices'] = $mdl_tax_company->getAddedList($memberId);
        } else if ($type == 'normal') {
            $this->pagedata['invoices'] = $mdl_tax_company->getNormalList($memberId);
        } else if ($type == 'address') {
            $member_addr_list = $obj_member_addrs->getInvoiceAddrList($memberId);
            $this->pagedata['member_invoice_addr_list'] = $member_addr_list;
        }
        $this->pagedata['_PAGE_'] = 'invoice_' . $type . '.php';
        $this->output();
    }

    protected function errorPageMsg($msg)
    {
        exit($msg);
    }

    /**
     * @param $order_id
     * @return mixed
     * @throws Exception
     */
    protected function checkOrderPayStatus($order_id)
    {
        $mdl_order = b2c_mdl_orders::getInstance();
        $order = $mdl_order->ensureMemberOrder($order_id, $this->getMemberId(), '*');
        if (!$order) {
            throw new Exception('订单不存在');
        }
        if ($order['deposit_status'] == b2c_mdl_orders::PAYMENT_STATUS_OK) {
            throw new Exception('保证金已支付');
        }
        if ($order['deposit_status'] == b2c_mdl_orders::PAYMENT_STATUS_WAIT_AUDIT) {
            throw new Exception('保证金线下支付凭证正在审核中');
        }
        return $order;
    }

    /**
     * 在线支付保证金
     * @param $order_id
     */
    public function pay_deposit($order_id)
    {
        $this->pagedata['hiddenLeftMenu'] = true;
        $this->pagedata['offer'] = [];
        try {
            $order = $this->checkOrderPayStatus($order_id);
            $this->pagedata['order'] = $order;
            $this->output();
        } catch (Exception $e) {
            $this->errorPageMsg($e->getMessage());
        }
    }

    private function setCachePayUrl($order_id,$pay_type,$data)
    {
        $data['time'] = time();
        $data['cache'] = 1;
        $key = 'url_'.$pay_type.$order_id;
        $_SESSION[$key] = [
            'expire_time'=> time() + 3000,
            'data'=>$data
        ];
    }

    private function getCachePayUrl($order_id,$pay_type)
    {
        $key = 'url_'.$pay_type.$order_id;
        if(!isset($_SESSION[$key])){
            return null;
        }
        $data = $_SESSION[$key];
        if($data['expire_time'] > time()){
            return $data['data'];
        }
        unset($_SESSION[$key]);
        return null;
    }

    /**
     * 生成支付二维码
     * @param $order_id
     * @param $pay_type
     */
    public function pay_qrcode($order_id,$pay_type)
    {
        $order = null;
        try{
            $order = $this->checkOrderPayStatus($order_id);
        }catch (Exception $e){
            $this->json_error($e->getMessage());
        }
        if($data = $this->getCachePayUrl($order_id,$pay_type)){
            $this->json_success($data);
        }
        $member_id = $this->getMemberId();
        $mdl_order = b2c_mdl_orders::getInstance();
        /**
         * @var $objPay ectools_pay
         */
        $objPay = kernel::single('ectools_pay');
        /**
         * @var $objMath ectools_math
         */
        $objMath = kernel::single('ectools_math');
        // 得到商店名称
        $shopName = app::get('site')->getConf('site.name');
        // Post payment information.

        $_POST =[
            'payment' => [
                    'order_id' => $order_id,
                    'def_pay' =>[
                            'cur_money' => $order['deposit'],
                            'pay_app_id' => $pay_type,
                    ],
                    'memo' => $order['memo'],
                ],
            'pay' =>[
                    'password' => '',
            ],
        ];
        $sdf = $_POST['payment'];
        unset($sdf['def_pay']);
        unset($sdf['other_online']);
        if($_POST['payment']['def_pay']){
            $sdf['pay_app_id'] = $_POST['payment']['def_pay']['pay_app_id'];
            $sdf['cur_money'] = $_POST['payment']['def_pay']['cur_money'] ? $_POST['payment']['def_pay']['cur_money'] : 0;
        }
        if ($member_id)
            $sdf['member_id'] = $member_id;

        if (!$sdf['pay_app_id']){
            $this->json_error($this->t('支付方式不能为空！'));
        }
        if(!in_array($sdf['pay_app_id'],['alipay','wechat'])){
            $this->json_error($this->t('不支持此支付方式'));
        }
        if($sdf['pay_app_id'] == 'wechat'){
            $this->json_error('暂不支持微信支付');
        }
        $offer = $mdl_order->getOrderLatestOffer($order_id);
        $sdf['body'] = $offer['items'][0]['name'];
        $sdf['pay_object'] = 'order';
        $sdf['shopName'] = $shopName;
        $arrOrders = $mdl_order->dump($order_id);
        $sdf['currency'] = $arrOrders['currency'];
        $sdf['total_amount'] = $arrOrders['deposit'];
        $sdf['payed'] =  '0.000';
        $sdf['money'] = $objMath->number_div(array($sdf['cur_money'], $arrOrders['cur_rate']));
        $sdf['payinfo']['cost_payment'] = $arrOrders['payinfo']['cost_payment'];
        //收货人信息
        $sdf['receiveArr'] = $arrOrders['consignee'];
        // 相关联的id.
        $sdf['rel_id'] = $sdf['order_id'];
        $payment_id = $sdf['payment_id'] = $objPay->get_payment_id($sdf['rel_id']);
        $sdf['return_url'] = "";
        $sdf['status'] = 'ready';

        $subject = $sdf['body'];
        $subject = str_replace("'",'`',trim($subject));
        $subject = str_replace('"','`',$subject);
        /**
         * @var $obj_payment_create ectools_payment_create
         */
        $obj_payment_create = kernel::single('ectools_payment_create');
        $is_save = $obj_payment_create->generate($sdf, $msg);
        if(!$is_save){
            $this->json_error('支付失败');
        }


        date_default_timezone_set('Asia/Shanghai');
        $aliConfig = require_once ROOT_DIR . '/config/aliconfig.php';
        $payData = [
            'body'    => $subject,
            'subject'    => $subject,
            'order_no'    => $payment_id,
            'timeout_express' => time() + 3600,// 表示必须 3600s 内付款
            'amount'    => intval($arrOrders['deposit']*100)/100,// 单位为元 ,最小为0.01
            'out_trade_no'=>$payment_id,
            'client_ip' => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1',// 客户地址
            'goods_type' => '1',// 0—虚拟类商品，1—实物类商品
            'store_id' => '',
            'operator_id' => '',
            'terminal_id' => '',// 终端设备号(门店号或收银设备ID) 默认值 web
        ];
        try {
            $url = Payment\Client\Charge::run(Payment\Config::ALI_CHANNEL_QR, $aliConfig, $payData);
            b2c_payment_notify::create_pay_log("success\tmember:{$member_id}\turl:{$url}\t".json_encode($payData),'info');
            $data = ['payment_id'=>$payment_id,'url'=>$url,'count_down'=>3600];
            $this->setCachePayUrl($order_id,$pay_type,$data);
            $this->json_success($data);
        } catch (Payment\Common\PayException $e) {
            b2c_payment_notify::create_pay_log($e->getMessage().PHP_EOL.$e->getTraceAsString(),'error');
            $this->json_error($e->getMessage());
        }
    }

    /**
     * 获取支付状态
     * @param $payment_id
     */
    public function check_pay_status($payment_id)
    {
        /**
         * @var $obj_payments ectools_mdl_payments
         */
        $obj_payments = app::get('ectools')->model('payments');
        $data = $obj_payments->getRow('status',['payment_id'=>$payment_id]);
        if($data['status'] == 'succ'){
            $this->json_success('支付成功');
        }else{
            $this->json_error('未支付');
        }
    }


    private function pay_offline($order_id,$type)
    {
        $this->pagedata['hiddenLeftMenu'] = true;
        $this->pagedata['order'] = $this->getOrder($order_id);
        $this->pagedata['type'] = $type;
        $this->pagedata['_PAGE_'] = 'pay_deposit_offline.php';
        $this->output();
    }

    /**
     *  线下支付保证金
     * @param $order_id
     */
    public function pay_deposit_offline($order_id)
    {
        $order = b2c_mdl_orders::getInstance();
        $this->pagedata['offer'] = $order->getOrderLatestOffer($order_id);
        $this->pay_offline($order_id,'deposit');
    }

    /**
     *  线下支付首款
     * @param $order_id
     */
    public function pay_down_payment($order_id)
    {
        $order = b2c_mdl_orders::getInstance();
        $this->pagedata['offer'] = $order->getOrderLatestOffer($order_id);
        $this->pay_offline($order_id,'down_payment');
    }

    /**
     *  线下支付尾款
     * @param $order_id
     */
    public function pay_final_payment($order_id)
    {
        $order = b2c_mdl_orders::getInstance();
        $this->pagedata['offer'] = $order->getOrderLatestOffer($order_id);
        $this->pay_offline($order_id,'final_payment');
    }

    /**
     * @param string $prefix
     * @param string $name
     * @return string|null
     */
    protected function upload_file($prefix='receipt_',$name='receipt')
    {
        $file = $_FILES[$name];
        if(empty($file) || $file['error'] != UPLOAD_ERR_OK){
            return null;
        }
        $allowedExts = array("gif", "jpeg", "jpg", "png");
        $allowedType = array("image/gif","image/jpeg","image/jpg","image/pjpeg","image/x-png","image/png");
        $temp = explode(".", $file["name"]);
        $extension = end($temp);     // 获取文件后缀名
        $type = $file['type'];
        if(!in_array($extension, $allowedExts) || !in_array($type, $allowedType)){
            return null;
        }
        $baseDir = PUBLIC_DIR.'/upload/';
        if(!is_dir($baseDir)){
            if(!mkdir($baseDir,0755)){
                return null;
            }
        }
        $targetFile = uniqid($prefix).'.'.$extension;
        if(move_uploaded_file($file["tmp_name"], $baseDir.$targetFile)){
            return '/public/upload/'.$targetFile;
        }
        return null;
    }
    /**
     * 提交凭证
     */
    public function submit_receipt()
    {
        $status = 'error';
        $allowType = ['deposit','down_payment','final_payment'];
        $order_id = $_POST['order_id'];
        $type = $_POST['name'];
        do{
            if(!in_array($type,$allowType)){
                break;
            }
            $order = $this->getOrder($order_id);
            if(!$order){
                break;
            }
            $file = $this->upload_file('deposit_');
            if(!$file){
                break;
            }
            $field = $type.'_img';
            /**
             * @var $mdl_orders b2c_mdl_orders
             */
            $order = [];
            $mdl_orders = $this->app->model('orders');
            $order[$field] = $file;
            $order[$type.'_status'] = b2c_mdl_orders::PAYMENT_STATUS_WAIT_AUDIT;
            $order[$type.'_at'] = time();
            if(!$mdl_orders->update($order,['order_id'=>$order_id])){
                break;
            }

            $status = 'success';
        }while(0);
        $this->splash('success',"/index.php/members-pay_status-{$status}-{$type}.html");
    }

    /**
     * @todo 需要国际化
     * @param $status
     * @param $type
     */
    public function pay_status($status,$type)
    {
        $tips =[
             'deposit'      => ['title'=>'上传保证金支付凭证成功','content'=>'请等待后台人员审核'],
             'down_payment' => ['title'=>'上传首款支付凭证成功','content'=>'请等待后台人员审核'],
             'final_payment'=> ['title'=>'上传尾款支付凭证成功','content'=>'请等待后台人员审核'],
             'order_create' => ['title'=>'创建订单成功','content'=>'请等待工作人员报价'],
             'confirm_yes'  => ['title'=>'操作成功','content'=>'您已确认报价，请尽快支付首款，商品将开始生产'],
             'confirm_no'   => ['title'=>'操作成功','content'=>'您已申请重新报价，请耐心等待新的报价结果'],
             'finish_ok'      => ['title'=>'操作成功','content'=>'您的订单已完成'],
             'finish_fail'      => ['title'=>'操作失败','content'=>'确认收货失败'],
        ];
        if($status == 'error'){
            $msg = ['title'=>'上传支付凭证失败','content'=>'上传凭证应该是图片文件,并且大小不能超过2M'];
        }else{
            $msg = $tips[$type];
        }
        $this->pagedata['msg'] = $msg;
        $this->pagedata['hiddenLeftMenu'] = true;
        $this->output();
    }


    public function company_save()
    {
        $member_id = $this->getMemberId();
        $data = $_POST;
        $data['member_id'] = $member_id;
        $tax_invoice_types =['normal','added'];
        if(!in_array($data['tax_invoice_type'],$tax_invoice_types)){
            $this->json_error($this->t('发票类型不正确')) ;
        }
        if(!$data['tax_company']){
            $this->json_error($this->t('公司名称不能为空'));
        }
        if(!$data['tax_code']){
            $this->json_error($this->t('税号不能为空'));
        }
        if($data['tax_invoice_type'] == 'added'){
            if(!$data['tax_addr']){
                $this->json_error($this->t('公司地址不能为空'));
            }
            if(!$data['tax_bank']){
                $this->json_error($this->t('开户行不能为空'));
            }
            if(!$data['bank_account']){
                $this->json_error($this->t('账号不能为空'));
            }
        }
        /**
         * @var $mdl_tax_company b2c_mdl_tax_company
         */
        $mdl_tax_company = $this->app->model('tax_company');
        if($data['id']){
            if(!$mdl_tax_company->getRowByUid($data['id'],$member_id)){
                $this->json_error($this->t('修改公司信息失败.'));
            }
        }
        if($mdl_tax_company->save($data)){
            $this->json_success($data);
        }else{
            $this->json_error($this->t('保存失败'));
        }
    }

    public function company_edit($id)
    {
        $member_id = $this->getMemberId();
        /**
         * @var $mdl_tax_company b2c_mdl_tax_company
         */
        $mdl_tax_company = $this->app->model('tax_company');
        $id = intval($id);
        if($id<1){
            $this->json_error($this->t('参数错误'));
        }
        $info = $mdl_tax_company->getRowByUid($id,$member_id);
        if(!$info){
            $this->json_error($this->t('公司信息不存在'));
        }
        $this->json_success($info);
    }

    public function company_delete($id)
    {
        $member_id = $this->getMemberId();
        /**
         * @var $mdl_tax_company b2c_mdl_tax_company
         */
        $mdl_tax_company = $this->app->model('tax_company');
        $id = intval($id);
        if($id<1){
            $this->json_error($this->t('参数错误'));
        }

        $flag = $mdl_tax_company->delete(['id'=>$id,'member_id'=>$member_id]);
        if($flag){
            $this->json_success($this->t('删除成功'));
        }else{
            $this->json_error($this->t('删除失败'));
        }
    }

    /**
     *详细页修改收货地址
     */
    public function change_order_addr()
    {
        $order_id = $_POST['order_id'];
        if(!$order_id){
            $this->json_error($this->t('订单号不能为空'));
        }
        if(empty($_POST['addr']) || empty($_POST['mobile']) || empty($_POST['name'])){
            $this->json_error($this->t('收货地址不完整'));
        }
        if(!is_numeric($_POST['mobile'])){
            $this->json_error($this->t('手机号不正确'));
        }
        $order = b2c_mdl_orders::getInstance();
        $row = $order->getRow('member_id,order_status',['order_id'=>$order_id]);
        if(!$row || $row['member_id'] != $this->getMemberId()){
            $this->json_error($this->t('订单不存在'));
        }
        if(!b2c_mdl_orders::canModifyShippingAddress($row)){
            $this->json_error($this->t('商品已发货，不能修改收货地址！'));
        }
        $data = [];
        $data['ship_addr'] = $_POST['addr'];
        $data['ship_mobile'] = $_POST['mobile'];
        $data['ship_name'] = $_POST['name'];
        if($order->update($data,['order_id'=>$order_id])){
            $this->json_success($data);
        }else{
            $this->json_error($this->t('修改收货地址失败'));
        }
    }

    /**
     * 确认报价单|异议报价单
     * @param $order_id
     * @param $status
     */
    public function offer_confirm($order_id,$status)
    {
        $order = b2c_mdl_orders::getInstance();
        if(!in_array($status,['yes','no'])){
            $this->errorPageMsg('参数不正确');
        }
        $row = $order->getRow('member_id',['order_id'=>$order_id]);
        if(!$row || $row['member_id'] != $this->getMemberId()){
            $this->errorPageMsg('订单不存在');
        }
        $data = [];
        $now = time();
        if($status == 'yes'){
            $data['order_current_status'] = b2c_mdl_orders::ORDER_STATUS_WAIT_DOWN_PAYMENT;
            $data['order_status'] =  b2c_mdl_orders::ORDER_STATUS_WAIT_DOWN_PAYMENT;
            $data['confirm_status'] = b2c_mdl_orders::CONFIRM_STATUS_OK;
            $data['confirm_at'] = $now;
            $data['down_payment_expire_at'] = b2c_mdl_orders::expireTime($now);
            $msg = '确认报价单';
            $type = 'confirm_yes';
        }else{
            $data['order_current_status'] = b2c_mdl_orders::ORDER_STATUS_WAIT_MODIFY_OFFER;
            $data['confirm_status'] = b2c_mdl_orders::CONFIRM_STATUS_FAIL;
            $data['confirm_at'] = $now;
            $msg = '对报价有异议';
            $type = 'confirm_no';
        }
        if($order->update($data,['order_id'=>$order_id])){
            $log_id = b2c_mdl_order_log::saveCustomerLog($order_id,$msg);
            $this->splash('success',"/index.php/members-pay_status-success-{$type}.html");
        }
    }

    public function finish($order_id)
    {
        $b2c_order = b2c_mdl_orders::getInstance();
        $order = $b2c_order->ensureMemberOrder($order_id,$this->getMemberId(),'*');
        if(!$order){
            $this->errorPageMsg('操作失败!');
        }
        if($order['order_status'] != b2c_mdl_orders::ORDER_STATUS_WAIT_RECEIVED){
            $this->errorPageMsg('操作失败.');
        }
        $data = [];
        $data['order_status'] = b2c_mdl_orders::ORDER_STATUS_FINISHED;
        $data['order_current_status'] = b2c_mdl_orders::ORDER_STATUS_FINISHED;
        $data['finish_at'] = time();
        if($b2c_order->update($data,['order_id'=>$order_id])){
            b2c_mdl_order_log::saveCustomerLog($order_id,'确认收货');
            $this->splash('success',"/index.php/members-pay_status-success-finish_ok.html");
        }else{
            $this->splash('success',"/index.php/members-pay_status-success-finish_fail.html");
        }
    }

    public function delivery($order_id)
    {
        $b2c_order = b2c_mdl_orders::getInstance();
        $order = $b2c_order->ensureMemberOrder($order_id,$this->getMemberId());
        if(!$order){
            $this->json_error($this->t('获取物流信息失败').'!');
        }
        $mdl_logisticstrack = b2c_mdl_logisticstrack::getInstance();
        $data = $mdl_logisticstrack->getData($order_id);
        if(!$data){
            $this->json_error($this->t('暂无物流信息'));
        }
        $html = $this->renderPartial('site/members/delivery.php',['data'=>$data]);
        $this->json_success($html);
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function filter_input($data)
    {
        $obj_filter = kernel::single('b2c_site_filter');
        return $obj_filter->check_input($data);
    }

    private function save_after_sales($order_id,$data,$offer)
    {
        $product_data = array();
        $item_ids = $data['item_id'];
        $nums = $data['item_nums'];

        if(empty($data['title'])){
            $this->json_error($this->t('请填写退换理由'));
        }
        if(empty($data['content'])){
            $this->json_error($this->t('请填写详细描述'));
        }
        if(!$item_ids || !is_array($item_ids)){
            $this->json_error($this->t('请选择要申请的售后商品'));
        }
        if(!$nums || !is_array($nums)){
            $this->json_error($this->t('请选择要申请的售后商品数量'));
        }

        $items = $offer['items'];
        foreach($items as $k=>$v){
            $items[$v['item_id']] = $v;
        }
        foreach ($item_ids as $key => $id)
        {
            if(!isset($items[$id])){
                $this->json_error($this->t('申请的售后商品不正确'));
            }
            $item = array();
            $item['bn'] = $items[$id]['bn'];
            $item['name'] = $items[$id]['name'];
            $item['num'] = $nums[$key];
            if( $items[$id]['nums'] < $item['num']){
                $this->json_error($this->t("您申请退换货的物品大于可退货换数量"));
            }
            $item['price'] = $items[$id]['price'];
            $item['product_id'] = $items[$id]['product_id'];
            $item['thumbnail_pic'] = $items[$id]['thumbnail_pic'];
            $item['link_url'] = $items[$id]['link_url'];
            $product_data[] = $item;
        }
        $images = [];

        foreach($data['images'] as $image){
            if(strncmp( $image,'/public/upload/',15)!=0){
                $this->json_error($this->t('图片不正确'));
            }
            $images[] = $image;
        }
        $aData = [];
        $aData['order_id'] = $order_id;
        $aData['member_id'] = $this->getMemberId();
        $aData['return_id'] = date('YmdHis').rand(100,999);
        $aData['return_bn'] = $aData['return_id'];
        $aData['title']    =  $data['title'];
        $aData['content'] = $data['content'];
        $aData['type']     = 1;
        $aData['status'] = 2;
        $aData['image_file'] = serialize($images);
        $aData['product_data'] = serialize($product_data);
        $aData['add_time'] = time();


        //开启事务处理
        $db = kernel::database();
        $db->beginTransaction();
        /**
         * @var $mdl_return_product  aftersales_mdl_return_product
         */
        $mdl_return_product = app::get('aftersales')->model('return_product');
        $b2c_order = b2c_mdl_orders::getInstance();
        $order_status = ['after_sale_status'=>b2c_mdl_orders::AFTER_SALE_STATUS_YES];

        if($mdl_return_product->insert($aData)
            && $b2c_order->update($order_status,['order_id'=>$order_id])){
            $db->commit();
            $this->json_success('提交售后成功');
        }else{
            $db->rollBack();
            $this->json_error('提交售后失败');
        }
    }

    public function after_sales($order_id)
    {
        $b2c_order = b2c_mdl_orders::getInstance();
        $order = $b2c_order->ensureMemberOrder($order_id,$this->getMemberId(),'*');
        if(!$order){
            $this->json_error($this->t('售后订单不存在').'!');
        }
        if(b2c_mdl_orders::AFTER_SALE_STATUS_YES == $order['after_sale_status']){
            $this->json_error($this->t('您已申请过售后').'!');
        }
        if(b2c_mdl_orders::ORDER_STATUS_FINISHED != $order['order_status']){
            $this->json_error($this->t('未完成的订单不能申请售后').'!');
        }
        $offer = $b2c_order->getOrderLatestOffer($order_id);
        /**
        $_POST = [
            'title'=>'退换理由',
            'content'=>'详细描述',
            'item_id' =>[7,8],
            'item_nums'=>[1,1],
            'images'=>'/public/upload/deposit_5d43cc800e9b1.jpg',
        ];
         * */
        if($_POST){
            $data = $this->filter_input($_POST);
            $this->save_after_sales($order_id,$data,$offer);
            return;
        }
        $this->pagedata['offer'] = $offer;
        $this->pagedata['order'] = $order;
        $this->pagedata['hiddenLeftMenu'] = true;
        $this->output();
    }

    public function upload_picture()
    {
        $path = $this->upload_file('sh','file');
        if($path){
            $this->json_success($path);
        }else{
            $this->json_error('上传失败');
        }
    }

    protected function json_error($msg,$exit=true)
    {
        $this->asJson(['code'=>0,'msg'=>$msg,'data'=>null],$exit);
    }

    protected function json_success($data,$exit=true)
    {
        $this->asJson(['code'=>10000,'msg'=>'','data'=>$data],$exit);
    }

    protected function asJson($data,$exit=true)
    {
        echo json_encode($data);
        if($exit){
            exit;
        }
    }

}
