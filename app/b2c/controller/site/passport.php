<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */
class b2c_ctl_site_passport extends b2c_frontpage{
    function __construct(&$app){
        parent::__construct($app);
        $this->_response->set_header('Cache-Control', 'no-store');
        kernel::single('base_session')->start();
        $this->userObject = kernel::single('b2c_user_object');
        $this->userPassport = kernel::single('b2c_user_passport');
    }

    /*
     * 如果是登录状态则直接跳转到会员中心
     * */
    public function check_login($mini=null){
        if( $this->userObject->is_login() )
        {
            $url = $this->gen_url(array('app'=>'b2c','ctl'=>'site_member','act'=>'index'));
            if($_GET['mini_passport']==1 || $mini){
                kernel::single('site_router')->http_status(302);return;
            }else{
                //您已经是登录状态，不需要重新登录
                $this->redirect($url);
            }
        }
        return false;
    }

    public function index(){
        //如果会员登录则直接跳转到会员中心
        $this->check_login();
        $this->login();
    }

    /*
     * 登录view
     * */
    public function login($mini=0){
        //如果会员登录则直接跳转到会员中心
        $this->check_login($mini);

        $flag = false;
        if($_GET['mini_passport']==1 || $mini) {
            $flag = true;
            $this->pagedata['mini_passport'] = 1;
        }

        //是否开启验证码
        $this->pagedata['show_varycode'] = kernel::single('b2c_service_vcode')->status();

        //是否关闭用户名自动补全
        $this->pagedata['close_autocomplete'] = $this->app->getConf('site.login_close_autocomplete');

        //信任登录openapi
        foreach(kernel::servicelist('openid_imageurl') as $object){
            if(is_object($object)){
                if(method_exists($object,'get_image_url')){
                    $login_image_urls[] = $object->get_image_url();
                }
            }
        }
        $data = app::get('trustlogin')->getConf('trustlogin_rule');
        //信任登录trustlogin
        if($data['status'] && $data['status'] == 'true')
        {
            foreach(kernel::servicelist('trustlogin_trustlogin.trustlogin_mdl_trustlogin_cfg') as $object)
            {
                if(is_object($object))
                {
                    if(method_exists($object,'get_logo'))
                    {
                        $login_image_url[] = $object->get_logo();
                    }
                }
            }
            //信任登录trustlogin判断后台启用状态
            foreach ($login_image_url as $key => $value)
            {
                if(!$value['status'])
                {
                    unset($login_image_url[$key]);
                }
                if($value['status'] && $value['status'] == 'false')
                {
                    unset($login_image_url[$key]);
                }
            }
        }

        //echo '<pre>';print_r($login_image_url);exit();
        //登录页面左侧大图
        $images_id = app::get('b2c')->getConf('site.loginlogo');
        $strorager = kernel::single("base_storager");
        $images_url = $strorager->image_path($images_id,'l');
        $this->pagedata['image_url'] = $images_url;
        if($login_image_urls)
        {
            $this->pagedata['login_image_urls'] = $login_image_urls;
        }
        if($login_image_url)
        {
            $this->pagedata['login_image_url'] = $login_image_url;
        }
        $this->pagedata['loginName'] = $_COOKIE['loginName'];
        //$this->userPassport->set_next_page();
        //$this->set_tmpl('passport');
        $this->layout = '';
        $this->render('site/passport/login.php');
    }//end function

    /**
     * 检查登录账号是否需要开启手机验证
     */
    public function login_ajax_account(){
        $login_account = trim($_POST['uname']);
        if( $this->userPassport->check_login_account($login_account,$msg) ){
            echo json_encode(array('needVerify'=>'true'));exit;
        }else{
            echo json_encode(array('needVerify'=>'false'));exit;
        }
    }

    /*
     * 登录验证
     * */
    public function post_login(){
        //_POST过滤
        ////modified by zengxinwen 2016-1-21 修改地方 status() 和 set_error_count()传入了 $post['uname']参数
        $post = utils::_filter_input($_POST);
        unset($_POST);
        $userData = array(
            'login_account' => $post['userName'],
            'login_password' => $post['pwd']
        );

        //是否需要进行手机验证
        if( !kernel::single('b2c_user_vcode')->mobile_login_verify($post['mobileVcode'],$post['userName'],'activation')){
            $msg = app::get('b2c')->_('手机短信验证码错误');
            $this->splash('failed',null,$msg,true);exit;
        }

        if(kernel::single('b2c_service_vcode')->status($post['userName']) && empty($post['vCode'])){
            $msg = app::get('b2c')->_($this->t('请输入验证码!'));
            $this->splash('failed',null,$msg,true);exit;
        }

        $member_id = kernel::single('pam_passport_site_basic')->login($userData,$post['vCode'],$msg);
        #$member_id = 11;
        if(!$member_id){
            //设置登录失败错误次数 一个小时三次错误后需要自动开启验证码
            kernel::single('b2c_service_vcode')->set_error_count($post['userName']);
            $data['needVcode'] = kernel::single('b2c_service_vcode')->status($post['userName']);
            $this->splash('failed',null,$this->t($msg),true,$data);exit;
        }

        $b2c_members_model = $this->app->model('members');
        $member_point_model = $this->app->model('member_point');

        $member_data = $b2c_members_model->getList( 'member_lv_id,experience,point', array('member_id'=>$member_id) );
        if(!$member_data){
            kernel::single('b2c_service_vcode')->set_error_count($post['userName']);
            $data['needVcode'] = kernel::single('b2c_service_vcode')->status($post['userName']);
            //在登录认证表中存在记录，但是在会员信息表中不存在记录
            $msg = $this->app->_($this->t('登录失败：会员数据存在问题,请联系商家或客服'));
            $this->splash('failed',null,$msg,true,$data);exit;
        }

        $member_data = $member_data[0];
        $member_data['order_num'] = $this->app->model('orders')->count( array('member_id'=>$member_id) );

        if($this->app->getConf('site.level_switch')==1)
        {
            $member_data['member_lv_id'] = $b2c_members_model->member_lv_chk($member_data['member_lv_id'],$member_data['experience']);
        }
        if($this->app->getConf('site.level_switch')==0)
        {
            $member_data['member_lv_id'] = $member_point_model->member_lv_chk($member_id,$member_data['member_lv_id'],$member_data['point']);
        }

        $b2c_members_model->update($member_data,array('member_id'=>$member_id));
        $this->userObject->set_member_session($member_id);
        $this->bind_member($member_id);
        $this->set_cookie('loginName',$post['userName'],time()+31536000);//用于记住密码
        // setCartNum()需要传入一个参数
        $aCart = array();
        $this->app->model('cart_objects')->setCartNum($aCart);
        $url = $this->userPassport->get_next_page('pc');
        if( !$url ){
            $url = kernel::single('b2c_frontpage')->gen_url(array('app'=>'site','ctl'=>'default','act'=>'index'));
        }
        kernel::single('pam_lock')->flush_lock($member_id);
        $this->splash('success',$url,app::get('b2c')->_($this->t('登录成功')),true);
    }//end function


    //注册页面
    public function signup($url=null){
        //检查是否登录，如果已登录则直接跳转到会员中心
        $this->check_login();
        $this->layout = '';
        $this->render("site/passport/personal_check_mail.php");
    }

    //个人注册
    public function signup_do(){
        $type = $_POST['type'] ? $_POST['type'] : '';
        $info = $_POST['info'] ? $_POST['info'] : '';
        //$this->splash('failed','',app::get('b2c')->_($this->t('暂停注册')),true);
        if($type == 'company'){
            $info = json_decode(b2c_Mcrypt::decode($info),true);
            $data['pam_account']['login_name'] = $info['email'];
            $data['pam_account']['nickname'] = $info['nickName'];
            $data['pam_account']['login_password'] = $info['pwd'];
            $data['pam_account']['psw_confirm'] = $info['rePwd'];
            $register['uname'] = $info['nickName'];
            $register['cp_name'] = $_POST['cpName'] ? $_POST['cpName'] : '';
            $register['cp_id'] = $_POST['cpId'] ? $_POST['cpId'] : '';
            $register['cp_addr'] = $_POST['cpAddr'] ? $_POST['cpAddr'] : '';
            $register['cp_ind'] = $_POST['cpInd'] ? $_POST['cpInd'] : '';
            $register['cp_dim'] = $_POST['cpDim'] ? $_POST['cpDim'] : '';
            $register['cp_contact'] = $_POST['cpConact'] ? $_POST['cpConact'] : '';
            $register['cp_web'] = $_POST['cpWeb'] ? $_POST['cpWeb'] : '';
            $register['pst_photo'] = $_POST['pstPhoto'] ? $_POST['pstPhoto'] : '';
            $register['member_type'] = $_POST['member_type'] ? $_POST['member_type'] : 1;
        }else{
            $nickName = $_POST['nickName'] ? $_POST['nickName'] : '';
            $pwd = $_POST['pwd'] ? $_POST['pwd'] : '';
            $rePwd = $_POST['rePwd'] ? $_POST['rePwd'] : '';
            $email = $_POST['email'] ? $_POST['email'] : '';
            $data['pam_account']['login_name'] = b2c_Mcrypt::decode($email);
            $data['pam_account']['nickname'] = $nickName;
            $data['pam_account']['login_password'] = $pwd;
            $data['pam_account']['psw_confirm'] = $rePwd;
        }

        if( !$this->userPassport->check_signup($data,$msg) ){
            $this->splash('failed',null,$this->t($msg),true);
        }

        $saveData = $this->userPassport->pre_signup_process($data);

        if( $member_id = $this->userPassport->save_members($saveData,$msg) ){
            //$this->userObject->set_member_session($member_id);
            //$this->bind_member($member_id);
            foreach(kernel::servicelist('b2c_save_post_om') as $object) {
                $object->set_arr($member_id, 'member');
                $refer_url = $object->get_arr($member_id, 'member');
            }
            //保存推荐关系
            if(!empty($_SESSION['referrals_code'])){
                $obj_policy = kernel::service("referrals.member_policy");
                if(is_object($obj_policy))
                {
                    $obj_policy ->save_referrals_member($_SESSION['referrals_code'],$member_id);
                }
            }
            //增加会员同步 2012-5-15
            if( $member_rpc_object = kernel::service("b2c_member_rpc_sync") ) {
                $member_rpc_object->createActive($member_id);
            }
            //添加注册积分
            if(!empty($_SESSION['referrals_code'])){
                $obj_policy = kernel::service("referrals.member_policy");
                if(is_object($obj_policy))
                {
                    $obj_policy ->referrals_member($_SESSION['referrals_code'],$member_id);
                }
            }
            /*注册完成后做某些操作! begin*/
            foreach(kernel::servicelist('b2c_register_after') as $object) {
                $object->registerActive($member_id);
            }
            /*end*/
            $data['member_id'] = $member_id;
            $data['uname'] = $saveData['pam_account']['login_account'];
            $data['passwd'] = $_POST['pam_account']['psw_confirm'];
            $data['email'] = $_POST['email'];
            $data['refer_url'] = $refer_url ? $refer_url : '';
            $data['is_frontend'] = true;
            $obj_account = $this->app->model('member_account');
            $obj_account->fireEvent('register',$data,$member_id);

            if($type == 'company'){
                app::get('b2c')->model('members')->update($register,['member_id' => $member_id]);
                $url = "/index.php/passport-company_over.html";
            }else{
                $personal['check_status'] = b2c_mdl_members::CHECK_STATUS_YES;
                $personal['state'] = b2c_mdl_members::CHECK_STATUS_YES;
                $personal['name'] = $nickName;
                app::get('b2c')->model('members')->update($personal,['member_id' => $member_id]);
                $url = "/index.php/passport-next_step.html";
            }
            $this->splash('success',$url,app::get('b2c')->_($this->t('注册成功')),true,b2c_Mcrypt::encode($member_id));
        }

        $this->splash('failed',$url,app::get('b2c')->_($this->t('注册失败')),true);
    }

    //检查邮箱页面
    public function personal_check_mail(){
        $email = $_POST['email'] ? $_POST['email'] : '';
        $emailCheck = app::get('b2c')->model('email_check')->dump(array('email' => $email));
        if(!$emailCheck || $emailCheck['email_check'] == 'false'){
            $this->splash('failed','',app::get('b2c')->_($this->t('邮箱未验证！')),true);
        }
        $member = app::get('b2c')->model('members')->getMemberByEmail($email);
        if($member){
            $this->splash('failed','',app::get('b2c')->_($this->t('此邮箱已注册！')),true);
        }
        $email = b2c_Mcrypt::encode($email);
        $url = "/index.php/passport-personal_check_mail_next.html?email=".$email;
        $this->splash('success',$url,app::get('b2c')->_($this->t('验证成功！')),true);
    }

    //验证邮箱下一步到填写资料页面
    public function personal_check_mail_next(){
        $email = $_GET['email'] ? $_GET['email'] : '';
        $this->layout = '';
        $this->render("site/passport/signup.php",['email' => $email]);
    }

    //发送邮件
    public function send_email(){
        $email = $_POST['email'] ? $_POST['email'] : '';
        if(!$email){
            $this->splash('failed','',app::get('b2c')->_($this->t('请填写邮箱地址！')),true);
        }

        $host = $_SERVER['SERVER_NAME'];
        //注册发送邮件
        $time = time();
        $emailCheck['email'] = $email;
        $emailCheck['sign'] = md5(md5(b2c_helper::createRandom(8).$time));
        $emailCheck['createtime'] = $time;
        $content = $this->t('你好，请验证之后继续注册,你申请账号的验证连接为').' http://vanseofei-shop.zkzw.xyz/index.php/passport-mail_link.html?&sign='.$emailCheck['sign'].'&p='.$time;

        $emailCheckData = app::get('b2c')->model('email_check')->dump(array('email' => $email));
        if($emailCheckData && $emailCheckData['email_check'] == 'true'){
            $this->splash('failed','',app::get('b2c')->_($this->t('此邮箱已注册！')),true);
        }
        if(!$emailCheckData || $emailCheckData['email_check'] == 'false'){
            $flag = b2c_send::sendEmail($email,$this->t('验证信息'),$content);
            if($flag && !$emailCheckData){
                app::get('b2c')->model('email_check')->save($emailCheck);
            }
            if($flag && $emailCheckData){
                app::get('b2c')->model('email_check')->update($emailCheck,['email' => $email]);
            }

        }
        if(!$flag){
            $this->splash('failed','',app::get('b2c')->_($this->t('发送邮件失败！')),true);
        }
        $this->splash('success','',app::get('b2c')->_($this->t('发送成功！')),true);
    }

    //邮箱验证连接
    public function mail_link(){
        $sign = $_GET['sign'] ? $_GET['sign'] : '';
        $time = $_GET['p'] ? $_GET['p'] : '';
        $data['email_check'] = true;

        $emailCheck = app::get('b2c')->model('email_check')->dump(array('sign' => $sign,'createtime' => $time));
        if($emailCheck){
            if($emailCheck['email_check'] == 'true'){
                echo $this->t('此邮箱已注册！');
                return ;
            }
            if(app::get('b2c')->model('email_check')->update($data,['id' => $emailCheck['id']])){
                echo $this->t('验证成功！');
                return ;
            }
            echo $this->t('验证失败！');
        }

    }


    //企业注册
    public function company_signup(){
        $this->render("site/passport/company_signup.php");
    }

    //企业注册验证邮箱下一步
    public function company_register_check(){
        $email = $_POST['email'] ? $_POST['email'] : '';
        $is_agree = $_POST['is_agree'] ? $_POST['is_agree'] : 0;
        if(!$email){
            $this->splash('failed','',app::get('b2c')->_($this->t('邮箱不能为空')),true);
        }
        if(!$is_agree){
            $this->splash('failed','',app::get('b2c')->_($this->t('未同意注册协议')),true);
        }
        $emailCheck = app::get('b2c')->model('email_check')->dump(array('email' => $email));
        if(!$emailCheck || $emailCheck['email_check'] == 'false'){
            $this->splash('failed','',app::get('b2c')->_($this->t('邮箱未验证')),true);
        }
        $member = app::get('b2c')->model('members')->getMemberByEmail($email);
        if($member){
            $this->splash('failed','',app::get('b2c')->_($this->t('此邮箱已注册')),true);
        }
        $url = "/index.php/passport-company_register_step2.html?p=".b2c_Mcrypt::encode($email);
        $this->splash('success',$url,app::get('b2c')->_($this->t('验证成功')),true);
    }

    //企业注册设置账号密码
    public function company_register_step2(){
        $email = $_GET['p'] ? $_GET['p'] : '';
        $email = b2c_Mcrypt::decode($email);
        $this->layout = '';
        $this->render("site/passport/company_step2.php",['email' => $email]);
    }

    //注册验证邮箱后下一步
    /*public function register_check(){
        $member_id = $_POST['member_id'] ? $_POST['member_id'] : 0;
        if(!$member_id){
            $this->splash('failed','',app::get('b2c')->_('用户id不合法！'),true);
        }
        $member = app::get('pam')->model('members')->dump(['member_id' => $member_id]);
        if(!$member){
            $this->splash('failed','',app::get('b2c')->_('注册失败！'),true);
        }
        if($member && $member['email_check'] == 'false'){
            $this->splash('failed','',app::get('b2c')->_('请在邮箱中验证！'),true);
        }
        $url = "/index.php/passport-next_step.html";
        $this->splash('success',$url,app::get('b2c')->_('注册成功！'),true);
    }*/

    public function company_info(){
        $data['nickName'] = $_POST['nickName'] ? $_POST['nickName'] : '';
        $data['email'] = $_POST['email'] ? $_POST['email'] : '';
        $data['pwd'] = $_POST['pwd'] ? $_POST['pwd'] : '';
        $data['rePwd'] = $_POST['rePwd'] ? $_POST['rePwd'] : '';
        if($data['pwd'] != $data['rePwd']){
            $this->splash('failed','',app::get('b2c')->_($this->t('两次密码不一致')),true);
        }
        $data = json_encode($data);
        $this->pagedata['data'] = b2c_Mcrypt::encode($data);
        $url = "/index.php/passport-company_register_step3.html?p=".b2c_Mcrypt::encode($data);
        $this->splash('success',$url,app::get('b2c')->_($this->t('提交成功')),true);
    }

    public function next_step(){
        $this->layout = '';
        $this->render("site/passport/next_step.php");
    }

    //设置企业信息
    public function company_register_step3(){
        $data = $_GET['p'] ? $_GET['p'] : '';
        $this->layout = '';
        $this->render("site/passport/company_step3.php",['data' => $data]);
    }

    public function company_over(){
        $this->layout = '';
        $this->render("site/passport/company_over.php");
    }

    public function license(){
        $this->pagedata['reg_license'] = app::get('b2c')->getConf('b2c.register.setting_member_license');
        $this->page('site/passport/license.html');
    }

    public function privacy(){
        $this->pagedata['reg_privacy'] = app::get('b2c')->getConf('b2c.register.setting_member_privacy');
        $this->page('site/passport/license.html');
    }

    //注册的时，检查账号
    public function signup_ajax_check_name(){

        if($this->app->getConf('site.sms_only_valide')=="true"){
            $flag = $this->userPassport->check_signup_account_pc_mobile( trim($_POST['pam_account']['login_name']),$msg );
        } else{
            $flag = $this->userPassport->check_signup_account( trim($_POST['pam_account']['login_name']),$msg );
        }
        if($flag){
            if($msg == 'mobile'){
                echo json_encode(array('needVerify'=>'true'));exit;
            }
            $this->splash('success',null,$this->app->_('该登录账号可用'),true );exit;
        }else{
            $this->splash('error',null,$msg,true);exit;
        }
    }

   /**
     * create
     * 创建会员
     * 采用事务处理,function save_attr 返回false 立即回滚
     * @access public
     * @return void
     */
    public function create(){
        //modified by zengxinwen
        $_POST = utils::_filter_input($_POST);
        if($_POST['response_json'] == 'true'){
            $ajax_request = true;
        }else{
            $ajax_request = false;
        }
        if( !$this->userPassport->check_signup($_POST,$msg) ){
            $this->splash('failed',null,$msg,$ajax_request);
        }

        $saveData = $this->userPassport->pre_signup_process($_POST);

        if( $member_id = $this->userPassport->save_members($saveData,$msg) ){
            $this->userObject->set_member_session($member_id);
            $this->bind_member($member_id);
            foreach(kernel::servicelist('b2c_save_post_om') as $object) {
                $object->set_arr($member_id, 'member');
                $refer_url = $object->get_arr($member_id, 'member');
            }
            //保存推荐关系
            if(!empty($_SESSION['referrals_code'])){
                $obj_policy = kernel::service("referrals.member_policy");
                if(is_object($obj_policy))
                {
                    $obj_policy ->save_referrals_member($_SESSION['referrals_code'],$member_id);
                }
            }
            //增加会员同步 2012-5-15
            if( $member_rpc_object = kernel::service("b2c_member_rpc_sync") ) {
                $member_rpc_object->createActive($member_id);
            }
            //添加注册积分
            if(!empty($_SESSION['referrals_code'])){
                $obj_policy = kernel::service("referrals.member_policy");
                if(is_object($obj_policy))
                {
                    $obj_policy ->referrals_member($_SESSION['referrals_code'],$member_id);
                }
            }
            /*注册完成后做某些操作! begin*/
            foreach(kernel::servicelist('b2c_register_after') as $object) {
                $object->registerActive($member_id);
            }
            /*end*/
            $data['member_id'] = $member_id;
            $data['uname'] = $saveData['pam_account']['login_account'];
            $data['passwd'] = $_POST['pam_account']['psw_confirm'];
            $data['email'] = $_POST['contact']['email'];
            $data['refer_url'] = $refer_url ? $refer_url : '';
            $data['is_frontend'] = true;
            $obj_account=$this->app->model('member_account');
            $obj_account->fireEvent('register',$data,$member_id);
            if(!strpos($_SESSION['pc_next_page'],'cart')){
                $url = $this->gen_url(array('app'=>'b2c','ctl'=>'site_passport','act'=>'sign_tips'));
            }else{
                $url = $_SESSION['pc_next_page'];
            }
            $this->splash('success',$url,app::get('b2c')->_('注册成功'),$ajax_request);
        }

        $this->splash('failed',$back_url,app::get('b2c')->_('注册失败'),$ajax_request);
    }

    //注册后跳转页面
    public function sign_tips(){
        $member_id = $this->userObject->get_member_id();
        if(!$member_id){
            $url = $this->gen_url(array('app'=>'b2c','ctl'=>'site_passport','act'=>'index'));
            $this->splash('failed',$url,app::get('b2c')->_('页面已过期，请重新登录在会员中心设置'));
        }

        $url = $this->userPassport->get_next_page('pc');
        if(!$url){
          $url = $this->gen_url(array('app'=>'b2c','ctl'=>'site_member','act'=>'index'));
        }
        $pamMembersData = $this->userObject->get_pam_data('login_account',$member_id);
        if($pamMembersData['local']){
            $this->redirect($url);//已近设置则不需要在设置 直接跳转到会员中心
        }

        $this->pagedata['data'] = $pamMembersData;
        $this->pagedata['url'] = $url;

        $this->set_tmpl('passport');
        $this->page("site/passport/sign-tips.html");
    }

    //设置用户名
    public function save_local_uname(){
        $url = $this->gen_url(array('app'=>'b2c','ctl'=>'site_member','act'=>'index'));
        if( $this->userPassport->set_local_uname($_POST,$msg) ){
            $member_id = $this->userObject->get_member_id();
            $this->bind_member($member_id);
            $this->splash('success',null,$msg,true);
        }else{
            $msg = $msg ? $msg : app::get('b2c')->_('页面已过期，请重新登录在会员中心设置');
            $this->splash('failed',null,$msg,true);
        }
    }


    /*----------- 次要流程 ---------------*/
    /**
     * 修改密码
     */
    public function lost(){
        $this->check_login();
        $this->layout = '';
        $this->render("site/passport/lost_check_email.php");
    }

    /**
     * 修改密码发送验证邮件
     */
    public function send_pass_email(){
        $email = $_POST['email'] ? $_POST['email'] : '';
        if(!$email){
            $this->splash('failed','',app::get('b2c')->_($this->t('请填写邮箱地址')),true);
        }
        $host = $_SERVER['SERVER_NAME'];
        //注册发送邮件
        $time = time();
        $emailCheck['email'] = $email;
        $emailCheck['createtime'] = $time;
        $content = $this->t('密码修改验证连接').' http://vanseofei-shop.zkzw.xyz/index.php/passport-mail_pass.html?&p='.b2c_Mcrypt::encode($email).'&s='.b2c_Mcrypt::encode($time);

        //是否注册用户
        $member = app::get('b2c')->model('members')->getMemberByEmail($email);
        if(!$member){
            $this->splash('failed','',app::get('b2c')->_($this->t('此邮箱未注册')),true);
        }

        $flag = b2c_send::sendEmail($email,$this->t('验证信息'),$content);
        if($flag ){
            app::get('b2c')->model('email_pass_check')->save($emailCheck);
        }

        if(!$flag){
            $this->splash('failed','',app::get('b2c')->_($this->t('发送邮件失败')),true);
        }
        $this->splash('success','',app::get('b2c')->_($this->t('发送成功')),true);
    }

    public function mail_pass(){
        $email = $_GET['p'] ? $_GET['p'] : '';
        $time = $_GET['s'] ? $_GET['s'] : '';
        $data['email_check'] = true;

        $emailCheck = app::get('b2c')->model('email_pass_check')->dump(array('email' =>b2c_Mcrypt::decode($email),'createtime' => b2c_Mcrypt::decode($time)));
        if(!$emailCheck){
            echo $this->t('验证失败');
            return ;
        }
        if($emailCheck){
            if(app::get('b2c')->model('email_pass_check')->update($data,['id' => $emailCheck['id']])){
                echo $this->t('验证成功');
                return ;
            }
            echo $this->t('验证失败');
        }
    }

    /**
     * 修改密码验证邮箱
     */
    public function check_pass_email(){
        $email = $_POST['email'] ? $_POST['email'] : '';
        $emailCheck = app::get('b2c')->model('email_pass_check')->getRow('*',array('email' => $email),$orderType='createtime DESC');
        if(!$emailCheck || $emailCheck['email_check'] == 'false'){
            $this->splash('failed','',app::get('b2c')->_($this->t('邮箱未验证')),true);
        }
        if((time() - $emailCheck['createtime']) > 60 * 15){
            $this->splash('failed','',app::get('b2c')->_($this->t('验证已超过15分钟,请重新发送邮件验证')),true);
        }
        $url = "/index.php/passport-set_pass.html?p=".b2c_Mcrypt::encode($email);
        $this->splash('success',$url,app::get('b2c')->_($this->t('验证成功')),true);
    }

    /**
     * 修改密码页面
     */
    public function set_pass(){
        $email = $_GET['p'] ? $_GET['p']  : '';
        $this->layout = '';
        $this->render("site/passport/set_pass.php",['email' => $email]);
    }

    /**
     * 修改密码验证
     */
    public function edit_pass(){
        $email = $_POST['p'] ? $_POST['p'] : '';
        $pwd = $_POST['pwd'] ? $_POST['pwd'] : '';
        $rePwd = $_POST['rePwd'] ? $_POST['rePwd'] : '';
        if( !$this->userPassport->check_passport($pwd,$rePwd,$msg) ){
            $this->splash('failed',null,$this->t($msg),true);exit;
        }
        $email = b2c_Mcrypt::decode($email);
        $emailCheck = app::get('b2c')->model('email_pass_check')->getRow('*',array('email' => $email),$orderType='createtime DESC');
        if((time() - $emailCheck['createtime']) > 60 * 15){
            $this->splash('failed','',app::get('b2c')->_($this->t('验证已超过15分钟,请重新发送邮件验证')),true);
        }
        $member_id = $this->userObject->get_member_id_by_username($email);
        if( !$this->userPassport->reset_passport($member_id,$pwd) ){
            $msg = app::get('b2c')->_($this->t('密码重置失败,请重试'));
            $this->splash('failed','',$msg,true);
        }
        kernel::single('pam_lock')->flush_lock($member_id);
        $url = "/index.php/passport-edit_pass_success.html";
        $this->splash('success',$url,app::get('b2c')->_($this->t('修改成功')),true);
    }

    /**
     * 修改成功
     */
    public function edit_pass_success(){
        $this->layout = '';
        $this->render("site/passport/set_pass_success.php");
    }

    public function sendPSW(){
        $username = $_POST['username'];
        $member_id = $this->userObject->get_member_id_by_username($username);

        if(!$member_id){
            $msg = app::get('b2c')->_('该账号不存在，请检查');
            $this->splash('failed',null,$msg,true);
        }

        $pamMemberData = app::get('pam')->model('members')->getList('*',array('member_id'=>$member_id));
        foreach($pamMemberData as $row){
            if($row['login_type'] == 'mobile' && $row['disabled'] == 'false'){
                $data['mobile'] = $row['login_account'];
                $verify['mobile'] = true;
            }

            if($row['login_type'] == 'email' && $row['disabled'] == 'false'){
                $data['email'] = $row['login_account'];
                $verify['email'] = true;
            }
        }

        if($verify['mobile'] || $verify['email']){
            $this->pagedata['send_status'] = 'true';
        }
        $this->pagedata['data'] = $data;

        $this->pagedata['site_sms_valide'] = $this->app->getConf('site.sms_valide');
        $this->display("site/passport/forgot/forgot2.html");
    }


    //发送发送邮件验证码
    public function send_vcode_email()
    {

        $email = $_POST['uname'];
        $type = $_POST['type']; //激活activation

        if( !$type || !$email ){
            $msg = app::get('b2c')->_('请填写正确的邮箱');
            $this->splash('failed',null,$msg,true);
        }

        $login_type = $this->userPassport->get_login_account_type($email);
        if($login_type != 'email'){
            $msg = app::get('b2c')->_('请填写正确的邮箱');
            $this->splash('failed',null,$msg,true);
        }

        if($type == 'reset' && !$this->userPassport->check_signup_account( trim($email),$msg )){
            $this->splash('failed',null,$msg,true);
        }
        //前台邮箱120秒验证
        $model_email_send=$this->app->model('email_send');
        $result=$model_email_send->getRow('send_time',array('email'=>$email));
        if(!empty($result)){
            $time=time()-$result['send_time'];
            if($time<120){
                $time=120-$time;
                $msg = app::get('b2c')->_('不能连续发送邮件,请等待'.$time.'秒');
                $this->splash('failed',null,$msg,true);
            }
        }
        $result=array('email'=>$email,'send_time'=>time());
        $model_email_send->save($result);
        $userVcode = kernel::single('b2c_user_vcode');
        if($email){
            $vcode = $userVcode->set_vcode($email,$type,$msg);
        }
        if($vcode){
            //发送邮箱验证码
            $data['vcode'] = $vcode;
            $data['uname'] = $_POST['uname'];
            if( !$userVcode->send_email($type,(string)$email,$data) ){
                $msg = app::get('b2c')->_('参数错误');
                $this->splash('failed',null,$msg,true);
            }
        }else{
            $this->splash('failed',null,$msg,true);
        }
        $msg = app::get('b2c')->_('邮件已发送');
        $this->splash('success',null,$msg,true);
    }

    //短信发送验证码
    public function send_vcode_sms($token){
        $mobile = $_POST['uname'];
        $type = $_POST['type']; //激活activation

        if($token!=$_SESSION['token_check']){
            $this->splash('failed',null,'验证码发送失败！',true);
        }
        //这里是验证码
        $this->pagedata['site_sms_valide'] = $this->app->getConf('site.sms_valide');
        if($this->pagedata['site_sms_valide'] == 'true')
        {
            $sms_vcode = $_POST['sms_vcode'];
            if( !isset($sms_vcode) || $sms_vcode == null )
            {
                $this->splash('failed',null,'请输入验证码！',true);
            }
            if(!base_vcode::verify('LOGINVCODE',$sms_vcode)){
                $this->splash('failed',null,'验证码输入错误！',true);
            }
        }

        if( !$type || !$mobile ){
            $msg = app::get('b2c')->_('请填写正确的手机号码');
            $this->splash('failed',null,$msg,true);
        }

        $login_type = $this->userPassport->get_login_account_type($mobile);
        if($login_type != 'mobile'){
            $msg = app::get('b2c')->_('请填写正确的手机号码');
            $this->splash('failed',null,$msg,true);
        }

        if($type == 'reset' && !$this->userPassport->check_signup_account( trim($mobile),$msg )){
            $this->splash('failed',null,$msg,true);
        }

        if($type == 'forgot' ){
            $obj_pam_account = app::get('pam')->model('members');
            $mobile_count = $obj_pam_account->count(array('login_account'=>$mobile,'login_type'=>'mobile'));
            if($mobile_count == 0)
            {
                $this->splash('failed',null,app::get('b2c')->_('手机号不存在'),true);
            }
        }

        $userVcode = kernel::single('b2c_user_vcode');
        if($mobile){
            $vcode = $userVcode->set_vcode($mobile,$type,$msg);
        }
        if($vcode){
            //发送验证码 发送短信
            //logger::info('vcode:'.$vcode);
            $data['vcode'] = $vcode;
            if( !$userVcode->send_sms($type,(string)$mobile,$data) ){
                $msg = app::get('b2c')->_('发送失败');
                $this->splash('failed',null,$msg,true);
            }
        }else{
            $this->splash('failed',null,$msg,true);
        }
    }

    public function resetpwd_code(){
        $this->check_login();
        $send_type = $_POST['send_type'];
        $userVcode = kernel::single('b2c_user_vcode');
        if( !$vcodeData = $userVcode->verify($_POST[$send_type.'vcode'],$_POST[$send_type],'forgot')){
            $msg = app::get('b2c')->_('验证码错误');
            $this->splash('failed',null,$msg,true);exit;
        }
        $data['key'] = $userVcode->get_vcode_key($_POST[$send_type],'forgot');
        $data['key'] = md5($vcodeData['vcode'].$data['key']);
        $data['account'] = $_POST[$send_type];
        $this->pagedata['data'] = $data;
        $this->display('site/passport/forgot/forgot3.html');
    }

    public function resetpassword(){
        $this->check_login();
        $userVcode = kernel::single('b2c_user_vcode');
        $vcodeData = $userVcode->get_vcode($_POST['account'],'forgot');
        $key = $userVcode->get_vcode_key($_POST['account'],'forgot');
        if($_POST['account'] !=$vcodeData['account']  || $_POST['key'] != md5($vcodeData['vcode'].$key) ){
            $msg = app::get('b2c')->_('页面已过期,请重新找回密码');
            $this->splash('failed',null,$msg,true);exit;
        }

        if( !$this->userPassport->check_passport($_POST['login_password'],$_POST['psw_confirm'],$msg) ){
            $this->splash('failed',null,$msg,true);exit;
        }

        $member_id = $this->userObject->get_member_id_by_username($_POST['account']);
        if( !$this->userPassport->reset_passport($member_id,$_POST['login_password']) ){
            $msg = app::get('b2c')->_('密码重置失败,请重试');
            $this->splash('failed','back',$msg,$_POST['response_json']);
        }
        kernel::single('pam_lock')->flush_lock($member_id);
        $this->display('site/passport/forgot/forgot4.html');
    }

    /*
     * ajax验证是否登录，前台使用dialog时，如果登录超时，系统会跳转到首页，这样dialog会加载成首页
     * */
    public function ajax_check_login(){
        $url = '';
        $status = false;
        if( $this->userObject->is_login() )
        {
            $status = true;
        }else
        {
            $url = app::get('site')->router()->gen_url(array('app'=>'b2c', 'ctl'=>'site_passport','act'=>'login','full'=>1));
        }
        echo json_encode(array('status'=>$status,'url'=>$url));
    }

    public function error(){
        $this->unset_member();
        $back_url = $this->gen_url(array('app'=>'b2c','ctl'=>'site_passport','act'=>'index'));
        $this->_response->set_redirect($back_url)->send_headers();
    }


    public function logout($url){
        if(!$url){
            $url = array('app'=>'site','ctl'=>'default','act'=>'index','full'=>1);
        }
        $this->unset_member();
        $this->app->model('cart_objects')->setCartNum($arr);
        $this->redirect($url);
    }

    public function unset_member(){
        $auth = pam_auth::instance(pam_account::get_account_type($this->app->app_id));
        foreach(kernel::servicelist('passport') as $k=>$passport){
           $passport->loginout($auth);
        }
        
        $this->app->member_id = 0;
        kernel::single('base_session')->set_cookie_expires(0);
        $this->cookie_path = kernel::base_url().'/';
        $this->set_cookie('MEMBER',null,time()-3600);
        $this->set_cookie('UNAME','',time()-3600);
        $this->set_cookie('MLV','',time()-3600);
        $this->set_cookie('CUR','',time()-3600);
        $this->set_cookie('LANG','',time()-3600);
        $this->set_cookie('S[MEMBER]','',time()-3600);
        $this->set_cookie('S[SIGN][AUTO]',0,time()-3600);
        
        foreach(kernel::servicelist('member_logout') as $service){
            $service->logout();
        }
    }

    public function set_pic(){
        $url = "/public/images/upload/file/";
        $path = dirname(dirname(dirname(dirname(dirname(__FILE__))))).$url;
        $file = $_FILES['file'];
        $data = b2c_helper::uploadImg($file,$path);
        if($data['result'] == 1){
            $res['url'] = $url.$data['name'];
            $this->splash('success','',app::get('b2c')->_('提交成功'),true,$res['url']);
        }
        $this->splash('failed','',app::get('b2c')->_($data['error']),true);
    }
}
