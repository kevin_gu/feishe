<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */
$db['email_pass_check'] = array(
    'columns'=>
    array(
        'id'=>
        array(
            'type'=>'number',
            'extra' => 'auto_increment',
            'pkey'=>true,
            'comment' => app::get('b2c')->_('自增id'),
        ),
        'email'=>
        array(
            'type'=>'varchar(50)',
            'required' => true,
            'comment' => app::get('b2c')->_('邮箱'),
        ),
        'email_check'=>
            array(
                'type'=>'bool',
                'default'=>'false',
            ),
        'createtime'=>
        array(
            'type'=>'int',
            'comment' => app::get('b2c')->_('创建时间'),
        ),
    ),
    'index' =>
    array (
        'idx_ema_time' =>
        array (
            'columns' =>
            array (
                0 => 'email',
                1 => 'createtime',
            ),
        ),
    ),
    'engine' => 'innodb',
    'comment' => app::get('b2c')->_('找回密码邮箱验证表'),
);
