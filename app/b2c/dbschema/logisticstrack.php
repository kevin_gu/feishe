<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */
$db['logisticstrack']=array (
    'columns' =>
        array (
            'id' =>
                array (
                    'type' => 'bigint unsigned',
                    'required' => true,
                    'extra' => 'auto_increment',
                    'pkey' => true,
                    'label' => app::get('b2c')->_('id'),
                    'width' => 110,
                    'editable' => false,
                    'filterdefault' => false,
                    'in_list' => false,
                    'default_in_list' => false,
                ),
            'order_id' =>
                array (
                    'type' => 'bigint unsigned',
                    'default' => 0,
                    'label' => app::get('b2c')->_('订单号'),
                    'editable' => false,
                    'filterdefault' => false,
                    'in_list' => false,
                    'default_in_list' => false,
                ),
            'time' =>
                array (
                    'type' => 'time',
                    'label' => app::get('b2c')->_('时间'),
                    'width' => 110,
                    'editable' => false,
                    'filterdefault' => false,
                    'in_list' => false,
                    'default_in_list' => false,
                ),
            'remark' =>
                array (
                    'type' => 'varchar(200)',
                    'label' => app::get('b2c')->_('说明'),
                    'width' => 75,
                    'editable' => false,
                    'filterdefault' => false,
                    'in_list' => false,
                    'default_in_list' => false,
                ),

        ),
    'index' =>
        array (
            'ind_time' =>
                array (
                    'columns' =>
                        array (
                            0 => 'time',
                        ),
                ),
        ),
    'engine' => 'innodb',
    'version' => '$Rev: 42376 $',
    'comment' => app::get('b2c')->_('订单物流状态表'),
);
