<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */

$db['order_offer'] = array(
    'columns' =>
        array(
            'id' =>
                array(
                    'type' => 'number',
                    'required' => true,
                    'pkey' => true,
                    'extra' => 'auto_increment',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单报价ID'),
                ),
            'order_id' =>
                array(
                    'type' => 'table:orders',
                    'required' => true,
                    'default' => 0,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('父订单ID'),
                ),
            'down_payment' => array(
                'type' => 'money',
                'default' => '0',
                'required' => true,
                'editable' => false,
                'comment' => app::get('b2c')->_('首款'),
            ),
            'final_payment' => array(
                'type' => 'money',
                'default' => '0',
                'required' => true,
                'editable' => false,
                'comment' => app::get('b2c')->_('尾款'),
            ),
            'status' => array(
                'type' => 'tinyint',
                'default' => '0',
                'required' => true,
                'editable' => false,
                'comment' => app::get('b2c')->_('订单状态'),
            ),
            'is_renew' =>
                array(
                    'type' =>
                        array(
                            'yes' => app::get('b2c')->_('是'),
                            'no' => app::get('b2c')->_('否'),
                        ),
                    'default' => 'no',
                    'required' => true,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('是否为重新报价'),
                ),
            'created_at' => array(
                'type' => 'time',
                'editable' => false,
                'in_list' => false,
                'comment' => app::get('b2c')->_('操作时间'),
            ),
            'operator' => array(
                'type' => 'number',
                'editable' => false,
                'in_list' => false,
                'default'=>0,
                'comment' => app::get('b2c')->_('操作人'),
            ),
            'remarks' => array(
                'type' => 'varchar(500)',
                'editable' => false,
                'comment' => app::get('b2c')->_('备注'),
            ),
            'name' =>
                array(
                    'type' => 'varchar(100)',
                    'comment' => app::get('b2c')->_('订单名称'),
                    'editable' => false,
                    'in_list' => false,
                    ),
           'version' =>
                array(
                    'type' => 'number',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单版本'),
                ),
        ),
    'index' =>
        array(
            'ind_order_id' =>
                array(
                    'columns' =>
                        array(
                            0 => 'order_id',
                        ),
                )
        ),
    'engine' => 'innodb',
    'version' => '$Rev: 44813 $',
    'comment' => app::get('b2c')->_('报价单表'),
);
