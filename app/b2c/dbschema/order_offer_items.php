<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */

$db['order_offer_items'] = array(
    'columns' =>
        array(
            'item_id' =>
                array(
                    'type' => 'number',
                    'required' => true,
                    'pkey' => true,
                    'extra' => 'auto_increment',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单明细ID'),
                ),
            'order_id' =>
                array(
                    'type' => 'table:orders',
                    'required' => true,
                    'default' => 0,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单ID'),
                ),
            'offer_id' =>
                array(
                    'type' => 'table:order_offer',
                    'required' => true,
                    'default' => 0,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('报价订单ID'),
                ),
            'product_id' =>
                array(
                    'type' => 'table:products',
                    'required' => true,
                    'default' => 0,
                    'editable' => false,
                    'sdfpath' => 'products/product_id',
                    'comment' => app::get('b2c')->_('货品ID'),
                ),
            'goods_id' =>
                array(
                    'type' => 'table:goods',
                    'required' => true,
                    'default' => 0,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('商品ID'),
                ),
            'type_id' =>
                array(
                    'type' => 'number',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('商品类型ID'),
                ),
            'bn' =>
                array(
                    'type' => 'varchar(40)',
                    'editable' => false,
                    'is_title' => true,
                    'comment' => app::get('b2c')->_('明细商品的品牌名'),
                ),
            'name' =>
                array(
                    'type' => 'varchar(200)',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('明细商品的名称'),
                ),
            'price' =>
                array(
                    'type' => 'money',
                    'default' => '0',
                    'required' => true,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('明细商品的销售价'),
                ),
            'amount' =>
                array(
                    'type' => 'money',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('明细商品总额'),
                ),
            'nums' =>
                array(
                    'type' => 'float',
                    'default' => 1,
                    'required' => true,
                    'editable' => false,
                    'sdfpath' => 'quantity',
                    'comment' => app::get('b2c')->_('明细商品购买数量'),
                ),
            'is_final' =>
                array(
                    'type' =>
                        array(
                            'yes' => app::get('b2c')->_('是'),
                            'no' => app::get('b2c')->_('否'),
                        ),
                    'default' => 'no',
                    'required' => true,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('是否为最后确定商品'),
                ),
        ),
    'index' =>
        array(
            'ind_order_id' =>
                array(
                    'columns' =>
                        array(
                            0 => 'order_id',
                        ),
                ),
            'ind_offer_id' =>
                array(
                    'columns' =>
                        array(
                            0 => 'offer_id',
                        ),
                ),
        ),
    'engine' => 'innodb',
    'version' => '$Rev: 44813 $',
    'comment' => app::get('b2c')->_('报价单明细表'),
);
