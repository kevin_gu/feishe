<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */

$db['order_share'] = array(
    'columns' =>
        array(
            'id' =>
                array(
                    'type' => 'number',
                    'required' => true,
                    'pkey' => true,
                    'extra' => 'auto_increment',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单分享ID'),
                ),
            'owner_id' =>
                array(
                    'type' => 'number',
                    'required' => true,
                    'default' => 0,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单所有者'),
                ),
            'member_id' =>
                array(
                    'type' => 'number',
                    'required' => true,
                    'default' => 0,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('分享所有者'),
                ),
            'order_id' =>
                array(
                    'type' => 'bigint unsigned',
                    'required' => true,
                    'default' => 0,
                    'comment' => app::get('b2c')->_('订单号'),
                ),
            'order_index'=>array(
                'type' => 'number',
                'required' => true,
                'default' => 0,
                'editable' => false,
                'comment' => app::get('b2c')->_('子订单ID'),
            ),
            'created_at' =>
                array(
                    'type' => 'time',
                    'editable' => false,
                    'in_list' => false,
                    'comment' => app::get('b2c')->_('操作时间'),
                ),
        ),
    'index' =>
        array(
            'ind_member_id' =>
                array(
                    'columns' =>
                        array(
                            0 => 'member_id',
                        ),
                ),
            'ind_owner_id' =>
                array(
                    'columns' =>
                        array(
                            0 => 'owner_id',
                        ),
                ),
        ),
    'engine' => 'innodb',
    'version' => '$Rev: 46974 $',
    'comment' => app::get('b2c')->_('订单分享表'),
);

