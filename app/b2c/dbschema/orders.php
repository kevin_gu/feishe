<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */
$db['orders'] = array(
    'columns' =>
        array(
            'order_id' =>
                array(
                    'type' => 'bigint unsigned',
                    'required' => true,
                    'default' => 0,
                    'pkey' => true,
                    'label' => app::get('b2c')->_('订单号'),
                    'is_title' => true,
                    'width' => 110,
                    'searchtype' => 'has',
                    'editable' => false,
                    'filtertype' => 'custom',
                    'filterdefault' => true,
                    'in_list' => true,
                    'default_in_list' => true,
                    'order'=>100
                ),
            'total_amount' =>
                array(
                    'type' => 'money',
                    'default' => '0',
                    'required' => true,
                    'editable' => false,
                    'in_list' => false,
                    'default_in_list' => false,
                    'comment' => app::get('b2c')->_('商品默认货币总值'),
                ),
            'final_amount' =>
                array(
                    'type' => 'money',
                    'default' => '0',
                    'required' => true,
                    'editable' => false,
                    'sdfpath' => 'cur_amount',
                    'label' => app::get('b2c')->_('订单总额'),
                    'width' => 75,
                    'filtertype' => 'number',
                    'filterdefault' => true,
                    'in_list' => false,
                    'default_in_list' => false,
                    'orderby' => false,
                    'comment' => app::get('b2c')->_('订单货币总值, 包含支付价格,税等'),
                ),
            'pay_status' =>
                array(
                    'type' =>
                        array(
                            0 => app::get('b2c')->_('未支付'),
                            1 => app::get('b2c')->_('已支付'),
                            2 => app::get('b2c')->_('已付款至到担保方'),
                            3 => app::get('b2c')->_('部分付款'),
                            4 => app::get('b2c')->_('部分退款'),
                            5 => app::get('b2c')->_('全额退款'),
                            6 => app::get('b2c')->_('退款申请中'),
                        ),
                    'default' => '0',
                    'required' => true,
                    'label' => app::get('b2c')->_('付款状态'),
                    'width' => 75,
                    'editable' => false,
                    'filtertype' => 'yes',
                    'filterdefault' => true,
                    'in_list' => false,
                    'default_in_list' => false,
                ),
            'ship_status' =>
                array(
                    'type' =>
                        array(
                            0 => app::get('b2c')->_('未发货'),
                            1 => app::get('b2c')->_('已发货'),
                            2 => app::get('b2c')->_('部分发货'),
                            3 => app::get('b2c')->_('部分退货'),
                            4 => app::get('b2c')->_('已退货'),
                        ),
                    'default' => '0',
                    'required' => true,
                    'label' => app::get('b2c')->_('发货状态'),
                    'width' => 75,
                    'editable' => false,
                    'filtertype' => 'yes',
                    'filterdefault' => true,
                    'in_list' => false,
                    'default_in_list' => false,
                ),
            'received_status' =>
                array(
                    'type' =>
                        array(
                            0 => app::get('b2c')->_('未收货'),
                            1 => app::get('b2c')->_('已收货'),
                        ),
                    'default' => '0',
                    'required' => true,
                    'label' => app::get('b2c')->_('收货状态'),
                    'width' => 75,
                    'editable' => false,
                    'filtertype' => 'yes',
                    'filterdefault' => true,
                    'in_list' => false,
                    'default_in_list' => false,
                ),
            'return_status' =>
                array(
                    'type' => 'tinyint',
                    'default' => '0',
                    'editable' => false,
                    'in_list' => false,
                    'default_in_list' => false,
                    'comment' => app::get('b2c')->_('分成状态：0-待返佣,1-已返佣,2-不返佣'),
                ),
            'percentage_status' =>
                array(
                    'type' => 'tinyint',
                    'default' => '0',
                    'editable' => false,
                    'in_list' => false,
                    'default_in_list' => false,
                    'comment' => app::get('b2c')->_('分成状态：0-待分成,1-已分成,2-不分成'),
                ),
            'is_delivery' =>
                array(
                    'type' => 'tinybool',
                    'default' => 'Y',
                    'required' => true,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('是否需要发货'),
                ),
            'createtime' =>
                array(
                    'type' => 'time',
                    'label' => app::get('b2c')->_('下单时间'),
                    'width' => 110,
                    'editable' => false,
                    'filtertype' => 'time',
                    'filterdefault' => true,
                    'in_list' => true,
                    'default_in_list' => true,
                    'orderby' => true,
                    'order'=>101,
                ),
            'received_time' =>
                array(
                    'type' => 'time',
                    'label' => app::get('b2c')->_('收货时间'),
                    'width' => 110,
                    'editable' => false,
                    'filtertype' => 'time',
                    'filterdefault' => true,
                    'in_list' => false,
                    'default_in_list' => false,
                    'orderby' => false,
                ),
            'last_modified' =>
                array(
                    'label' => app::get('b2c')->_('最后更新时间'),
                    'type' => 'last_modify',
                    'width' => 110,
                    'editable' => false,
                    'in_list' => false,
                    'default_in_list' => false,
                ),
            'payment' =>
                array(
                    'type' => 'varchar(100)',
                    'label' => app::get('b2c')->_('支付方式'),
                    'width' => 75,
                    'editable' => false,
                    'filtertype' => 'yes',
                    'filterdefault' => true,
                    'sdfpath' => 'payinfo/pay_app_id',
                    'in_list' => false,
                    'default_in_list' => false,
                ),
            'shipping_id' =>
                array(
                    'type' => 'table:dlytype',
                    'label' => app::get('b2c')->_('配送方式'),
                    'filtertype' => 'yes',
                    'filterdefault' => true,
                    'editable' => false,
                    'sdfpath' => 'shipping/shipping_id',
                    'in_list' => false,
                ),

            'shipping' =>
                array(
                    'type' => 'varchar(100)',
                    'label' => app::get('b2c')->_('配送方式'),
                    'width' => 75,
                    'editable' => false,
                    'sdfpath' => 'shipping/shipping_name',
                    'in_list' => false,
                ),
            'member_id' =>
                array(
                    'type' => 'table:members',
                    'label' => app::get('b2c')->_('会员用户名'),
                    'width' => 75,
                    'editable' => false,
                    'filtertype' => 'yes',
                    'filterdefault' => true,
                    'in_list' => true,
                    'default_in_list' => true,
                    'order'=>100
                ),
            'promotion_type' =>
                array(
                    'type' =>
                        array(
                            'normal' => app::get('b2c')->_('普通订单'),
                            'prepare' => app::get('b2c')->_('预售订单'),
                        ),
                    'sdfpath' => 'promotion_type',
                    'default' => 'normal',
                    'required' => true,
                    'label' => app::get('b2c')->_('销售类型'),
                    'width' => 110,
                    'editable' => false,
                    'hidden' => true,
                    'in_list' => false,
                ),
            'status' =>
                array(
                    'type' =>
                        array(
                            'active' => app::get('b2c')->_('活动订单'),
                            'dead' => app::get('b2c')->_('已作废'),
                            'finish' => app::get('b2c')->_('已完成'),
                        ),
                    'default' => 'active',
                    'required' => true,
                    'label' => app::get('b2c')->_('订单状态'),
                    'width' => 75,
                    'hidden' => true,
                    'editable' => false,
                    'in_list' => false,

                ),
            'confirm' =>
                array(
                    'type' => 'tinybool',
                    'default' => 'N',
                    'required' => true,
                    'label' => app::get('b2c')->_('确认状态'),
                    'width' => 75,
                    'hidden' => true,
                    'editable' => false,
                    'in_list' => false,
                ),
            'ship_area' =>
                array(
                    'type' => 'region',
                    'label' => app::get('b2c')->_('收货地区'),
                    'width' => 180,
                    'editable' => false,
                    'filtertype' => 'yes',
                    'sdfpath' => 'consignee/area',
                    'in_list' => false,
                    'default_in_list' => false,
                ),
            'ship_name' =>
                array(
                    'type' => 'varchar(50)',
                    'label' => app::get('b2c')->_('收货人'),
                    'sdfpath' => 'consignee/name',
                    'width' => 75,
                    'searchtype' => 'head',
                    'editable' => false,
                    'filtertype' => 'normal',
                    'filterdefault' => true,
                    'in_list' => true,
                    'default_in_list' => true,
                    'order'=>103
                ),

            'weight' =>
                array(
                    'type' => 'money',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单总重量'),
                ),
            'tostr' =>
                array(
                    'type' => 'longtext',
                    'editable' => false,
                    'sdfpath' => 'title',
                    'comment' => app::get('b2c')->_('订单文字描述'),
                ),
            'itemnum' =>
                array(
                    'type' => 'number',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单子订单数量'),
                ),

            'ip' =>
                array(
                    'type' => 'varchar(15)',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('IP地址'),
                    'label' => app::get('b2c')->_('IP地址'),
                    'in_list' => true,
                ),

            'ship_addr' =>
                array(
                    'type' => 'text',
                    'label' => app::get('b2c')->_('收货地址'),
                    'searchtype' => 'has',
                    'width' => 180,
                    'editable' => false,
                    'filtertype' => 'normal',
                    'sdfpath' => 'consignee/addr',
                    'in_list' => true,
                    'default_in_list' => true,
                    'comment' => app::get('b2c')->_('收货地址'),
                    'order'=>102
                ),
            'ship_zip' =>
                array(
                    'type' => 'varchar(20)',
                    'editable' => false,
                    'sdfpath' => 'consignee/zip',
                    'comment' => app::get('b2c')->_('收货人邮编'),
                ),
            'ship_tel' =>
                array(
                    'type' => 'varchar(50)',
                    'label' => app::get('b2c')->_('收货人电话'),
                    'searchtype' => 'has',
                    'width' => 75,
                    'editable' => false,
                    'filtertype' => 'normal',
                    'filterdefault' => true,
                    'sdfpath' => 'consignee/telephone',
                    'in_list' => false,
                    'default_in_list' => false,
                    'comment' => app::get('b2c')->_('收货电话'),

                ),
            'ship_email' =>
                array(
                    'type' => 'varchar(200)',
                    'editable' => false,
                    'sdfpath' => 'consignee/email',
                    'comment' => app::get('b2c')->_('收货人email'),
                ),
            'ship_time' =>
                array(
                    'type' => 'varchar(50)',
                    'editable' => false,
                    'sdfpath' => 'consignee/r_time',
                    'comment' => app::get('b2c')->_('配送时间'),
                ),
            'ship_mobile' =>
                array(
                    'label' => app::get('b2c')->_('收货人手机'),
                    'hidden' => true,
                    'searchtype' => 'has',
                    'type' => 'varchar(50)',
                    'editable' => false,
                    'sdfpath' => 'consignee/mobile',
                    'in_list' => true,
                    'default_in_list' => true,
                    'comment' => app::get('b2c')->_('收货人手机'),
                    'order'=>104
                ),
            'cost_item' =>
                array(
                    'type' => 'money',
                    'default' => '0',
                    'required' => true,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单商品总价格'),
                ),
            'is_tax' =>
                array(
                    'label' => app::get('b2c')->_('是否需要发票'),
                    'type' => 'bool',
                    'default' => 'false',
                    'required' => true,
                    'editable' => false,
                    'in_list' => true,
                    'filtertype' => 'yes',
                    'default_in_list' => true,
                    'comment' => app::get('b2c')->_('是否要开发票'),
                    'order'=>106,
                ),
            'tax_type' =>
                array(
                    'type' => array(
                        'false' => app::get('b2c')->_('不需发票'),
                        'personal' => app::get('b2c')->_('个人发票'),
                        'company' => app::get('b2c')->_('公司发票'),
                    ),
                    'default' => 'false',
                    'required' => true,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('发票类型'),
                ),
            'tax_content' =>
                array(
                    'type' => 'varchar(255)',
                    'editable' => false,
                    'label' => app::get('b2c')->_('发票内容'),
                ),
            'cost_tax' =>
                array(
                    'type' => 'money',
                    'default' => '0',
                    'required' => true,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单税率'),
                ),
            'tax_company' =>
                array(
                    'type' => 'varchar(255)',
                    'editable' => false,
                    'sdfpath' => 'tax_title',
                    'comment' => app::get('b2c')->_('发票抬头'),
                ),
            'tax_code' =>
                array(
                    'type' => 'varchar(255)',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('税号'),
                ),
            'tax_bank' =>
                array(
                    'type' => 'varchar(255)',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('开户行'),
                ),
            'bank_account' =>
                array(
                    'type' => 'varchar(255)',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('银行账号'),
                ),
            'tax_addr' =>
                array(
                    'type' => 'varchar(255)',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('开票地址'),
                ),
            'tax_remark' =>
                array(
                    'type' => 'varchar(255)',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('备注信息'),
                ),
            'is_protect' =>
                array(
                    'type' => 'bool',
                    'default' => 'false',
                    'required' => true,
                    'editable' => false,
                    'sdfpath' => 'shipping/is_protect',
                    'comment' => app::get('b2c')->_('是否还有保价费'),
                ),
            'cost_protect' =>
                array(
                    'type' => 'money',
                    'default' => '0',
                    'required' => true,
                    'editable' => false,
                    'sdfpath' => 'shipping/cost_protect',
                    'comment' => app::get('b2c')->_('保价费'),
                ),
            'cost_payment' =>
                array(
                    'type' => 'money',
                    'editable' => false,
                    'sdfpath' => 'payinfo/cost_payment',
                    'comment' => app::get('b2c')->_('支付费用'),
                ),
            'currency' =>
                array(
                    'type' => 'varchar(8)',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单支付货币'),
                ),
            'cur_rate' =>
                array(
                    'type' => 'decimal(10,4)',
                    'default' => '1.0000',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单支付货币汇率'),
                ),
            'score_u' =>
                array(
                    'type' => 'money',
                    'default' => '0',
                    'required' => true,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单使用积分'),
                ),
            'score_g' =>
                array(
                    'type' => 'money',
                    'default' => '0',
                    'required' => true,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单获得积分'),
                ),
            'discount' =>
                array(
                    'type' => 'money',
                    'default' => '0',
                    'required' => true,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单减免'),
                ),
            'pmt_goods' =>
                array(
                    'type' => 'money',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('商品促销优惠'),
                ),
            'pmt_order' =>
                array(
                    'type' => 'money',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单促销优惠'),
                ),
            'payed' =>
                array(
                    'type' => 'money',
                    'default' => '0',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单支付金额'),
                ),
            'memo' =>
                array(
                    'type' => 'longtext',
                    'editable' => false,
                    'label' => app::get('b2c')->_('订单附言'),
                    'comment' => app::get('b2c')->_('订单附言'),
                    'in_list' => true,
                ),
            'disabled' =>
                array(
                    'type' => 'bool',
                    'default' => 'false',
                    'editable' => false,
                ),
            'displayonsite' =>
                array(
                    'type' => 'bool',
                    'default' => 'true',
                    'editalbe' => false,
                ),
            'mark_type' =>
                array(
                    'type' => 'varchar(3)',
                    'default' => 'b10',
                    'required' => true,
                    'label' => app::get('b2c')->_('订单备注图标'),
                    'hidden' => true,
                    'width' => 150,
                    'editable' => false,
                    'in_list' => true,
                ),
            'mark_text' =>
                array(
                    'type' => 'longtext',
                    'label' => app::get('b2c')->_('订单备注'),
                    'width' => 50,
                    'editable' => false,
                    //'searchtype' => 'has',
                    'filtertype' => 'normal',
                    'in_list' => true,
                ),
            'cost_freight' =>
                array(
                    'type' => 'money',
                    'default' => '0',
                    'required' => true,
                    'label' => app::get('b2c')->_('配送费用'),
                    'width' => 75,
                    'editable' => false,
                    'filtertype' => 'number',
                    'sdfpath' => 'shipping/cost_shipping',
                    'in_list' => true,
                ),
            'extend' =>
                array(
                    'type' => 'varchar(255)',
                    'default' => 'false',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单扩展'),
                ),
            'order_refer' =>
                array(
                    'type' => 'varchar(20)',
                    'required' => true,
                    'default' => 'local',
                    'hidden' => true,
                    'comment' => app::get('b2c')->_('订单来源'),
                ),
            'addon' =>
                array(
                    'type' => 'longtext',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('订单附属信息(序列化)'),
                ),
            'source' =>
                array(
                    'type' => array(
                        'pc' => app::get('b2c')->_('标准平台'),
                        'wap' => app::get('b2c')->_('手机触屏'),
                        'weixin' => app::get('b2c')->_('微信商城'),
                        'penker' => app::get('b2c')->_('朋客'),
                    ),
                    'required' => false,
                    'label' => app::get('b2c')->_('平台来源'),
                    'width' => 110,
                    'editable' => false,
                    'default' => 'pc',
                    'in_list' => true,
                    'default_in_list' => false,
                    'filterdefault' => false,
                    'filtertype' => 'yes',
                ),
            'is_oversold' =>
                array(
                    'type' => 'bool',
                    'default' => 'false',
                    'label' => app::get('b2c')->_('是否超卖'),
                    'width' => 80,
                    'editable' => false,
                    'in_list' => true,
                ),


            'offer_count' =>
                array(
                    'type' => 'number',
                    'default' => 0,
                    'required' => true,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('报价单数量'),
                ),
            'tax_ship_addr' => array(
                'type' => 'text',
                'label' => app::get('b2c')->_('发票收货地址'),
                'searchtype' => 'has',
                'width' => 180,
                'editable' => false,
                'filtertype' => 'normal',
                'in_list' => false,
                'default_in_list' => false,
                'comment' => app::get('b2c')->_('发票收货地址'),
            ),
            'tax_company_tel' =>
                array(
                    'type' => 'varchar(255)',
                    'editable' => false,
                    'sdfpath' => 'tax_title',
                    'comment' => app::get('b2c')->_('公司电话'),
                ),
            'tax_invoice_type' =>
                array(
                    'type' => array(
                        'normal' => app::get('b2c')->_('普通发票'),
                        'added' => app::get('b2c')->_('增值税发票'),
                    ),
                    'default' => 'normal',
                    'required' => true,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('发票类型'),
                ),
            'admin_id' => array(
                'type' => 'number',
                'editable' => false,
                'in_list' => false,
                'default' => 0,
                'comment' => app::get('b2c')->_('分配的销售账号'),
            ),
            'assign_uid' => array(
                'type' => 'number',
                'editable' => false,
                'in_list' => false,
                'default' => 0,
                'comment' => app::get('b2c')->_('分配人'),
            ),
            'assign_at' => array(
                'type' => 'time',
                'editable' => false,
                'in_list' => false,
                'default' => 0,
                'comment' => app::get('b2c')->_('分配时间'),
            ),
            'order_status' => array(
                'type' => 'tinyint',
                'default' => '0',
                'required' => true,
                'editable' => false,
                'comment' => app::get('b2c')->_('总订单状态'),
                'in_list'=>true,
            ),
            'order_current_status' => array(
                'type' => 'tinyint',
                'default' => '0',
                'required' => true,
                'editable' => false,
                'comment' => app::get('b2c')->_('订单状当前态'),
                'in_list'=>true,
            ),
            'confirm_status' => array(
                'type' => 'tinyint',
                'default' => '0',
                'required' => true,
                'editable' => false,
                'comment' => app::get('b2c')->_('确认状态'),
            ),
            'confirm_at'=>array(
                'type' => 'time',
                'editable' => false,
                'in_list' => false,
                'comment' => app::get('b2c')->_('确认时间'),
            ),
            'deposit' =>
                array(
                    'type' => 'money',
                    'default' => '0',
                    'required' => true,
                    'label' => app::get('b2c')->_('保证金'),
                    'width' => 75,
                    'editable' => false,
                    'filtertype' => 'number',
                    'in_list' => true,
                ),
            'deposit_at' => array(
                'type' => 'time',
                'editable' => false,
                'in_list' => false,
                'comment' => app::get('b2c')->_('保证金支付时间'),
            ),
            'deposit_img' => array(
                'type' => 'varchar(500)',
                'editable' => false,
                'comment' => app::get('b2c')->_('保证金支付凭证'),
            ),
            'deposit_status'=>array(
                'type' => 'tinyint',
                'default' => '0',
                'required' => true,
                'editable' => false,
                'comment' => app::get('b2c')->_('保证金支付状态'),
            ),
            'deposit_expire_at' => array(
                'type' => 'time',
                'editable' => false,
                'in_list' => false,
                'comment' => app::get('b2c')->_('保证金支付超时时间'),
            ),
            'down_payment_at' => array(
                'type' => 'time',
                'editable' => false,
                'in_list' => false,
                'comment' => app::get('b2c')->_('首款支付时间'),
            ),
            'down_payment_img' => array(
                'type' => 'varchar(500)',
                'editable' => false,
                'comment' => app::get('b2c')->_('首款支付凭证'),
            ),
            'down_payment_status'=>array(
                'type' => 'tinyint',
                'default' => '0',
                'required' => true,
                'editable' => false,
                'comment' => app::get('b2c')->_('首款支付状态'),
            ),
            'down_payment_expire_at' => array(
                'type' => 'time',
                'editable' => false,
                'in_list' => false,
                'comment' => app::get('b2c')->_('首款支付超时时间'),
            ),
            'final_payment_at' => array(
                'type' => 'time',
                'editable' => false,
                'in_list' => false,
                'comment' => app::get('b2c')->_('尾款支付时间'),
            ),
            'final_payment_img' => array(
                'type' => 'varchar(2000)',
                'editable' => false,
                'comment' => app::get('b2c')->_('尾款支付凭证'),
            ),
            'final_payment_status'=>array(
                'type' => 'tinyint',
                'default' => '0',
                'required' => true,
                'editable' => false,
                'comment' => app::get('b2c')->_('尾款支付状态'),
            ),
            'final_payment_expire_at' => array(
                'type' => 'time',
                'editable' => false,
                'in_list' => false,
                'comment' => app::get('b2c')->_('尾款支付超时时间'),
            ),
            'product_begin_at'=>array(
                'type' => 'time',
                'editable' => false,
                'in_list' => false,
                'comment' => app::get('b2c')->_('开始生产时间'),
            ),
            'product_end_at'=>array(
                'type' => 'time',
                'editable' => false,
                'in_list' => false,
                'comment' => app::get('b2c')->_('生产完成时间'),
            ),
            'shipping_at'=>array(
                'type' => 'time',
                'editable' => false,
                'in_list' => false,
                'comment' => app::get('b2c')->_('发货时间'),
            ),
            'finish_at'=>array(
                'type' => 'time',
                'editable' => false,
                'in_list' => false,
                'comment' => app::get('b2c')->_('确认收货时间'),
            ),
            'after_sale_status'=>array(
                'type' => 'tinyint',
                'default' => '0',
                'required' => true,
                'editable' => false,
                'comment' => app::get('b2c')->_('是否已经申请过售后'),
            ),
            'percentage_member' =>
                array(
                    'type' =>'number',
                    'default' => '0',
                    'label' => app::get('b2c')->_('分成上线'),
                    'editable' => false,
                    'in_list' => false,
                    'default_in_list' => false,
                ),
        ),
    'index' =>
        array(
            'ind_ship_status' =>
                array(
                    'columns' =>
                        array(
                            0 => 'ship_status',
                        ),
                ),
            'ind_pay_status' =>
                array(
                    'columns' =>
                        array(
                            0 => 'pay_status',
                        ),
                ),
            'ind_promotion_type' =>
                array(
                    'columns' =>
                        array(
                            0 => 'promotion_type',
                        ),
                ),
            'ind_status' =>
                array(
                    'columns' =>
                        array(
                            0 => 'status',
                        ),
                ),
            'ind_mid_rsta' =>
                array(
                    'columns' =>
                        array(
                            0 => 'member_id',
                            1 => 'return_status',
                        ),
                ),
            'ind_mid_psta' =>
                array(
                    'columns' =>
                        array(
                            0 => 'member_id',
                            1 => 'percentage_status',
                        ),
                ),
            'ind_disabled' =>
                array(
                    'columns' =>
                        array(
                            0 => 'disabled',
                        ),
                ),
            'ind_last_modified' =>
                array(
                    'columns' =>
                        array(
                            0 => 'last_modified',
                        ),
                ),
            'ind_createtime' =>
                array(
                    'columns' =>
                        array(
                            0 => 'createtime',
                        ),
                ),
            'idx_order_refer' =>
                array(
                    'columns' =>
                        array(
                            0 => 'order_refer',
                        ),
                ),
            'ind_member_id' =>
                array(
                    'columns' =>
                        array(
                            0 => 'member_id',
                        ),
                ),
            'idx_admin_id' =>
                array(
                    'columns' =>
                        array(
                            0 => 'admin_id',
                        ),
                ),
            'idx_percentage_member' =>
                array(
                    'columns' =>
                        array(
                            0 => 'percentage_member',
                        ),
                ),
        ),
    'engine' => 'innodb',
    'version' => '$Rev: 42376 $',
    'comment' => app::get('b2c')->_('订单主表'),
);

