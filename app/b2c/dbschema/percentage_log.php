<?php
/**
* @table sell_logs;
*
* @package Schemas
* @version $
* @copyright 2003-2009 ShopEx
* @license Commercial
*/

$db['percentage_log']=array (
  'columns' =>
  array (
    'id' =>
        array (
          'type' => 'number',
          'required' => true,
          'pkey' => true,
          'extra' => 'auto_increment',
          'editable' => false,
          'comment' => app::get('b2c')->_('自增id'),
        ),
    'member_id' =>
        array (
          'type' => 'table:members',
          'default' => 0,
          'required' => true,
          'editable' => false,
          'comment' => app::get('b2c')->_('会员ID'),
        ),
    'order_id' =>
        array (
          'type' => 'bigint unsigned',
          'default' => '0.00',
          'required' => true,
          'editable' => false,
          'comment' => app::get('b2c')->_('订单号'),
        ),
    'percentage_money' =>
        array (
          'type' => 'decimal(12,2)',
          'default' => 0,
          'editable' => false,
          'comment' => app::get('b2c')->_('分成金额'),
        ),
     'percentage_status' =>
         array(
          'type' => 'tinyint',
          'default' => 0,
          'required' => false,
          'editable' => false,
          'comment' => app::get('b2c')->_('分成状态:0-不分成,1-已分成'),
     ),
    'createtime' =>
        array (
          'type' => 'int',
          'editable' => false,
          'comment' => app::get('b2c')->_('记录创建时间'),
        ),
  ),
  'index' =>
  array (
    'ind_mid_oid' =>
    array (
      'columns' =>
      array (
        0 => 'member_id',
        1 => 'order_id',
      ),
    ),
  ),
  'comment' => app::get('b2c')->_('返佣记录表'),
);
