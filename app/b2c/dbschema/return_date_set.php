<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */

$db['return_date_set']=array (
  'columns' =>
  array (
      'id' =>
          array (
              'type' => 'int',
              'extra' => 'auto_increment',
              'pkey' => true,
              'label' => app::get('b2c')->_('id'),
          ),
    'date_nums' =>
        array (
          'type' => 'int',
          'label' => app::get('b2c')->_('期限'),
          'comment' => app::get('b2c')->_('返佣分成期限'),
        ),
  ),

  'comment' => app::get('b2c')->_('返佣分成期限'),
  'engine' => 'innodb',
  'version' => '$Rev:  $',
);
