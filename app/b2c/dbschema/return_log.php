<?php
/**
* @table sell_logs;
*
* @package Schemas
* @version $
* @copyright 2003-2009 ShopEx
* @license Commercial
*/

$db['return_log']=array (
  'columns' =>
  array (
    'id' =>
        array (
          'type' => 'number',
          'required' => true,
          'pkey' => true,
          'extra' => 'auto_increment',
          'editable' => false,
          'comment' => app::get('b2c')->_('自增id'),
        ),
    'member_id' =>
        array (
          'type' => 'table:members',
          'default' => 0,
          'required' => true,
          'editable' => false,
          'comment' => app::get('b2c')->_('会员ID'),
        ),
    'order_id' =>
        array (
          'type' => 'bigint unsigned',
          'default' => '0.00',
          'required' => true,
          'editable' => false,
          'comment' => app::get('b2c')->_('订单号'),
        ),
    'return_money' =>
        array (
          'type' => 'decimal(12,2)',
          'default' => 0,
          'editable' => false,
          'comment' => app::get('b2c')->_('返佣金额'),
        ),
     'return_status' =>
         array(
          'type' => 'int',
          'default' => 0,
          'required' => false,
          'editable' => false,
          'comment' => app::get('b2c')->_('返佣状态:0-不返佣,1-已返佣'),
     ),
    'createtime' =>
        array (
          'type' => 'int',
          'editable' => false,
          'comment' => app::get('b2c')->_('记录创建时间'),
        ),
  ),
  'index' =>
  array (
    'ind_mid_oid' =>
    array (
      'columns' =>
      array (
        0 => 'member_id',
        1 => 'order_id',
      ),
    ),
  ),
  'comment' => app::get('b2c')->_('返佣记录表'),
);
