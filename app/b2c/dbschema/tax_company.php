<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */
$db['tax_company'] = array(
    'columns' =>
        array(
            'id' =>
                array(
                    'type' => 'number',
                    'required' => true,
                    'pkey' => true,
                    'extra' => 'auto_increment',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('ID'),
                ),
            'member_id' =>
                array(
                    'type' => 'table:members',
                    'label' => app::get('b2c')->_('会员用户名'),
                    'width' => 75,
                    'editable' => false,
                    'filtertype' => 'yes',
                    'filterdefault' => true,
                    'in_list' => true,
                    'default_in_list' => true,
                ),
            'tax_company' =>
                array(
                    'type' => 'varchar(255)',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('发票抬头'),
                ),
            'tax_code' =>
                array(
                    'type' => 'varchar(255)',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('税号'),
                ),
            'tax_bank' =>
                array(
                    'type' => 'varchar(255)',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('开户行'),
                ),
            'bank_account' =>
                array(
                    'type' => 'varchar(255)',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('银行账号'),
                ),
            'tax_addr' =>
                array(
                    'type' => 'varchar(255)',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('开票地址'),
                ),
            'tax_company_tel' =>
                array(
                    'type' => 'varchar(255)',
                    'editable' => false,
                    'comment' => app::get('b2c')->_('公司电话'),
                ),
            'created_at'=>array(
                'type' => 'time',
                'editable' => false,
                'in_list' => false,
                'comment' => app::get('b2c')->_('创建时间'),
            ),
            'updated_at'=>array(
                'type' => 'time',
                'editable' => false,
                'in_list' => false,
                'comment' => app::get('b2c')->_('修改时间'),
            ),
            'tax_invoice_type' =>
                array(
                    'type' => array(
                        'normal' => app::get('b2c')->_('普通发票'),
                        'added' => app::get('b2c')->_('增值税发票'),
                    ),
                    'default' => 'normal',
                    'required' => true,
                    'editable' => false,
                    'comment' => app::get('b2c')->_('发票类型'),
                ),
        ),
    'index' =>
        array(

            'ind_member_id' =>
                array(
                    'columns' =>
                        array(
                            0 => 'member_id',
                        ),
                ),
        ),
    'engine' => 'innodb',
    'version' => '$Rev: 42376 $',
    'comment' => app::get('b2c')->_('发票公司表'),
);

