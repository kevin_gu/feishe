<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */

class b2c_finder_members{
    var $detail_basic;
    var $detail_edit;
    var $detail_experience;
    var $detail_point;
    var $detail_order;
    var $detail_recommend;
    var $detail_return_order;
    var $detail_percentage_order;
    var $detail_msg;
    var $detail_remark;
    var $detail_coupon;
    var $column_editbutton;
    var $pagelimit = 10;

    public function __construct($app,$member_id)
    {
        $this->app = $app;
        $this->controller = app::get('b2c')->controller('admin_member');

        $this->detail_basic = app::get('b2c')->_('会员信息');
        $this->detail_edit = app::get('b2c')->_('编辑会员');
        $this->detail_experience = app::get('b2c')->_('经验值');
        $this->detail_point = app::get('b2c')->_('积分');
        $this->detail_order = app::get('b2c')->_('订单');
        $this->detail_recommend = app::get('b2c')->_('下线列表');
        $this->detail_msg = app::get('b2c')->_('站内信');
        $this->detail_remark = app::get('b2c')->_('会员备注');
        $this->detail_coupon = app::get('b2c')->_('优惠券');
        $this->detail_return_order = app::get('b2c')->_('返佣订单');
        $this->detail_percentage_order = app::get('b2c')->_('分成订单');
        $this->column_editbutton = app::get('b2c')->_('操作');
        $this->column_email = app::get('b2c')->_('EMAIL');
        $this->userObject = kernel::single('b2c_user_object');
    }


    var $column_edit = '白名单';
    var$column_edit_order = 10;
    var $column_edit_width = 50;
    public function column_edit($member)
    {
        $act = '';
        $action = $_GET['act'];
        $member = $this->app->model('members')->dump(['member_id' => $member['member_id']]);
        if($action == 'check_pending_list'){
            if($member['check_status'] != b2c_mdl_members::CHECK_STATUS_YES){
                $act = '未审核';
            }
            /*if($member['member_type'] > 0 && $member['check_status'] >0 && $member['is_white'] == 0){
                $act .= '<a href="index.php?app=b2c&ctl=admin_member&act=set_white&member_id='.$member_id.'"  target="dialog::{title:\''.app::get('b2c')->_('白名单').'\', width:360, height:100}">'.app::get('b2c')->_('白名单').'</a>';
            }*/
            if($member['member_type'] > 0 && $member['check_status'] > 1 && $member['is_white']){
                $act= '未通过';
            }
        }else{
           if($member['is_white'] == b2c_mdl_members::IS_WHITE && $member['member_type'] > 0 && $member['check_status'] == '1'){
               $act .= 'A';
           }else if(!$member['is_white'] && $member['member_type'] > 0 && $member['check_status'] == '1'){
               $act .= 'B';
           }else if($member['check_status'] != '1' && $member['member_type'] > 0){
               $act .= '待审核';
           }else{
               $act .= '';
           }
        }
        return $act;
    }

    var $column_edit_check = '审核';
    var $column_edit_check_order = 11;
    var $column_edit_check_width = 70;
    public function column_edit_check($member)
    {
        $act = '';
        $action = $_GET['act'];
        $member = $this->app->model('members')->dump(['member_id' => $member['member_id']]);
        if($action == 'check_pending_list'){
           if($member['member_type'] > 0 && $member['check_status'] != '1'){
               $act .= '<a href="index.php?app=b2c&ctl=admin_member&act=check_page&member_id='.$member['member_id'].'"  target="dialog::{title:\''.app::get('b2c')->_('审核').'\', width:360, height:200}">'.app::get('b2c')->_('审核').'</a>';
           }
        }else if($action == 'group_member_list'){
            $act .= '已通过';
        }else{
            $act .= '无权限';
        }
        return $act;
    }

    var $column_edit_return = '返佣比例';
    var $column_edit_return_order = 12;
    var $column_edit_return_width = 70;
    public function column_edit_return($member)
    {
        $act = '';
        $action = $_GET['act'];
        $member = $this->app->model('members')->dump(['member_id' => $member['member_id']]);
        if($action == 'group_member_list'){
            if($member['member_type'] > 0 && $member['is_white'] && $member['check_status'] == '1'){
                $str = $member['scale'] ? $member['scale'].'%': '返佣比例';
                $act .= '<a href="index.php?app=b2c&ctl=admin_member&act=return_set&member_id='.$member['member_id'].'"  target="dialog::{title:\''.app::get('b2c')->_('返佣比例').'\', width:360, height:120}">'.$str.'</a>';
            }
        }else{
            $act .= '无权限';
        }
        return $act;
    }

    var $column_edit_percentage = '分成比例';
    var $column_edit_percentage_order = 13;
    var $column_edit_percentage_width = 70;
    public function column_edit_percentage($member)
    {
        $act = '';
        $action = $_GET['act'];
        $member = $this->app->model('members')->dump(['member_id' => $member['member_id']]);
        if($action == 'retail_member_list'){
            if($member['f_level'] > 0 && $member['f_level'] != '3'){
                $str = $member['percentage'] ? $member['percentage'].'%': '分成比例';
                $act .= '<a href="index.php?app=b2c&ctl=admin_member&act=percentage_set&member_id='.$member['member_id'].'"  target="dialog::{title:\''.app::get('b2c')->_('分成比例').'\', width:360, height:120}">'.$str.'</a>';
            }
        }else{
            $act .= '无权限';
        }
        return $act;
    }

    var $column_edit_bind = '绑定下线';
    var $column_edit_bind_order = 14;
    var $column_edit_bind_width = 70;
    public function column_edit_bind($member)
    {
        $act = '';
        $action = $_GET['act'];
        $member = $this->app->model('members')->dump(['member_id' => $member['member_id']]);
        if($action == 'retail_member_list' || $action == 'group_member_list') {
            if($member['is_white'] || ($member['f_level'] >0 && $member['f_level'] != '3')){
                $act .= '<a href="index.php?app=b2c&ctl=admin_member&act=bind_page&member_id=' . $member['member_id'] . '"  target="dialog::{title:\'' . app::get('b2c')->_('绑定下线') . '\', width:360, height:120}">' . app::get('b2c')->_('绑定下线') . '</a>';
            }else{
                $act .= '无权限';
            }

        }
        return $act;
    }

    var $column_member_type = '会员类型';
    var $column_member_type_order = 15;
    var $column_member_type_width = 80;
    public function column_member_type($member){
        $member = $this->app->model('members')->dump(['member_id' => $member['member_id']]);
        if(!$member['member_type']){
            return  '普通会员';
        }elseif ($member['member_type'] > 1){
            return '国外企业会员';
        }else{
            return '国内企业会员';
        }
    }

    var $column_level = '分销等级';
    var $column_level_order = 16;
    var $column_level_width = 80;
    public function column_level($member){
        $act = '';
        $member = $this->app->model('members')->dump(['member_id' => $member['member_id']]);
        if($member['f_level'] == '1'){
            $act .= '一级';
        }
        if($member['f_level'] == '2'){
            $act .= '二级';
        }
        if($member['f_level'] == '3'){
            $act .= '三级';
        }
        return $act;
    }



    function detail_basic($member_id){
        $app = app::get('b2c');
        $member_model = $this->app->model('members');
        $member = $member_model->dump($member_id);
        if(!$member['member_type']){
            $member['member_type_name'] = '普通会员';
        }
        if($member['member_type'] == '1'){
            $member['member_type_name'] = '国内企业会员';
        }
        if($member['member_type'] == '2'){
            $member['member_type_name'] = '国外企业会员';
        }

        if($member['cp_dim'] == '1' ){
            $member['cp_dim_name'] = '20人以下';
        }
        if($member['cp_dim'] == '2' ){
            $member['cp_dim_name'] = '20-50人';
        }
        if($member['cp_dim'] == '3' ){
            $member['cp_dim_name'] = '50-100人';
        }
        if($member['cp_dim'] == '4' ){
            $member['cp_dim_name'] = '100人以上';
        }
        switch ($member['cp_ind']){
            case '1':
                $member['cp_ind_name'] = '地产';break;
            case '2':
                $member['cp_ind_name'] = '酒店';break;
            case '3':
                $member['cp_ind_name'] = '建筑设计';break;
            case '4':
                $member['cp_ind_name'] = '室内设计';break;
            case '5':
                $member['cp_ind_name'] = '产品设计';break;
            case '6':
                $member['cp_ind_name'] = '卖场及代理';break;
            case '7':
                $member['cp_ind_name'] = '项目管控';break;
            default:
                $member['cp_ind_name'] = '地产';
        }
        $render = $app->render();
        $render->pagedata['mem'] = $member;
        $render->pagedata['member_id'] = $member_id;

        return $render->fetch('admin/member/detail.html');
    }


    function detail_edit($member_id){
        $app = app::get('b2c');
        $member_model = $app->model('members');
        $userPassport = kernel::single('b2c_user_passport');
        $userObject = kernel::single('b2c_user_object');

        if($_POST){
            $_POST['member_id'] = $member_id;
            $saveData['b2c_members'] = $_POST;
            unset($saveData['b2c_members']['pam_members']);
            #↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓记录管理员操作日志@lujy↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
            if($obj_operatorlogs = kernel::service('operatorlog.members')){
                $olddata = app::get('b2c')->model('members')->dump($member_id);
            }
            #↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑记录管理员操作日志@lujy↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
            if( $member_model->save($saveData['b2c_members']) ){
                #↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓记录管理员操作日志@lujy↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
                if($obj_operatorlogs = kernel::service('operatorlog.members')){
                    if(method_exists($obj_operatorlogs,'detail_edit_log')){
                        $newdata = app::get('b2c')->model('members')->dump($member_id);
                        $obj_operatorlogs->detail_edit_log($newdata['contact'],$olddata['contact']);
                    }
                }
                #↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑记录管理员操作日志@lujy↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
                if( $member_rpc_object = kernel::service("b2c_member_rpc_sync") ) {
                    $member_rpc_object->modifyActive($member_id);
                }
                $msg = app::get('b2c')->_('保存成功');
                header('Content-Type:text/jcmd; charset=utf-8');
                echo '{success:"'.$msg.'",_:null}';
                exit;
            }else{
                $msg = app::get('b2c')->_('保存失败');
                header('Content-Type:text/jcmd; charset=utf-8');
                echo '{error:"'.$msg.'",_:null}';
                exit;
            }
        }

        $membersData = $userObject->get_members_data(array('account'=>'*','members'=>'*'),$member_id,false);
        $member_lv=$app->model("member_lv");
        foreach($member_lv->getMLevel() as $row){
            $options[$row['member_lv_id']] = $row['name'];
        }
        $membersData['lv']['options'] = is_array($options) ? $options : array(app::get('b2c')->_('请添加会员等级')) ;
        $membersData['lv']['value'] = $membersData['members']['member_lv_id'];

        $render = $app->render();
        $membersData['account']['local'] = kernel::single('weixin_wechat')->emoji_decode($membersData['account']['local']);
        $render->pagedata['mem'] = $membersData;
        $render->pagedata['attr'] = $userPassport->get_signup_attr($member_id);
        $render->pagedata['member_id'] = $member_id;
        return $render->fetch('admin/member/edit.html');
    }

    function detail_experience($member_id){
        $app = app::get('b2c');
        $member = $app->model('members');
        $aMem = $member->dump($member_id,'*',array('contact'=>array('*')));
        if($_POST){
            #↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓记录管理员操作日志@lujy↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
            if($obj_operatorlogs = kernel::service('operatorlog.members')){
                $olddata = app::get('b2c')->model('members')->dump($member_id);
            }
            #↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑记录管理员操作日志@lujy↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
            if(!$member->change_exp($member_id,$_POST['experience'],$msg)){
                header('Content-Type:text/jcmd; charset=utf-8');
                echo '{error:"'.$msg.'",_:null}';
                exit;
            }
            #↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓记录管理员操作日志@lujy↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
            if($obj_operatorlogs = kernel::service('operatorlog.members')){
                if(method_exists($obj_operatorlogs,'detail_experience_log')){
                    $newdata = app::get('b2c')->model('members')->dump($member_id);
                    $obj_operatorlogs->detail_experience_log($newdata,$olddata);
                }
            }
            #↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑记录管理员操作日志@lujy↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
        }
        $aMem = $member->dump($member_id,'*',array('contact'=>array('*')));
        $render = $app->render();
        $render->pagedata['mem'] = $aMem;
        return $render->fetch('admin/member/experience.html');
    }


    function detail_point($member_id=null){
        if(!$member_id) return null;
        $nPage = $_GET['detail_point'] ? $_GET['detail_point'] : 1;
        $singlepage = $_GET['singlepage'] ? $_GET['singlepage']:false;
        $app = app::get('b2c');
        $member = $app->model('members');
        $mem_point = $app->model('member_point');
        $obj_user = kernel::single('desktop_user');

        if($_POST){
            #↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓记录管理员操作日志@lujy↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
            if($obj_operatorlogs = kernel::service('operatorlog.members')){
                $olddata = app::get('b2c')->model('members')->dump($member_id);
            }
            #↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑记录管理员操作日志@lujy↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
            $change_point = $_POST['modify_point'];
            $msg = $_POST['modify_remark'];
            if(kernel::single('pointprofessional_point_common')->point_change_action($member_id,$change_point,$msg,'operator_adjust',3,0,$obj_user->user_id,'charge',false)){
                #↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓记录管理员操作日志@lujy↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
                if($obj_operatorlogs = kernel::service('operatorlog.members')){
                    if(method_exists($obj_operatorlogs,'detail_point_log')){
                        $newdata = app::get('b2c')->model('members')->dump($member_id);
                        $obj_operatorlogs->detail_point_log($newdata,$olddata);
                    }
                }
                #↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑记录管理员操作日志@lujy↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
            }
            else{
                header('Content-Type:text/jcmd; charset=utf-8');
                echo '{error:"'.$msg.'",_:null}';
                exit;
            }
        }
        $data = $member->dump($member_id,'*',array('score/event'=>array('*',null,array($this->pagelimit*($nPage-1),$this->pagelimit))));
        $accountObj = app::get('pam')->model('account');
        $nodes_obj = $this->app->model('shop');
        $nodes = $nodes_obj->count( array('node_type'=>'ecos.taocrm','status'=>'bind'));

        if($nodes > 0){
            $getlog_params = array('member_id'=>$member_id,'page'=>$nPage,'page_size'=>10);
            $pointlog = kernel::single('b2c_member_point_contact_crm')->getPointLog($getlog_params);

            $count = $pointlog['total'];
            $data['score']['event'] = $pointlog['historys'];
            foreach((array)$data['score']['event'] as $key=>$val){
                $data['score']['event'][$key]['operator_name'] = '';
            }
        }else{
            $row = $mem_point->getList('id',array('member_id' => $member_id,'status'=>'false'));
            $count = count($row);
            //获取日志操作管理员名称@lujy--start--
            foreach((array)$data['score']['event'] as $key=>$val){
                if( $val['status'] == 'false' ){
                    $operatorInfo = $accountObj->getList('login_name',array('account_id' => $val['operator']));
                    $data['score']['event'][$key]['operator_name'] = $operatorInfo['0']['login_name'];
                }else
                {
                    unset($data['score']['event'][$key]);
                }
            }
        }
        $render = $app->render();
        $obj_extend_point = kernel::service('b2c.member_extend_point_info');
        if ($obj_extend_point)
        {
            // 当前会员拥有的积分
            $obj_extend_point->get_real_point($member_id, $data['score']['total']);
            // 当前会员实际可以使用的积分
            $obj_extend_point->get_usage_point($member_id, $data['score']['usage']);

            $render->pagedata['extends_html'] = $obj_extend_point->gen_extend_detail_point($member_id);
        }
        else
        {
            $data['score']['total'] = $mem_point->get_total_count($member_id);
            $data['score']['usage'] = $mem_point->get_total_count($member_id);
        }
        $render->pagedata['member'] = $data;
        $render->pagedata['event'] = $data['score']['event'];
        if($_GET['page']) unset($_GET['page']);
        $_GET['page'] = 'detail_point';
        $render->pagedata['pager']= $this->controller->pagination($nPage,$count,$_GET);
        return $render->fetch('admin/member/point_list.html');
    }


    function detail_order($member_id=null){
        if(!$member_id) return null;
        $nPage = $_GET['detail_order'] ? $_GET['detail_order'] : 1;
        $app = app::get('b2c');
        $member = $app->model('members');
        $orders = $member->getOrderByMemId($member_id,$this->pagelimit*($nPage-1),$this->pagelimit);
        $order =  $app->model('orders');
        if($member_id){
            $row = $order->getList('order_id',array('member_id' => $member_id));
            $count = count($row);
        }
        foreach($orders as $key=>$order1){
            $orders[$key]['status'] = b2c_mdl_orders::getOrderStatusTxtForUser($orders[$key]['order_status']);
            $orders[$key]['pay_status'] = $order->trasform_status('pay_status',$orders[$key]['pay_status'] );
            $orders[$key]['ship_status'] = $order->trasform_status('ship_status', $orders[$key]['ship_status']);
        }

        $render = $app->render();
        $render->pagedata['orders'] = $orders;
        if($_GET['page']) unset($_GET['page']);
        $_GET['page'] = 'detail_order';
        $render->pagedata['pager']= $this->controller->pagination($nPage,$count,$_GET);
        return $render->fetch('admin/member/order.html');
    }

    function detail_recommend($member_id=null){
        if(!$member_id) return null;
        $nPage = $_GET['detail_recommend'] ? $_GET['detail_recommend'] : 1;
        $app = app::get('b2c');
        $memberModel = $app->model('members');
        $members = $memberModel->getList('member_id,name,email,order_num',['foreign_id' => $member_id],$this->pagelimit*($nPage-1),$this->pagelimit);
        $count = $memberModel->getList('member_id',['foreign_id' => $member_id]);

        $render = $app->render();
        $render->pagedata['members'] = $members;
        if($_GET['page']) unset($_GET['page']);
        $_GET['page'] = 'detail_recommend';
        $render->pagedata['pager']= $this->controller->pagination($nPage,count($count),$_GET);
        return $render->fetch('admin/member/page_recommend.html');
    }

    public function detail_return_order($member_id=null){
        if(!$member_id) return null;
        $nPage = $_GET['detail_return_order'] ? $_GET['detail_return_order'] : 1;
        $app = app::get('b2c');
        $member = $app->model('members');
        $memberInfo = $member->getRow('member_id,scale,member_type,is_white,check_status',['member_id' => $member_id]);
        if($memberInfo['member_type'] > 0 && $memberInfo['is_white'] == '1' && $memberInfo['check_status'] == '1'){
            $orders = $member->getReturnOrderByMemberId($member_id,$this->pagelimit*($nPage-1),$this->pagelimit);
            $orderModel =  $app->model('orders');
            $count = $orderModel->getList('order_id',array('member_id' => $member_id,'order_status'=> b2c_mdl_orders::ORDER_STATUS_FINISHED));
            if($orders){
                foreach($orders as $key =>$order){
                    if(!$order['return_status']){
                        //$orders[$key]['operate'] = '<a href="index.php?app=b2c&ctl=admin_member&act=returnSet&order_id='.$order['order_id'].'&member_id='.$member_id.'&is_return=1" onclick="if(confirm(\'确定返佣?\')==false)return false;">返佣</a>|<a href="index.php?app=b2c&ctl=admin_member&act=returnSet&order_id='.$order['order_id'].'&member_id='.$member_id.'" onclick="if(confirm(\'确定不设置返佣?\')==false)return false;">不返佣</a>';
                        $orders[$key]['operate'] = '<span onclick="new Dialog(\'index.php?app=b2c&ctl=admin_member&act=returnSet&member_id='.$member_id.'&order_id='.$order['order_id'].'&is_return=1\',{width:200,height:70,title:\'确认返佣\'})" class="lnk">返佣</span>|
                            <span onclick="new Dialog(\'index.php?app=b2c&ctl=admin_member&act=returnSet&member_id='.$member_id.'&order_id='.$order['order_id'].'\',{width:200,height:70,title:\'确认不返佣\'})" class="lnk">不返佣</span>';
                    }
                    if($order['return_status'] == '1'){
                        $orders[$key]['operate'] = '已返佣';
                    }
                    if($order['return_status'] == '2'){
                        $orders[$key]['operate'] = '不返佣';
                    }
                    $orders[$key]['scale'] = $memberInfo['scale'] ? bcmul($order['total_amount'],bcdiv($memberInfo['scale'],100,2),2) : '未设置返佣比例';
                    $orders[$key]['order_status'] = b2c_mdl_orders::getOrderStatusTxtForUser($orders[$key]['order_status']);
                    $orders[$key]['pay_status'] = $orderModel->trasform_status('pay_status',$orders[$key]['pay_status'] );
                    $orders[$key]['return_status'] = $orderModel->trasform_status('return_status',$orders[$key]['return_status']);
                }
            }
        }else{
            $orders = [];
            $count = [];
        }
        $render = $app->render();
        $render->pagedata['orders'] = $orders;
        if($_GET['page']) unset($_GET['page']);
        $_GET['page'] = 'detail_return_order';
        $render->pagedata['pager']= $this->controller->pagination($nPage,count($count),$_GET);
        return $render->fetch('admin/member/return_order.html');
    }

    public function detail_percentage_order($member_id=null){
        if(!$member_id) return null;
        $nPage = $_GET['detail_percentage_order'] ? $_GET['detail_percentage_order'] : 1;
        $app = app::get('b2c');

        $member = $app->model('members');
        $memberInfo = $member->getRow('member_id,scale,member_type,is_white,percentage',['member_id' => $member_id]);
        $orders = $member->getPercentageOrderByMemberId($member_id,$this->pagelimit*($nPage-1),$this->pagelimit);
        $orderModel =  $app->model('orders');
        $count = $orderModel->getList('order_id',array('percentage_member' => $member_id,'order_status'=> 11));
        if($orders){
            foreach($orders as $key =>$order){
                if(!$order['percentage_status']){
                    //$orders[$key]['operate'] = '<a href="index.php?app=b2c&ctl=admin_member&act=returnSet&order_id='.$order['order_id'].'&member_id='.$member_id.'&is_return=1" onclick="if(confirm(\'确定返佣?\')==false)return false;">返佣</a>|<a href="index.php?app=b2c&ctl=admin_member&act=returnSet&order_id='.$order['order_id'].'&member_id='.$member_id.'" onclick="if(confirm(\'确定不设置返佣?\')==false)return false;">不返佣</a>';
                    $orders[$key]['operate'] = '<span onclick="new Dialog(\'index.php?app=b2c&ctl=admin_member&act=percentage&member_id='.$member_id.'&order_id='.$order['order_id'].'&is_return=1\',{width:200,height:70,title:\'确认分成\'})" class="lnk">分成</span>|
                    <span onclick="new Dialog(\'index.php?app=b2c&ctl=admin_member&act=percentage&member_id='.$member_id.'&order_id='.$order['order_id'].'\',{width:200,height:70,title:\'确认不分成\'})" class="lnk">不分成</span>';
                }
                if($order['percentage_status'] == '1'){
                    $orders[$key]['operate'] = '已分成';
                }
                if($order['percentage_status'] == '2'){
                    $orders[$key]['operate'] = '不分成';
                }
                $orders[$key]['percentage'] = $memberInfo['percentage'] ? bcmul($order['total_amount'],bcdiv($memberInfo['percentage'],100,2),2) : '未设置分成比例';
                $orders[$key]['order_status'] = b2c_mdl_orders::getOrderStatusTxtForUser($orders[$key]['order_status']);
                $orders[$key]['pay_status'] = $orderModel->trasform_status('pay_status',$orders[$key]['pay_status'] );
                $orders[$key]['percentage_status'] = $orderModel->trasform_status('percentage_status',$orders[$key]['percentage_status']);
            }
        }
        $render = $app->render();
        $render->pagedata['orders'] = $orders;
        if($_GET['page']) unset($_GET['page']);
        $_GET['page'] = 'detail_percentage_order';
        $render->pagedata['pager']= $this->controller->pagination($nPage,count($count),$_GET);
        return $render->fetch('admin/member/percentage_order.html');
    }


    function detail_coupon($member_id){
        $app = app::get('b2c');
        if(!$member_id) return null;
        $nPage = $_GET['detail_coupon'] ? $_GET['detail_coupon'] : 1;
        $oCoupon = kernel::single('b2c_coupon_mem');
        $aData = $oCoupon->get_list_m($member_id,$nPage);
        if($member_id){
            $row = $oCoupon->get_list_m($member_id);
            $count = count($row);
        }
        if ($aData) {
            foreach ($aData as $k => $item) {
                if ($item['coupons_info']['cpns_status'] !=1) {
                    $aData[$k]['coupons_info']['cpns_status'] = false;
                    $aData[$k]['memc_status'] = app::get('b2c')->_('此种优惠券已取消');
                    continue;
                }

                $curTime = time();
                if ($curTime>=$item['time']['from_time'] && $curTime<$item['time']['to_time']) {
                    if ($item['memc_used_times']<$this->app->getConf('coupon.mc.use_times')){
                        if ($item['coupons_info']['cpns_status']){
                            $aData[$k]['memc_status'] = app::get('b2c')->_('可使用');
                        }else{
                            $aData[$k]['memc_status'] = app::get('b2c')->_('本优惠券已作废');
                        }
                    }else{
                        $aData[$k]['coupons_info']['cpns_status'] = false;
                        if($item['disabled'] == 'busy'){
                            $aData[$k]['memc_status'] = app::get('b2c')->_('使用中');
                        }else{
                            $aData[$k]['memc_status'] = app::get('b2c')->_('本优惠券次数已用完');
                        }
                    }
                }else{
                    $aData[$k]['coupons_info']['cpns_status'] = false;
                    $aData[$k]['memc_status'] = app::get('b2c')->_('还未开始或已过期');
                }
            }
        }
        $render = $app->render();
        $render->pagedata['coupons'] = $aData;
        if($_GET['page']) unset($_GET['page']);
        $_GET['page'] = 'detail_coupon';
        $render->pagedata['pager']= $this->controller->pagination($nPage,$count,$_GET);
        return $render->fetch('admin/member/coupon_list.html');
    }

    function detail_msg($member_id){
        if(!$member_id) return null;
        $member_id = intval($member_id);
        $nPage = $_GET['detail_msg'] ? $_GET['detail_msg'] : 1;
        $app = app::get('b2c');
        $obj_msg = kernel::single('b2c_message_msg');
        $this->db = kernel::database();
        $_count_row = $this->db->select('select * from sdb_b2c_member_comments where has_sent="true" and object_type="msg" and (to_id ='.$this->db->quote($member_id).' or author_id='.$this->db->quote($member_id).')');
        $row = $this->db->select('select * from sdb_b2c_member_comments where has_sent="true" and object_type="msg" and (to_id ='.$this->db->quote($member_id).' or author_id='.$this->db->quote($member_id).') limit '.$this->pagelimit*($nPage-1).','.$this->pagelimit);
        $count = count($_count_row);
        $render = $app->render();
        $render->pagedata['msgs'] =  $row;
        if($_GET['page']) unset($_GET['page']);
        $_GET['page'] = 'detail_msg';
        $render->pagedata['pager']= $this->controller->pagination($nPage,$count,$_GET);
        return $render->fetch('admin/member/member_msg.html');
    }


    function detail_remark($member_id){
        $app = app::get('b2c');
        $member = $app->model('members');
        if($_POST){
            $sdf['remark'] = $_POST['remark'];
            $sdf['remark_type'] = $_POST['remark_type'];
            #↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓记录管理员操作日志@lujy↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
            if($obj_operatorlogs = kernel::service('operatorlog.members')){
                $olddata = app::get('b2c')->model('members')->dump($member_id);
            }
            #↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑记录管理员操作日志@lujy↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
            if(!$member->update($sdf,array('member_id' => $member_id))){
                $msg = app::get('b2c')->_('保存失败!');
                header('Content-Type:text/jcmd; charset=utf-8');
                echo '{error:"'.$msg.'",_:null}';
                exit;
            }
            #↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓记录管理员操作日志@lujy↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
            if($obj_operatorlogs = kernel::service('operatorlog.members')){
                if(method_exists($obj_operatorlogs,'detail_remark_log')){
                    $newdata = app::get('b2c')->model('members')->dump($member_id);
                    $obj_operatorlogs->detail_remark_log($newdata,$olddata);
                }
            }
            #↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑记录管理员操作日志@lujy↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
            if($_GET['singlepage']=='true'){
                $msg = app::get('b2c')->_('保存成功!');
                header('Content-Type:text/jcmd; charset=utf-8');
                echo '{success:"'.$msg.'",_:null}';
                exit;
            }
        }
        $remark = $member->getRemarkByMemId($member_id);
        $render = $app->render();
        $render->pagedata['remark_type'] = $remark['remark_type'];
        $render->pagedata['remark'] =  $remark['remark'];
        $render->pagedata['res_url'] = $app->res_url;
        return $render->fetch('admin/member/remark.html');
    }



    var $column_editbutton_order = 10;
    var $column_editbutton_width = 40;
    public function column_editbutton($row)
    {
        $render = $this->app->render();
        $arr = array(
            'app'=>$_GET['app'],
            'ctl'=>$_GET['ctl'],
            'act'=>$_GET['act'],
            'finder_id'=>$_GET['_finder']['finder_id'],
            'action'=>'detail',
            'finder_name'=>$_GET['_finder']['finder_id'],
        );

        $arr_link = array(
            'info'=>array(
                'detail_edit'=>array(
                    'href'=>'javascript:void(0);',
                    'submit'=>'index.php?'.utils::http_build_query($arr).'&finderview=detail_edit&id='.$row['member_id'].'&_finder[finder_id]='.$_GET['_finder']['finder_id'],'label'=>app::get('b2c')->_('编辑会员信息'),
                    'target'=>'tab',
                ),
            ),
            'finder'=>array(
                'detail_experience'=>array(
                    'href'=>'javascript:void(0);',
                    'submit'=>'index.php?'.utils::http_build_query($arr).'&finderview=detail_experience&id='.$row['member_id'].'&_finder[finder_id]='.$_GET['_finder']['finder_id'],'label'=>app::get('b2c')->_('经验值'),
                    'target'=>'tab',
                ),
                'detail_point'=>array(
                    'href'=>'javascript:void(0);',
                    'submit'=>'index.php?'.utils::http_build_query($arr).'&finderview=detail_point&id='.$row['member_id'].'&_finder[finder_id]='.$_GET['_finder']['finder_id'],'label'=>app::get('b2c')->_('积分'),
                    'target'=>'tab',
                ),
                'detail_remark'=>array(
                    'href'=>'javascript:void(0);',
                    'submit'=>'index.php?'.utils::http_build_query($arr).'&finderview=detail_remark&id='.$row['member_id'].'&_finder[finder_id]='.$_GET['_finder']['finder_id'],'label'=>app::get('b2c')->_('会员备注'),
                    'target'=>'tab',
                ),
                'detail_coupon'=>array(
                    'href'=>'javascript:void(0);',
                    'submit'=>'index.php?'.utils::http_build_query($arr).'&finderview=detail_coupon&id='.$row['member_id'].'&_finder[finder_id]='.$_GET['_finder']['finder_id'],'label'=>app::get('b2c')->_('优惠券'),
                    'target'=>'tab',
                ),
            ),
        );

        //增加编辑菜单权限@lujy
        $permObj = kernel::single('desktop_controller');
        if(!$permObj->has_permission('editexp')){
            unset($arr_link['finder']['detail_experience']);
        }
        if(!$permObj->has_permission('editadvance')){
            unset($arr_link['finder']['editscore']);
        }


        $site_get_policy_method = $this->app->getConf('site.get_policy.method');
        if ($site_get_policy_method == '1')
        {
            unset($arr_link['finder']['detail_point']);
        }

        $render->pagedata['arr_link'] = $arr_link;
        $render->pagedata['handle_title'] = app::get('b2c')->_('编辑');
        $render->pagedata['is_active'] = 'true';
        return $render->fetch('admin/actions.html');
    }

    var $column_email_order = 12;
    public function column_email($row){
        if(!$this->pam_member_info[$row['member_id']]){
            $pam_member_info = $this->userObject->get_members_data(array('account'=>'login_account'),$row['member_id']);
        }else{
            $pam_member_info = $this->pam_member_info[$row['member_id']];
        }
        return $pam_member_info['account']['email'];
    }




    /*
     * 先注释异步加载积分字段方法
     *
    private $js_shop_stock = false;
    public $column_point = '积分';
    public $column_point_order = 16;
    public function column_point($row){
        $member_id = $row['member_id'];
        if ($this->js_shop_stock === false){
            $this->js_shop_stock = true;
            $return = <<<EOF
            <script>
                void function(){
                    function member_point_request(member_id){
                        new Request.JSON({
                            url:"index.php?app=b2c&ctl=admin_member&act=getMemberPoint",
                            method:"post",
                            data:{"member_id":member_id},
                            onComplete:function(rsp){
                                if(rsp.status=='fail'){console.log(rsp.msg);return;}
                                if(rsp.status=='succ'){
                                    if (\$defined(\$("member-point-"+member_id))){
                                        \$("member-point-"+member_id).setHTML(rsp.point);
                                    }
                                }
                            }
                        }).send();
                    }
                    \$('main').addEvent('domready',function(){
                        \$ES('.member-point').each(function(i){
                            member_point_request(i.get("member_id"));
                        });
                    });
                }();
            </script>
EOF;
            }

            $return .= <<<EOF
        <div class='member-point' member_id="{$member_id}" id="member-point-{$member_id}">{$row["point"]}</div>
EOF;
            return $return;
    }*/

}
