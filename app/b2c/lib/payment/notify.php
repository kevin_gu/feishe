<?php

use Payment\Client\Notify;
use Payment\Common\PayException;
use Payment\Notify\PayNotifyInterface;
use Payment\Config;
class  b2c_payment_notify implements PayNotifyInterface
{
    public static function create_pay_log($msg,$type='info')
    {
        $notify = new static();
        $notify->log($msg,$type,'create_pay.log');
    }

    public function log($msg,$type='info',$file='notify.log')
    {
        $log_dir = ROOT_DIR.'/data/payment/'.date("Ymd").'/';
        if(!is_dir($log_dir)){
            mkdir($log_dir,'0755',true);
        }
        $file = $log_dir.$file;
        if(is_array($msg)){
            $msg = serialize($msg);
        }
        file_put_contents($file,date("Y-m-d H:i:s")."\t$type\t".$msg.PHP_EOL,FILE_APPEND);
    }

    protected function buildAlipayData($recv)
    {
        $recv['is_success'] = 'T';
        $ret['payment_id'] = $recv['order_no'];
        if($ret['payment_id'] == '190805143603304'){
            $ret['payment_id'] ='514360330468971';
        }
        $ret['account'] = '2019080666164095';
        $ret['bank'] = '支付宝';
        $ret['pay_account'] = '付款帐号';
        $ret['currency'] = 'CNY';
        $ret['money'] = $recv['pay_amount'];
        $ret['paycost'] = '0.000';
        $ret['cur_money'] = $recv['pay_amount'];
        $ret['trade_no'] = $recv['transaction_id'];
        $ret['t_payed'] = strtotime($recv['notify_time']) ? strtotime($recv['notify_time']) : time();
        $ret['pay_app_id'] = "alipay";
        $ret['pay_type'] = 'online';
        $ret['memo'] = $recv['body'];
        switch($recv['trade_state']){
            case Config::TRADE_STATUS_SUCC:
                $ret['status'] = 'succ';
                break;
            case Config::TRADE_STATUS_FAILD:
                    $ret['status'] =  'failed';
                break;
            default:
                break;
        }
        return $ret;
    }

    protected function buildWeChatData($recv)
    {
        return $recv;
    }
    public function notifyProcess(array $data)
    {
        $channel = $data['channel'];
        $ret = [];

        if ($channel === Config::ALI_CHARGE) {// 支付宝支付
            $ret = $this->buildAlipayData($data);
        } elseif ($channel === Config::WX_CHARGE) {// 微信支付
            $ret =$this->buildWeChatData($data);
        }
        if(!$ret){
            return false;
        }
        if (!isset($ret['status']) || $ret['status'] == ''){
            $ret['status'] = 'failed';
        }
        /**
         * @var $obj_payments ectools_mdl_payments
         */
        $obj_payments = app::get('ectools')->model('payments');
        $sdf = $obj_payments->dump($ret['payment_id'], '*', '*');
        $this->log($channel."-notify-payments-id\t".$ret['payment_id']);
        if(!$sdf){
            $this->log($channel."-notify-payments-not-found\t".$ret['payment_id']);
            return false;
        }
        $order_info = current($sdf['orders']);
        $order_id =$order_info['rel_id'] ;
        if(!$order_id){
            $this->log($channel."-notify-payments-order-found\t".$ret['payment_id']);
            return false;
        }

        /**
         * @var $obj_payment_update ectools_payment_update
         */
        $obj_payment_update = kernel::single('ectools_payment_update');
        $msg = '';
        $is_updated = $obj_payment_update->generate($ret, $msg);
        if($is_updated){
            $this->log($channel."-notify-payments-update-success\t".$ret['payment_id']);
            if($ret['status'] == 'succ'){
                $mdl_order = b2c_mdl_orders::getInstance();
                $order_data = [
                    'deposit_at'=>time(),
                    'deposit_status'=>b2c_mdl_orders::PAYMENT_STATUS_OK,
                    'order_status'=>b2c_mdl_orders::ORDER_STATUS_WAIT_CONFIRM,
                    'order_current_status'=>b2c_mdl_orders::ORDER_STATUS_WAIT_CONFIRM,
                ];
                $is_updated = false;
                if($mdl_order->update($order_data,['order_id'=>$order_id])){
                    $this->log($channel."-notify-order-update-success\t".$order_id);
                    $is_updated = true;
                    b2c_mdl_order_log::saveCustomerLog($order_id,'在线支付保证金成功',$sdf['op_id'],'会员'.$sdf['op_id']);
                }else{
                    $this->log($channel."-notify-order-update-failed\t".$order_id);
                }
            }else{
                $this->log($channel."-notify-pay-status \t".$ret['status']);
            }
        }else{
            $this->log($channel."-notify-payments-update-failed\t".$ret['payment_id']);
        }
        return $is_updated;
    }

    public function alipay()
    {
        $aliConfig = require_once ROOT_DIR . '/config/aliconfig.php';
        /**
        $_POST = [
            'gmt_create' => '2019-06-17 10:30:10',
            'charset' => 'UTF-8',
            'seller_email' => 'koosa@cmaar.com',
            'subject' => '报名比赛费用',
            'sign' => 'N2ftZ+c3BgPuTT55erhjRNPZtQ7pPHHmuXFynTZDTWkVB72J7qqkFt/+jx2LVjHVW/WsZ4aYSo023N0RtKBeWmdSOlvadZB7XONcWOCU4AD6B+CuqjSVoZmSopU/Q+uVkGhdcSYLVok9ITW6JCtYMVjhPpOBgVy6aSQhyE4TMcWtI0y567LbHTy5mYi3A1UAY7c6DsB4Ceh/4n8x+EWbX/Csu8a/Qs6s5yNlHjSAIJz3TW6id/Sks9et9HxPz7m
            kg0i983rKuLvBpd+vDLjptF22Cgh9tbis7vlm8cNKaI+YbIsV8W/ETC0WLvvaRUUCOaMgdYma6joGVpgj7Ls1gg==',
            'buyer_id' => '2088702764904953',
            'invoice_amount' => '300.00',
            'notify_id' => '2019061700222103022004950532441307',
            'fund_bill_list' => '[{"amount":"0.01","fundChannel":"PCREDIT"}]',
            'notify_type' => 'trade_status_sync',
            'trade_status' => 'TRADE_SUCCESS',
            'receipt_amount' => '0.01',
            'buyer_pay_amount' => '0.01',
            'app_id' => '2019080666164095',
            'sign_type' => 'RSA2',
            'seller_id' => '2088912702013313',
            'gmt_payment' => '2019-06-17 10:30:22',
            'notify_time' => '2019-06-17 10:30:22',
            'version' => '1.0',
            'out_trade_no' => '19051513311381220353',
            'total_amount' => '300.00',
            'trade_no' => '2019061722001404950582096422',
            'auth_app_id' => '2019022563326158',
            'buyer_logon_id' => '133****5985',
            'point_amount' => '0.00',
        ];
        */
        $config = $aliConfig;
        $this->log(Config::ALI_CHARGE."-notify-data\t".json_encode($_POST));
        try {
            $ret = Notify::run('ali_charge', $config, $this);
            echo $ret;
        } catch (PayException $e) {
            $this->log("ali_charge-notify-exception\t".$e->getMessage().PHP_EOL.$e->getTraceAsString(),'error');
            echo $e->errorMessage();
        }
    }

    public function wechat()
    {
        echo 'fail';
    }
}