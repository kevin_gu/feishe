<?php
class b2c_send{
    /**
     * @param $email
     * @param $subject
     * @param $content
     * @param $config
     * @param  $error
     * @return bool
     */
    public static function sendByEmail($email,$subject,$content,$config=array(),&$error='')
    {
        $mail = new b2c_mail();
        if($config) {
            $mail_config = $config;
        }else{
            $mail_config = include ROOT_DIR.'/config/email.php';
        }
        if(!$mail_config){
            return false;
        }
        $mail_username = $mail_config['username'];
        $mail_password = $mail_config['password'];
        $mail_server = $mail_config['server'];
        $mail_port = $mail_config['port'];
        $mail_encryption = $mail_config['encryption'];
        $mail->setServer($mail_server, $mail_username, $mail_password, $mail_port, $mail_encryption); //到服务器的SSL连接
        $mail->setFrom($mail_username);
        $mail->setReceiver($email);
        $mail->setMail($subject, $content);
        $flag = $mail->sendMail();
        if(!$flag){
            $error = $mail->error();var_dump($error);
        }
        return $flag;
    }

    public static function sendEmail($email,$subject,$content,$config=array(),&$error='')
    {
        //$mail = new b2c_PHPMailer();
        if($config) {
            $mail_config = $config;
        }else{
            $mail_config = include ROOT_DIR.'/config/email.php';
        }
        if(!$mail_config){
            return false;
        }
        $mail = new b2c_PHPMailer();
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = $mail_config['server'];
        $mail->SMTPSecure = $mail_config['encryption'];
        $mail->Port = $mail_config['port'];
        $mail->CharSet = 'UTF-8';
        $mail->FromName = $mail_config['username'];
        $mail->Username = $mail_config['username'];
        $mail->Password = $mail_config['password'];
        $mail->From = $mail_config['username'];
        $mail->addAddress($email);
        $mail->Subject = $subject;
        $mail->Body = $content;
        $status = $mail->send();
        return $status;
    }

}