<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */
 
class b2c_service_view_menu{
    function function_menu(){
        $xml = simplexml_load_file(ROOT_DIR.'/config/deploy.xml');
        if($xml){
            $version=$xml->product_version;
        }else{
            $version='3.0.0';
        }
        $url='http://ego.shopex.cn/help/manual/ecstore/v'.$version;
        $shop_base = app::get('site')->router()->gen_url(array('app'=>'site', 'ctl'=>'default'));
        $html[] = "<a href='$shop_base' target='_blank'>".app::get('b2c')->_('浏览商店')."</a>";
        $html[] = "<a href=$url target='_blank'>".app::get('b2c')->_('帮助')."</a>";
        return $html;
    
    }
}