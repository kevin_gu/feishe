<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */


/**
 * 
 */
class b2c_mdl_logisticstrack extends dbeav_model{

    /**
     * @return b2c_mdl_logisticstrack
     */
    public static function getInstance()
    {
        /**
         * @var $model b2c_mdl_logisticstrack
         */
        static $model = null;
        if ($model == null) {
            $model = app::get('b2c')->model('logisticstrack');
        }
        return $model;
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function getData($order_id)
    {
        return $this->getList('*',['order_id'=>$order_id],0,-1,' id desc');
    }
}
