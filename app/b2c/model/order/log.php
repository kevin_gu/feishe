<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */
 

// ־ģ
class b2c_mdl_order_log extends dbeav_model{

    /**
     * Ϊ
     * @params array - ׼
     * @return null
     */
    public function save(&$sdf, $mustUpdate = NULL, $mustInsert = false)
    {
        return parent::save($sdf);
    }
    
    /**
     * ı־Ĳid
     * @params string - id
     * @params string bill type
     * @params string behavior
     * @return boolean
     */
    public function changeResult($rel_id, $bill_type, $behavior='payments', $result='PROCESS')
    {
        $order_log = $this->dump(array('rel_id' => $rel_id, 'sign' => 'CURRENT', 'bill_type' => $bill_type, 'behavior' => $behavior));
        $order_log['result'] = $result;
        
        return $this->save($order_log);
    }
    
     /**
     * õָrel_id, bill_type, behaviorµһ־
     * @params string rel_id
     * @params string pay object
     * @params string behavior
     */
    public function get_latest_orderlist($rel_id, $pay_object, $behavior)
    {
        $sql = "SELECT * FROM ".$this->table_name(1)." WHERE rel_id='". intval($rel_id) . "' AND bill_type='" . $this->db->quote($pay_object) . "' AND behavior='" . $this->db->quote($behavior) . "' ORDER BY alttime DESC";
        
        return $this->db->selectrow($sql);
    }
    
    /**
     * getList
     * @params string - 
     * @params array - 
     * @params ƫʼֵ
     * @params ƫλֵ
     * @params 
     */
    public function getList($cols='*', $filter=array(), $offset=0, $limit=-1, $orderby=null)
    {
        if ($filter)
            return parent::getList($cols, $filter, $offset, $limit, $orderby);
        else
            return parent::getList($cols, null, $offset, $limit, $orderby);
    }


    /**
     * @param $source
     * @param $order_id
     * @param $content
     * @param $op_id
     * @param $op_name
     * @return bool
     */
    public static function saveLog($source,$order_id,$content,$op_id,$op_name)
    {
        $orderLog = app::get('b2c')->model("order_log");
        $log_text = array(
            'txt_key'=>$content,
            'source'=>$source,
        );
        $log_text = serialize($log_text);
        $sdf_order_log = array(
            'rel_id' => $order_id,
            'op_id' => $op_id,
            'op_name' => $op_name,
            'alttime' => time(),
            'bill_type' => 'order',
            'behavior' => 'creates',
            'result' => 'SUCCESS',
            'log_text' => $log_text,
        );
        return  $orderLog->save($sdf_order_log);
    }

    /**
     * @param mixed $source
     * @param $order_id
     * @param $sort
     * @return array
     */
    public static function getLogs($source,$order_id,$sort=SORT_DESC)
    {
        $orderLog = app::get('b2c')->model("order_log");
        $orderBy = $sort == SORT_DESC ? 'log_id desc': 'log_id asc';
        $rows = $orderLog->getList('alttime,log_text,op_name',['rel_id'=>$order_id],0,-1,$orderBy);
        $data = [];
        foreach($rows as $k=>$v){
            $log_text = unserialize($v['log_text']);
            if(!$log_text || !is_array($log_text)){
                continue;
            }
            if(!isset($log_text['source'])){
                continue;
            }
            if(is_string($source) && $source == 'all'){
                $data[] = ['alttime'=>$v['alttime'],'log_text'=>$log_text['txt_key'],'op_name'=>$v['op_name'],'source'=>$log_text['source']];
            }else {
                if(in_array($log_text['source'],$source)){
                    $data[] = ['alttime'=>$v['alttime'],'log_text'=>$log_text['txt_key'],'op_name'=>$v['op_name']];
                }
            }
        }
        return $data;
    }
    /**
     * @param $order_id
     * @param $content
     * @return bool
     */
    public static function saveSystemLog($order_id,$content)
    {
        $user = new desktop_user();
        $op_id = $user->get_id();
        $op_name = $user->get_login_name();
        return static::saveLog('system',$order_id,$content,$op_id,$op_name);
    }
    /**
     * 记录订单日志 ，用于订单详细页
     * @param $order_id
     * @param $content
     * @param $op_id
     * @param $op_name
     * @return bool
     */
    public static function saveCustomerLog($order_id,$content,$op_id=null,$op_name=null)
    {
        if($op_id == null && $op_name == null){
            $arrMember = kernel::single('b2c_user_object')->get_current_member();
            $op_id = $arrMember['member_id'];
            $op_name = $arrMember['uname'];
        }
        return static::saveLog('customer',$order_id,$content,$op_id,$op_name);
    }

    public static function saveSystemAndCustomerLog($order_id,$content)
    {
        $user = new desktop_user();
        $op_id = $user->get_id();
        $op_name = $user->get_login_name();
        return static::saveLog('both',$order_id,$content,$op_id,$op_name);
    }

    /**
     * 获取前台用户展示日志
     * @param $order_id
     * @return array
     */
    public static function getCustomerLog($order_id)
    {
        return static::getLogs(['customer','both'],$order_id);
    }

    /**
     * 获取后台用户展示日志
     * @param $order_id
     * @return array
     */
    public static function getAllLog($order_id)
    {
        return static::getLogs('all',$order_id,SORT_ASC);
    }
}
