<?php
class b2c_mdl_order_offer extends dbeav_model
{
    /**
     * @return b2c_mdl_order_offer
     */
    public static function getInstance()
    {
        /**
         * @var $_model b2c_mdl_order_offer
         */
        static $_model = null;
        if($_model == null){
            $_model = app::get('b2c')->model('order_offer');
        }
        return $_model;
    }
    public function getLatestOne($order_id)
    {
        $offer =  $this->getRow('*',['order_id'=>$order_id],' id desc');
        if(!$offer){
            return null;
        }
        return $offer;
    }
}