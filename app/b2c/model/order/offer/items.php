<?php

class b2c_mdl_order_offer_items extends dbeav_model
{
    public function getItemsByOfferId($offer_id)
    {
        $image_set = app::get('image')->getConf('image.set');
        $obj_goods = app::get('b2c')->model('goods');
        $obj_product = app::get('b2c')->model('products');
        $router =  app::get('site')->router();
        $items= $this->getList('*',['offer_id'=>$offer_id]);
        foreach ($items as $k=>$v){
            $arr_goods_list = $obj_goods->getList('image_default_id,tax_number', array('goods_id' => $v['goods_id']));
            $arr_goods = $arr_goods_list[0];
            if (!$arr_goods['image_default_id']){
                $arr_goods['image_default_id'] = $image_set['S']['default_image'];
            }
            $items[$k]['thumbnail_pic'] = $arr_goods['image_default_id'];
            $items[$k]['tax_number'] = $arr_goods['tax_number'];
            $items[$k]['link_url'] = $router->gen_url(array('app'=>'b2c','ctl'=>'site_product','act'=>'index','arg0'=>$v['product_id']));;
            $arr_product = $obj_product->getRow("spec_info",['product_id'=>$v['product_id']]);
            $items[$k]['spec_info'] = $arr_product['spec_info'];
        }
        return $items;
    }
}
