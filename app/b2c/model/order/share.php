<?php
class b2c_mdl_order_share extends dbeav_model
{
    /**
     * @return b2c_mdl_order_share
     */
    public static function getInstance()
    {
        /**
         * @var $_model b2c_mdl_order_share
         */
        static $_model = null;
        if($_model == null){
            $_model = app::get('b2c')->model('order_share');
        }
        return $_model;
    }

    public function getMemberById($offer_id)
    {
        $rows = $this->getList('member_id',['order_index'=>$offer_id]);
        return b2c_arrayhelper::getColumn($rows,'member_id');
    }

    /**
     * @param $member_id
     * @param $offer_id
     * @return array
     */
    public static function getAvailableMembers($member_id,$offer_id)
    {
        /**
         * @var $mdl_members b2c_mdl_members
         */
        $mdl_members = app::get('b2c')->model('members');
        $members = $mdl_members->getRecommendList($member_id);
        if(!$members){
           return [];
        }
        $mdl_order_share = static::getInstance();
        $sharedMemberId = $mdl_order_share->getMemberById($offer_id);
        if($sharedMemberId){
            foreach($members as $k=>$v){
                if(in_array($v['member_id'],$sharedMemberId)){
                    unset($members[$k]);
                }
            }
        }
        return $members;
    }

    public function saveShare($owner_id,$order_id,$offer_id,$member_id)
    {
        if($this->getRow('id',['order_index'=>$offer_id,'member_id'=>$member_id])){
            return true;
        }
        $data = ['owner_id'=>$owner_id,'order_id'=>$order_id,'order_index'=>$offer_id,'member_id'=>$member_id,'created_at'=>time()];
        return $this->insert($data);
    }


    /**
     * @param $member_id
     * @param int $nPage
     * @param int $limit
     * @return mixed
     */
    public function getMemberOffers($member_id, $nPage=1,$limit=10)
    {
        if (!$limit)
            $limit = 10;
        $limitStart = ($nPage-1) * $limit;
        $filter = array(
            'member_id' => $member_id,
        );

        $offers = $this->getList('id,owner_id,order_index', $filter, $limitStart, $limit, 'id DESC');

        // 生成分页组建
        $countRd = $this->count($filter);
        $total = ceil($countRd/$limit);
        $current = $nPage;
        $token = 'token';
        $arrPager = array(
            'current' => $current,
            'total' => $total,
            'token' => $token,
        );
        $mdl_offer = b2c_mdl_orders::getInstance();
        foreach ($offers as $k=>$v){
            $offers[$k]['owner'] = '会员'.$v['owner_id'];
            $offers[$k]['data']  = $mdl_offer->getOrderOffer($v['order_index']);
        }
        $result['data'] = $offers;
        $result['pager'] = $arrPager;
        return $result;
    }

    /**
     *
     * @param $offer_id
     * @param $member_id
     * @param string $field
     * @return mixed
     */
    public function ensureMemberOffer($offer_id,$member_id,$field='*')
    {
        return $this->getRow( $field,['id'=>$offer_id,'member_id'=>$member_id]);
    }
}