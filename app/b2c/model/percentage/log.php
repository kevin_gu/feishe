<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */


/**
 * brand 模板
 */
class b2c_mdl_percentage_log extends dbeav_model{
    var $defaultOrder = array('id',' asc');
    const IS_RETURN = 0; //不分成
    const NO_RETURN = 1; //分成
    function __construct($app){
        parent::__construct($app);
        $this->use_meta();
    }

    function save( &$data,$mustUpdate = null, $mustInsert = false){
        $rs = parent::save($data,$mustUpdate);
        return $rs;
    }

    public  function getLogList($where,$start = 0, $limit = 10){
        $sql = "select id,member_id,order_id,percentage_status,percentage_money,createtime from sdb_b2c_percentage_log".$where." order by createtime desc limit $start,$limit";
        $logList = $this->db->select($sql);
        return $logList ? $logList : [];
    }

    public function getLogCount($where){
        $sql = "select count(id) as num from sdb_b2c_percentage_log".$where;
        $res = $this->db->selectrow($sql);
        return $res ? $res['num'] : 0;
    }

    public function getSumMoney(){
        $sql = "select SUM(percentage_money) as total from sdb_b2c_percentage_log where percentage_status = 1";
        $res = $this->db->selectrow($sql);
        return $res ? $res['total'] : 0;
    }

}
