<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */


/**
 * brand 模板
 */
class b2c_mdl_return_date_set extends dbeav_model{
    var $defaultOrder = array('id',' asc');

    function __construct($app){
        parent::__construct($app);
        $this->use_meta();
    }

    function save( &$data,$mustUpdate = null, $mustInsert = false){
        $rs = parent::save($data,$mustUpdate);
        return $rs;
    }

}
