<?php
class b2c_mdl_tax_company extends dbeav_model
{
    public function getRowByUid($id,$member_id)
    {
        $data = $this->dump($id);
        if($data && $data['member_id'] == $member_id){
            return $data;
        }
        return null;
    }

    public function getNormalList($member_id)
    {
        return $this->getList('*',['member_id'=>$member_id,'tax_invoice_type'=>'normal']);
    }

    public function getAddedList($member_id)
    {
        return $this->getList('*',['member_id'=>$member_id,'tax_invoice_type'=>'added']);
    }
}