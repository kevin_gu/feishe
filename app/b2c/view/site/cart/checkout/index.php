<?php
/**
 * @var base_controller $controller
 * @var $company_normal array
 * @var $company_added array
 * @var $member_invoice_addr_list array
 */
$controller->pre_footer_js[] ='area.js';
$controller->bodyClass = 'bg-fb header-holder';
/** @var $is_fastbuy */
/** @var $def_addr */
/** @var $def_area */
/** @var $md5_cart_info */
/** @var $json_args */
/** @var $member_addr_list */
/** @var $aCart */

/**
* @param $attrs
 * @return string
 */
$ship_area = function($attrs) {
        if(!strpos($attrs,',') || false!==strpos($attrs,',')){
            return $attrs = app::get('ectools')->model('regions')->change_regions_data($attrs);
        }
        return '';
    };
$goods = b2c_arrayhelper::getValue($aCart,'object.goods');
?>
<?=$controller->renderPartial('/header.php',['headerClass'=>'header-hasfix'])?>
<div class="oh main">
    <form id="order-form" action="<?=$controller->link(['app'=>'b2c', 'ctl'=>'site_order','act'=>'create'])?>" method="post">
    <?php if($is_fastbuy){?>
    <input type="hidden" name="isfastbuy" value="true" />
    <?php } ?>
    <input type="hidden" name="purchase[addr_id]" value="<?=$def_addr['addr_id']?>"/>
    <input type="hidden" name="purchase[def_area]" value="<?=$def_area?>" />
    <input type="hidden" name="md5_cart_info" value="<?=$md5_cart_info?>" />
    <!--<input type="hidden" name="extends_args" id="op_extends_args" value="<?=$json_args?>" />-->
    <div class="oh wrapper">
        <div class="container" goodsList>
            <div class="crumbs-panel">
                <div class="final-crumbs clearfix">
                    <em class="fl fb"><?=$controller->t('收货人信息')?></em>
                    <a class="fr fb js-add-address" href="javascript:;" data-value="order"><?=$controller->t('新增收货地址')?></a>
                </div>
            </div>
            <div class="final-address-panel js-address-panel">
                <table>
                    <?php foreach ($member_addr_list as $addr){
                        $is_default =$def_addr['addr_id'] == $addr['addr_id'];
                        ?>
                    <tr class="<?=$is_default?'default':''?>">
                        <td><?=$addr['name']?></td>
                        <td><?=$controller->t("收货地址")?>：<?=$ship_area($addr['area'] ). $addr['addr']?> <?=$addr['mobile']?></td>
                        <td class="tr">
                            <?php if($is_default){?>
                                <span class="default"><?=$controller->t("默认地址")?> </span>
                                <a href="javascript:;" class="js-edit-address" data-addr_id="<?=$addr['addr_id']?>" data-area="<?=$addr['area']?>" data-default="<?=$is_default?>"><?=$controller->t("编辑")?></a>
                            <?php } else {?>
                                <span class="set-default" data-default="<?=$addr['addr_id']?>"><?=$controller->t("设为默认地址")?> </span>
                                <a href="javascript:;" class="js-edit-address" data-value="order" data-addr_id="<?=$addr['addr_id']?>" data-area="<?=$addr['area']?>"><?=$controller->t("编辑")?></a>
                            <?php } ?>
                            <a href="javascript:;" class="js-remove-address" data-addr_id="<?=$addr['addr_id']?>" data-area="<?=$addr['area']?>" ><?=$controller->t("删除")?></a>
                        </td>
                    </tr>
                    <?php }?>
                </table>
            </div>
        </div>
        <div class="container" goodsList>
            <div class="crumbs-panel">
                <div class="final-crumbs clearfix">
                    <em class="fl fb"><?=$controller->t("发票信息")?></em>
                </div>
            </div>
            <div class="receipt-panel">
                <input type="hidden" id="receipt" name="receipt" value="0" />
                <ul class="receipt-tab f14">
                    <li class="current" data-value="0"><?=$controller->t("不开具发票")?></li>
                    <li data-value="1"><?=$controller->t("开具发票")?></li>
                </ul>
                <div class="receipt-bd">
                    <div class="receipt-item"></div>
                    <div class="receipt-item f14 clearfix none">
                        <div class="pr clearfix">
                            <div class="fl receipt-desc"><?=$controller->t("发票类型")?>：</div>
                            <div class="fl receipt-type">
                                <span id="normal-receipt" class="dib cp vm radio-box">
                                    <i class="radio" data-value="normal"></i>
                                    <em class="vm"><?=$controller->t("普通发票")?></em>
                                </span>
                                <span id="added-receipt" class="dib cp vm radio-box">
                                    <i class="radio" data-value="added"></i>
                                    <em class="vm"><?=$controller->t("增值税发票")?></em>
                                </span>
                                <input type="hidden" class="receipt-hidden" name="receipt_type" value="-1" />
                            </div>
                            <div class="pa form-tip none" style="left:350px;bottom:-6px;"></div>
                        </div>
                        <div class="pr clearfix">
                            <div class="fl receipt-desc"><?=$controller->t("发票抬头")?>：</div>
                            <div class="fl receipt-title js-receipt">
                                <div>
                                    <span id="person-check" class="cp radio-box">
                                        <i class="radio" data-value="person"></i>
                                        <em class="vm"><?=$controller->t("个人")?></em>
                                    </span>
                                </div>
                                <div class="pr clearfix">
                                    <div class="fl">
                                        <span id="company-check" class="cp radio-box">
                                            <i class="radio" data-value="company"></i>
                                            <em class="vm"><?=$controller->t("公司")?></em>
                                        </span>
                                    </div>
                                    <div class="pr fl select-panel js-select-panel company-panel none" style="width:331px">
                                        <span class="pa selected"><?=$controller->t("请选择")?></span>
                                        <ul class="pa select-list none" data-pos="1">
                                            <?php foreach($company_normal as $company){?>
                                            <li data-value="<?=$company['id']?>"><?=$company['tax_company']?></li>
                                            <?php }?>
                                            <li class="add js-add-company" data-value="company_normal"><?=$controller->t("新增公司")?></li>
                                        </ul>
                                        <i class="select-arrow"></i>
                                    </div>
                                    <div class="pr fl select-panel js-select-panel company-panel none" style="width:331px">
                                        <span class="pa selected"><?=$controller->t("请选择")?></span>
                                        <ul class="pa select-list none" data-pos="1">
                                            <?php foreach($company_added as $company){?>
                                                <li data-value="<?=$company['id']?>"><?=$company['tax_company']?></li>
                                            <?php }?>
                                            <li class="add js-add-company" data-value="company_added"><?=$controller->t("新增公司")?></li>
                                        </ul>
                                        <i class="select-arrow"></i>
                                    </div>
                                    <input type="hidden" name="company" class="receipt-addnew" value="-1" />
                                </div>
                                <input type="hidden" class="receipt-hidden" name="receipt_title" value="-1" />
                            </div>
                            <div class="pa form-tip none" style="left:350px;bottom:50px;"></div>
                        </div>
                        <div class="pr clearfix">
                            <div class="fl receipt-desc"><?=$controller->t("收票人地址")?>：</div>
                            <div class="fl receipt-type js-receipt">
                                <span id="same-address" class="dib vm cp radio-box">
                                    <i class="radio" data-value="0"></i>
                                    <em class="vm"><?=$controller->t("同收货地址")?></em>
                                </span>
                                <span id="receipt-address" class="dib vm cp radio-box">
                                    <i class="radio" data-value="1"></i>
                                    <em class="vm"><?=$controller->t("选择收票地址")?></em>
                                </span>
                                <input type="hidden" class="receipt-hidden" name="receipt_address" value="-1" />
                            </div>
                            <div class="pa form-tip none" style="left:350px;bottom:-6px;"></div>
                        </div>
                        <div class="receipt-address-panel clearfix none">
                            <div class="fl receipt-desc"><?=$controller->t("详细地址")?>：</div>
                            <div class="fl receipt-addr">
                                <div class="pr fl select-panel js-select-panel">
                                    <span class="pa selected"><?=$controller->t("请选择")?></span>
                                    <ul class="pa select-list none" data-pos="2">
                                        <?php foreach ($member_invoice_addr_list as $addr){
                                            ?>
                                            <li data-value="<?=$addr['addr_id']?>"><?=$ship_area($addr['area'] ). $addr['addr']?> <?=$addr['mobile']?></li>
                                        <?php }?>
                                        <li class="add js-add-address" data-value="invoice"><?=$controller->t("新增地址")?></li>
                                    </ul>
                                    <i class="select-arrow"></i>
                                    <input type="hidden" name="address" class="receipt-addnew" value="-1" />
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="fl receipt-desc"></div>
                            <button type="button"><?=$controller->t("保存发票信息")?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" goodsList>
            <div class="crumbs-panel">
                <div class="final-crumbs clearfix">
                    <em class="fl fb"><?=$controller->t("商品清单")?></em>
                    <a class="fr fb" href="<?=$controller->link(['app'=>'b2c', 'ctl'=>'site_cart','act'=>'index'])?>"><?=$controller->t("返回修改购物车")?></a>
                </div>
            </div>
        </div>
        <div class="container" goodsList>
            <div class="crumbs-panel clearfix">
                <div class="fl quota-caption"><?=$controller->t("商品名称")?></div>
                <div class="fl quota-caption"><?=$controller->t("商品信息")?></div>
                <div class="fl quota-caption"><?=$controller->t("数量")?></div>
            </div>
        </div>
        <div class="container">
            <div class="quota-panel">
                <ul class="quota-list clearfix">
                    <?php foreach ($goods as $_goods) {
                        if(!$_goods['selected']){
                            continue;
                        }
                        ?>
                    <li>
                        <div class="fl quota-goods">
                            <a href="" class="db">
                                <span><img src="<?=$controller->image_path(b2c_arrayhelper::getValue($_goods,'obj_items.products.0.thumbnail'))?>" alt="" /></span>
                                <p><?=b2c_arrayhelper::getValue($_goods,'obj_items.products.0.name')?></p>
                            </a>
                        </div>
                        <div class="fl quota-goods-intro">
                            <?php
                            $spec_info = b2c_arrayhelper::getValue($_goods,'obj_items.products.0.spec_info');
                            $spec_info_html = '';
                            if ($spec_info) {
                                foreach (b2c_mdl_orders::formatProductSpecInfo($spec_info) as  $_spec_info) {
                                    $spec_info_html .= '<p>' . $_spec_info . '</p>';
                                }
                            }
                            ?>
                            <?=$spec_info_html?>
                        </div>
                        <div class="fl quota-goods-amount">x<?=$_goods['quantity']?></div>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="container" goodsList>
            <div class="crumbs-panel">
                <div class="final-crumbs clearfix">
                    <em class="fl fb"><?=$controller->t("订单备注")?></em>
                </div>
            </div>
        </div>
        <div class="goods-panel receipt-panel">
            <div>
                <textarea name="memo" class="remark" placeholder="<?=$controller->t("请输入备注内容...")?>"></textarea>
            </div>
            <div class="cart-exec tc">
                <div class="fr cart-core js-comfirm-order"><?=$controller->t("确认下单")?></div>
                <div class="fr cart-core keep-shopping"><a class="db" href=""><?=$controller->t("继续购物")?></a></div>
            </div>
        </div>
    </div>
    </form>
</div>
<?=$controller->renderPartial('/footer.php')?>
<script id="poplayer-tpl" type="text/tpl">
    <div class="pop-layer fadeInDown">
        <div class="pop-layer-inner pr">
            <div class="pop-layer-title f16">{{=it.popTitle}}</div>
            <div class="pop-layer-bd"></div>
            <div class="loading"></div>
        </div>
        <a href="javascript:;" class="pa pop-layer-close"></a>
    </div>
</script>
<script id="address-tpl" type="text/tpl">
{{ if(it.type == 'address'){ }}
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:12px;">*</span><?=$controller->t("收货地区")?>：</div>
        <div class="flex clearfix f14 pr">
            <div class="pr fl select-panel js-select-panel">
                <span class="pa selected">{{! it.data ? it.data.area.province.selected : '<?=$controller->t("请选择")?>'}}</span>
                <ul class="pa select-list scrollbar province none">
                    <li data-value="-1"><?=$controller->t("请选择")?></li>
                    {{ for(var i = 0; i < it.province.length; i++){ }}
                    <li data-value="{{=it.province[i].split(':')[2]}}">{{=it.province[i].split(':')[0]}}</li>
                    {{ } }}
                </ul>
                <i class="select-arrow"></i>
                <input type="hidden" name="province" class="province-hidden" value="{{! it.data ? it.data.area.province.selected : '<?=$controller->t("请选择")?>:-1'}}" />
            </div>
            <div class="pr fl select-panel js-select-panel">
                <span class="pa selected">{{! it.data ? it.data.area.city.selected : '<?=$controller->t("请选择")?>'}}</span>
                <ul class="pa select-list scrollbar city none">
                   <li data-value="-1"><?=$controller->t("请选择")?></li>
                   {{ if(it.data){ }}
                       {{ for(var m = 0; m < it.data.area.city.list.length; m++){ }}
                        <li data-value="{{=it.data.area.city.list[m].split(':')[2]}}">{{=it.data.area.city.list[m].split(':')[0]}}</li>
                       {{ } }}
                   {{ } }}
                </ul>
                <i class="select-arrow"></i>
                <input type="hidden" name="city" class="city-hidden" value="{{! it.data ? it.data.area.city.selected : '<?=$controller->t("请选择")?>:-1'}}" />
            </div>
            <div class="pr fl select-panel js-select-panel">
                <span class="pa selected">{{! it.data ? it.data.area.county.selected.split(':')[0] : '<?=$controller->t("请选择")?>'}}</span>
                <ul class="pa select-list scrollbar county none">
                    <li data-value="-1"><?=$controller->t("请选择")?></li>
                    {{ if(it.data){ }}
                       {{ for(var n = 0; n < it.data.area.county.list.length; n++){ }}
                        <li data-value="{{=it.data.area.county.list[n].split(':')[1]}}">{{=it.data.area.county.list[n].split(':')[0]}}</li>
                       {{ } }}
                   {{ } }}
                </ul>
                <i class="select-arrow"></i>
                <input type="hidden" name="county" class="county-hidden" value="{{! it.data ? it.data.area.county.selected : '<?=$controller->t("请选择")?>:-1'}}" />
            </div>
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:12px;">*</span><?=$controller->t("详细地址")?>：</div>
        <div class="flex pr">
            <input type="text" class="address-input" value="{{! it.data ? it.data.addr : ''}}" placeholder="<?=$controller->t("请输入详细地址")?>" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:-4px;">*</span><?=$controller->t("收货人姓名")?>：</div>
        <div class="flex pr">
            <input type="text" class="name-input" value="{{! it.data ? it.data.name : ''}}" placeholder="<?=$controller->t("请输入收货人姓名")?>" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:42px;">*</span><?=$controller->t("手机")?>：</div>
        <div class="flex pr">
            <input type="text" class="mobile-input" value="{{! it.data ? it.data.mobile : ''}}" placeholder="<?=$controller->t("请输入手机号")?>" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label"><?=$controller->t("固定电话")?>：</div>
        <div class="flex pr">
            <input type="text" class="phone-input" value="{{! it.data ? it.data.tel : ''}}" placeholder="<?=$controller->t("固话格式：010-88888888(选填)")?>" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label"><?=$controller->t("邮编")?>：</div>
        <div class="flex pr">
            <input type="text" class="post-input" value="{{! it.data ? it.data.zip : ''}}" placeholder="<?=$controller->t("请输入邮编(选填)")?>" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    {{ if(it.addressType == 'order'){ }}
    <div class="flex-box" style="display:none;margin-top:15px;">
        <div class="pop-layer-label" style="margin-left:22px;line-height:normal">
        {{ if(it.data && it.data.isDefault == '1'){}}
            <span class="checkbox checkbox-checked" style="margin-right:10px;"></span>
            <input type="hidden" name="default" class="set-default" value="{{=it.data.isDefault}}" />
        {{ }else{ }}
            <span class="checkbox" style="margin-right:10px;"></span>
            <input type="hidden" name="default" class="set-default" value="0" />
        {{ } }}
        </div>
        <div class="flex f14" style="line-height:22px;">
            <?=$controller->t('设为默认地址')?>
        </div>
     {{ } }}
    </div>
    <div class="flex-box pop-layer-wrap">
        <button class="less-btn" style="margin:0 auto;" data-addr_id="{{! it.data ? it.data.addressId : ''}}" data-area_id="{{! it.data ? it.data.areaId : ''}}" data-address_type="{{=it.addressType}}" type="button">{{! it.data ? '确认修改' : '确认添加'}}</button>
    </div>
   {{ }else if(it.type == 'company'){ }}
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:12px;">*</span><?=$controller->t("公司名称")?>：</div>
        <div class="flex pr">
            <input type="text" value="" class="name-input" placeholder="<?=$controller->t("请输入公司名称")?>" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:42px;">*</span><?=$controller->t("税号")?>：</div>
        <div class="flex pr">
            <input type="text" value="" class="tax-input" placeholder="<?=$controller->t("请输入纳税人识别号")?>" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    {{ if(it.companyType != 'company_normal'){ }}
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:12px;">*</span><?=$controller->t("公司地址")?>：</div>
        <div class="flex pr">
            <input type="text" value="" class="address-input" placeholder="<?=$controller->t("请输入公司地址")?>" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:26px;">*</span><?=$controller->t("开户行")?>：</div>
        <div class="flex pr">
            <input type="text" value="" class="bank-input" placeholder="<?=$controller->t("请输入开户行名称")?>" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:42px;">*</span><?=$controller->t("账号")?>：</div>
        <div class="flex pr">
            <input type="text" value="" class="account-input" placeholder="<?=$controller->t("请输入开户行账号")?>" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><?=$controller->t("公司电话")?>：</div>
        <div class="flex pr">
            <input type="text" value="" class="phone-input" placeholder="<?=$controller->t("固话格式：010-88888888(选填)")?>" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    {{ } }}
    <div class="flex-box pop-layer-wrap">
        <button class="less-btn" data-type="{{=it.companyType}}" style="margin:0 auto;" type="button"><?=$controller->t("确认添加")?></button>
    </div>
   {{ } }}
</script>
<div id="hook"></div>
<div class="icon-sprite close-panel none"></div>