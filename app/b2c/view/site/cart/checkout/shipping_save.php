<?php
/**
 * @var base_controller $controller
 * @var $save_addr
 */
/**
 * @param $attrs
 * @return string
 */
$ship_area = function($attrs) {
    if(!strpos($attrs,',') || false!==strpos($attrs,',')){
        return $attrs = app::get('ectools')->model('regions')->change_regions_data($attrs);
    }
    return '';
};
$addr = $save_addr;
$page = $_POST['page'];
?>
<?php if($page == 'members'){?>
    <?php if($addr['addr_type'] == 'order'){?>
        <li>
            <div class="fl order-goods"><?=$ship_area($addr['area'] ). $addr['addr']?></div>
            <div class="fl tc order-goods-intro"><?=$addr['name']?></div>
            <div class="fl tc order-state"><?=$addr['mobile']?></div>
            <div class="fl tc order-exec">
                <a class="set-default" href="javascript:;" data-address_source="members" data-default="<?=$addr['addr_id']?>" set=""><?=$controller->t("设为默认")?></a>
                <a class="js-edit-address" href="javascript:;" data-addr_id="<?=$addr['addr_id']?>" data-area="<?=$addr['area']?>" data-value="order"><?=$controller->t("编辑")?></a>
                <a class="js-remove-address" href="javascript:;" data-addr_id="<?=$addr['addr_id']?>" data-area="<?=$addr['area']?>"><?=$controller->t("删除")?></a>
            </div>
        </li>
    <?php } else {?>
        <li>
            <div class="fl order-goods"><?=$ship_area($addr['area'] ). $addr['addr']?></div>
            <div class="fl tc order-goods-intro"><?=$addr['name']?></div>
            <div class="fl tc order-state"><?=$addr['mobile']?></div>
            <div class="fl tc order-exec">
                <a class="js-edit-address" href="javascript:;" data-addr_id="<?=$addr['addr_id']?>" data-area="<?=$addr['area']?>" data-value="invoice"><?=$controller->t("编辑")?></a>
                <a class="js-remove-address" href="javascript:;" data-addr_id="<?=$addr['addr_id']?>" data-area="<?=$addr['area']?>"><?=$controller->t("删除")?></a>
            </div>
        </li>
    <?php }?>
<?php } else {?>
    <?php if($addr['addr_type'] == 'order'){?>
        <tr>
            <td><?=$addr['name']?></td>
            <td><?=$controller->t("收货地址")?>：<?=$ship_area($addr['area'] ). $addr['addr']?> <?=$addr['mobile']?></td>
            <td class="tr">
                <span class="set-default" data-default="<?=$addr['addr_id']?>"><?=$controller->t("设为默认地址")?> </span>
                <a href="javascript:;" class="js-edit-address" data-addr_id="<?=$addr['addr_id']?>" data-area="<?=$addr['area']?>" data-value="order"><?=$controller->t("编辑")?></a>
                <a href="javascript:;" class="js-remove-address" data-addr_id="<?=$addr['addr_id']?>" data-area="<?=$addr['area']?>" ><?=$controller->t("删除")?></a>
            </td>
        </tr>
    <?php } else {?>
        <li data-value="<?=$addr['addr_id']?>"><?=$ship_area($addr['area'] ). $addr['addr']?> <?=$addr['mobile']?></li>
    <?php }?>
<?php }?>
