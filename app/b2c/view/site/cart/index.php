<?php
/**
 * @var b2c_ctl_site_cart $controller
 */
/** @var $aCart */
$goods = b2c_arrayhelper::getValue($aCart,'object.goods');
//$controller->footer_js[] = 'cart.js';
$selectedCount = 0;
?>
<?=$controller->renderPartial('/header.php')?>
<script type="text/tpl" id="compare-tpl">
<div class="global-hd f16 tc">
    {{ if(it.length > 1){ }}
    <a href="javascript:;" class="compare-now"><?=$controller->t("查看对比详情")?></a>
    {{ }else{ }}
    <span><?=$controller->t("查看对比详情")?></span>
    {{ } }}
</div>
<div class="pa compare-panel" style="left:1.8rem;top:0;width:9.25rem;height:1.42rem;">
    {{ if(it.length){}}
    <ul class="pa compare-list clearfix" style="width:12rem">
        {{ for(var i = 0; i < it.length; i++){ }}
        <li data-typeid="{{=it[i].typeId}}" data-goodsid="{{=it[i].goodsId}}">
            <a href="{{=it[i].goodsUrl}}" class="db">
                <div class="thumb"><img src="{{=it[i].goodsImg}}" alt="" /></div>
                <p>{{=it[i].goodsTitle}}</p>
            </a>
            <span class="icon-sprite remove"></span>
        </li>
        {{ } }}
   </ul>
   {{ }else{ }}
   <div class="tc" style="padding-top:.2rem;">
        <img style="width:2rem;opacity:.5" src="/public/assets/images/del_ico.png" alt="" />
        <div class="f14" style="margin-top:.1rem;color:#ccc;"><?=$controller->t("还没有商品哦")?></div>
   </div>
   {{ } }}
</div>
<a class="remove-panel" href="javascript:;"></a>
</script>
<div class="pr slider-container">
    <div class="pr slider-wrapper">
        <div class="slider-item" h324>
            <a href=""><img src="../public/assets/images/banner_324_01.jpg" alt="" /></a>
        </div>
    </div>
    <div class="curpos-panel tc">
        <a href="/" class="home"><?=$controller->t('返回首页')?></a>
        <span class="split">/</span>
        <span class="curpos"><?=$controller->t('心愿单')?></span>
    </div>
</div>
<div class="oh main">
    <form id="placeorder-form" action="<?=$controller->link(['app'=>'b2c', 'ctl'=>'site_cart','act'=>'_check_checkout'])?>" method="post">
    <div class="wrapper">
        <div class="container clearfix cart-checkbox" goodsList>
            <?php if($goods){ ?>
            <?php 
                $indent = array();
                $isCheckedAll = true;
            ?>
            <div class="goods-panel">
                <ul class="goods-list cart-list clearfix">
                    <?php foreach ($goods as $_goods) {
                     
                    if ($_goods['selected']) {
                        $selectedCount += $_goods['quantity'];
                        $indent[] = array($_goods['obj_ident'], $_goods['quantity']);
                    }else{
                        $isCheckedAll = false;
                    }
                    $_goods_id = b2c_arrayhelper::getValue($_goods, 'obj_items.products.0.goods_id');
                    $_product_id =b2c_arrayhelper::getValue($_goods, 'obj_items.products.0.product_id');
                    $thumb_image = $controller->get_product_spec_image($_goods_id,$_product_id,$_goods['product'][0]);
                    if(!$thumb_image){
                        $thumb_image=b2c_arrayhelper::getValue($_goods, 'obj_items.products.0.thumbnail');
                    }
                    ?>
                    <li data-typeid="<?= b2c_arrayhelper::getValue($_goods, 'obj_items.products.0.type_id')?>"
                         data-goodsid="<?= $_goods_id ?>"
                         data-goodstitle="<?= b2c_arrayhelper::getValue($_goods, 'obj_items.products.0.name') ?>"
                         data-goodsurl="<?= $controller->link(['app' => 'b2c', 'ctl' => 'site_product', 'arg0' => $_product_id]) ?>"
                         data-goodsimg="<?= $controller->image_path(b2c_arrayhelper::getValue($_goods, 'obj_items.products.0.thumbnail')) ?>">
                        <div class="pr thumb-item" data-type="goods" data-goods_ident="<?= $_goods['obj_ident'] ?>"
                             data-selected="<?= $_goods['selected'] ?>"
                             data-goods_id="<?= $_goods_id ?>"
                             data-min="1" data-max="999" data-stock="999">
                            <a href="<?= $controller->link(['app' => 'b2c', 'ctl' => 'site_product', 'arg0' => $_product_id]) ?>"
                               class="db">
                                <div class="thumb"><img
                                            alt="<?= b2c_arrayhelper::getValue($_goods, 'obj_items.products.0.name') ?>"
                                            src="<?= $controller->image_path($thumb_image) ?>"/>
                                </div>
                                <p><?= b2c_arrayhelper::getValue($_goods, 'obj_items.products.0.name') ?></p>
                            </a>

                            <?php if($controller->isGoodsFav($_goods_id)){ ?>
                                <span class="pa icon-sprite favor favored" data-goods_id="<?= $_goods_id ?>"></span>
                                <img class="pa favoring none" style="left:.19rem;top:.17rem;" src="/public/assets/images/favoring.gif?v=<?=$_goods_id?>" />
                            <?php }else{ ?>

                                <span class="pa icon-sprite favor"
                                      data-goods_id="<?= $_goods_id ?>"></span>
                                <img class="pa favoring none" style="left:.19rem;top:.17rem;"
                                     src="/public/assets/images/favoring.gif?v=<?= $_goods_id?>"/>
                            <?php } ?>



                            <span class="pa icon-sprite add-compare"
                                  data-goods_id="<?= b2c_arrayhelper::getValue($_goods, 'obj_items.products.0.goods_id') ?>"></span>
                            <span class="pa icon-sprite remove"></span>
                            <span class="checkbox <?= $_goods['selected'] ? 'checkbox-checked' : 'checkbox-nochecked' ?>"></span>
                            <div class="pa tc f14 remove-layer flex-box flex-hor">
                                <span class="dib vm cancel"><em><?=$controller->t("取消删除")?></em></span>
                                <span class="dib vm confirm"><em><?=$controller->t("确定删除")?></em></span>
                            </div>
                        </div>
                        <div class="pa annex-item">
                            <div class="clearfix js-cartcount">
                                <i class="fl pr tc reduce"></i>
                                <input class="fl entry-data" type="text"
                                       name="modify_quantity[<?= $_goods['obj_ident'] ?>]"
                                       value="<?= $_goods['quantity'] ?>"/>
                                <i class="fl pr tc increase">+</i>
                            </div>
                            <div class="oh ruler">
                                <?php
                                    $spec_info = b2c_arrayhelper::getValue($_goods, 'obj_items.products.0.spec_info');
                                    $spec_info_html = '';
                                    if ($spec_info) {
                                        foreach (b2c_mdl_orders::formatProductSpecInfo($spec_info) as  $_spec_info) {
                                            $spec_info_html .= '<p>' . $_spec_info . '</p>';
                                        }
                                    }else{
                                        $spec_info_html = '<p>　</p><p>　</p><p>　</p>';
                                    }
                                ?>
                                <?=$spec_info_html?>
                            </div>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
                <div class="cart-exec tc">
                    <a href="javascript:;" class="fl cart-core checkbox checkall <?= $isCheckedAll ? 'checkbox-checked' : '';?>"><?= $controller->t($isCheckedAll ? '反选购物车' : '全选购物车')?></a>
                    <div class="fl cart-core keep-shopping cart-remove<?php if($selectedCount){echo ' show';}?>" data-indent='<?=json_encode($indent);?>'><a class="db" href="javascript:;"><?=$controller->t("删除")?></a></div>
                    <div class="fr cart-core js-placeorder <?php if($selectedCount){echo '';}else{echo 'none';} ?>"><?=$controller->t("去下单")?></div>
                    <div class="fr cart-core <?php if($selectedCount){echo 'none';}else{echo '';} ?>" unuse><?=$controller->t("去下单")?></div>
                    <div class="fr cart-core keep-shopping"><a class="db" href="/index.php/gallery.html"><?=$controller->t("继续购物")?></a></div>
                    <div class="fr cart-amount"><?=$controller->t('共<span>{count}</span>件商品',['count'=>$selectedCount])?></div>
                </div>
            </div>
            <?php } ?>
            <div class="flex-box flex-ver cart-empty tc f16" style="display:<?php if($goods){echo 'none';}else{echo 'inherit';}?>">
                <img src="../public/assets/images/cart_empty.png" width="100" alt="" />
                <p><?=$controller->t("心愿单内还没有商品哦")?></p>
                <div>
                    <a class="empty-gohome" href="/"><?=$controller->t("回首页")?></a>
                    <a class="empty-gobuy" href="/index.php/gallery-0.html"><?=$controller->t("去逛逛")?></a>
                </div>
            </div>
        </div>
    </div>
    </form>
    <div class="global-nav cp">
        <img src="/public/assets/images/mico_st.gif" alt="" />
        <img class="mico " src="/public/assets/images/mico.png" alt="" />
        <span><?=$controller->t('添加成功')?></span>
    </div>
    <div class="global-panel" cart></div>
</div>
<?=$controller->renderPartial('/footer.php')?>

