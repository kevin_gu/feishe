<?php
/**
 *  @var base_controller $controller
 */
$controller->bodyClass = 'bg-fb';
?>
<div class="header">
    <div class="wrapper">
        <div class="header-wrapper">
            <div class="pr clearfix">

                <?= $this->renderPartial('/top.php'); ?>

            </div>
            <div class="header-sub-menu">
                <?= $this->renderPartial('/menu.php'); ?>
            </div>
        </div>
    </div>
</div>
<div class="pr slider-container">
    <div class="pr slider-wrapper">
        <div class="slider-item" h324>
            <a href=""><img src="../public/assets/images/banner_324_01.jpg" alt="" /></a>
        </div>
    </div>
    <div class="curpos-panel tc">
        <a href="/" class="home"><?=$controller->t("返回首页")?></a>
        <span class="split">/</span>
        <span class="curpos"><?=$controller->t("懒人沙发")?></span>
    </div>
</div>
<div class="oh main">
    <div class="oh wrapper">
        <div class="container" goodsList>
            <div class="compare-panel">
                <div class="pr compare-hd compare-detail">
                    <span class="fl f18"><?=$controller->t("对比详情")?></span>
                </div>
            </div>
        </div>
        <div class="container clearfix" goodsList>
            <div class="fl compare-res-intro">
                <div class="res-intro-name" resAll>
                    <?=$controller->t("商品")?>
                </div>
                <div class="res-intro-name" resName>
                    <?=$controller->t("商品名称")?>
                </div>
                <div class="res-intro-name">
                    <?=$controller->t("品牌")?>
                </div>
                <div class="res-intro-name">
                    <?=$controller->t("重量")?>
                </div>
                <?php
                foreach ($goodsParamsType as $k=>$v)
                { ?>
                    <div class="res-intro-name">
                        <?=$controller->t($k)?>
                    </div>
                <?php }
                ?>

                <?php
                foreach ($goodsProps as $k=>$v)
                { ?>
                    <div class="res-intro-name">
                        <?=$v['name']?>
                    </div>
                <?php }
                ?>
            </div>
            <div class="fl compare-res-detail">
                <?php
                foreach ($goodsFullInfo as $v)
                { ?>
                <div class="fl res-detail-list">
                    <div class="res-detail-item" data-productid="<?=$v['basic']['product_id']?>" data-goodsid="<?=$v['basic']['goods_id']?>" resGoods>
                        <a class="db" href="/index.php/product-<?=$v['basic']['product_id']?>.html">
                            <div thumb><img src="<?=$img_model->get_url($v['basic']['image_default_id'])?>" alt="" /></div>
                            <p><?=$v['basic']['name']?></p>
                        </a>
                        <a class="dib" href="/index.php/product-<?=$v['basic']['product_id']?>.html" seeDetail><?=$controller->t("查看详情")?></a>
                        <div cart>
                            <span class="dib vm cp add-cart"><?=$controller->t("加入心愿单")?></span>
                        </div>
                    </div>
                    <div class="res-detail-item" resGoodsName>
                        <p><?=$v['basic']['name']?></p>
                    </div>
                    <div class="res-detail-item">
                        <?=$v['brand']['brand_name']?>
                    </div>
                    <div class="res-detail-item">
                        <?=floatval($v['weight']['weight'])?>g
                    </div>

                    <?php
                    if (!empty($v['params']['params'])) {
                        foreach ($v['params']['params'] as $params) { ?>
                            <div class="res-detail-item">
                                <?= $params['value'] ?>
                            </div>

                        <?php }
                    }
                    else {
                        foreach ($goodsParamsType as $kk => $vv) { ?>
                            <div class="res-detail-item">
                            </div>
                        <?php }
                    }
                    ?>

                    <?php
                    if (!empty($v['props']['props'])) {
                        foreach ($v['props']['props'] as $props)
                        { ?>
                            <div class="res-detail-item">
                                <?=$props['value']?>
                            </div>

                        <?php }
                    }
                    else {
                        foreach ($goodsProps as $kk => $vv) { ?>
                            <div class="res-detail-item">
                            </div>
                        <?php }
                    }
                    ?>


                </div>

                <?php }
                ?>
            </div>


        </div>
    </div>
</div>
<?= $this->renderPartial('/footer.php'); ?>
</body>
</html>
