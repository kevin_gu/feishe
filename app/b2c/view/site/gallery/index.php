<?php
/** @var $controller base_controller */
?>
<script>
var filterJSON = <?=$screen?>;
</script>
<script type="text/tpl" id="filter-tpl"><!--筛选项模板-->
<div class="clearfix">
    <ul class="filter-tab f16 fl">
        {{ if(it.brand.length){ }}
        <li><?=$controller->t("品牌")?></li>
        {{ } }}
        {{ if(it.spec){ }}
        {{ for(var p in it.spec){ }}
        {{ if(it.spec[p].name){ }}
        <li>{{=it.spec[p].name}}</li>
        {{ } }}
        {{ } }}
        {{ } }}
        {{ if(it.props){ }}
        {{ for(var p in it.props){ }}
        {{ if(it.props[p].name && it.props[p].options){ }}
        <li>{{=it.props[p].name}}</li>
        {{ } }}
        {{ } }}
        {{ } }}
    </ul>
    <ul class="filter-holder f14 fl"><!--每次的筛选项-->
        {{ if(it.brand.length){ }}
            {{ for(var i = 0, len = it.brand.length; i < len; i++){ }}
                {{ if(it.brand[i]){ }}
                {{ if(it.brand[i].active == 'active'){ }}
                <li data-brandid="{{=it.brand[i].brand_id}}" data-key="brand_id" data-value="{{=it.brand[i].brand_id}}" class="filter-remove"><span class="dib vm filter-remove">{{=it.brand[i].brand_name}}</span><i class="dib vm filter-remove">×</i></li>
                {{ } }}
                {{ } }}
            {{ } }}
        {{ } }}
        {{ if(it.spec){ }}
            {{ for(var p in it.spec){ }}
                {{ for(var opt in it.spec[p].spec_value){ }}
                {{ if( it.spec[p].active){ }}
                {{ if( it.spec[p].active[opt] == 'active'){ }}
                <li data-key="s_{{=p}}" data-value="{{=opt}}" class="filter-remove"><span class="dib vm filter-remove">{{=it.spec[p].spec_value[opt].spec_value}}</span><i class="dib vm filter-remove">×</i></li>
                {{ } }}
                {{ } }}
                {{ } }}
            {{ } }}
        {{ } }}
        {{ if(it.props){ }}
            {{ for(var p in it.props){ }}
                {{ for(var opt in it.props[p].options){ }}
                {{ if( it.props[p].active){ }}
                {{ if( it.props[p].active[opt] == 'active'){ }}
                <li data-key="p_{{=p}}" data-value="{{=opt}}" class="filter-remove"><span class="dib vm filter-remove">{{=it.props[p].options[opt]}}</span><i class="dib vm filter-remove">×</i></li>
                {{ } }}
                {{ } }}
                {{ } }}
            {{ } }}
        {{ } }}
    </ul>
</div>
<div class="filter-bd" style="width:1920px;">
    <div class="filter-wrapper flex-box">
        <div class="filter-item filter-brand"><!--品牌-->
            <ul class="brand-fw clearfix"><!--按字母分组-->
                {{var index = 0;}}
                {{ for(var t in it.groupBrand){ }}
                {{ if(it.groupBrand[t].length){ }}
                <li class="{{! index == 0 ? 'current':'' }}">{{=t}}</li>
                {{index = 1;}}
                {{ } }}
                {{ } }}
            </ul>
            <div class="brand-panel">
                {{var index = 0;}}
                {{ for(var t in it.groupBrand){ }}
                {{ if(it.groupBrand[t].length){ }}
                <ul class="brand-word clearfix {{! index == 0 ? '':'none'}}">
                    {{index = 1;}}
                    {{ for(var i = 0; i < it.groupBrand[t].length; i++){ }}
                       <li><a data-brandid="{{=it.groupBrand[t][i].brand_id}}" class="js-brand" title="{{=it.groupBrand[t][i].brand_name}}" href="javascript:;">{{=it.groupBrand[t][i].brand_name}}</a></li>
                    {{ } }}
                </ul>
                {{ } }}
                {{ } }}
            </div>
        </div>
        {{ for(var p in it.spec){ }}<!--规格-->
        {{ if(it.spec[p].spec_type == 'text'){ }}
        <div class="filter-item scrollbar filter-style">
            <ul class="clearfix">
                {{ for(var m in it.spec[p].spec_value){ }}
                <li><a data-key="s_{{=p}}" data-value="{{=m}}" class="js-spec" title="{{=it.spec[p].spec_value[m].spec_value}}" href="javascript:;">{{=it.spec[p].spec_value[m].spec_value}}</a></li>
                {{ } }}
            </ul>
        </div>
        {{ }else if(it.spec[p].spec_type == 'image'){ }}
        <div class="filter-item scrollbar filter-color">
            <ul class="clearfix">
                {{ for(var m in it.spec[p].spec_value){ }}
                <li><i style="background:url({{=it.spec[p].spec_value[m].spec_image}}) no-repeat;background-size:contain"></i><span class="pa zoom-effect" style="background:url({{=it.spec[p].spec_value[m].spec_image}}) no-repeat;background-size:contain"></span><a data-key="s_{{=p}}" data-value="{{=m}}" class="js-spec" title="{{=it.spec[p].spec_value[m].spec_value}}" href="javascript:;">{{=it.spec[p].spec_value[m].spec_value}}</a></li>
                {{ } }}
            </ul>
        </div>
        {{ } }}
        {{ } }}
        {{ for(var p in it.props){ }}<!--扩展属性-->
        {{ if(it.props[p].options){ }}
        <div class="filter-item scrollbar filter-style">
            <ul class="clearfix">
                {{ for(var m in it.props[p].options){ }}
                <li><a data-key="p_{{=p}}" data-value="{{=m}}" class="js-props" title="{{=it.props[p].options[m]}}" href="javascript:;">{{=it.props[p].options[m]}}</a></li>
                {{ } }}
            </ul>
        </div>
        {{ } }}
        {{ } }}
    </div>
</div>
<span class="pa icon-sprite filter-switch hide"></span>
</script>
<script type="text/tpl" id="compare-tpl">
<div class="global-hd f16 tc">
    {{ if(it.length > 1){ }}
    <a href="javascript:;" class="compare-now"><?=$controller->t("查看对比详情")?></a>
    {{ }else{ }}
    <span><?=$controller->t("查看对比详情")?></span>
    {{ } }}
</div>
<div class="pa compare-panel" style="left:1.8rem;top:0;width:9.25rem;height:1.42rem;">
    {{ if(it.length){}}
    <ul class="pa compare-list clearfix" style="width:12rem">
        {{ for(var i = 0; i < it.length; i++){ }}
        <li data-typeid="{{=it[i].typeId}}" data-goodsid="{{=it[i].goodsId}}">
            <a href="{{=it[i].goodsUrl}}" class="db">
                <div class="thumb"><img src="{{=it[i].goodsImg}}" alt="" /></div>
                <p>{{=it[i].goodsTitle}}</p>
            </a>
            <span class="icon-sprite remove"></span>
        </li>
        {{ } }}
   </ul>
   {{ }else{ }}
   <div class="tc" style="padding-top:.2rem;">
        <img style="width:2rem;opacity:.5" src="/public/assets/images/del_ico.png" alt="" />
        <div class="f14" style="margin-top:.1rem;color:#ccc;"><?=$controller->t("还没有商品哦")?></div>
   </div>
   {{ } }}
</div>
<a class="remove-panel" href="javascript:;"></a>
</script>
<div class="header">
    <div class="wrapper">
        <div class="header-wrapper">
            <div class="pr clearfix">
                <?= $this->renderPartial('/top.php'); ?>
            </div>
            <div class="header-sub-menu">
                <?= $this->renderPartial('/menu.php'); ?>
            </div>
        </div>
    </div>
</div>
<div class="pr slider-container">
    <div class="pr slider-wrapper">
        <div class="slider-item" h324>
            <a href=""><img src="/public/assets/images/banner_324_01.jpg" alt="" /></a>
        </div>
    </div>
    <div class="curpos-panel tc">
        <a href="/" class="home"><?=$controller->t("返回首页")?></a>
        <span class="split">/</span>
        <span class="curpos"><?=$controller->t("懒人沙发")?></span>
    </div>
</div>
<div class="oh main">
    <div class="wrapper">
        <div class="container clearfix" goodsList>
            <div id="list-filter-panel" class="pr filter-panel">
                <div class="filter-hd f18"><?=$controller->t("快速筛选商品")?></div>
            </div>
        </div>
        <div class="container clearfix" goodsList>
            <div class="goods-panel">
                <div class="tr goods-amount"><?=$controller->t("共{count}件商品",['count'=>intval($this->pagedata['total'])])?></div>
                <ul class="goods-list clearfix">
                    <?php
                    $where = [
                        'selector'=>'select',
                        'pricefrom'=>'',
                        'priceto'=>'',
                        'searchname'=>'',
                        'type_id'=>'_ANY_',
                        'cat_id'=>['_ANY_'],
                        'brand_id'=>['_ANY_'],
                        'tag'=>['_ANY_'],
                        'goods_filter'=>'pricefrom=&priceto=&searchname=&type_id=_ANY_&cat_id[]=_ANY_&brand_id[]=_ANY_&tag[]=_ANY_',
                        'goods_order_by'=>'1',
                        'goods_select'=>'',
                        'goods_select_linkobj'=>'',
                    ];

                    foreach ($goodsData as $k=>$v)
                    {
                        $where['goods_select'] = $v['goods_id'];
                        $good_info = i18n_mdl_words::translateArray($this->index_tab_goods($where));
                        $good_info = $good_info['goodsRows'][$v['goods_id']];
                        $type_id = $goodsData[$k]['type_id'];
                        ?>
                        <li data-productid="<?=$good_info['products'][0]['product_id']?>" data-typeid="<?=$type_id?>" data-goodsid="<?=$v['goods_id']?>" data-goodstitle="<?=$good_info['goodsName'] ?>" data-goodsurl="<?=$good_info['goodsLink'] ?>" data-goodsimg="<?=$img_model->get_url($good_info['goodsPicS'])?>">
                            <div class="pr thumb-item">
                                <a href="<?=$good_info['goodsLink'] ?>" class="db">
                                    <div class="thumb">
                                        <img src="<?=$img_model->get_url($good_info['goodsPicS'])?>" data-source="<?=$img_model->get_url($good_info['goodsPicS'])?>" data-sence="<?=$img_model->get_url($v['images'][0])?>" alt="" />
                                    </div>
                                    <img class="none" src="<?=$img_model->get_url($v['images'][0])?>" alt="" />
                                    <p><?=$good_info['goodsName'] ?></p>
                                </a>
                                <span class="pa icon-sprite add-compare" title="加入对比"></span>
                                <?php if($this->pagedata['is_login']){ ?>
                                <span class="pa icon-sprite add-cart" title="加入心愿单"></span>
                                <?php }else{ ?>
                                <a href="/index.php/passport-login.html" class="pa icon-sprite add-cart" title="加入心愿单"></a>
                                <?php } ?>
                            </div>
                            <?php if($this->pagedata['is_login']){ ?>
                                <?php if($v['is_fav'] == true){ ?>
                                    <span class="pa icon-sprite favor favored"></span>
                                    <img class="pa favoring none" src="/public/assets/images/favoring.gif?v=<?=$v['goods_id']?>" />
                                <?php }else{ ?>
                                    <span class="pa icon-sprite favor"></span>
                                    <img class="pa favoring none" src="/public/assets/images/favoring.gif?v=<?=$v['goods_id']?>" />
                                <?php } ?>
                            <?php }else{ ?>
                                <a href="/index.php/passport-login.html" class="pa icon-sprite favor"></a>
                            <?php } ?>
                        </li>
                    <?php }
                    ?>
                </ul>
            </div>

            <div class="pagination tc">
                <?php
                $syy = '###';
                $xyy = '###';
                if ($this->pagedata['pager']['current'] != 1)
                {
                    $syy = $this->pagedata['pager']['link'].'&page='.($this->pagedata['pager']['current']-1);
                }

                if ($this->pagedata['pager']['current'] != $this->pagedata['pager']['total'])
                {
                    $xyy = $this->pagedata['pager']['link'].'&page='.($this->pagedata['pager']['current']+1);

                }
                ?>
                <span total>
                    <?=$controller->t("共{count}页",['count'=>intval($this->pagedata['pager']['total'])])?>

                </span>
                <a href="<?=$syy?>"><?=$controller->t("上一页")?></a>
                <a href="<?=$syy?>" prev></a>
                <span pageNum>
                <?php
                for ($i = 1; $i <= $this->pagedata['pager']['total']; $i++)
                {

                    if ($i == $this->pagedata['pager']['current']) {
                        ?>

                        <b><?=$i?></b>
                        <?php
                    }
                    else {
                        if(($i >($this->pagedata['pager']['current'] - 4)) && ($i < ($this->pagedata['pager']['current'] + 4))) {
                            ?>
                            <a href="<?= $this->pagedata['pager']['link'] . '&page=' . $i ?>"><?= $i ?></a>
                            <?php
                        }
                    }
                }
                ?>
                </span>
                <a href="<?=$xyy?>" next></a>
                <a href="<?=$xyy?>"><?=$controller->t("下一页")?></a>
            </div>
        </div>
    </div>
    <div class="global-nav cp">
        <img src="/public/assets/images/mico_st.gif" alt="" />
        <img class="mico " src="/public/assets/images/mico.png" alt="" />
        <span><?=$controller->t("添加成功")?></span>
    </div>
    <div class="global-panel"></div>
</div>
<?= $this->renderPartial('/footer.php'); ?>
