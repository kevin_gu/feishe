<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <title>Vanseo Fei</title>
    <link type="text/css" rel="stylesheet" href="/public/assets/css/user-center.css">
</head>
<body bg-fb header-holder>
    <div class="header header-hasfix">
        <div class="wrapper">
             <div class="header-wrapper">
                 <div class="pr clearfix">
                    <div class="fl logo">
                        <img src="/public/assets/images/logo.png" alt="" white />
                        <img src="/public/assets/images/black_logo.png" black alt="" />
                    </div>
                    <div class="fr header-menu">
                        <a href="" class="fl icon-sprite search"></a>
                        <ul class="fl f14">
                            <li><a href="">我的订单</a></li>
                            <li><a href="">会员中心</a></li>
                            <li>购物车中有<em>0</em>件商品<a href="" class="place-order">去下单</a></li>
                        </ul>
                        <a href="" class="fr f14 login">退出登录</a>
                    </div>
                 </div>
                 <div class="header-sub-menu">
                    <ul class="sub-menu-item pr">
                        <li class="item">舒适</li>
                        <li class="item">尊享</li>
                        <li class="item">奢华</li>
                        <li class="slider none"></li>
                    </ul>
                    <div class="pr cate-item-wrap clearfix none">
                        <div class="cate-level-1 pr fl">
                            <ul class="fl">
                                <li class="" data-src="images/goods_10.jpg"><a href="">卧室</a></li>
                                <li><a href="">客厅</a></li>
                                <li><a href="">厨房及餐厅</a></li>
                                <li><a href="">浴室</a></li>
                                <li><a href="">办公&书房</a></li>
                                <li><a href="">衣帽间</a></li>
                                <li><a href="">户外</a></li>
                                <li><a href="">休闲娱乐</a></li>
                                <li><a href="">配件</a></li>
                            </ul>
                            <ul class="fl none">
                                <li class=""><a href="">卧室2</a></li>
                                <li><a href="">客厅</a></li>
                                <li><a href="">厨房及餐厅</a></li>
                                <li><a href="">浴室</a></li>
                                <li><a href="">办公&书房</a></li>
                                <li><a href="">衣帽间</a></li>
                                <li><a href="">户外</a></li>
                                <li><a href="">休闲娱乐</a></li>
                                <li><a href="">配件</a></li>
                            </ul>
                            <ul class="fl none">
                                <li class=""><a href="">卧室3</a></li>
                                <li><a href="">客厅</a></li>
                                <li><a href="">厨房及餐厅</a></li>
                                <li><a href="">浴室</a></li>
                                <li><a href="">办公&书房</a></li>
                                <li><a href="">衣帽间</a></li>
                                <li><a href="">户外</a></li>
                                <li><a href="">休闲娱乐</a></li>
                                <li><a href="">配件</a></li>
                            </ul>
                        </div> 
                        <div class="pa cate-level-2">
                            <div class="clearfix">
                                <ul class="fl">
                                    <li class=""><a href="">床</a></li>
                                    <li><a href="">床头柜</a></li>
                                    <li><a href="">卧室用椅子</a></li>
                                </ul>
                                <ul class="fl none">
                                    <li class=""><a href="">床2</a></li>
                                    <li><a href="">床头柜</a></li>
                                    <li><a href="">卧室用椅子</a></li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="pa categoods-scene">
                            <a class="db" href="">
                                <div class="scene"><img src="../public/assets/images/goods_10.jpg" alt="" /></div>
                                <p>Molteni&C HIGH-WAVE 纺织布双人床</p>
                                <span>点击查看</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="oh main">
        <div class="oh wrapper">
           <div class="container" goodsList>
                <div class="crumbs-panel">
                    <a href="" class="vm">会员中心</a>
                    <i class="icon-sprite vm"></i>
                    <a href="" class="vm">我的账户</a>
                    <i class="icon-sprite vm"></i>
                    <span class="vm">成为企业客户</span>
                </div>
            </div>
           <div class="container clearfix" goodsList>
                <div class="fl order-side">
                    <dl class="order-nav">
                    	<dt>订单管理</dt>
                    	<dd><span><a href="">我的订单</a></span></dd>
                        <dd><span><a href="">售后订单</a></span></dd>
                        <dd><span><a href="">更多报价单</a></span></dd>
                    </dl>
                    <dl class="order-nav">
                    	<dt>我的账户</dt>
                    	<dd><span><a href="">商品收藏</a></span></dd>
                        <dd><span><a href="">我的发票</a></span></dd>
                        <dd class="current"><span><a href="">成为企业客户</a></span></dd>
                    </dl>
                     <dl class="order-nav">
                    	<dt>个人信息管理</dt>
                    	<dd><span><a href="">个人信息</a></span></dd>
                        <dd><span><a href="">站内信</a></span></dd>
                        <dd><span><a href="">安全中心</a></span></dd>
                        <dd><span><a href="">收货地址</a></span></dd>
                    </dl>
                </div>
                <div class="fl order-main">
                    <ul class="order-tab clearfix">
                    	<li>企业会员申请</li>
                    </ul>
                    <div class="business-apply">
                        <div class="flex-box">
                            <ul class="apply-tab f16">
                                <li class="current"><a href="javascript:;">国内企业</a></li>
                                <li><a href="javascript:;">国外企业</a></li>
                            </ul>
                            <div class="flex f14">
                                <div class="apply-item">
                                    <div class="apply-form-item pr flex-box">
                                        <span class="label">注册类型：</span>
                                        <div class="flex clearfix">
                                            <div class="pr fl select-panel" style="width:355px">
                                                <span class="pa selected">请选择</span>
                                                <ul class="pa select-list none">
                                                    <li>个人</li>
                                                    <li>企业</li>
                                                    <li>其他</li>
                                                </ul>
                                                <i class="select-arrow"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="apply-form-item pr flex-box">
                                        <span class="label">公司名称：</span>
                                        <div class="pr flex">
                                            <input type="text" value="" class="corp-name" placeholder="请按营业执照填写" />
                                            <a href="" class="pa icon-sprite help"></a>
                                        </div>
                                    </div>
                                    <div class="apply-form-item pr flex-box">
                                        <span class="label">统一社会信用代码：</span>
                                        <div class="flex">
                                            <input type="text" value="" placeholder="请按营业执照填写" />
                                        </div>
                                    </div>
                                    <div class="apply-form-item pr flex-box">
                                        <span class="label">公司地址：</span>
                                        <div class="flex">
                                            <input type="text" value="" placeholder="请填写实际所在地" />
                                        </div>
                                    </div>
                                    <div class="apply-form-item pr flex-box">
                                        <span class="label">公司行业：</span>
                                        <div class="flex clearfix">
                                            <div class="pr fl select-panel" style="width:355px">
                                                <span class="pa selected">请选择</span>
                                                <ul class="pa select-list none">
                                                    <li>个人</li>
                                                    <li>企业</li>
                                                    <li>其他</li>
                                                </ul>
                                                <i class="select-arrow"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="apply-form-item pr flex-box">
                                        <span class="label">公司人数：</span>
                                        <div class="flex clearfix">
                                            <div class="pr fl select-panel" style="width:355px">
                                                <span class="pa selected">请选择</span>
                                                <ul class="pa select-list none">
                                                    <li>个人</li>
                                                    <li>企业</li>
                                                    <li>其他</li>
                                                </ul>
                                                <i class="select-arrow"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="apply-form-item pr flex-box">
                                        <span class="label">联系人：</span>
                                        <div class="flex">
                                            <input type="text" value="" placeholder="请填写您的姓名" />
                                        </div>
                                    </div>
                                    <div class="apply-form-item pr flex-box">
                                        <span class="label">公司网址：</span>
                                        <div class="flex">
                                            <input type="text" value="" placeholder="请填写公司网址（选填）" />
                                        </div>
                                    </div>
                                    <div class="apply-form-item pr flex-box">
                                        <div class="flex upload-pic f12">
                                            <span class="dib pr">
                                                <i>上传有效证件</i>
                                                <input type="file" value="" />
                                            </span>
                                            <span>营业执照、组织机构代码等，图片大小不超过8M</span>
                                        </div>
                                    </div>
                                    <div class="tc" style="margin-top:30px;">
                                        <button type="submit" class="primary-btn less-btn">提交申请</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="apply-success tc f14" style="display:none;">
                            <p>您的公司信息已经提交成功，请耐心等待工作人员进行审核。</p>
                            <p>审核结果或通过您的注册邮箱下发通知。</p>
                        </div>
                        
                    </div>
                   
                </div>
           </div>
        </div>
    </div>
    <div class="footer">
        <div class="wrapper">
            <div class="container clearfix">
                <dl class="fl footer-nav">
                	<dt>购物指南</dt>
                	<dd><a href="">购物指南</a></dd>
                    <dd><a href="">会员介绍</a></dd>
                    <dd><a href="">常见问题</a></dd>
                    <dd><a href="">帮助中心</a></dd>
                    <dd><a href="">联系我们</a></dd>
                </dl>
                <dl class="fl footer-nav">
                	<dt>配送方式</dt>
                	<dd><a href="">物流介绍</a></dd>
                    <dd><a href="">配送服务查询</a></dd>
                    <dd><a href="">配送费用标准</a></dd>
                    <dd><a href="">海外配送</a></dd>
                </dl>
                <dl class="fl footer-nav">
                	<dt>支付方式</dt>
                	<dd><a href="">在线支付</a></dd>
                    <dd><a href="">保证金说明</a></dd>
                    <dd><a href="">支付超时声明</a></dd>
                    <dd><a href="">收款尾款支付方式</a></dd>
                </dl>
                <dl class="fl footer-nav">
                	<dt>售后服务</dt>
                	<dd><a href="">售后政策</a></dd>
                    <dd><a href="">退货说明</a></dd>
                    <dd><a href="">换货说明</a></dd>
                    <dd><a href="">保修说明</a></dd>
                    <dd><a href="">取消订单</a></dd>
                </dl>
                <dl class="fl footer-nav">
                	<dt>特色服务</dt>
                	<dd style="margin-right:-18px;"><a href="">设计师独创</a></dd>
                    <dd><a href="">1V1客服</a></dd>
                    <dd><a href="">优质选料</a></dd>
                    <dd><a href="">安全运输</a></dd>
                </dl>
            </div>
            <div class="container">
                <div class="tc footer-copyright">
                    <p>Copyright@2019XXXXXXXXXX有限公司 All rights reserved.京ICP备12345678号-2经营性网站许可证：京B2-201912334</p>
                    <p>严正声明，凡盗用「XXXXXXXX」官网、公众号等平台图片及资料进行虚假宣传，一经发现必追究其法律责任。</p>
                    <p>本站所有产品图片均来自合作厂家，案例新闻等图片为「XXXXXXXXX」拍摄，如有侵权，请联系我们。</p>
                </div>
            </div>
        </div>
    </div>
    <div class="search-panel none">
        <form action="">
            <div class="flex-box pr search-input">
                 <div class="flex">
                    <input type="text" value="" placeholder="你可以在这里搜索你想要的商品..." />
                </div>
                <button type="submit" class="pa icon-sprite search-btn"></button>
            </div>
            <div class="keyword clearfix">
                <div class="fl"><a href="">双人沙发</a></div>
                <div class="fl"><a href="">梳妆台</a></div>
                <div class="fl"><a href="">玄关柜</a></div>
                <div class="fl"><a href="">餐边柜</a></div>
                <div class="fl"><a href="">双人床</a></div>
                <div class="fl"><a href="">贵妃椅</a></div>
                <div class="fl"><a href="">酒柜</a></div>
                <div class="fl"><a href="">茶几</a></div>
            </div>
        </form>
    </div>
    <div class="icon-sprite close-panel none"></div>
    <div class="mask none" menuMask></div>
    <div class="mask none" searchMask></div>
    <script type="text/javascript" src="/public/assets/js/jquery-min.js"></script>
    <script type="text/javascript" src="/public/assets/js/webfont.js"></script>
    <script type="text/javascript" src="/public/assets/js/main.js"></script>
</body>
</html>
