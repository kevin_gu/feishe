<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <title>Vanseo Fei</title>
    <link type="text/css" rel="stylesheet" href="../public/assets/css/user-center.css">
</head>
<body bg-fb header-holder>
<div class="header header-hasfix">
    <div class="wrapper">
        <div class="header-wrapper">
            <div class="pr clearfix">
                <?= $this->renderPartial('/top.php'); ?>
            </div>
            <div class="header-sub-menu">
                <?= $this->renderPartial('/menu.php'); ?>
            </div>
        </div>
    </div>
</div>
<div class="oh main">
    <div class="oh wrapper">
        <div class="container" goodsList>
            <div class="crumbs-panel">
                <a href="" class="vm">会员中心</a>
                <i class="icon-sprite vm"></i>
                <a href="" class="vm">个人信息管理</a>
                <i class="icon-sprite vm"></i>
                <span class="vm">站内信</span>
            </div>
        </div>
        <div class="container clearfix" goodsList>
            <div class="fl order-side">
                <dl class="order-nav">
                    <dt>订单管理</dt>
                    <dd><span><a href="">我的订单</a></span></dd>
                    <dd><span><a href="">售后订单</a></span></dd>
                    <dd><span><a href="">更多报价单</a></span></dd>
                </dl>
                <dl class="order-nav">
                    <dt>我的账户</dt>
                    <dd class="current"><span><a href="">商品收藏</a></span></dd>
                    <dd><span><a href="">我的发票</a></span></dd>
                    <dd><span><a href="">成为企业客户</a></span></dd>
                </dl>
                <dl class="order-nav">
                    <dt>个人信息管理</dt>
                    <dd><span><a href="">个人信息</a></span></dd>
                    <dd class="current"><span><a href="">站内信</a></span></dd>
                    <dd><span><a href="">安全中心</a></span></dd>
                    <dd><span><a href="">收货地址</a></span></dd>
                </dl>
            </div>
            <div class="fl order-main">
                <ul class="order-tab clearfix">

                    <li><a href="/index.php/member-inbox.html">收件箱</a></li>
                    <li><a href="/index.php/member-send.html">发消息</a></li>
                    <li class="current"><a href="javascript:;">发件箱</a></li>
                    <li><a href="/index.php/member-outbox.html">草稿箱</a></li>
                    <li><a href="/index.php/member-message.html">给管理员发消息</a></li>
                </ul>
                <ul class="order-caption message-option clearfix">
                    <li>发件人</li>
                    <li>标题</li>
                    <li>时间</li>
                    <li>操作</li>
                </ul>
                <div class="order-panel message-panel">
                    <div class="order-bd">
                        <ul class="order-list clearfix">
                            <li>
                                <div>共<?=$count?>条消息</div>
                            </li>
                            <?php
                            foreach ($list as $k=>$v)
                            {
                                ?>
                                <li>
                                    <div class="clearfix">
                                        <div class="fl order-goods">
                                            <i class="checkbox"></i>
                                            <em class="dib"><?=$v['author']?></em>
                                        </div>
                                        <div class="fl tc order-goods-intro">

                                        </div>
                                        <div class="fl tc order-state">
                                            <?=date('Y-m-d H:i:s',$v['time'])?>
                                        </div>
                                        <div class="fl tc order-exec">
                                            <a href="javascript:;">回复</a>
                                            <a href="javascript:;" remove <?=$v['comment_id']?>>删除</a>
                                        </div>
                                    </div>
                                    <div class="message-content">
                                        <div class="flex-box message-title">
                                            <div>标题：</div>
                                            <div class="flex"><?=$v['title']?></div>
                                        </div>
                                        <div class="flex-box message-yar">
                                            <div>内容：</div>
                                            <div class="flex"><?=$v['comment']?></div>
                                        </div>
                                    </div>
                                </li>
                            <?php }
                            ?>

                        </ul>
                        <div class="check-all">
                            <i class="checkbox"></i>
                            <em class="dib">全选</em>
                            <a href="javascript:;" class="dib" remove>删除</a>
                        </div>
                    </div>
                </div>


                <div class="pagination tc">
                    <?php
                    $syy = '###';
                    $xyy = '###';
                    if ($pager['current'] != 1)
                    {
                        $syy = '/index.php/member-track-'.($pager['current']-1).'.html';
                    }

                    if ($pager['current'] != $pager['total'])
                    {
                        $xyy = '/index.php/member-track-'.($pager['current']+1).'.html';
                    }
                    ?>
                    <span total>
                    共<?=$pager['total']?>页
                </span>
                    <a href="<?=$syy?>">上一页</a>
                    <a href="<?=$syy?>" prev></a>
                    <span pageNum>
                <?php
                for ($i = 1; $i <= $pager['total']; $i++)
                {

                    if ($i == $pager['current']) {
                        ?>

                        <b><?=$i?></b>
                        <?php
                    }
                    else {
                        ?>
                        <a href="<?='/index.php/member-track-'.$i.'.html'?>"><?=$i?></a>
                        <?php
                    }
                }
                ?>
                </span>
                    <a href="<?=$xyy?>" next></a>
                    <a href="<?=$xyy?>">下一页</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="wrapper">
        <div class="container clearfix">
            <dl class="fl footer-nav">
                <dt>购物指南</dt>
                <dd><a href="">购物指南</a></dd>
                <dd><a href="">会员介绍</a></dd>
                <dd><a href="">常见问题</a></dd>
                <dd><a href="">帮助中心</a></dd>
                <dd><a href="">联系我们</a></dd>
            </dl>
            <dl class="fl footer-nav">
                <dt>配送方式</dt>
                <dd><a href="">物流介绍</a></dd>
                <dd><a href="">配送服务查询</a></dd>
                <dd><a href="">配送费用标准</a></dd>
                <dd><a href="">海外配送</a></dd>
            </dl>
            <dl class="fl footer-nav">
                <dt>支付方式</dt>
                <dd><a href="">在线支付</a></dd>
                <dd><a href="">保证金说明</a></dd>
                <dd><a href="">支付超时声明</a></dd>
                <dd><a href="">收款尾款支付方式</a></dd>
            </dl>
            <dl class="fl footer-nav">
                <dt>售后服务</dt>
                <dd><a href="">售后政策</a></dd>
                <dd><a href="">退货说明</a></dd>
                <dd><a href="">换货说明</a></dd>
                <dd><a href="">保修说明</a></dd>
                <dd><a href="">取消订单</a></dd>
            </dl>
            <dl class="fl footer-nav">
                <dt>特色服务</dt>
                <dd style="margin-right:-18px;"><a href="">设计师独创</a></dd>
                <dd><a href="">1V1客服</a></dd>
                <dd><a href="">优质选料</a></dd>
                <dd><a href="">安全运输</a></dd>
            </dl>
        </div>
        <div class="container">
            <div class="tc footer-copyright">
                <p>Copyright@2019XXXXXXXXXX有限公司 All rights reserved.京ICP备12345678号-2经营性网站许可证：京B2-201912334</p>
                <p>严正声明，凡盗用「XXXXXXXX」官网、公众号等平台图片及资料进行虚假宣传，一经发现必追究其法律责任。</p>
                <p>本站所有产品图片均来自合作厂家，案例新闻等图片为「XXXXXXXXX」拍摄，如有侵权，请联系我们。</p>
            </div>
        </div>
    </div>
</div>
<div class="search-panel none">
    <form action="">
        <div class="flex-box pr search-input">
            <div class="flex">
                <input type="text" value="" placeholder="你可以在这里搜索你想要的商品..." />
            </div>
            <button type="submit" class="pa icon-sprite search-btn"></button>
        </div>
        <div class="keyword clearfix">
            <div class="fl"><a href="">双人沙发</a></div>
            <div class="fl"><a href="">梳妆台</a></div>
            <div class="fl"><a href="">玄关柜</a></div>
            <div class="fl"><a href="">餐边柜</a></div>
            <div class="fl"><a href="">双人床</a></div>
            <div class="fl"><a href="">贵妃椅</a></div>
            <div class="fl"><a href="">酒柜</a></div>
            <div class="fl"><a href="">茶几</a></div>
        </div>
    </form>
</div>
<div class="icon-sprite close-panel none"></div>
<div class="mask none" menuMask></div>
<div class="mask none" searchMask></div>
<script type="text/javascript" src="../public/assets/js/jquery-min.js"></script>
<script type="text/javascript" src="../public/assets/js/webfont.js"></script>
<script type="text/javascript" src="../public/assets/js/main.js"></script>
</body>
</html>
