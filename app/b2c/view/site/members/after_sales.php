<?php
/**
 *  @var base_controller $controller
  * @var $order array
 * @var $offer array
 */
?>
<style type="text/css">
.after-sale-remove{width:20px;height:20px;line-height:17px;background:rgba(150,79,76,.9);border-radius:50%;font-size:20px;text-align:center;color:#fff;top:5px;right:5px}
</style>
<div class="oh main">
    <div class="oh wrapper">
        <div class="container" goodsList>
            <div class="crumbs-panel">
                <a href="/index.php/members-orders.html" class="vm"><?=$controller->t('会员中心')?></a>
                <i class="icon-sprite vm"></i>
                <a href="/index.php/members-orders.html" class="vm"><?=$controller->t('订单管理')?></a>
                <i class="icon-sprite vm"></i>
                <a href="/index.php/members-orders.html" class="vm"><?=$controller->t('我的订单')?></a>
                <i class="icon-sprite vm"></i>
                <span class="vm"><?=$controller->t('申请售后')?></span>
            </div>
        </div>
        <div class="container" goodsList>
            <div class="crumbs-panel clearfix">
                <div class="fl quota-caption"><?=$controller->t('请选择需要售后的商品及数量')?></div>
                <div class="fl quota-caption"><?=$controller->t('数量')?></div>
                <div class="fl quota-caption"><?=$controller->t('单价')?></div>
            </div>
        </div>
        <div class="container" goodsList>
            <div class="quota-panel js-quota-panel">
                <ul class="quota-list clearfix">
                    <?php
                    foreach ($offer['items'] as $goods_item){
                        $product = $goods_item;
                        $quantity = b2c_arrayhelper::getValue($product,'nums');
                        $amount = $goods_item['amount'];
                        $price = $goods_item['price'];
                        $link = b2c_arrayhelper::getValue($product,'link_url');
                        $thumb = b2c_arrayhelper::getValue($product,'thumbnail_pic');
                        $name = b2c_arrayhelper::getValue($product,'name');
                    ?>
                    <li>
                        <i class="checkbox" data-id="<?=$goods_item['item_id']?>"></i>
                        <div class="fl quota-goods">
                            <a href="<?=$link?>" class="db">
                                <span><img src="<?=$controller->image_path($thumb)?>" alt="<?=$name?>" /></span>
                                <p><?=$name?></p>
                            </a>
                        </div>
                        <div class="fl quota-goods-intro">
                            <p>颜色：象牙白</p>
                            <p>材质：象牙白</p>
                            <p>规格：单人位1.5米</p>
                        </div>
                        <div class="fl quota-goods-amount" style="padding-top:58px;text-indent:0;font-size:16px;">
                            <div class="pr fr select-panel js-select-panel" style="width:70px;margin-right:0">
                                <span class="pa selected"><?=$quantity?></span>
                                <dl class="pa select-list js-select-list none">
                                    <?php for($i=1;$i<=$quantity;$i++){?>
                                    <dd data-value="<?=$i?>"><?=$i?></dd>
                                    <?php }?>
                                </dl>
                                <i class="select-arrow"></i>
                            </div>
                            <input type="hidden" value="<?=$quantity?>" name="after-sale-amount" />
                        </div>
                        <div class="fl tc quota-state">
                            <span>￥<?=$price?></span>
                        </div>
                    </li>
                    <?php }?>
                </ul>
                <div class="pr check-all">
                    <span class="dib pr">
                        <i class="checkbox checkall"></i>
                        <span><?=$controller->t('全选')?></span>
                    </span>
                </div>
                <input type="hidden" id="after-sale-goods" value="" />
            </div>
        </div>
        <div class="container" goodsList>
            <div class="quota-panel after-sale">
                <div class="after-sale-hd"><?=$controller->t('请填写售后理由')?></div>
                <div class="after-sale-item flex-box">
                    <span class="pr"><b style="left:-10px;top:1px;color:#964F4C">*</b><?=$controller->t('退换理由')?>：</span>
                    <span class="flex"><input id="after-sale-reason" type="text" value="" placeholder="<?=$controller->t('请填写退换理由')?>" /></span>
                </div>
                <div class="after-sale-item flex-box">
                    <span class="pr"><b style="left:-10px;top:1px;color:#964F4C">*</b><?=$controller->t("详细描述")?>：</span>
                    <span class="flex"><textarea id="after-sale-desc" placeholder="请描述遇到的问题及详细退换理由"></textarea></span>
                </div>
                <div class="after-sale-item flex-box">
                    <span class="pr"><b style="left:-10px;top:1px;color:#964F4C">*</b><?=$controller->t("上传图片")?>：</span>
                    <div class="flex">
                        <div class="pr upload-img">
                            <span class="db tc"><?=$controller->t("选择图片")?></span>
                            <input id="upload-after-sale-pic" class="pa" type="file" value="" />
                            <input type="hidden" id="after-sale-pic" value="" />
                        </div>
                        <div id="after-sale-pic-panel" class="clearfix">
                            
                        </div>
                    </div>
                </div>
                <div class="after-sale-item tc">
                    <button type="button" class="primary-btn js-after-sale-submit"><?=$controller->t("提交申请")?></button>
                </div>
            </div>
        </div>
    </div>
</div>
