<?php
/**
 * @var base_controller $controller
 * @var $memberData
 */
?>
<ul class="order-tab clearfix">
    <li><?=$controller->t('企业会员申请')?></li>
</ul>
<div class="business-apply">
    <?php if($memberData['member_type'] > 0 && $memberData['check_status'] == 0){?>
    <div class="apply-success tc f14">
        <p><?=$controller->t('您的公司信息已经提交成功，请耐心等待工作人员进行审核')?></p>
        <p><?=$controller->t('审核结果或通过您的注册邮箱下发通知')?></p>
    </div>
    <?php }else if($memberData['member_type'] && $memberData['check_status'] == '1'){ ?>
        <div class="apply-success tc f14">
            <p><?=$controller->t('您已经是企业用户')?>！</p>
        </div>
    <?php }else{ ?>
    <div id="apply-company" class="flex-box">
        <ul class="apply-tab f16 js-reg-comptab">
            <li class="current"><a href="javascript:;"><?=$controller->t('国内企业')?></a></li>
            <li><a href="javascript:;"><?=$controller->t('国外企业')?></a></li>
        </ul>
        <div class="flex f14">
            <div class="apply-item reg-comp-panel">
                <form action="###" name="member-company" class="js-verifi-company" method="post">
                <!--<div class="apply-form-item pr flex-box">
                    <span class="label">注册类型：</span>
                    <div class="flex clearfix">
                        <div class="pr fl select-panel" style="width:355px">
                            <span class="pa selected">请选择</span>
                            <ul class="pa select-list none">
                                <li>个人</li>
                                <li>企业</li>
                                <li>其他</li>
                            </ul>
                            <i class="select-arrow"></i>
                        </div>
                    </div>
                </div>-->
                <div class="apply-form-item pr flex-box">
                    <span class="label"><?=$controller->t('公司名称')?>：</span>
                    <div class="pr flex">
                        <input name="companyname" type="text" value="" class="corp-name" placeholder="<?=$controller->t('请按营业执照填写')?>" />
                        <!--<a href="javascript:;" class="pa icon-sprite help"></a>-->
                    </div>
                    <div class="pa form-tip f14 none" style="left:29%;padding-left:0;bottom:-.22rem;"></div>
                </div>
                <div class="apply-form-item pr flex-box">
                    <span class="label"><?=$controller->t('统一社会信用代码')?>：</span>
                    <div class="pr flex">
                        <input name="companyid" type="text" value="" placeholder="<?=$controller->t('请按营业执照填写')?>" />
                    </div>
                    <div class="pa form-tip f14 none" style="left:29%;padding-left:0;bottom:-.22rem;"></div>
                </div>
                <div class="apply-form-item pr flex-box">
                    <span class="label"><?=$controller->t('公司地址')?>：</span>
                    <div class="pr flex">
                        <input name="companyaddr" type="text" value="" placeholder="<?=$controller->t('请填写实际所在地')?>" />
                    </div>
                    <div class="pa form-tip f14 none" style="left:29%;padding-left:0;bottom:-.22rem;"></div>
                </div>
                <div class="apply-form-item pr flex-box">
                    <span class="label"><?=$controller->t('公司行业')?>：</span>
                    <div class="flex pr clearfix">
                        <div class="pr fl select-panel js-select-panel" style="width:3.55rem">
                            <span class="pa selected"><?=$controller->t('请选择')?></span>
                            <ul class="pa select-list js-select-list none">
                                <li data-value="1"><?=$controller->t('地产')?></li>
                                <li data-value="2"><?=$controller->t('酒店')?></li>
                                <li data-value="3"><?=$controller->t('建筑设计')?></li>
                                <li data-value="4"><?=$controller->t('室内设计')?></li>
                                <li data-value="5"><?=$controller->t('产品设计')?></li>
                                <li data-value="6"><?=$controller->t('卖场及代理')?></li>
                                <li data-value="7"><?=$controller->t('项目管控')?></li>
                            </ul>
                            <i class="select-arrow"></i>
                        </div>
                        <input type="hidden" value="-1" name="industry" />
                        <div class="pa form-tip f14 none" style="padding-left:0;bottom:-.22rem;"></div>
                    </div>
                </div>
                <div class="apply-form-item pr flex-box">
                    <span class="label"><?=$controller->t('公司人数')?>：</span>
                    <div class="flex pr clearfix">
                        <div class="pr fl select-panel js-select-panel" style="width:3.55rem">
                            <span class="pa selected"><?=$controller->t('请选择')?></span>
                            <ul class="pa select-list js-select-list none">
                                <li data-value="1"><?=$controller->t('20人以下')?></li>
                                <li data-value="2"><?=$controller->t('20-50人')?></li>
                                <li data-value="3"><?=$controller->t('50-100人')?></li>
                                <li data-value="4"><?=$controller->t('100人以上')?></li>
                            </ul>
                            <i class="select-arrow"></i>
                        </div>
                        <input type="hidden" value="-1" name="dim" />
                        <div class="pa form-tip f14 none" style="padding-left:0;bottom:-.22rem;"></div>
                    </div>
                </div>
                <div class="apply-form-item pr flex-box">
                    <span class="label"><?=$controller->t('联系人')?>：</span>
                    <div class="pr flex">
                        <input name="conact" type="text" value="" placeholder="<?=$controller->t('请填写您的姓名')?>" />
                    </div>
                    <div class="pa form-tip f14 none" style="left:29%;padding-left:0;bottom:-.22rem;"></div>
                </div>
                <div class="apply-form-item pr flex-box">
                    <span class="label"><?=$controller->t('公司网址')?>：</span>
                    <div class="pr flex">
                        <input name="website" type="text" value="" placeholder="<?=$controller->t('请填写公司网址(选填)')?>" />
                    </div>
                    <div class="pa form-tip f14 none" style="left:29%;padding-left:0;bottom:-.22rem;"></div>
                </div>
                <div class="apply-form-item pr flex-box">
                    <div class="pr flex upload-pic f12">
                        <span class="dib pr">
                            <i><?=$controller->t('上传有效证件')?></i>
                            <input class="upload-blid" type="file" value="" />
                        </span>
                        <span><?=$controller->t('营业执照、组织机构代码等，图片大小不超过8M')?></span>
                        <input type="hidden" name="passport_photo" value="" />
                        <div class="pa form-tip f14 none" style="left:29%;padding-left:0;bottom:-.22rem;"></div>
                    </div>
                </div>
                <div class="tc" style="margin-top:.3rem;">
                    <button type="submit" class="primary-btn less-btn"><?=$controller->t('提交申请')?></button>
                </div>
              </form>
            </div>
            <div class="apply-item reg-comp-panel none">
                <form action="###" name="member-company" class="js-verifi-foreign-company" method="post">
                <!--<div class="apply-form-item pr flex-box">
                    <span class="label">注册类型：</span>
                    <div class="flex clearfix">
                        <div class="pr fl select-panel" style="width:355px">
                            <span class="pa selected">请选择</span>
                            <ul class="pa select-list none">
                                <li>个人</li>
                                <li>企业</li>
                                <li>其他</li>
                            </ul>
                            <i class="select-arrow"></i>
                        </div>
                    </div>
                </div>-->
                <div class="apply-form-item pr flex-box">
                    <span class="label"><?=$controller->t('公司名称')?>：</span>
                    <div class="pr flex">
                        <input name="companyname" type="text" value="" class="corp-name" placeholder="<?=$controller->t('请按营业执照填写')?>" />
                        <!--<a href="javascript:;" class="pa icon-sprite help"></a>-->
                    </div>
                    <div class="pa form-tip f14 none" style="left:29%;padding-left:0;bottom:-.22rem;"></div>
                </div>
                <div class="apply-form-item pr flex-box">
                    <span class="label"><?=$controller->t('商业注册码')?>：</span>
                    <div class="pr flex">
                        <input name="foreign_companyid" type="text" value="" placeholder="<?=$controller->t('请按营业执照填写')?>" />
                    </div>
                    <div class="pa form-tip f14 none" style="left:29%;padding-left:0;bottom:-.22rem;"></div>
                </div>
                <div class="apply-form-item pr flex-box">
                    <span class="label"><?=$controller->t('公司地址')?>：</span>
                    <div class="pr flex">
                        <input name="companyaddr" type="text" value="" placeholder="<?=$controller->t('请填写实际所在地')?>" />
                    </div>
                    <div class="pa form-tip f14 none" style="left:29%;padding-left:0;bottom:-.22rem;"></div>
                </div>
                <div class="apply-form-item pr flex-box">
                    <span class="label"><?=$controller->t('公司行业')?>：</span>
                    <div class="flex pr clearfix">
                        <div class="pr fl select-panel js-select-panel" style="width:3.55rem">
                            <span class="pa selected"><?=$controller->t('请选择')?></span>
                            <ul class="pa select-list js-select-list none">
                                <li data-value="1"><?=$controller->t('地产')?></li>
                                <li data-value="2"><?=$controller->t('酒店')?></li>
                                <li data-value="3"><?=$controller->t('建筑设计')?></li>
                                <li data-value="4"><?=$controller->t('室内设计')?></li>
                                <li data-value="5"><?=$controller->t('产品设计')?></li>
                                <li data-value="6"><?=$controller->t('卖场及代理')?></li>
                                <li data-value="7"><?=$controller->t('项目管控')?></li>
                            </ul>
                            <i class="select-arrow"></i>
                        </div>
                        <input type="hidden" value="-1" name="industry" />
                        <div class="pa form-tip f14 none" style="padding-left:0;bottom:-.22rem;"></div>
                    </div>
                </div>
                <div class="apply-form-item pr flex-box">
                    <span class="label"><?=$controller->t('公司人数')?>：</span>
                    <div class="flex pr clearfix">
                        <div class="pr fl select-panel js-select-panel" style="width:3.55rem">
                            <span class="pa selected"><?=$controller->t('请选择')?></span>
                            <ul class="pa select-list js-select-list none">
                                <li data-value="1"><?=$controller->t('20人以下')?></li>
                                <li data-value="2"><?=$controller->t('20-50人')?></li>
                                <li data-value="3"><?=$controller->t('50-100人')?></li>
                                <li data-value="4"><?=$controller->t('100人以上')?></li>
                            </ul>
                            <i class="select-arrow"></i>
                        </div>
                        <input type="hidden" value="-1" name="dim" />
                        <div class="pa form-tip f14 none" style="padding-left:0;bottom:-.22rem;"></div>
                    </div>
                </div>
                <div class="apply-form-item pr flex-box">
                    <span class="label"><?=$controller->t('联系人')?>：</span>
                    <div class="pr flex">
                        <input name="conact" type="text" value="" placeholder="<?=$controller->t('请填写您的姓名')?>" />
                    </div>
                    <div class="pa form-tip f14 none" style="left:29%;padding-left:0;bottom:-.22rem;"></div>
                </div>
                <div class="apply-form-item pr flex-box">
                    <span class="label"><?=$controller->t('公司网址')?>：</span>
                    <div class="pr flex">
                        <input name="website" type="text" value="" placeholder="<?=$controller->t('请填写公司网址(选填)')?>" />
                    </div>
                    <div class="pa form-tip f14 none" style="left:29%;padding-left:0;bottom:-.22rem;"></div>
                </div>
                <div class="apply-form-item pr flex-box">
                    <div class="pr flex upload-pic f12">
                        <span class="dib pr" style="width:.8rem">
                            <i><?=$controller->t('上传有效证件')?></i>
                            <input class="upload-blid" type="file" value="" />
                        </span>
                        <span><?=$controller->t('营业执照、组织机构代码等，图片大小不超过8M')?></span>
                        <input type="hidden" name="passport_photo" value="" />
                        <div class="pa form-tip f14 none" style="left:29%;padding-left:0;bottom:-.22rem;"></div>
                    </div>
                </div>
                <div id="passport_thumb" class="pr flex-box"></div>
                <div class="tc" style="margin-top:.2rem;">
                    <button type="submit" class="primary-btn less-btn"><?=$controller->t('提交申请')?></button>
                </div>
              </form>
            </div>
        </div>
    </div>
    <div class="apply-success tc f14" style="display:none;">
        <p><?=$controller->t('您的公司信息已经提交成功，请耐心等待工作人员进行审核')?></p>
        <p><?=$controller->t('审核结果或通过您的注册邮箱下发通知')?></p>
    </div>
    <?php } ?>
</div>