<?php
/**
 * @var $order_id
 * @var $data array
 */
?>
<ul>
    <?php

    $isCurrentRow = true;
    foreach ($data as $row){
        ?>
        <?php if($isCurrentRow){ $isCurrentRow = false;?>
            <li class="current">
                <em><i></i></em>
                <span><?=date("Y-m-d",$row['time'])?></span>
                <span><?=date("H:i:s",$row['time'])?></span>
                <span><?=$row['remark']?></span>
            </li>
        <?php } else {?>
            <li>
                <span><?=date("Y-m-d",$row['time'])?></span>
                <span><?=date("H:i:s",$row['time'])?></span>
                <span><?=$row['remark']?></span>
            </li>
        <?php }?>
    <?php }?>
</ul>
