<script type="text/tpl" id="compare-tpl">
<div class="global-hd f16 tc">
    {{ if(it.length > 1){ }}
    <a href="javascript:;" class="compare-now"><?=$controller->t("查看对比详情")?></a>
    {{ }else{ }}
    <span><?=$controller->t("查看对比详情")?></span>
    {{ } }}
</div>
<div class="pa compare-panel" style="left:1.8rem;top:0;width:9.25rem;height:1.42rem;">
    {{ if(it.length){}}
    <ul class="pa compare-list clearfix" style="width:12rem">
        {{ for(var i = 0; i < it.length; i++){ }}
        <li data-typeid="{{=it[i].typeId}}" data-goodsid="{{=it[i].goodsId}}">
            <a href="{{=it[i].goodsUrl}}" class="db">
                <div class="thumb"><img src="{{=it[i].goodsImg}}" alt="" /></div>
                <p>{{=it[i].goodsTitle}}</p>
            </a>
            <span class="icon-sprite remove"></span>
        </li>
        {{ } }}
   </ul>
   {{ }else{ }}
   <div class="tc" style="padding-top:.2rem;">
        <img style="width:2rem;opacity:.5" src="/public/assets/images/del_ico.png" alt="" />
        <div class="f14" style="margin-top:.1rem;color:#ccc;"><?=$controller->t("还没有商品哦")?></div>
   </div>
   {{ } }}
</div>
<a class="remove-panel" href="javascript:;"></a>
</script>
<ul class="order-caption fav-option clearfix" noMargin>
    <li><?=$controller->t("商品名称")?></li>
    <li><?=$controller->t("商品信息")?></li>
    <li><?=$controller->t("状态")?></li>
    <li><?=$controller->t("操作")?></li>
</ul>
<?php if($favorite){ ?>
<div class="order-panel fav-panel">
    <div class="order-bd">
        <ul class="order-list clearfix">
            <?php
            foreach ($favorite as $k=>$v)
            {
                ?>
                <li data-typeid="<?=$v['type_id']?>" data-productid="<?=$v['product_id']?>" data-goodsid="<?=$v['goods_id']?>" data-goodstitle="<?=$v['name'] ?>" data-goodsurl="<?="/index.php/product-".$v['product_id'].".html"?>" data-goodsimg="<?=$img_model->get_url($v['image_default_id'])?>">
                    <div class="fl order-goods">
                        <a href="<?="/index.php/product-".$v['product_id'].".html"?>" class="db">
                            <span><img src="<?=$img_model->get_url($v['image_default_id'])?>" alt="" /></span>
                            <p><?=$v['name'] ?></p>
                        </a>
                    </div>
                    <div class="fl order-goods-intro">
                        <p><?=$v['spec_info'] ?></p>
                    </div>
                    <div class="fl tc order-state">
                        <span><?=$controller->t("有货")?></span>
                    </div>
                    <div class="fl tc order-exec js-favorder-exec">
                        <span class="cp add-compare"><?=$controller->t("加入对比")?></span>
                        <span class="cp add-cart"><?=$controller->t("加入购物车")?></span>
                        <span class="cp remove"><?=$controller->t("删除收藏")?></span>
                    </div>
                </li>
            <?php }
            ?>
        </ul>
    </div>
</div>
<?php } ?>
<div class="pagination tc">
    <?php
    $syy = '###';
    $xyy = '###';
    if ($pager['current'] != 1)
    {
        $syy = '/index.php/members-favorite-'.($pager['current']-1).'.html';
    }

    if ($pager['current'] != $pager['total'])
    {
        $xyy = '/index.php/members-favorite-'.($pager['current']+1).'.html';
    }
    ?>
    <span total>
        <?=$controller->t("共{count}页",['count'=>intval($pager['total'])])?>
                </span>
    <a href="<?=$syy?>"><?=$controller->t("上一页")?></a>
    <a href="<?=$syy?>" prev></a>
    <span pageNum>
                <?php
                for ($i = 1; $i <= $pager['total']; $i++)
                {

                    if ($i == $pager['current']) {
                        ?>

                        <b><?=$i?></b>
                        <?php
                    }
                    else {
                        ?>
                        <a href="<?='/index.php/members-favorite-'.$i.'.html'?>"><?=$i?></a>
                        <?php
                    }
                }
                ?>
                </span>
    <a href="<?=$xyy?>" next></a>
    <a href="<?=$xyy?>"><?=$controller->t("下一页")?></a>
</div>
<div class="global-nav cp">
    <img src="/public/assets/images/mico_st.gif" alt="" />
    <img class="mico " src="/public/assets/images/mico.png" alt="" />
    <span><?=$controller->t("添加成功")?></span>
</div>
<div class="global-panel"></div>