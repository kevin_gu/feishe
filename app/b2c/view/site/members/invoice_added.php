<?php
/**
 * @var base_controller $controller
 * @var $invoices
 */
?>
<ul class="order-tab clearfix">
    <li class="current"><a href="javascript:;"><?=$controller->t("开票信息")?></a></li>
    <li><a href="/index.php/members-invoice-address.html"><?=$controller->t("寄送地址管理")?></a></li>
</ul>
<ul class="order-tab clearfix" subTab>
    <li><a href="/index.php/members-invoice-normal.html"><?=$controller->t("普通发票")?></a></li>
    <li class="current"><a href="javascript:;"><?=$controller->t("增值税专用发票")?></a></li>
</ul>
<div class="vat-invoice clearfix js-invoice-panel">
    <?php foreach($invoices as $item){?>
    <div class="fl pr invoice">
        <div class="receiver-panel">
            <div class="receiver-hd"><?=$controller->t("发票信息")?></div>
            <div class="receiver-item"><?=$controller->t("公司名称")?>：<?=$item['tax_company']?></div>
            <div class="receiver-item"><?=$controller->t("税号")?>：<?=$item['tax_code']?></div>
            <div class="receiver-item"><?=$controller->t("开户银行")?>：<?=$item['tax_bank']?></div>
            <div class="receiver-item"><?=$controller->t("开户银行账号")?>：<?=$item['bank_account']?></div>
            <div class="receiver-item"><?=$controller->t("注册电话")?>：<?=$item['tax_company_tel']?></div>
            <div class="receiver-item flex-box">
                <span><?=$controller->t("注册地址")?>：</span>
                <span class="flex"><?=$item['tax_addr']?></span>
            </div>
        </div>
        <a href="javascript:;" class="pa tc js-edit-invoice" data-value="company_added" data-invoice_source="members" data-id="<?=$item['id']?>" edit><?=$controller->t("修改")?></a>
        <a href="javascript:;" class="pa tc js-remove-invoice" data-id="<?=$item['id']?>" edit remove><?=$controller->t("删除")?></a>
    </div>
    <?php }?>
</div>
<div class="tc" style="margin-top:.1rem;">
    <a href="javascript:;" data-value="company_added" data-invoice_source="members" class="dib primary-btn less-btn js-add-company"><?=$controller->t("新增发票")?></a>
</div>
<script id="poplayer-tpl" type="text/tpl">
    <div class="pop-layer fadeInDown">
        <div class="pop-layer-inner pr">
            <div class="pop-layer-title f16">{{=it.popTitle}}</div>
            <div class="pop-layer-bd"></div>
            <div class="loading"></div>
        </div>
        <a href="javascript:;" class="pa pop-layer-close"></a>
    </div>
</script>
<script id="invoice-tpl" type="text/tpl">
    <div class="fl pr invoice">
        <div class="receiver-panel">
            <div class="receiver-hd"><?=$controller->t("发票信息")?></div>
            <div class="receiver-item"><?=$controller->t("公司名称")?>：{{=it.tax_company}}</div>
            <div class="receiver-item"><?=$controller->t("税号")?>：{{=it.tax_code}}</div>
            <div class="receiver-item"><?=$controller->t("开户银行")?>：{{=it.tax_bank}}</div>
            <div class="receiver-item"><?=$controller->t("开户银行账号")?>：{{=it.bank_account}}</div>
            <div class="receiver-item"><?=$controller->t("注册电话")?>：{{=it.tax_company_tel}}</div>
            <div class="receiver-item flex-box">
                <span><?=$controller->t("注册地址")?>：</span>
                <span class="flex">{{=it.tax_addr}}</span>
            </div>
        </div>
        <a href="javascript:;" class="pa tc js-edit-invoice" data-value="company_added" data-invoice_source="members" data-id="{{=it.id}}" edit><?=$controller->t("修改")?></a>
        <a href="javascript:;" class="pa tc js-remove-invoice" data-id="{{=it.id}}" edit remove><?=$controller->t("删除")?></a>
    </div>
</script>
<script id="address-tpl" type="text/tpl">
   {{ if(it.type == 'company'){ }}
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:12px;">*</span><?=$controller->t("公司名称")?>：</div>
        <div class="flex pr">
            <input type="text" value="{{! it.data ? it.data.tax_company : ''}}" class="name-input" placeholder="请输入公司名称" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:42px;">*</span><?=$controller->t("税号")?>：</div>
        <div class="flex pr">
            <input type="text" value="{{! it.data ? it.data.tax_code : ''}}" class="tax-input" placeholder="请输入纳税人识别号" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    {{ if(it.companyType != 'company_normal'){ }}
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:12px;">*</span><?=$controller->t("公司地址")?>：</div>
        <div class="flex pr">
            <input type="text" value="{{! it.data ? it.data.tax_addr : ''}}" class="address-input" placeholder="请输入公司地址" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:26px;">*</span><?=$controller->t("开户行")?>：</div>
        <div class="flex pr">
            <input type="text" value="{{! it.data ? it.data.tax_bank : ''}}" class="bank-input" placeholder="请输入开户行名称" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:42px;">*</span><?=$controller->t("账号")?>：</div>
        <div class="flex pr">
            <input type="text" value="{{! it.data ? it.data.bank_account : ''}}" class="account-input" placeholder="请输入开户行账号" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><?=$controller->t("公司电话")?>：</div>
        <div class="flex pr">
            <input type="text" value="{{! it.data ? it.data.tax_company_tel : ''}}" class="phone-input" placeholder="固话格式：010-88888888(选填)" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    {{ } }}
    <div class="flex-box pop-layer-wrap">
        <button class="less-btn" data-type="{{=it.companyType}}" data-invoice_source="{{=it.source}}" data-company_id="{{=it.companyId}}" style="margin:0 auto;" type="button">{{! it.companyId ? '确认修改' : '确认添加'}}</button>
    </div>
   {{ } }}
</script>
<div id="hook"></div>