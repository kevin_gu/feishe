<?php
/**
 * @var base_controller $controller
 * @var $member_invoice_addr_list array
 */
/**
 * @param $attrs
 * @return string
 */
$ship_area = function($attrs) {
    if(!strpos($attrs,',') || false!==strpos($attrs,',')){
        return $attrs = app::get('ectools')->model('regions')->change_regions_data($attrs);
    }
    return '';
};
$controller->pre_footer_js=['area.js'];
?>
<ul class="order-tab clearfix">
    <li><a href="/index.php/members-invoice-normal.html"><?=$controller->t("开票信息")?></a></li>
    <li class="current"><a href="javascript:;"><?=$controller->t("寄送地址管理")?></a></li>
</ul>
<ul class="order-caption fav-option clearfix">
    <li><?=$controller->t("地址")?></li>
    <li><?=$controller->t("收货人")?></li>
    <li><?=$controller->t("联系电话")?></li>
    <li><?=$controller->t("操作")?></li>
</ul>
<div class="order-panel invoice-addr-panel">
    <div class="order-bd">
        <ul class="order-list clearfix js-address-panel">
            <li>
                <div><?=$controller->t("已有<span id=\"address-amount\">{count}</span>个寄送发票地址,（最多添加10个）",['count'=>count($member_invoice_addr_list)])?></div>
            </li>
            <?php foreach ($member_invoice_addr_list as $addr){
                $isDefault = $addr['def_addr'];
                ?>
                <li>
                    <div class="fl order-goods">
                        <?=$ship_area($addr['area'] ). $addr['addr']?>
                    </div>
                    <div class="fl tc order-goods-intro">
                        <?=$addr['name']?>
                    </div>
                    <div class="fl tc order-state">
                        <?=$addr['mobile']?>
                    </div>
                    <div class="fl tc order-exec">
                        <!--<em <?=$isDefault?'class="current" ':''?>data-default="<?=$addr['addr_id']?>"><?=$controller->t("默认地址")?></em>-->
                        <a class="js-edit-address" href="javascript:;" data-value="invoice" data-addr_id="<?=$addr['addr_id']?>" data-area="<?=$addr['area']?>"><?=$controller->t("编辑")?></a>
                        <a class="js-remove-address" href="javascript:;" data-addr_id="<?=$addr['addr_id']?>" data-area="<?=$addr['area']?>"><?=$controller->t("删除")?></a>
                    </div>
                </li>
            <?php }?>
        </ul>
    </div>
    <div class="tc" style="padding-bottom:30px;">
        <a href="javascript:;" data-value="invoice" class="dib primary-btn less-btn js-add-address"><?=$controller->t("添加地址")?></a>
    </div>
</div>
<script id="poplayer-tpl" type="text/tpl">
    <div class="pop-layer fadeInDown">
        <div class="pop-layer-inner pr">
            <div class="pop-layer-title f16">{{=it.popTitle}}</div>
            <div class="pop-layer-bd"></div>
            <div class="loading"></div>
        </div>
        <a href="javascript:;" class="pa pop-layer-close"></a>
    </div>
</script>
<script id="address-tpl" type="text/tpl">
{{ if(it.type == 'address'){ }}
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:12px;">*</span><?=$controller->t("收货地区")?>：</div>
        <div class="flex clearfix f14 pr">
            <div class="pr fl select-panel js-select-panel">
                <span class="pa selected">{{! it.data ? it.data.area.province.selected : '请选择'}}</span>
                <ul class="pa select-list scrollbar province none">
                    <li data-value="-1"><?=$controller->t("请选择")?></li>
                    {{ for(var i = 0; i < it.province.length; i++){ }}
                    <li data-value="{{=it.province[i].split(':')[2]}}">{{=it.province[i].split(':')[0]}}</li>
                    {{ } }}
                </ul>
                <i class="select-arrow"></i>
                <input type="hidden" name="province" class="province-hidden" value="{{! it.data ? it.data.area.province.selected : '请选择:-1'}}" />
            </div>
            <div class="pr fl select-panel js-select-panel">
                <span class="pa selected">{{! it.data ? it.data.area.city.selected : '请选择'}}</span>
                <ul class="pa select-list scrollbar city none">
                   <li data-value="-1"><?=$controller->t("请选择")?></li>
                   {{ if(it.data){ }}
                       {{ for(var m = 0; m < it.data.area.city.list.length; m++){ }}
                        <li data-value="{{=it.data.area.city.list[m].split(':')[2]}}">{{=it.data.area.city.list[m].split(':')[0]}}</li>
                       {{ } }}
                   {{ } }}
                </ul>
                <i class="select-arrow"></i>
                <input type="hidden" name="city" class="city-hidden" value="{{! it.data ? it.data.area.city.selected : '请选择:-1'}}" />
            </div>
            <div class="pr fl select-panel js-select-panel">
                <span class="pa selected">{{! it.data ? it.data.area.county.selected.split(':')[0] : '请选择'}}</span>
                <ul class="pa select-list scrollbar county none">
                    <li data-value="-1"><?=$controller->t("请选择")?></li>
                    {{ if(it.data){ }}
                       {{ for(var n = 0; n < it.data.area.county.list.length; n++){ }}
                        <li data-value="{{=it.data.area.county.list[n].split(':')[1]}}">{{=it.data.area.county.list[n].split(':')[0]}}</li>
                       {{ } }}
                   {{ } }}
                </ul>
                <i class="select-arrow"></i>
                <input type="hidden" name="county" class="county-hidden" value="{{! it.data ? it.data.area.county.selected : '请选择:-1'}}" />
            </div>
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:12px;">*</span><?=$controller->t("详细地址")?>：</div>
        <div class="flex pr">
            <input type="text" class="address-input" value="{{! it.data ? it.data.addr : ''}}" placeholder="请输入详细地址" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:-4px;">*</span><?=$controller->t("收货人姓名")?>：</div>
        <div class="flex pr">
            <input type="text" class="name-input" value="{{! it.data ? it.data.name : ''}}" placeholder="请输入收货人姓名" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:42px;">*</span><?=$controller->t("手机")?>：</div>
        <div class="flex pr">
            <input type="text" class="mobile-input" value="{{! it.data ? it.data.mobile : ''}}" placeholder="请输入手机号" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label"><?=$controller->t("固定电话")?>：</div>
        <div class="flex pr">
            <input type="text" class="phone-input" value="{{! it.data ? it.data.tel : ''}}" placeholder="固话格式：010-88888888(选填)" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label"><?=$controller->t("邮编")?>：</div>
        <div class="flex pr">
            <input type="text" class="post-input" value="{{! it.data ? it.data.zip : ''}}" placeholder="请输入邮编(选填)" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    {{ if(it.addressType == 'order'){ }}
    <div class="flex-box" style="margin-top:15px;">
        <div class="pop-layer-label" style="margin-left:22px;line-height:normal">
        {{ if(it.data && it.data.isDefault == '1'){}}
            <span class="checkbox checkbox-checked" style="margin-right:10px;"></span>
            <input type="hidden" name="default" class="set-default" value="{{=it.data.isDefault}}" />
        {{ }else{ }}
            <span class="checkbox" style="margin-right:10px;"></span>
            <input type="hidden" name="default" class="set-default" value="0" />
        {{ } }}
        </div>
        <div class="flex f14" style="line-height:22px;">
            设为默认地址
        </div>
     {{ } }}
    </div>
    <div class="flex-box pop-layer-wrap">
        <button class="less-btn" style="margin:0 auto;" data-addr_id="{{! it.data ? it.data.addressId : ''}}" data-area_id="{{! it.data ? it.data.areaId : ''}}" data-address_type="{{=it.addressType}}" data-address_source="members" type="button">{{! it.data ? '确认修改' : '确认添加'}}</button>
    </div>
  {{ } }}
</script>
<div id="hook"></div>