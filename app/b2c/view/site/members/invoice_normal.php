<?php
/**
 * @var base_controller $controller
 * @var $invoices
 */
?>
<ul class="order-tab clearfix">
    <li class="current"><a href="javascript:;"><?=$controller->t("开票信息")?></a></li>
    <li><a href="/index.php/members-invoice-address.html"><?=$controller->t("寄送地址管理")?></a></li>
</ul>
<ul class="order-tab clearfix" subTab>
    <li class="current"><a href="javascript:;"><?=$controller->t("普通发票")?></a></li>
    <li><a href="/index.php/members-invoice-added.html"><?=$controller->t("增值税专用发票")?></a></li>
</ul>
<div class="bill-panel">
    <?php if($invoices){ ?>
    <ul class="clearfix f14 js-invoice-panel">
        <?php foreach($invoices as $item){?>
        <li>
            <div class="fl bill-title"><span><?=$item['tax_company']?></span></div>
            <div class="fl bill-no"><span><?=$item['tax_code']?></span></div>
            <div class="fl bill-exec">
                <a class="js-edit-invoice" href="javascript:;" data-value="company_normal" data-invoice_source="members" data-id="<?=$item['id']?>" edit><?=$controller->t("修改")?></a>
                <a class="js-remove-invoice" href="javascript:;" data-id="<?=$item['id']?>" remove><?=$controller->t("删除")?></a>
            </div>
        </li>
        <?php }?>
    </ul>
    <?php } ?>
    <div class="tc">
        <a href="javascript:;" data-value="company_normal" data-invoice_source="members" class="dib primary-btn less-btn js-add-company"><?=$controller->t("新增发票")?></a>
    </div>
</div>
<script id="poplayer-tpl" type="text/tpl">
    <div class="pop-layer fadeInDown">
        <div class="pop-layer-inner pr">
            <div class="pop-layer-title f16">{{=it.popTitle}}</div>
            <div class="pop-layer-bd"></div>
            <div class="loading"></div>
        </div>
        <a href="javascript:;" class="pa pop-layer-close"></a>
    </div>
</script>
<script id="invoice-tpl" type="text/tpl">
<li>
    <div class="fl bill-title"><span>{{=it.tax_company}}</span></div>
    <div class="fl bill-no"><span>{{=it.tax_code}}</span></div>
    <div class="fl bill-exec">
        <a class="js-edit-invoice" href="javascript:;" data-value="company_normal" data-invoice_source="members" data-id="{{=it.id}}" edit><?=$controller->t("修改")?></a>
        <a class="js-remove-invoice" href="javascript:;" data-id="{{=it.id}}" remove><?=$controller->t("删除")?></a>
    </div>
</li>
</script>
<script id="address-tpl" type="text/tpl">
   {{ if(it.type == 'company'){ }}
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:12px;">*</span><?=$controller->t("公司名称")?>：</div>
        <div class="flex pr">
            <input type="text" value="{{! it.data ? it.data.tax_company : ''}}" class="name-input" placeholder="请输入公司名称" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap" style="margin-bottom:60px">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:42px;">*</span><?=$controller->t("税号")?>：</div>
        <div class="flex pr">
            <input type="text" value="{{! it.data ? it.data.tax_code : ''}}" class="tax-input" placeholder="请输入纳税人识别号" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    {{ if(it.companyType != 'company_normal'){ }}
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:12px;">*</span><?=$controller->t("公司地址")?>：</div>
        <div class="flex pr">
            <input type="text" value="{{! it.data ? it.data.tax_addr : ''}}" class="address-input" placeholder="请输入公司地址" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:26px;">*</span><?=$controller->t("开户行")?>：</div>
        <div class="flex pr">
            <input type="text" value="{{! it.data ? it.data.tax_bank : ''}}" class="bank-input" placeholder="请输入开户行名称" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:42px;">*</span><?=$controller->t("账号")?>：</div>
        <div class="flex pr">
            <input type="text" value="{{! it.data ? it.data.tax_account : ''}}" class="account-input" placeholder="请输入开户行账号" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><?=$controller->t("公司电话")?>：</div>
        <div class="flex pr">
            <input type="text" value="{{! it.data ? it.data.tax_company_tel : ''}}" class="phone-input" placeholder="固话格式：010-88888888(选填)" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    {{ } }}
    <div class="flex-box pop-layer-wrap">
        <button class="less-btn" data-type="{{=it.companyType}}" data-invoice_source="{{=it.source}}" data-company_id="{{=it.companyId}}" style="margin:0 auto;" type="button">{{! it.companyId ? '确认修改' : '确认添加'}}</button>
    </div>
   {{ } }}
</script>
<div id="hook"></div>