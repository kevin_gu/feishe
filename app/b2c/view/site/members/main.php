<?php
/**
 * @var base_controller $__controller
 */
$__controller->bodyClass = 'bg-fb header-holder';
/** @var $_PAGE_ */
/** @var $cpmenu */
/** @var $current */
/** @var $controller */
/** @var $hiddenLeftMenu */

$currentMenu = $cpmenu[0];
$currentMenuIndex = 0;
foreach($cpmenu as $menu){
    foreach ($menu['items'] as $k=>$item){
        if($item['link'] == $__controller->action){
            $currentMenu = $menu;
            $currentMenuIndex = $k;
            break;
        }
    }
}
if(in_array($__controller->action,['inbox','send','track','outbox','message']) ){
    $currentMenuIndex = 1;
    $currentMenu = $cpmenu[2];
}
?>
<?=$__controller->renderPartial('/header.php',['headerClass'=>'header-hasfix'])?>
<?php if($hiddenLeftMenu){?>
    <?=$__controller->renderPartial($_PAGE_,$controller->pagedata)?>
<?php } else {?>
<div class="oh main">
    <div class="oh wrapper">
        <div class="container" goodsList>
            <div class="crumbs-panel">
                <a href="/index.php/members-orders.html" class="vm"><?=$__controller->t('会员中心')?></a>
                <i class="icon-sprite vm"></i>
                <a href="/index.php/members-<?=$currentMenu['items'][0]['link']?>.html" class="vm"><?=$__controller->t($currentMenu['label'])?></a>
                <i class="icon-sprite vm"></i>
                <span class="vm"><?=$__controller->t($currentMenu['items'][$currentMenuIndex]['label'])?></span>
            </div>
        </div>
            <div class="container clearfix" goodsList>
                <div class="fl order-side">
                    <?php foreach($cpmenu as $menus){?>
                    <dl class="order-nav">
                        <dt><?=$menus['label']?></dt>
                        <?php foreach ($menus['items'] as $menu){
                             $link = $menu['href'];
                             if(!$link){
                                 $link =  $__controller->link(['app'=>$menu['app'], 'ctl'=>$menu['ctl'],'act'=>$menu['link'],'args'=>$menu['args']]);
                             }
                             $isActive = $current == $menu['link'] || $menu['link'] == $controller;

                             if(in_array($__controller->action,['inbox','send','track','outbox','message'])
                                && $menu['link'] == 'inbox'
                             ){
                                 $isActive = true;
                             }
                            ?>
                            <dd <?=$isActive?'class="current"':''?>><span><a href="<?=$link?>"><?=$menu['label']?></a></span></dd>
                        <?php }?>
                    </dl>
                    <?php }?>
                </div>
                <div class="fl order-main">
                    <?=$__controller->renderPartial($_PAGE_,$controller->pagedata)?>
                </div>
            </div>
    </div>
</div>
<?php }?>
<?=$__controller->renderPartial('/footer.php')?>
