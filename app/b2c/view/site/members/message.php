<ul class="order-tab clearfix">
    <li><a href="/index.php/members-inbox.html">收件箱</a></li>
    <li><a href="/index.php/members-send.html">发消息</a></li>
    <li><a href="/index.php/members-track.html">发件箱</a></li>
    <li><a href="/index.php/members-outbox.html">草稿箱</a></li>
    <li class="current"><a href="javascript:;">给管理员发消息</a></li>
</ul>
<style type="text/css">
.letter-label{width:.8rem;}
.letter-input{width: 3.5rem;height: .3rem;line-height:.3rem;border: 1px solid #eee;margin-top: -.1rem;padding-left: .05rem;}
.message-panel .reply-content .message-exec{padding-left:.9rem}
</style>
<div class="order-panel message-panel">
    <div class="order-bd">
        <div class="message-content reply-content js-message-content oh">
            <div class="flex-box reply-user">
                <div class="letter-label tr">收件人：</div>
                <div class="flex">管理员</div>
            </div>
            <div class="flex-box reply-title">
                <div class="letter-label tr">标题：</div>
                <div class="flex"><input type="text" name="send-title" class="letter-input" placeholder="请输入标题" value="" /></div>
            </div>
            <div class="flex-box message-yar">
                <div class="letter-label tr">内容：</div>
                <div class="flex"><textarea class="reply-area" placeholder="请输入发送内容"></textarea></div>
            </div>
            <div class="flex-box message-exec">
                <a href="javascript:;" class="dib indraft js-save-indraft">
                    <em class="dib vm">保存至草稿箱</em>
                </a>
                <a href="javascript:;" class="dib send js-send-letter">
                    <em class="dib vm">发送</em>
                </a>
            </div>
        </div>
    </div>
</div>