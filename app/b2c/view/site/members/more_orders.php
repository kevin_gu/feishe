<?php
/**
 * @var base_controller $controller
 * @var $offers array
 * @var $pager array
 */
$is_white = $controller->getMemberIsWhite();
?>
<ul class="order-caption clearfix" noMargin>
    <li><?=$controller->t("报价单信息")?></li>
    <li><?=$controller->t("作者")?></li>
    <li><?=$controller->t("操作")?></li>
</ul>
<?php if($offers){?>
<div class="order-panel">
    <div class="order-bd">
        <ul class="order-list clearfix">
            <?php foreach($offers as $offer){
                $thumb = b2c_arrayhelper::getValue($offer,'data.items.0.thumbnail_pic');
                $name = b2c_arrayhelper::getValue($offer,'data.items.0.name');
                $share_id = $offer['id'];
                $link = b2c_arrayhelper::getValue($offer,'data.items.0.link_url');
                $goods_id = b2c_arrayhelper::getColumn($offer['data']['items'],'goods_id');
                if($is_white){
                    $files = b2c_mdl_orders::getGoodsDownloadFiles($goods_id);
                }
                ?>
            <li>
                <div class="fl order-goods">
                    <a href="<?=$link?>" class="db">
                        <span><img src="<?=$controller->image_path($thumb)?>" alt="" /></span>
                        <p><?=$name?></p>
                    </a>
                </div>
                <div class="fl order-goods-amount"></div>
                <div class="fl tc order-state">
                    <span><?=$offer['owner']?></span>
                </div>
                <div class="fl pr tc order-exec">
                    <a href="/index.php/members-offer_share_detail-<?=$share_id?>.html"><?=$controller->t("查看报价单")?></a>
                    <?php if($is_white){?>
                    <a href="javascript:;" class="js-download-means"><?=$controller->t("下载资料")?></a>
                    <div class="pa tl download-panel none">
                        <dl class="scrollbar">
                            <?php foreach ($files as $file){?>
                            <dd><a href="<?=$file['file_path']?>" target="_blank"><?=$file['file_name']?></a></dd>
                            <?php }?>
                        </dl>
                    </div>
                    <?php }?>
                </div>
            </li>
            <?php }?>
        </ul>
    </div>
</div>
<?php } ?>
<?=$controller->pager($pager)?>
