<?php
/**
 * @var base_controller $controller
 * @var $order array
 * @var $offer array
 */
$memberId = $controller->getMemberId();
$sharedMembers = b2c_mdl_order_share::getAvailableMembers($memberId,$offer['id']);
?>
<div class="oh main">
    <div class="wrapper">
        <div class="container" goodsList>
            <div class="pr quota-detail">
                <h2><?=$offer['name']?></h2>
                <ul>
                    <li><?=$controller->t("日期")?>：<?=date("Y-m-d",$offer['created_at'])?></li>
                    <li><?=$controller->t("报价单号")?>：<?=$order['order_id'].'_'.$order['offer_count']?></li>
                    <li><?=$controller->t("报价版本")?>：V<?=$order['offer_count']?></li>
                    <li><?=$controller->t("客户名称")?>：用户名称</li>
                </ul>
                <dl>
                    <dt><?=$controller->t("报价说明")?></dt>
                    <dd><?=$controller->t("1.此报价单包含产品单价，产品总价")?></dd>
                    <dd><?=$controller->t("2.报价有效期为自报价之后3个月内")?></dd>
                    <dd><?=$controller->t("3.付款方式：保证金为在线支付，请报价后3个月内进行支付；首款和尾款请打至公司账户")?></dd>
                    <dd><?=$controller->t("4.对报价单有异议，请支付保证金后进行议价")?></dd>
                    <dd><?=$controller->t("5.对报价单无异议，请支付保证金后确认报价")?></dd>
                </dl>
                <div class="pa share-quota js-share-order hide">
                    <span><?=$controller->t("分享此报价单")?></span>
                </div>
                <div class="pa friend-panel" style="display:none">
                    <div class="friend-hd clearfix">
                        <span class="fl f16"><?=$controller->t("好友列表")?></span>
                        <!--<span class="fr f14"><em class="dib vm"><?=$controller->t("收起")?></em></span>-->
                    </div>
                    <div class="friend-list">
                        <ul class="clearfix">
                            <?php foreach($sharedMembers as $sharedMember){?>
                            <li>
                                <div class="fl friend-photo"><img src="../public/assets/images/up.jpg" alt="" /></div>
                                <div class="fl friend-infor">
                                    <p><?=$sharedMember['name']?></p>
                                    <p><?=$sharedMember['email']?></p>
                                </div>
                                <div class="fr pr friend-check">
                                    <i class="pa checkbox" data-id="<?=$sharedMember['member_id']?>"></i>
                                </div>
                            </li>
                            <?php }?>
                        </ul>
                    </div>
                    <div class="quota-nickname">
                        <div class="f14"><?=$controller->t("报价单别名")?></div>
                        <div class="nick-input"><input type="text" value="" placeholder="给报价单起个别名吧（选填）" data-order_id="<?=$offer['order_id']?>" data-offer_id="<?=$offer['id']?>" /></div>
                        <a href="javascript:;" class="db f16 tc save"><?=$controller->t("确定")?></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="bg-3f">
                <div class="crumbs-panel clearfix" bg-3e>
                    <div class="fl quota-caption"><?=$controller->t("商品清单")?></div>
                    <div class="fl quota-caption"><?=$controller->t("数量")?></div>
                    <div class="fl quota-caption"><?=$controller->t("报价")?></div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="quota-panel">
                <ul class="quota-list clearfix">
                    <?php foreach ($offer['items'] as $goods_item){
                    $link = b2c_arrayhelper::getValue($goods_item,'link_url');
                    $thumb = b2c_arrayhelper::getValue($goods_item,'thumbnail_pic');
                    $name = b2c_arrayhelper::getValue($goods_item,'name');
                    $quantity = b2c_arrayhelper::getValue($goods_item,'nums');
                    $amount = $goods_item['amount'];
                    ?>
                    <li>
                        <div class="fl quota-goods">
                            <a href="<?=$link?>" class="db">
                                <span><img src="<?=$controller->image_path($thumb)?>" alt="" /></span>
                                <p><?=$name?></p>
                            </a>
                        </div>
                        <div class="fl quota-goods-intro">
                            <?php foreach(b2c_mdl_orders::formatProductSpecInfo($goods_item['spec_info']) as $spec_info){?>
                            <p><?=$spec_info?></p>
                            <?php }?>
                            <?php if(false){?>
                            <p><?=$controller->t("税务编号")?>：<?=$goods_item['tax_number']?></p>
                            <?php }?>
                        </div>
                        <div class="fl quota-goods-amount">x<?=$quantity?></div>
                        <div class="fl tc quota-state">
                            <span>￥<?=$amount?></span>
                        </div>
                    </li>
                    <?php }?>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="quota-panel quota-res">
                <ul class="tr">
                    <li>
                        <span><?=$controller->t("商品总报价")?>：</span>
                        <span>￥<?=$order['deposit']+$offer['down_payment']+$offer['final_payment']?></span>
                    </li>
                    <li>
                        <span><?=$controller->t("保证金")?>：</span>
                        <span>￥<?=$order['deposit']?></span>
                    </li>
                    <li>
                        <span><?=$controller->t("首款")?>：</span>
                        <span>￥<?=$offer['down_payment']?></span>
                    </li>
                    <li>
                        <span><?=$controller->t("尾款")?>：</span>
                        <span>￥<?=$offer['final_payment']?></span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="quota-detail">
                <dl>
                    <dt><?=$controller->t("备注")?></dt>
                    <?php foreach (explode("\n",$offer['remarks']) as $line){?>
                    <dd><?=$line?></dd>
                    <?php }?>
                </dl>
            </div>
        </div>
        <?php if($order['order_current_status'] == b2c_mdl_orders::ORDER_STATUS_WAIT_CONFIRM){?>
        <div class="container">
            <div class="clearfix tc">
                <div class="fl quota-btn"><a href="/index.php/members-offer_confirm-<?=$order['order_id']?>-no.html"><?=$controller->t("对报价有异议")?></a></div>
                <div class="fl quota-btn"><a href="/index.php/members-offer_confirm-<?=$order['order_id']?>-yes.html"><?=$controller->t("确认报价")?></a></div>
            </div>
        </div>
        <?php }?>
    </div>
</div>