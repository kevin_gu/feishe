<?php
/**
 * @var base_controller $controller
 * @var $order array
 * @var $offer array
 */
$current_status = $order['order_current_status'];
if($current_status == b2c_mdl_orders::ORDER_STATUS_WAIT_ASSIGN){
    $current_status = b2c_mdl_orders::ORDER_STATUS_WAIT_OFFER;
}
$order_status =  $order['order_status'];
$step_active_class =  'current';
$step = 0;
$step_tips  = [
        1=>'订单已提交成功，我们将在7个工作日内对您的订单完成报价！',
        2=>'您的订单报价已完成，请您支付保证金后进行下一步操作。',
        3=>'您的订单保证金已支付，请对报价单进行确认后进行下一步操作。',
        4=>'您的报价已确认，请您在 <span id="counter-down" data-seconds="SECOND" style="color:#964F4C"></span> 内支付首款，超时将会产生额外费用。',
        5=>'您的订单已支付首款，商品将开始进行生产，请等待生产完成后进行尾款支付',
        6=>'您的商品已生产完成，请您在<span id="counter-down" data-seconds="SECOND" style="color:#964F4C"></span>内支付尾款，超时将会产生额外费用，支付尾款后商品将发货。',
        7=>'您的订单已支付尾款，请等待商品发货',
        8=>'您的订单已发货，请等待收货，您可以进行查看物流状态操作',
        9=>'您的订单已完成'
];
$product_txt = '';
$deadLine = false;
$step_class = [1=>$step_active_class,2=>'',3=>'',4=>'',5=>'',6=>'',7=>'',8=>'',9=>''];
if($current_status < b2c_mdl_orders::ORDER_STATUS_WAIT_DEPOSIT  && $order_status <  b2c_mdl_orders::ORDER_STATUS_WAIT_DEPOSIT){
    $step = 1;
}else if(b2c_mdl_orders::ORDER_STATUS_WAIT_DEPOSIT == $current_status){
    $step = 2;
}else if(in_array($order_status,[b2c_mdl_orders::ORDER_STATUS_WAIT_CONFIRM])){
    $step = 3;
}else if(b2c_mdl_orders::ORDER_STATUS_WAIT_DOWN_PAYMENT == $order_status){
    $step = 4;
    $left = b2c_mdl_orders::getLeftSeconds($order);
    if($left){
        $step_tips[4] = str_replace('SECOND',$left,$step_tips[4]);
    }else{
        $deadLine = true;
        $step_tips[4] = '首款支付超时，请联系管理员';
    }
}else if(in_array($current_status,[b2c_mdl_orders::ORDER_STATUS_WAIT_PRODUCT,b2c_mdl_orders::ORDER_STATUS_PRODUCTION])){
    $step = 5;
    if($current_status == b2c_mdl_orders::ORDER_STATUS_WAIT_PRODUCT){
        $product_txt = '待生产';
    }else{
        $product_txt = '生产中';
    }

}else if($order_status == b2c_mdl_orders::ORDER_STATUS_WAIT_FINAL_PAYMENT ){
    $step = 6;
    $left = b2c_mdl_orders::getLeftSeconds($order);
    if($left){
        $step_tips[6] = str_replace('SECOND',$left,$step_tips[4]);
    }else{
        $deadLine = true;
        $step_tips[4] = '尾款支付超时，请联系管理员';
    }
}else if($order_status == b2c_mdl_orders::ORDER_STATUS_WAIT_SHIPPED){
    $step = 7;
}else if($order_status == b2c_mdl_orders::ORDER_STATUS_WAIT_RECEIVED){
    $step = 8;
}else if($order_status == b2c_mdl_orders::ORDER_STATUS_FINISHED){
    $step = 9;
}
for($i=1;$i<=$step;$i++){
    $step_class[$i] = $step_active_class;
}
$date_time_span = function($name,$condition)use($order){
    if($condition){
    $html  = '<span>'.date("Y-m-d",$order[$name]).'</span>';
    $html .= '<span>'.date("H:i:s",$order[$name]).'</span>';
    }else{
        $html = '<span></span><span></span>';
    }
    return $html;
};
$consignee = $order['consignee'];
foreach($step_tips as $k=>$v){
    $step_tips[$k] = $controller->t($v);
}
?>
<div class="oh main">
    <div class="oh wrapper">
        <div class="container" goodsList>
            <div class="crumbs-panel">
                <a href="/index.php/members-orders.html" class="vm"><?=$controller->t("会员中心")?></a>
                <i class="icon-sprite vm"></i>
                <a href="/index.php/members-orders.html" class="vm"><?=$controller->t("订单管理")?></a>
                <i class="icon-sprite vm"></i>
                <a href="/index.php/members-orders.html" class="vm"><?=$controller->t("我的订单")?></a>
                <i class="icon-sprite vm"></i>
                <span class="vm"><?=$controller->t("订单详情")?></span>
            </div>
        </div>
        <div class="container clearfix" goodsList>
            <div class="fl order-overview">
                <div class="tc order-sign"><?=$controller->t("订单号")?>：<?=$order['order_id']?></div>
                <div class="tc order-state"><?=b2c_mdl_orders::getOrderStatusTxtForUser($current_status)?></div>
                <div class="tc order-exec">
                    <?php if(b2c_mdl_orders::ORDER_STATUS_WAIT_OFFER != $current_status){?>
                        <span><a href="/index.php/members-offer_self_detail-<?=$order['order_id']?>-<?=$offer['id']?>.html"><?=$controller->t("查看报价单")?></a></span>
                    <?php } ?>
                    <?php if(b2c_mdl_orders::ORDER_STATUS_WAIT_DEPOSIT == $current_status){?>
                    <span><a href="/index.php/members-pay_deposit-<?=$order['order_id']?>.html"><?=$controller->t("在线支付保证金")?></a></span>
                        <span>   <a href="/index.php/members-pay_deposit_offline-<?=$order['order_id']?>.html"><?=$controller->t("线下支付保证金")?></a></span>
                    <?php } else if(b2c_mdl_orders::ORDER_STATUS_WAIT_CONFIRM == $current_status) {?>
                        <span><a href="/index.php/members-offer_confirm-<?=$order['order_id']?>-yes.html"><?=$controller->t("确认报价")?></a></span>
                        <span><a href="/index.php/members-offer_confirm-<?=$order['order_id']?>-no.html"><?=$controller->t("对报价有异议")?></a></span>
                    <?php } else if(b2c_mdl_orders::ORDER_STATUS_WAIT_DOWN_PAYMENT == $current_status && !$deadLine){?>
                        <span unuse><?=$controller->t("支付首款（线下支付）")?></span>
                        <span><a href="/index.php/members-pay_down_payment-<?=$order['order_id']?>.html"><?=$controller->t("上传付款凭证")?></a></span>
                    <?php } else if(b2c_mdl_orders::ORDER_STATUS_WAIT_FINAL_PAYMENT == $current_status && !$deadLine){?>
                        <span unuse><?=$controller->t("支付尾款（线下支付）")?></span>
                        <span><a href="/index.php/members-pay_final_payment-<?=$order['order_id']?>.html"><?=$controller->t("上传付款凭证")?></a></span>
                    <?php } else if(b2c_mdl_orders::ORDER_STATUS_WAIT_RECEIVED == $current_status){?>
                        <span><a class="js-check-express" href="javascript:;" data-id="<?=$order['order_id']?>"><?=$controller->t("查看物流")?></a></span>
                        <span><a href="/index.php/members-finish-<?=$order['order_id']?>.html"><?=$controller->t("确认收货")?></a></span>
                    <?php } else if(b2c_mdl_orders::ORDER_STATUS_FINISHED == $current_status){?>
                        <span><a class="js-check-express" href="javascript:;" data-id="<?=$order['order_id']?>"><?=$controller->t("查看物流")?></a></span>
                        <?php if(b2c_mdl_orders::AFTER_SALE_STATUS_NO == $order['after_sale_status']){?>
                            <span><a href="/index.php/members-after_sales-<?=$order['order_id']?>.html"><?=$controller->t("申请售后")?></a></span>
                        <?php } else {?>
                            <span unuse><?=$controller->t("已申请售后")?></span>
                        <?php }?>
                    <?php }?>
                </div>
            </div>
            <div class="fl order-process">
                <div class="order-tips"><?=$step_tips[$step]?></div>
                <div class="order-chart">
                    <div class="clearfix">
                        <div class="fl order-step step-1 <?=$step_class[1]?>">
                            <div class="flex-box">
                                <div class="step-panel pr tc">
                                    <span><?=$controller->t("提交订单")?></span>
                                    <?=$date_time_span('createtime',true)?>
                                </div>
                                <div class="step-arrow">
                                    <span><?=$step==1?$controller->t("等待报价"):''?></span>
                                </div>
                            </div>
                        </div>

                        <div class="fl order-step step-2 <?=$step_class[2]?>">
                            <div class="flex-box">
                                <div class="step-panel pr tc">
                                    <span><?=$controller->t("支付保证金")?></span>
                                    <span><?=$step_class[2]?'￥'.$order['deposit']:''?></span>
                                    <?=$date_time_span('deposit_at',$step_class[3])?>
                                </div>
                                <div class="step-arrow">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="fl order-step step-3 <?=$step_class[3]?>">
                            <div class="flex-box">
                                <div class="step-panel pr tc">
                                    <span><?=$controller->t("确认报价")?></span>
                                    <?=$date_time_span('confirm_at',$step_class[4])?>
                                </div>
                                <div class="step-arrow">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="fl order-step step-4 <?=$step_class[4]?>">
                            <div class="flex-box">
                                <div class="step-panel pr tc">
                                    <span><?=$controller->t("支付首款")?></span>
                                    <?=$step_class[5]?'￥'.$offer['down_payment']:''?>
                                    <?=$date_time_span('down_payment_at', $step_class[5])?>
                                </div>
                                <div class="step-arrow">
                                    <span><?=$product_txt?></span>
                                </div>
                            </div>
                        </div>
                        <div class="fl order-step pr step-5 <?=$step_class[6]?>">
                            <div class="flex-box">
                                <div class="step-panel pr tc">
                                    <span><?=$controller->t("生产完成")?></span>
                                    <?=$date_time_span('product_end_at',$step_class[6])?>
                                </div>
                            </div>
                            <div class="pr step-arrow">
                                <span class="tr"></span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="fr order-step step-6 <?=$step_class[6]?>">
                            <div class="flex-box">
                                <div class="step-arrow">
                                    <span class="tr"></span>
                                </div>
                                <div class="step-panel pr tc">
                                    <span><?=$controller->t("支付尾款")?></span>
                                    <?=$step_class[7]?'￥'.$offer['final_payment']:''?>
                                    <?=$date_time_span('final_payment_at',$step_class[7])?>
                                </div>
                            </div>
                        </div>
                        <div class="fr order-step step-7 <?=$step_class[7]?>">
                            <div class="flex-box">
                                <div class="step-arrow">
                                    <span class="tr"></span>
                                </div>
                                <div class="step-panel pr tc">
                                    <span style="margin-top:0.02rem"><?=$controller->t("商品发货")?></span>
                                    <?=$date_time_span('shipping_at',$step_class[8])?>
                                </div>
                            </div>
                        </div>
                        <div class="fr order-step step-8 <?=$step_class[8]?>">
                            <div class="flex-box">
                                <div class="step-arrow">
                                    <span class="tr"></span>
                                </div>
                                <div class="step-panel pr tc">
                                    <span><?=$controller->t("确认收货")?></span>
                                    <?=$date_time_span('finish_at',$step_class[9])?>
                                </div>
                            </div>
                        </div>
                        <div class="fr order-step step-9 <?=$step_class[9]?>">
                            <div class="flex-box">
                                <div class="step-panel pr tc">
                                    <span><?=$controller->t("完成")?></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div class="container clearfix" goodsList>
            <div class="order-textprocess-panel">
                <div class="order-textprocess">
                    <ul>
                        <?php
                        $currentLogClass = 'class="current"';
                        $logs = b2c_mdl_order_log::getCustomerLog($order['order_id']);
                        foreach ($logs as $log){?>
                        <li <?=$currentLogClass?>>
                            <i></i>
                            <span><?=date("Y-m-d",$log['alttime'])?></span>
                            <span><?=date("H:i:s",$log['alttime'])?></span>
                            <span><?=$log['log_text']?></span>
                        </li>
                        <?php
                        $currentLogClass = 'class="none"';
                        }?>
                    </ul>
                    <div class="tr textprocess-hide"><?=$controller->t("展开")?></div>
                </div>
            </div>
        </div>
        <div class="container clearfix" goodsList>
            <div class="fl pr receiver">
                <div class="receiver-panel">
                    <div class="receiver-hd"><?=$controller->t("收货人信息")?></div>
                    <div class="receiver-item"><?=$controller->t("收货人")?>：<?=$consignee['name']?></div>
                    <div class="receiver-item flex-box">
                        <span><?=$controller->t("收货地址")?>：</span>
                        <span class="flex"><?=$consignee['addr']?></span>
                    </div>
                    <div class="receiver-item"><?=$controller->t("手机号")?>：<?=$consignee['mobile']?></div>
                </div>
                <?php if(b2c_mdl_orders::canModifyShippingAddress($order)){?>
                <a href="javascript:;" class="pa tc js-edit-custom" data-id="<?=$order['order_id']?>" edit><?=$controller->t("修改")?></a>
                <?php }?>
            </div>
            <div class="fl invoice">
                <div class="receiver-panel">
                    <div class="receiver-hd"><?=$controller->t("发票信息")?></div>
                    <?php if($order['is_tax'] == 'true'){
                        $tax_type = $order['tax_invoice_type'] == 'normal' ?'普通':'增值税';
                        $tax_title = $order['tax_type']== 'personal' ?'个人':$order['tax_title'][0];
                        $addr = unserialize($order['tax_ship_addr']);
                        ?>
                    <div class="receiver-item"><?=$controller->t("发票类型")?>：<?=$tax_type?><?=$controller->t("发票")?></div>
                    <div class="receiver-item"><?=$controller->t("发票抬头")?>：<?=$tax_title?></div>
                    <div class="receiver-item"><?=$controller->t("发票内容")?>：家具</div>
                    <div class="receiver-item"><?=$controller->t("纳税人识别号")?>：<?=$order['tax_code']?></div>
                    <div class="receiver-item"><?=$controller->t("收票人姓名")?>：<?=$addr['ship_name']?></div>
                    <div class="receiver-item flex-box">
                        <span><?=$controller->t("收票地址")?>：</span>
                        <span class="flex"><?=$addr['ship_addr']?></span>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="container" goodsList>
            <div class="crumbs-panel clearfix">
                <div class="fl quota-caption"><?=$controller->t("商品清单")?></div>
                <div class="fl quota-caption"><?=$controller->t("数量")?></div>
                <div class="fl quota-caption"><?=$controller->t("报价")?></div>
            </div>
        </div>
        <div class="container" goodsList>
            <div class="quota-panel">
                <ul class="quota-list clearfix">
                    <?php
                    $goods_items = $offer ? $offer['items']:$order['goods_items'];
                    foreach ($goods_items as $goods_item){
                        $link = b2c_arrayhelper::getValue($goods_item,'link_url');
                        $thumb = b2c_arrayhelper::getValue($goods_item,'thumbnail_pic');
                        $name = b2c_arrayhelper::getValue($goods_item,'name');
                        $quantity = b2c_arrayhelper::getValue($goods_item,'nums');

                        if($offer){
                            $product = $goods_item;
                            $quantity = b2c_arrayhelper::getValue($product,'nums');
                            $amount = $goods_item['amount'];
                            $goods_spec_info = $product['spec_info'];
                        }else{
                            $product = $goods_item['product'];
                            $quantity = b2c_arrayhelper::getValue($product,'quantity');
                            $amount = null;
                            $goods_spec_info = $product['products']['spec_info'];
                        }
                        $link = b2c_arrayhelper::getValue($product,'link_url');
                        $thumb = b2c_arrayhelper::getValue($product,'thumbnail_pic');
                        $name = b2c_arrayhelper::getValue($product,'name');

                    ?>
                    <li>
                        <div class="fl quota-goods">
                            <a href="<?=$link?>" class="db">
                                <span><img src="<?=$controller->image_path($thumb)?>" alt="<?=$name?>" /></span>
                                <p><?=$name?></p>
                            </a>
                        </div>
                        <div class="fl quota-goods-intro">
                            <?php foreach(b2c_mdl_orders::formatProductSpecInfo($goods_spec_info) as $spec_info){?>
                            <p><?=$spec_info?></p>
                            <?php }?>

                            <?php if(false){?>
                            <p><?=$controller->t("税务编号")?>：<?=$product['tax_number']?></p>
                            <?php }?>

                        </div>
                        <div class="fl quota-goods-amount">x<?=$quantity?></div>
                        <div class="fl tc quota-state">
                            <span><?=$amount?'￥'.$amount:'-'?></span>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <?php if($offer){?>
        <div class="container" goodsList>
            <div class="quota-panel quota-res">
                <ul class="tr">
                    <li>
                        <span><?=$controller->t("商品总报价")?>：</span>
                        <span>￥<?=$order['deposit']+$offer['down_payment']+$offer['final_payment']?></span>
                    </li>
                    <li>
                        <span><?=$controller->t("保证金")?>：</span>
                        <span>￥<?=$order['deposit']?></span>
                    </li>
                    <li>
                        <span><?=$controller->t("首款")?>：</span>
                        <span>￥<?=$offer['down_payment']?></span>
                    </li>
                    <li>
                        <span><?=$controller->t("尾款")?>：</span>
                        <span>￥<?=$offer['final_payment']?></span>
                    </li>
                </ul>
            </div>
        </div>
        <?php }?>
    </div>
</div>
<script id="custom-infor-tpl" type="text/tpl">
<div class="receiver-hd"><?=$controller->t("收货人信息")?></div>
<div class="receiver-item"><?=$controller->t("收货人")?>：{{=it.ship_name}}</div>
<div class="receiver-item flex-box">
    <span><?=$controller->t("收货地址")?>：</span>
    <span class="flex">{{=it.ship_addr}}</span>
</div>
<div class="receiver-item"><?=$controller->t("手机号")?>：{{=it.ship_mobile}}</div>
</script>
<script id="poplayer-tpl" type="text/tpl">
    <div class="pop-layer fadeInDown">
        <div class="pop-layer-inner pr">
            <div class="pop-layer-title f16">{{=it.popTitle}}</div>
            <div class="pop-layer-bd"></div>
            <div class="loading"></div>
        </div>
        <a href="javascript:;" class="pa pop-layer-close"></a>
    </div>
</script>
<script id="custom-edit-tpl" type="text/tpl">
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:-4px;">*</span><?=$controller->t("收货人姓名")?>：</div>
        <div class="flex pr">
            <input type="text" class="name-input" value="{{=it.username}}" placeholder="请输入收货人姓名" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:12px;">*</span><?=$controller->t("收货地址")?>：</div>
        <div class="flex pr">
            <input type="text" class="address-input" value="{{=it.address}}" placeholder="请输入收货地址：" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <div class="pop-layer-label pr"><span class="pa fb color-964f4c" style="left:28px;">*</span><?=$controller->t("手机号")?>：</div>
        <div class="flex pr">
            <input type="text" class="mobile-input" value="{{=it.mobile}}" placeholder="请输入手机号" />
            <div class="pa form-tip f14 none" style="padding-left:0;bottom:-22px;"></div>
        </div>
    </div>
    <div class="flex-box pop-layer-wrap">
        <button class="less-btn" style="margin:0 auto;" data-oid="{{=it.id}}" type="button"><?=$controller->t("确认修改")?></button>
    </div>
</script>
<div class="express-detail none">
    <div class="loading"></div>
    <div class="order-textprocess-panel">
        <div class="express-title f16"><?=$controller->t("物流跟踪详情")?></div>
        <div class="order-textprocess">
        </div>
    </div>
    <a href="javascript:;" class="db pr express-close"></a>
</div>
<div id="custom-baseinfor"></div>