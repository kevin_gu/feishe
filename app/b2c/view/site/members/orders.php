<?php
/**
 * @var base_controller $controller
 * @var $pager_pay_status string
 * @var $orders array
 * @var $pager array
 * @var  $nav
 */
    $nav = [
            ['label'=>'全部订单','status'=>'all'],
            ['label'=>'待报价','status'=>'wait_offer'],
            ['label'=>'待付保证金','status'=>b2c_mdl_orders::ORDER_STATUS_WAIT_DEPOSIT],
            ['label'=>'待确认','status'=>b2c_mdl_orders::ORDER_STATUS_WAIT_CONFIRM],
            ['label'=>'待付款','status'=>'wait_pay'],
            ['label'=>'待发货','status'=>'wait_shipped'],
            ['label'=>'待收货','status'=>b2c_mdl_orders::ORDER_STATUS_WAIT_RECEIVED],
            ['label'=>'已完成','status'=>b2c_mdl_orders::ORDER_STATUS_FINISHED],
    ];
    $memberId = $controller->getMemberId();
    $payAbleStatus = [
        b2c_mdl_orders::PAYMENT_STATUS_FAIL,b2c_mdl_orders::PAYMENT_STATUS_DEFAULT
    ];
?>
<ul class="order-tab clearfix">
    <?php foreach($nav as $_nav){
        $class = $pager_pay_status == $_nav['status'] ?'class="current"':'';
        $url = '/index.php/members-orders-'.$_nav['status'].'.html';
        ?>
    <li <?=$class?> ><a href="<?=$url?>"><?=$controller->t($_nav['label'])?></a></li>
    <?php } ?>
</ul>
<ul class="order-caption clearfix">
    <li><?=$controller->t("商品内容")?></li>
    <li><?=$controller->t("订单状态")?></li>
    <li><?=$controller->t("操作")?></li>
</ul>
<?php foreach ($orders as $order){
    $first_goods = true;
    $offer = $order['offer'];
    $goods_items = $offer ? $offer['items']:$order['goods_items'];
    $current_status = $order['order_current_status'];
    $order_status = $order['order_status'];
    $deadLine = false;
    if($order_status == b2c_mdl_orders::ORDER_STATUS_WAIT_DOWN_PAYMENT  || $order_status == b2c_mdl_orders::ORDER_STATUS_WAIT_FINAL_PAYMENT){
        if(b2c_mdl_orders::getLeftSeconds($order)<1){
            $deadLine = true;
        }
    }
    ?>
<div class="order-panel">
    <div class="order-hd clearfix">
        <div class="fl order-no"><span class="dib vm"></span><em class="dib vm"><?=$controller->t("订单号")?>：<?=$order['order_id']?></em></div>
        <div class="fr order-date"><?=date("Y-m-d H:i:s",$order['createtime'])?></div>
    </div>
    <div class="order-bd">
        <ul class="order-list clearfix">

            <?php foreach ($goods_items as $goods_item){
                if($offer){
                    $product = $goods_item;
                    $quantity = b2c_arrayhelper::getValue($product,'nums');
                }else{
                    $product = $goods_item['product'];
                    $quantity = b2c_arrayhelper::getValue($product,'quantity');
                }
                $link = b2c_arrayhelper::getValue($product,'link_url');
                $thumb = b2c_arrayhelper::getValue($product,'thumbnail_pic');
                $name = b2c_arrayhelper::getValue($product,'name');
                ?>
            <li>
                <div class="fl order-goods">
                    <a href="<?=$link?>" class="db">
                        <span><img src="<?=$controller->image_path($thumb)?>" alt="" /></span>
                        <p><?=$name?></p>
                    </a>
                </div>
                <div class="fl order-goods-amount">x<?=$quantity?></div>
                <?php if($first_goods){
                        $first_goods = false;
                    ?>
                <div class="fl tc order-state">
                    <span><?=b2c_mdl_orders::getOrderStatusTxtForUser($current_status)?></span>
                    <?php if($order['deposit_status'] == b2c_mdl_orders::PAYMENT_STATUS_WAIT_AUDIT){?>
                    <span now><?=$controller->t("保证金支付凭证审核中")?></span>
                    <?php }?>
                    <?php if($order['down_payment_status'] == b2c_mdl_orders::PAYMENT_STATUS_WAIT_AUDIT){?>
                        <span now><?=$controller->t("首款支付凭证审核中")?></span>
                    <?php }?>
                    <?php if($order['final_payment_status'] == b2c_mdl_orders::PAYMENT_STATUS_WAIT_AUDIT){?>
                        <span now><?=$controller->t("尾款支付凭证审核中")?></span>
                    <?php }?>
                </div>
                <div class="fl tc order-exec">
                    <?php if(!$offer){?>
                        <span unuse><?=$controller->t("查看报价")?></span>
                        <span unuse><?=$controller->t("分享")?></span>
                    <?php }else{?>
                        <span class="js-share-order hide"><?=$controller->t("分享")?></span>
                        <a href="/index.php/members-offer_self_detail-<?=$order['order_id']?>-<?=$offer['id']?>.html"><?=$controller->t("查看报价")?></a>
                    <?php }?>
                    <a href="/index.php/members-orderdetail-<?=$order['order_id']?>.html"><?=$controller->t("订单详情")?></a>
                    <?php if(false){?>
                    <a href="/index.php/members-cancel-<?=$order['order_id']?>.html"><?=$controller->t("取消订单")?></a>
                    <?php }?>

                    <?php if(b2c_mdl_orders::ORDER_STATUS_WAIT_DEPOSIT == $current_status && in_array($order['deposit_status'],$payAbleStatus)){?>
                        <a href="/index.php/members-pay_deposit-<?=$order['order_id']?>.html"><?=$controller->t("在线支付保证金")?></a>
                        <a href="/index.php/members-pay_deposit_offline-<?=$order['order_id']?>.html"><?=$controller->t("线下支付保证金")?></a>
                    <?php }?>

                    <?php if(b2c_mdl_orders::ORDER_STATUS_WAIT_CONFIRM == $current_status){?>
                        <a href="/index.php/members-offer_confirm-<?=$order['order_id']?>-yes.html"><?=$controller->t("确认报价")?></a>
                        <a href="/index.php/members-offer_confirm-<?=$order['order_id']?>-no.html"><?=$controller->t("对报价有异议")?></a>
                    <?php }?>

                    <?php if(b2c_mdl_orders::ORDER_STATUS_WAIT_DOWN_PAYMENT == $current_status && !$deadLine && in_array($order['down_payment_status'],$payAbleStatus)){?>
                        <a href="/index.php/members-pay_down_payment-<?=$order['order_id']?>.html"><?=$controller->t("支付首款")?></a>
                    <?php }?>

                    <?php if(b2c_mdl_orders::ORDER_STATUS_WAIT_FINAL_PAYMENT == $current_status && !$deadLine && in_array($order['final_payment_status'],$payAbleStatus)){?>
                        <a href="/index.php/members-pay_final_payment-<?=$order['order_id']?>.html"><?=$controller->t("支付尾款")?></a>
                    <?php }?>

                    <?php if(b2c_mdl_orders::ORDER_STATUS_WAIT_RECEIVED == $order_status){?>
                        <a class="js-check-express" href="javascript:;" data-id="<?=$order['order_id']?>"><?=$controller->t("查看物流")?></a>
                        <a href="/index.php/members-finish-<?=$order['order_id']?>.html"><?=$controller->t("确认收货")?></a>
                    <?php }?>

                    <?php if(b2c_mdl_orders::ORDER_STATUS_FINISHED == $current_status){?>
                        <a class="js-check-express" href="javascript:;" data-id="<?=$order['order_id']?>"><?=$controller->t("查看物流")?></a>
                        <?php if(b2c_mdl_orders::AFTER_SALE_STATUS_NO == $order['after_sale_status']){?>
                            <a href="/index.php/members-after_sales-<?=$order['order_id']?>.html"><?=$controller->t("申请售后")?></a>
                        <?php } else {?>
                            <span unuse><?=$controller->t("已申请售后")?></span>
                        <?php }?>
                    <?php }?>
                </div>
                    <?php if($offer){
                        $sharedMembers = b2c_mdl_order_share::getAvailableMembers($memberId,$offer['id']);
                        ?>
                        <div class="pa friend-panel" style="top:20px;right:120px; display: none">
                            <div class="friend-hd clearfix">
                                <span class="fl f16"><?=$controller->t("好友列表")?></span>
                                <!--<span class="fr f14"><em class="dib vm"><?=$controller->t("关闭")?></em></span>-->
                            </div>
                            <div class="friend-list scrollbar">
                                <ul class="clearfix">
                                    <?php foreach($sharedMembers as $sharedMember){?>
                                    <li>
                                        <div class="fl friend-photo"><img src="../public/assets/images/up.jpg" alt="" /></div>
                                        <div class="fl friend-infor">
                                            <p><?=$controller->t("用户名")?></p>
                                            <p><?=$sharedMember['name']?></p>
                                            <!--<p><?=$sharedMember['email']?></p>-->
                                        </div>
                                        <div class="fr pr friend-check">
                                            <i class="pa checkbox" data-id="<?=$sharedMember['member_id']?>"></i>
                                        </div>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <div class="quota-nickname">
                                <div class="f14"><?=$controller->t("报价单别名")?></div>
                                <div class="nick-input"><input type="text" value="" placeholder="给报价单起个别名吧（选填）" data-order_id="<?=$order['order_id']?>" data-offer_id="<?=$offer['id']?>" /></div>
                                <a href="javascript:;" class="db f16 tc save"><?=$controller->t("确定")?></a>
                            </div>
                        </div>
                    <?php }?>
                <?php }?>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>
<?php }?>
<?=$controller->pager($pager)?>
<div class="express-detail none">
    <div class="loading"></div>
    <div class="order-textprocess-panel">
        <div class="express-title f16"><?=$controller->t("物流跟踪详情")?></div>
        <div class="order-textprocess">
        </div>
    </div>
    <a href="javascript:;" class="db pr express-close"></a>
</div>