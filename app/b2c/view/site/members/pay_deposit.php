<?php
/**
 * @var base_controller $controller
 * @var $order
 */
$controller->bodyClass = 'header-holder';
$amount = $order['deposit'];
$controller->footer_js = ['qrcode.min.js'];
?>
<div class="oh main">
    <div class="oh wrapper">
        <div class="container" goodsList>
            <div class="pay-panel">
                <div class="pay-tips f14"><?=$controller->t('如对此订单报价有异议（如对此订单报价确认无误），请支付保证金后进行其他操作，请尽快支付~')?></div>
                <!--<div class="pay-time f16"><?=$controller->t("请在")?><span>50分39秒</span><?=$controller->t("内完成支付")?></div>-->
                <div class="pay-money f16 tl"><?=$controller->t("待支付")?>：￥<?=$amount?></div>
                <input type="hidden" id="check-pay-time" value="3000">
                <ul class="pay-list">
                    <li>
                        <div class="dib cp">
                            <i class="radio radio-checked" data-type="alipay"></i>
                            <span class="dib vm icon-sprite pay-alipay"></span>
                            <span class="dib vm pay-intro"><?=$controller->t("支付宝支付")?></span>
                            <span class="dib vm f16 pay-me"><?=$controller->t("支付")?>：￥<?=$amount?></span>
                        </div>
                    </li>
                    <li>
                        <div class="dib cp">
                            <i class="radio" data-type="wechat"></i>
                            <span class="dib vm icon-sprite pay-weixin"></span>
                            <span class="dib vm pay-intro"><?=$controller->t("微信支付")?></span>
                            <span class="dib vm f16 pay-me" style="display:none"><?=$controller->t("支付")?>：￥<?=$amount?></span>
                        </div>
                    </li>
                    <!--
                    <li>
                        <i class="radio"></i>
                        <span class="dib vm icon-sprite pay-paypal"></span>
                        <span class="dib vm pay-intro">Paypal</span>
                    </li>
                    <li>
                        <i class="radio"></i>
                        <span class="dib vm icon-sprite pay-bank"></span>
                        <span class="dib vm pay-intro"><?=$controller->t("银行转账")?></span>
                        <div class="bank-list clearfix none">
                            <div class="fl pr bank-abc"></div>
                            <div class="fl pr bank-bc "></div>
                            <div class="fl pr bank-ccb "></div>
                            <div class="fl pr bank-cmc "></div>
                            <div class="fl pr bank-icbc "></div>
                        </div>
                    </li>
                    -->
                </ul>
                <div class="pay-btn tr">
                    <button type="button" class="primary-btn" data-type="alipay" data-id="<?=$order['order_id']?>"><?=$controller->t("前往支付")?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="hook"></div>
<script type="text/tpl" id="poplayer-tpl">
<div class="pop-layer fadeInDown" style="width:900px;height:500px;margin:-250px 0 0 -450px;">
    <div class="pop-layer-inner pr" style="min-height:440px;">
        <div class="pop-layer-title f16">{{=it.popTitle}}</div>
        <div class="pop-layer-bd pr"></div>
        <div class="loading"></div>
    </div>
    <a href="javascript:;" class="pa pop-layer-close"></a>
</div>
</script>
<script type="text/tpl" id="quick-pay-tpl">
<div class="pa erwm-panel">
    <div class="pr erwm-pic"></div>
    <div class="wxsao tc">
        {{ if(it.type == 'wechat'){ }}
        <img class="dib vm" src="/public/assets/images/pwxsao.png" alt="" />
        <em class="dib vm f14 tl"><?=$controller->t("点击微信右上角")?><br /><?=$controller->t("扫一扫付款哦")?></em>
        {{ }else{ }}
        <img class="dib vm" src="/public/assets/images/palisao.png" alt="" />
        <em class="dib vm f14 tl"><?=$controller->t("点击支付宝左上角")?><br /><?=$controller->t("扫一扫付款哦")?></em>
        {{ } }}
    </div>
</div>
<div class="pa erwm-pay-money">
    <div class="f20"><?=$controller->t("应付金额")?></div>
    <div class="f16"><b class="f20">￥<?=$amount?></b><?=$controller->t("元")?></div>
</div>
<div class="pa erwm-change">
    <div class="erwm-change-wrapper">
        <div class="pr cp erwm-change-item weixin{{! it.type == 'wechat' ? ' on' : ''}}" data-type="wechat" data-id="<?=$order['order_id']?>"></div>
        <div class="pr cp erwm-change-item alipay{{! it.type == 'alipay' ? ' on' : ''}}" data-type="alipay" data-id="<?=$order['order_id']?>"></div>
    </div>
    <p class="f12 tc"><?=$controller->t("点击上方卡片切换其他支付方式")?></p>
</div>
</script>