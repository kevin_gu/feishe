<?php
/**
 * @var base_controller $controller
 * @var $order
 * @var $type
 * @var $offer;
 */
$payAble = true;
if( in_array($type,['down_payment','final_payment'])){
    $payAble = b2c_mdl_orders::getLeftSeconds($order);
}
$config = [
        'deposit'=>['amount'=>$order['deposit'],'title'=>'保证金'],
        'down_payment'=>['amount'=>$offer['down_payment'],'title'=>'首款'],
        'final_payment'=>['amount'=>$offer['final_payment'],'title'=>'尾款'],
];
$controller->bodyClass = 'header-holder';
?>
<div class="oh main">
    <div class="oh wrapper">
        <div class="container" goodsList>
            <div class="pay-panel">
                <?php if($payAble){?>
                <div class="pay-time f20 tc">
                    <span class="db"><?=$controller->t("您汇款完成后")?>，请上传您的汇款凭证</span>
                </div>
                <div class="pay-account">
                    <div class="pay-tips f16 tc"><?=$controller->t("请将")?><?=$config[$type]['title']?>￥<?=$config[$type]['amount']?>，汇款至以下账户：</div>
                    <div class="pay-tips f16 tc"><?=$controller->t("银行账户")?>：123456789123456789</div>
                    <div class="pay-tips f16 tc"><?=$controller->t("开户银行")?>：厦门市 中国银行 湖里支行</div>
                    <div class="pay-tips f16 tc"><?=$controller->t("开户公司")?>：珠海棐舍科技有限公司</div>
                </div>
                <div class="pay-btn tc" style="padding-top:60px;">
                    <form action="/index.php/members-submit_receipt.html" method="post" enctype="multipart/form-data" id="receipt_form">
                        <input type="hidden" name="name" value="<?=$type?>">
                        <input type="hidden" name="order_id" value="<?=$order['order_id']?>">
                    <div class="dib pr primary-btn"><input type="file" name="receipt" value="" class="upload-proof" onchange="document.getElementById('receipt_form').submit();" /><?=$controller->t("上传凭证")?></div>
                    </form>
                </div>
                <?php } else {?>
                    <div class="pay-time f20 tc">
                        <span class="db"><?=$controller->t("支付超时")?>，请联系管理员</span>
                    </div>
                <?php }?>
                <div class="pay-annex tc">
                    <div class="pr annex-hd"><span><?=$controller->t("您可能还需要")?></span></div>
                    <div class="annex-bd">
                        <span class="dib vm icon-sprite icon-1"></span>
                        <a href="/index.php/members-orderdetail-<?=$order['order_id']?>.html" class="dib vm"><?=$controller->t("查看订单")?></a>
                        <span class="dib vm icon-sprite icon-2"></span>
                        <a href="" class="dib vm"><?=$controller->t("查看帮助")?></a>
                        <span class="dib vm icon-sprite icon-3"></span>
                        <a href="/index.php/gallery.html" class="dib vm"><?=$controller->t("继续购物")?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
