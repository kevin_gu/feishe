<?php
/**
 *  @var base_controller $controller
 * @var $msg
 */
$controller->bodyClass = 'header-holder';
?>
<div class="oh main">
    <div class="oh wrapper">
        <div class="container" goodsList>
            <div class="pay-panel">
                <div class="pay-time f20 tc">
                    <span class="db"><?=$msg['title']?></span>
                    <span class="db" style="margin-top:20px;"><?=$msg['content']?></span>
                </div>
                <div class="pay-annex tc">
                    <div class="pr annex-hd"><span><?=$controller->t("您可能还需要")?></span></div>
                    <div class="annex-bd">
                        <span class="dib vm icon-sprite icon-1"></span>
                        <a href="/index.php/members-orders.html" class="dib vm"><?=$controller->t("查看订单")?></a>
                        <span class="dib vm icon-sprite icon-2"></span>
                        <a href="" class="dib vm"><?=$controller->t("查看帮助")?></a>
                        <span class="dib vm icon-sprite icon-3"></span>
                        <a href="/index.php/gallery.html" class="dib vm"><?=$controller->t("继续购物")?></a>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
