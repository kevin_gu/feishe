<?php
/**
 *  @var base_controller $controller
 *  @var $order
 */
$controller->bodyClass = 'header-holder';
?>
<div class="oh main">
    <div class="oh wrapper">
        <div class="container" goodsList>
            <div class="crumbs-panel">
                <a href="/index.php/members-orders.html" class="vm"><?=$controller->t("会员中心")?></a>
                <i class="icon-sprite vm"></i>
                <a href="/index.php/members-orders.html" class="vm"><?=$controller->t("订单管理")?></a>
                <i class="icon-sprite vm"></i>
                <a href="/index.php/members-orders.html" class="vm"><?=$controller->t("我的订单")?></a>
                <i class="icon-sprite vm"></i>
                <span class="vm"><?=$controller->t("售后详情")?></span>
            </div>
        </div>
        <div class="container" goodsList>
            <div class="crumbs-panel clearfix">
                <div class="fl quota-caption"><?=$controller->t("需要售后的商品")?></div>
                <div class="fl quota-caption"><?=$controller->t("数量")?></div>
                <div class="fl quota-caption"><?=$controller->t("单价")?></div>
            </div>
        </div>
        <div class="container" goodsList>
            <div class="quota-panel">
                <ul class="quota-list clearfix">
                    <?php foreach ($order['product_data'] as $product){
                        $thumb = b2c_arrayhelper::getValue($product,'thumbnail_pic');
                        $name = b2c_arrayhelper::getValue($product,'name');
                        ?>
                        <li>
                            <div class="fl quota-goods">
                                <a href="<?=$product['link_url']?>" class="db">
                                    <span><img src="<?=$controller->image_path($thumb)?>" alt="<?=$name?>" /></span>
                                    <p><?=$name?></p>
                                </a>
                            </div>
                            <div class="fl quota-goods-intro">
                                <p><?=$controller->t("颜色：象牙白")?></p>
                                <p><?=$controller->t("材质：象牙白")?></p>
                                <p><?=$controller->t("规格：单人位1.5米")?></p>
                            </div>
                            <div class="fl quota-goods-amount">x<?=$product['num']?></div>
                            <div class="fl tc quota-state">
                                <span>￥<?=$product['price']?></span>
                            </div>
                        </li>
                    <?php }?>
                </ul>
            </div>
        </div>
        <div class="container" goodsList>
            <div class="quota-panel after-sale">
                <div class="after-sale-hd"><?=$controller->t("售后详情")?></div>
                <div class="after-sale-item flex-box">
                    <span><?=$controller->t("退换理由")?>：</span>
                    <span class="flex"><?=$order['title']?></span>
                </div>
                <div class="after-sale-item flex-box">
                    <span><?=$controller->t("详细描述")?>：</span>
                    <span class="flex"><?=$order['content']?></span>
                </div>
                <div class="after-sale-item flex-box">
                    <span><?=$controller->t("商品图片")?>：</span>
                    <div class="flex clearfix">
                        <?php foreach(unserialize($order['image_file']) as $image){?>
                        <span class="fl after-sale-img"><img src="<?=$image?>" alt="" /></span>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" goodsList>
            <div class="quota-panel after-sale">
                <div class="after-sale-hd"><?=$controller->t("售后状态")?></div>
                <div class="after-sale-item flex-box">
                    <span><?=$controller->t("状态")?>：</span>
                    <span class="flex" style="color:#964F4C"><?=$order['status']?></span>
                </div>

            </div>
        </div>
    </div>
</div>
