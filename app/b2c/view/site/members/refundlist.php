<?php
/**
 * @var base_controller $controller
 * @var $orders array
 * @var $pager array
 * @var $statusOptions
 * @var $status
 */
$nav = [
    ['label'=>'全部订单','status'=>'all'],
    ['label'=>'待审核','status'=>'wait'],
    ['label'=>'处理中','status'=>'ing'],
    ['label'=>'已完成','status'=>'done'],
];
?>
<ul class="order-tab clearfix">
    <?php foreach($nav as $_nav){
        $class = $status == $_nav['status'] ?'class="current"':'';
        $url = '/index.php/members-refundlist-'.$_nav['status'].'.html';
        ?>
        <li <?=$class?> ><a href="<?=$url?>"><?=$controller->t($_nav['label'])?></a></li>
    <?php } ?>
</ul>
<ul class="order-caption clearfix">
    <li><?=$controller->t("商品内容")?></li>
    <li><?=$controller->t("处理状态")?></li>
    <li><?=$controller->t("申请时间")?></li>
</ul>
<?php foreach($orders as $order){
    $isFirst = true;
    ?>
<div class="order-panel">
    <div class="order-hd clearfix">
        <div class="fl order-no"><span class="dib vm"></span><em class="dib vm"><?=$controller->t("订单号")?>：<?=$order['order_id']?></em></div>
        <div class="fr order-date"><?=date("Y-m-d H:i:s",$order['add_time'])?></div>
    </div>
    <div class="order-bd">
        <ul class="order-list clearfix">
            <?php foreach ($order['product_data'] as $product){
                $thumb = b2c_arrayhelper::getValue($product,'thumbnail_pic');
                $name = b2c_arrayhelper::getValue($product,'name');

                ?>
            <li>
                <div class="fl order-goods">
                    <a href="" class="db">
                        <span><img src="<?=$controller->image_path($thumb)?>" alt="<?=$name?>" /></span>
                        <p><?=$name?></p>
                    </a>
                </div>
                <div class="fl order-goods-amount">x1</div>
                <?php if($isFirst){ $isFirst = false?>
                <div class="fl tc order-state">
                    <span><?=$statusOptions[$order['status']]?></span>
                </div>
                <div class="fl tc order-exec">
                    <a href="/index.php/members-refund_detail-<?=$order['return_bn']?>.html"><?=$controller->t("查看详情")?></a>
                </div>
                <?php }?>
            </li>
            <?php }?>
        </ul>
    </div>
</div>
<?php } ?>
<?=$controller->pager($pager)?>
