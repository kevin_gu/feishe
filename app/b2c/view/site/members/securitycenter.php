<?php
/**
 * @var base_controller $controller
 * @var $memberInfo
 */
?>
<ul class="order-tab clearfix">
    <li>修改密码</li>
</ul>
<div class="business-apply">
    <div class="apply-item f14" style="margin-left:0;">
        <form action="###" name="security-form" class="js-security" method="post">
            <div class="apply-form-item pr flex-box">
                <span class="label">原密码：</span>
                <div class="pr flex">
                    <input name="old-pwd" type="password" value="" class="old-pwd" placeholder="请输入原密码" />
                    <div class="pa form-tip f14 none" style="left:0;padding-left:0;bottom:-22px;"></div>
                </div>
            </div>
            <div class="apply-form-item pr flex-box">
                <span class="label">新密码：</span>
                <div class="pr flex">
                    <input name="new-pwd" type="password" value="" class="new-pwd" placeholder="请输入新密码" />
                    <div class="pa form-tip f14 none" style="left:0;padding-left:0;bottom:-22px;"></div>
                </div>
            </div>
            <div class="apply-form-item pr flex-box">
                <span class="label">确认新密码：</span>
                <div class="pr flex">
                    <input name="renew-pwd" type="password" value="" class="renew-pwd" placeholder="请再次输入新密码" />
                    <div class="pa form-tip f14 none" style="left:0;padding-left:0;bottom:-22px;"></div>
                </div>
            </div>
            <div class="tc" style="margin-top:30px;">
                <button type="submit" class="primary-btn less-btn">确认修改</button>
            </div>
        </form>
    </div>
</div>
