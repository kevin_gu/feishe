<?php
/**
 * @var base_controller $controller
 * @var $memberInfo
 */
?>
    <ul class="order-tab clearfix js-person-tab">
        <li class="current"><a href="javascript:;"><?=$controller->t('基本信息')?></a></li>
       <?php if($memberInfo['member_type'] > 0 && $memberInfo['check_status'] == '1') { ?>
        <li><a href="javascript:;"><?=$controller->t('企业信息')?></a></li>
        <?php } ?>
    </ul>
    <div class="business-apply private-infor">
        <div class="flex-box">
            <div class="flex f14">
                <div class="apply-item js-apply-item">
                    <div class="apply-form-item pr flex-box">
                        <span class="label"><?=$controller->t('用户类型')?>：</span>
                        <div class="flex">
                            <?php
                                if(!$memberInfo['member_type'] || ($memberInfo['member_type'] > 0 && !$memberInfo['check_status'])){ echo $controller->t('个人用户');}
                                if($memberInfo['member_type'] == '1' && $memberInfo['check_status']){ echo $controller->t('国内企业用户');}
                                if($memberInfo['member_type'] == '2' && $memberInfo['check_status']){ echo $controller->t('国外企业用户');}
                            ?>
                        </div>
                    </div>
                    <div class="apply-form-item pr flex-box">
                        <span class="label"><?=$controller->t('登录名')?>：</span>
                        <div class="flex">
                            <?php
                                if($memberInfo['email']){ echo $memberInfo['email'];}
                            ?>
                        </div>
                    </div>
                    <div class="apply-form-item pr flex-box">
                        <span class="label"><?=$controller->t('昵称')?>：</span>
                        <div class="flex">
                            <input type="text" name="nickname" value="<?php if($memberInfo['uname']){echo $memberInfo['uname'];}  ?>" placeholder="<?=$controller->t('可以填写一个昵称')?>" />
                        </div>
                        <div class="pa form-tip none" style="left:70px;bottom:-24px;"></div>
                    </div>
                    <div class="apply-form-item pr flex-box">
                        <span class="label"><?=$controller->t('性别')?>：</span>
                        <div class="flex sex">
                            <span class="dib vm cp js-radio-sex" data-value="0">
                                <i class="radio <?php if($memberInfo['sex'] == '0'){echo 'radio-checked';} ?>"></i>
                                <em class="dib vm"><?=$controller->t('男')?></em>
                            </span>
                            <span class="dib vm cp js-radio-sex" data-value="1">
                                <i class="radio <?php if($memberInfo['sex'] == '1'){echo 'radio-checked';} ?>"></i>
                                <em class="dib vm"><?=$controller->t('女')?></em>
                            </span>
                            <span class="dib vm cp js-radio-sex" data-value="2">
                                <i class="radio <?php if($memberInfo['sex'] == '2'){echo 'radio-checked';} ?>"></i>
                                <em class="dib vm"><?=$controller->t('保密')?></em>
                            </span>
                            <input type="hidden" name="sex" value="<?=$memberInfo['sex']?>" />
                        </div>
                    </div>
                    <div class="apply-form-item pr flex-box">
                        <span class="label"><?=$controller->t('生日')?>：</span>
                        <div class="flex birth">
                            <div class="pr fl select-panel js-select-panel" style="width:.84rem;margin-right:.3rem;border:0;border-bottom:1px solid #333;"> 
                                <span class="pa selected"><?php if($memberInfo['b_year']){ echo $memberInfo['b_year'];}else{echo '1980';} ?></span>
                                <ul class="pa select-list scrollbar birth-year none">
                                    <li data-value="1980">1980</li>
                                </ul> 
                                <i class="select-arrow"></i> 
                                <input type="hidden" name="year" class="year-hidden" value="<?php if($memberInfo['b_year']){echo $memberInfo['b_year'];}else{echo '1980';} ?>"> 
                            </div>
                            <div class="pr fl select-panel js-select-panel" style="width:.84rem;margin-right:.3rem;border:0;border-bottom:1px solid #333;"> 
                                <span class="pa selected"><?php if($memberInfo['b_month']){echo $memberInfo['b_month'];}else{echo '01';} ?></span> 
                                <ul class="pa select-list scrollbar birth-month none"> 
                                    <li data-value="01">01</li>
                                    <li data-value="02">02</li>
                                    <li data-value="03">03</li>
                                    <li data-value="04">04</li>
                                    <li data-value="05">05</li>
                                    <li data-value="06">06</li>
                                    <li data-value="07">07</li>
                                    <li data-value="08">08</li>
                                    <li data-value="09">09</li>
                                    <li data-value="10">10</li>
                                    <li data-value="11">11</li>
                                    <li data-value="12">12</li>
                                </ul> 
                                <i class="select-arrow"></i> 
                                <input type="hidden" name="month" class="month-hidden" value="<?php if($memberInfo['b_month']){echo $memberInfo['b_month'];}else{echo '01';} ?>"> 
                            </div>
                            <div class="pr fl select-panel js-select-panel" style="width:.84rem;margin-right:.3rem;border:0;border-bottom:1px solid #333;"> 
                                <span class="pa selected"><?php if($memberInfo['b_day']){echo $memberInfo['b_day'];}else{echo '01';} ?></span> 
                                <ul class="pa select-list scrollbar birth-date none"> 
                                    <li data-value="01">01</li>
                                </ul> 
                                <i class="select-arrow"></i> 
                                <input type="hidden" name="date" class="date-hidden" value="<?php if($memberInfo['b_day']){echo $memberInfo['b_day'];}else{echo '01';} ?>"> 
                            </div>
                        </div>
                    </div>
                    <div class="apply-form-item pr flex-box">
                        <span class="label"><?=$controller->t('邮箱')?>：</span>
                        <div class="flex">
                            <?php if($memberInfo['email']){echo $memberInfo['email'];} ?>
                        </div>
                    </div>
                    <div class="apply-form-item pr flex-box" style="margin-top:.2rem;">
                        <div class="pr tc upload-uphoto">
                            <img src="<?php if($memberInfo['head_url']){echo $memberInfo['head_url'];}else{echo '../public/assets/images/up.jpg';} ?>" alt="" />
                            <input class="js-user-photo" type="file" name="file" value="" />
                            <p class="f14"><?=$controller->t('修改头像')?></p>
                            <input type="hidden" value="<?php if($memberInfo['head_url']){echo $memberInfo['head_url'];}else{echo '../public/assets/images/up.jpg';} ?>" name="photo" />
                        </div>
                        <div class="flex">
                            <p class="uphoto-tips f12"><?=$controller->t('上传小于2M的jpg、png、bmp、或gif文件，头像图片尺寸不小于100*100像素')?></p>
                        </div>
                    </div>
                    <div style="margin:40px 0 0 45px;">
                        <button type="button" class="primary-btn less-btn"><?=$controller->t('提交')?></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex-box" style="display:none">
            <div class="flex f14">
                <div class="apply-item">
                    <!--<div class="apply-form-item pr flex-box">
                        <span class="label">注册类型：</span>
                        <div class="flex">
                            公司
                        </div>
                    </div>-->
                    <div class="apply-form-item pr flex-box">
                        <span class="label"><?=$controller->t('公司名称')?>：</span>
                        <div class="flex">
                            <?=$controller->t($memberInfo['cp_name'])?>
                        </div>
                    </div>
                    <div class="apply-form-item pr flex-box">
                        <span class="label"><?=$controller->t('统一社会信用代码')?>：</span>
                        <div class="flex">
                            <?=$controller->t($memberInfo['cp_id'])?>
                        </div>
                    </div>
                    <div class="apply-form-item pr flex-box">
                        <span class="label"><?=$controller->t('公司地址')?>：</span>
                        <div class="flex">
                            <?=$controller->t($memberInfo['cp_addr'])?>
                        </div>
                    </div>
                    <div class="apply-form-item pr flex-box">
                        <span class="label"><?=$controller->t('公司行业')?>：</span>
                        <div class="flex">
                            <?=$controller->t($memberInfo['cp_ind_name'])?>
                        </div>
                    </div>
                    <div class="apply-form-item pr flex-box">
                        <span class="label"><?=$controller->t('公司人数')?>：</span>
                        <div class="flex">
                            <?=$controller->t($memberInfo['cp_dim_name'])?>
                        </div>
                    </div>
                    <div class="apply-form-item pr flex-box">
                        <span class="label"><?=$controller->t('联系人')?>：</span>
                        <div class="flex">
                            <?=$controller->t($memberInfo['cp_contact'])?>
                        </div>
                    </div>
                    <div class="apply-form-item pr flex-box">
                        <span class="label"><?=$controller->t('公司网址')?>：</span>
                        <div class="flex">
                            <?=$controller->t($memberInfo['cp_web'])?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>