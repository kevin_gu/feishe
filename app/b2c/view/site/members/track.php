<ul class="order-tab clearfix">
    <li><a href="/index.php/members-inbox.html">收件箱</a></li>
    <li><a href="/index.php/members-send.html">发消息</a></li>
    <li class="current"><a href="javascript:;">发件箱</a></li>
    <li><a href="/index.php/members-outbox.html">草稿箱</a></li>
    <li><a href="/index.php/members-message.html">给管理员发消息</a></li>
</ul>
<ul class="order-caption message-option clearfix">
    <li>发件人</li>
    <li>标题</li>
    <li>时间</li>
    <li>操作</li>
</ul>
<div class="order-panel message-panel">
    <div class="order-bd letter-box">
        <ul class="order-list clearfix js-letter-list">
            <li>
                <div>共<?=$count?>条消息</div>
            </li>
            <?php
            foreach ($list as $k=>$v)
            {
                ?>
                 <li data-id="<?=$v['comment_id']?>">
                    <div class="clearfix">
                        <div class="fl order-goods">
                            <i class="checkbox"></i>
                            <em class="dib vm" title="<?=$v['author']?>"><?=$v['author']?></em>
                            <input type="hidden" name="comment_id" value="" />
                        </div>
                        <div class="fl tc cp order-goods-intro">
                            <?=$v['title']?>
                        </div>
                        <div class="fl tc order-state">
                            <?=date('Y-m-d H:i:s',$v['time'])?>
                        </div>
                        <div class="fl tc order-exec">
                            <a href="javascript:;" class="js-remove-letter" remove>删除</a>
                        </div>
                    </div>
                    <div class="message-content none">
                        <div class="flex-box message-title">
                            <div>标题：</div>
                            <div class="flex"><?=$v['title']?></div>
                        </div>
                        <div class="flex-box message-yar">
                            <div>内容：</div>
                            <div class="flex"><?=$v['comment']?></div>
                        </div>
                    </div>
                </li>
            <?php }
            ?>
        </ul>
        <?php if($count){ ?>
        <div class="check-all">
            <i class="checkbox checkall"></i>
            <em class="dib">全选</em>
            <a href="javascript:;" class="dib js-remove-checked-letter" remove>删除</a>
        </div>
        <?php } ?>
    </div>
</div>
<div class="pagination tc">
    <?php
    $syy = '###';
    $xyy = '###';
    if ($pager['current'] != 1)
    {
        $syy = '/index.php/members-track-'.($pager['current']-1).'.html';
    }

    if ($pager['current'] != $pager['total'])
    {
        $xyy = '/index.php/members-track-'.($pager['current']+1).'.html';
    }
    ?>
    <span total>
                    共<?=$pager['total']?>页
                </span>
    <a href="<?=$syy?>">上一页</a>
    <a href="<?=$syy?>" prev></a>
    <span pageNum>
                <?php
                for ($i = 1; $i <= $pager['total']; $i++)
                {

                    if ($i == $pager['current']) {
                        ?>

                        <b><?=$i?></b>
                        <?php
                    }
                    else {
                        ?>
                        <a href="<?='/index.php/members-track-'.$i.'.html'?>"><?=$i?></a>
                        <?php
                    }
                }
                ?>
                </span>
    <a href="<?=$xyy?>" next></a>
    <a href="<?=$xyy?>">下一页</a>
</div>