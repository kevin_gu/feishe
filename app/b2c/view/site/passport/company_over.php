<!DOCTYPE html>
<html lang="zh-CN" fixed>
<head>
    <meta charset="utf-8">
    <title>Vanseo Fei</title>
    <link type="text/css" rel="stylesheet" href="/public/assets/css/main.css">
     <script type="text/javascript" src="/public/assets/js/response.js?t=<?=date("i")?>"></script>
     <style type="text/css">
    html,body{overflow:hidden;}
    </style>
</head>
<body>
    <div class="login-panel comp-reg-panel">
        <div class="pr wrapper" hPer100>
            <div class="container">
                <div class="login-top clearfix">
                    <div class="fr" goLogin>
                        <a href="/index.php/passport-login.html"><?=$controller->t('已有账号？请登录')?></a>
                        <i>〉</i>
                    </div>
                    <a href="/"><img src="/public/assets/images/logo.svg" alt="" /></a>
                    <span class="dib"><?=$controller->t('欢迎注册')?>！</span>
                </div>
            </div>
            <div class="reg-area-outer">
                <div class="reg-step-panel clearfix" style="padding-left:0;padding-right:0">
                      <div class="fl reg-step-5 reg-step-5_1 tc current ok">
                      <div class="fl step-text">
                          <i></i>
                          <span><?=$controller->t('验证邮箱')?></span>
                      </div>
                      <div class="fl icon-sprite step-mark"></div>
                  </div>
                  <div class="fl reg-step-5 reg-step-5_2 tc current ok">
                      <div class="fl step-text">
                          <i></i>
                          <span><?=$controller->t('填写账号信息')?></span>
                      </div>
                      <div class="fl icon-sprite step-mark"></div>
                  </div>
                  <div class="fl reg-step-5 reg-step-5_3 tc current ok">
                      <div class="fl step-text">
                          <i></i>
                          <span><?=$controller->t('填写公司信息')?></span>
                      </div>
                      <div class="fl icon-sprite step-mark"></div>
                  </div>
                  <div class="fl reg-step-5 reg-step-5_4 tc current">
                      <div class="fl step-text">
                          <i></i>
                          <span><?=$controller->t('等待审核')?></span>
                      </div>
                      <div class="fl icon-sprite step-mark"></div>
                  </div>
                  <div class="fl reg-step-5 reg-step-5_5 tc">
                      <div class="fl step-text">
                          <i></i>
                          <span><?=$controller->t('注册成功')?></span>
                      </div>
                  </div>
                </div>
                <div class="reg-area-inner">
                   
                   <div class="reg-step-tip reg-finish-tip tc">
                        <p><?=$controller->t('您的公司信息已经提交成功，请耐心等待工作人员进行审核')?></p>
                        <p><?=$controller->t('审核结果会通过您的注册邮箱通知您')?></p>
                        <p><?=$controller->t('正在为您跳转至注册前页面')?>...</p>
                   </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-sub">
        <div class="wrapper">
            <div class="container tc">
                <p>Copyright ©2019 珠海市棐舍科技有限公司 版权所有</p>
                <p>“Vanseo Fei”以及“Vanseo Fei”图形均为Vanseo Fei Technology Co.,Ltd的商标，且在世界多个地点均有注册</p>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/public/assets/js/jquery-min.js"></script>
    <script type="text/javascript" src="/public/assets/js/webfont.js"></script>
    <script type="text/javascript" src="/public/assets/js/main.js"></script>
</body>
</html>
<script>
    setTimeout(function(){window.location="/index.php/passport-login.html";},3000);
</script>
