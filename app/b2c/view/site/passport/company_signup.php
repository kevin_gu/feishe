<!DOCTYPE html>
<html lang="zh-CN" fixed>
<head>
    <meta charset="utf-8">
    <title>Vanseo Fei</title>
    <link type="text/css" rel="stylesheet" href="/public/assets/css/main.css">
     <script type="text/javascript" src="/public/assets/js/response.js?t=<?=date("i")?>"></script>
     <style type="text/css">
    html,body{overflow:hidden;}
    </style>
    <script id="poplayer-tpl" type="text/tpl">
        <div class="pop-layer user-rules fadeInDown">
            <div class="pop-layer-inner user-rules-inner scrollbar pr">
                <div class="pop-layer-bd"></div>
            </div>
            <a href="javascript:;" class="pa pop-layer-close"></a>
        </div>
    </script>
    <script id="reg-rule-tpl" type="text/tpl">
        <b>Vanseo Fei用户注册协议</b>
        <p>本协议是您与Vanseo Fei网站（简称"本站"，网址：www.Vanseo Fei.com）所有者（以下简称为"Vanseo Fei"）之间就网站服务等相关事宜所订立的契约，请您仔细阅读本注册协议，当您点击“我已阅读并同意”选项、且提交注册后，本协议即构成对双方有约束力的法律文件。</p>
        <b>第1条 本站服务条款的确认和接纳</b>
        <p>•1.1本站的各项电子服务的所有权和运作权归VanseoFei所有。用户同意所有注册协议条款并完成注册程序，才能成为本站的正式用户。用户确认：本协议条款是处理双方权利义务的契约，始终有效，法律另有强制性规定或双方另有特别约定的，依其规定。</p>
        <p>•1.2用户点击阅读并同意本协议的，即视为用户确认自己具有享受本站服务、下单购物等相应的权利能力和行为能力，能够独立承担法律责任。</p>
        <p>•1.3如果您在18周岁以下，您只能在父母或监护人的监护参与下才能使用本站。</p>
        <p>• 1.4Vanseo Fei保留在中华人民共和国大陆地区法施行之法律允许的范围内独自决定拒绝服务、关闭用户账户、清除或编辑内容或取消订单的权利。</p>
        <b>第2条 本站服务</b>
        <p>• 2.1Vanseo Fei通过互联网依法为用户提供互联网信息等服务，用户在完全同意本协议及本站规定的情况下，方有权使用本站的相关服务。</p>
        <p>•2.2用户必须自行准备如下设备和承担如下开支：（1）上网设备，包括并不限于电脑或者其他上网终端、调制解调器及其他必备的上网装置；（2）上网开支，包括并不限于网络接入费、上网设备租用费、手机流量费等。</p>
        <div class="tc agree-rule"><button class="less-btn">同意并继续注册</button></div>
    </script>
</head>
<body>
    <div class="login-panel comp-reg-panel">
        <div class="pr wrapper" hPer100>
            <div class="container">
                <div class="login-top clearfix">
                    <div class="fr" goLogin>
                        <a href="/index.php/passport-login.html"><?=$controller->t('已有账号？请登录')?></a>
                        <i>〉</i>
                    </div>
                    <a href="/"><img src="/public/assets/images/logo.svg" alt="" /></a>
                    <span class="dib"><?=$controller->t('欢迎注册')?>！</span>
                </div>
            </div>
            <div class="reg-area-outer">
                <div class="reg-step-panel clearfix">
                  <div class="fl reg-step-5 reg-step-5_1 tc current">
                      <div class="fl step-text">
                          <i></i>
                          <span><?=$controller->t('验证邮箱')?></span>
                      </div>
                      <div class="fl icon-sprite step-mark"></div>
                  </div>
                  <div class="fl reg-step-5 reg-step-5_2 tc">
                      <div class="fl step-text">
                          <i></i>
                          <span><?=$controller->t('填写账号信息')?></span>
                      </div>
                      <div class="fl icon-sprite step-mark"></div>
                  </div>
                  <div class="fl reg-step-5 reg-step-5_3 tc">
                      <div class="fl step-text">
                          <i></i>
                          <span><?=$controller->t('填写公司信息')?></span>
                      </div>
                      <div class="fl icon-sprite step-mark"></div>
                  </div>
                  <div class="fl reg-step-5 reg-step-5_4 tc">
                      <div class="fl step-text">
                          <i></i>
                          <span><?=$controller->t('等待审核')?></span>
                      </div>
                      <div class="fl icon-sprite step-mark"></div>
                  </div>
                  <div class="fl reg-step-5 reg-step-5_5 tc">
                      <div class="fl step-text">
                          <i></i>
                          <span><?=$controller->t('注册成功')?></span>
                      </div>
                  </div>
                </div>
                <div class="reg-area-inner js-verifi-email-company">
                   <div class="login-input-outer reg-input-outer pr flex-box">
                    <div class="icon-sprite icon-email"></div>
                    <div class="flex">
                        <input type="text" name="email" class="login-input" autocomplete="off" value="" placeholder="请输入邮箱地址" />
                    </div>
                    <div class="pa f14 form-tip none"></div>
                    <div class="send-email" use><?=$controller->t('发送验证邮件')?></div>
                    <div class="send-email none" unuse><?=$controller->t('60秒后重发')?></div>
                  </div>
                  <div class="reg-rules pr">
                    <s class="dib pr vm current"></s>
                    <div class="pa f14 form-tip none" style="padding-left:35px;"></div>
                    <input type="hidden" name="rules" value="1" />
                    <em class="dib vm"><?=$controller->t('我已阅读并同意')?><a class="js-reg-rule" href="javascript:;">《企业用户注册协议》</a></em>
                  </div>
                  <div class="reg-step-tip tc"><?=$controller->t('请发送验证邮件至您输入的邮箱，如果您已点击确认操作，请点击下一步')?></div>
                   <div class="reg-step-btn"><button type="button">下一步</button></div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-sub">
        <div class="wrapper">
            <div class="container tc">
                <p>Copyright ©2019 珠海市棐舍科技有限公司 版权所有</p>
                <p>“Vanseo Fei”以及“Vanseo Fei”图形均为Vanseo Fei Technology Co.,Ltd的商标，且在世界多个地点均有注册</p>
            </div>
        </div>
    </div>
    <div id="reg-rule-hook"></div>
    <div id="search-mark" class="mask none" searchMask></div>
    <script type="text/javascript" src="/public/assets/js/jquery-min.js"></script>
    <script type="text/javascript" src="/public/assets/js/webfont.js"></script>
    <script type="text/javascript" src="/public/assets/js/main.js"></script>
</body>
</html>
