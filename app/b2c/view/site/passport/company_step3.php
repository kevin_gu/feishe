<!DOCTYPE html>
<html lang="zh-CN" fixed>
<head>
    <meta charset="utf-8">
    <title>Vanseo Fei</title>
    <link type="text/css" rel="stylesheet" href="/public/assets/css/main.css">
     <script type="text/javascript" src="/public/assets/js/response.js?t=<?=date("i")?>"></script>
     <style type="text/css">
    html,body{overflow:hidden;}
    </style>
</head>
<body>
    <div class="login-panel comp-reg-panel">
        <div class="pr wrapper" hPer100>
            <div class="container">
                <div class="login-top clearfix">
                    <div class="fr" goLogin>
                        <a href="/index.php/passport-login.html"><?=$controller->t('已有账号？请登录')?></a>
                        <i>〉</i>
                    </div>
                    <a href="/"><img src="/public/assets/images/logo.svg" alt="" /></a>
                    <span class="dib"><?=$controller->t('欢迎注册')?>！</span>
                </div>
            </div>
            <div class="reg-area-outer">
                <div class="reg-step-panel clearfix" style="padding-left:0;padding-right:0">
                      <div class="fl reg-step-5 reg-step-5_1 tc current ok">
                      <div class="fl step-text">
                          <i></i>
                          <span><?=$controller->t('验证邮箱')?></span>
                      </div>
                      <div class="fl icon-sprite step-mark"></div>
                  </div>
                  <div class="fl reg-step-5 reg-step-5_2 tc current ok">
                      <div class="fl step-text">
                          <i></i>
                          <span><?=$controller->t('填写账号信息')?></span>
                      </div>
                      <div class="fl icon-sprite step-mark"></div>
                  </div>
                  <div class="fl reg-step-5 reg-step-5_3 tc current">
                      <div class="fl step-text">
                          <i></i>
                          <span><?=$controller->t('填写公司信息')?></span>
                      </div>
                      <div class="fl icon-sprite step-mark"></div>
                  </div>
                  <div class="fl reg-step-5 reg-step-5_4 tc">
                      <div class="fl step-text">
                          <i></i>
                          <span><?=$controller->t('等待审核')?></span>
                      </div>
                      <div class="fl icon-sprite step-mark"></div>
                  </div>
                  <div class="fl reg-step-5 reg-step-5_5 tc">
                      <div class="fl step-text">
                          <i></i>
                          <span><?=$controller->t('注册成功')?></span>
                      </div>
                  </div>
                </div>
                <div class="reg-comp-infor">
                    <div class="flex-box">
                        <ul class="js-reg-comptab">
                        	<li class="current"><em><?=$controller->t('国内企业')?></em></li>
                            <li><em><?=$controller->t('国外企业')?></em></li>
                        </ul>
                        <div class="flex">
                            <div class="reg-comp-panel">
                                <form action="###" class="js-verifi-company">
                                    <div class="login-input-outer reg-input-outer reg-compinput-outer pr flex-box">
                                        <div class="label"><?=$controller->t('公司名称')?>：</div>
                                        <div class="flex">
                                            <input type="text" name="companyname" class="login-input" autocomplete="off" value="" placeholder="<?=$controller->t('请按营业执照填写')?>" />
                                        </div>
                                        <div class="pa f14 form-tip none"></div>
                                    </div>
                                    <div class="login-input-outer reg-input-outer reg-compinput-outer pr flex-box">
                                        <div class="label"><?=$controller->t('统一社会信用代码')?>：</div>
                                        <div class="flex">
                                            <input type="text" name="companyid" class="login-input" autocomplete="off" value="" placeholder="<?=$controller->t('请按营业执照填写')?>" />
                                        </div>
                                        <div class="pa f14 form-tip none"></div>
                                    </div>
                                    <div class="login-input-outer reg-input-outer reg-compinput-outer pr flex-box">
                                        <div class="label"><?=$controller->t('公司地址')?>：</div>
                                        <div class="flex">
                                            <input type="text" name="companyaddr" class="login-input" autocomplete="off" value="" placeholder="<?=$controller->t('请填写实际所在地')?>" />
                                        </div>
                                        <div class="pa f14 form-tip none"></div>
                                    </div>
                                    <div class="login-input-outer reg-input-outer reg-compinput-outer pr flex-box">
                                        <div class="label"><?=$controller->t('公司行业')?>：</div>
                                        <div class="flex pr js-select-panel">
                                            <div class="login-input" style="color:#b4b4b4;cursor:pointer"><?=$controller->t('请选择行业')?></div>
                                            <div class="pa select-panel js-select-list none">
                                                <dl class="select-list">
                                                    <dd data-value="1"><?=$controller->t('地产')?></dd>
                                                    <dd data-value="2"><?=$controller->t('酒店')?></dd>
                                                    <dd data-value="3"><?=$controller->t('建筑设计')?></dd>
                                                    <dd data-value="4"><?=$controller->t('室内设计')?></dd>
                                                    <dd data-value="5"><?=$controller->t('产品设计')?></dd>
                                                    <dd data-value="6"><?=$controller->t('卖场及代理')?></dd>
                                                    <dd data-value="7"><?=$controller->t('项目管控')?></dd>
                                                </dl>
                                            </div>
                                            <b class="pa icon-sprite select-arrow"></b>
                                        </div>
                                        <input type="hidden" value="-1" name="industry" />
                                        <div class="pa f14 form-tip none"></div>
                                    </div>
                                    <div class="login-input-outer reg-input-outer reg-compinput-outer pr flex-box">
                                        <div class="label"><?=$controller->t('公司人数')?>：</div>
                                        <div class="flex pr js-select-panel">
                                            <div class="login-input" style="color:#b4b4b4;cursor:pointer"><?=$controller->t('请选择人数')?></div>
                                            <div class="pa select-panel js-select-list none">
                                                <dl class="select-list">
                                                    <dd data-value="1"><?=$controller->t('20人以下')?></dd>
                                                    <dd data-value="2">20-50<?=$controller->t('人')?></dd>
                                                    <dd data-value="3">50-100<?=$controller->t('人')?></dd>
                                                    <dd data-value="4"><?=$controller->t('100人以上')?></dd>
                                                </dl>
                                            </div>
                                            <b class="pa icon-sprite select-arrow"></b>
                                        </div>
                                        <input type="hidden" value="-1" name="dim" />
                                        <div class="pa f14 form-tip none"></div>
                                    </div>
                                    <div class="login-input-outer reg-input-outer reg-compinput-outer pr flex-box">
                                        <div class="label"><?=$controller->t('联系人')?>：</div>
                                        <div class="flex">
                                            <input type="text" name="conact" class="login-input" autocomplete="off" value="" placeholder="<?=$controller->t('请填写联系人的姓名')?>" />
                                        </div>
                                        <div class="pa f14 form-tip none"></div>
                                    </div>
                                    <div class="login-input-outer reg-input-outer reg-compinput-outer pr flex-box">
                                        <div class="label"><?=$controller->t('公司网址')?>：</div>
                                        <div class="flex">
                                            <input type="text" name="website" class="login-input" autocomplete="off" value="" placeholder="<?=$controller->t('请填写公司网址(选填)')?>" />
                                        </div>
                                        <div class="pa f14 form-tip none"></div>
                                    </div>
                                    <input type="hidden" name = "member_id" value="<{$member_id}>">
                                    <div class="pr upload-blpic">
                                        <em class="dib pr vm">
                                            <input class="upload-blid" type="file" value="" />
                                            <s><?=$controller->t('上传有效证件')?></s>
                                        </em>
                                        <b class="dib vm"><?=$controller->t('营业执照、组织机构代码等，图片大小不超过10M')?></b>
                                        <input type="hidden" name="passport_photo" value="" />
                                        <div class="pa f14 form-tip none" style="padding-left:.62rem;"></div>
                                        <input type="hidden" name="data" value="<?=$data?>" />
                                    </div>
                                    <div class="reg-step-btn"><button type="submit" style="margin-left:-1rem;"><?=$controller->t('立即注册')?></button></div>
                                </form>
                            </div>
                            <div class="reg-comp-panel none">
                                <form action="###" class="js-verifi-foreign-company">
                                    <div class="login-input-outer reg-input-outer reg-compinput-outer pr flex-box">
                                        <div class="label"><?=$controller->t('公司名称')?>：</div>
                                        <div class="flex">
                                            <input type="text" name="companyname" class="login-input" autocomplete="off" value="" placeholder="<?=$controller->t('请按营业执照填写')?>" />
                                        </div>
                                        <div class="pa f14 form-tip none"></div>
                                    </div>
                                    <div class="login-input-outer reg-input-outer reg-compinput-outer pr flex-box">
                                        <div class="label"><?=$controller->t('商业注册码')?>：</div>
                                        <div class="flex">
                                            <input type="text" name="foreign_companyid" class="login-input" autocomplete="off" value="" placeholder="<?=$controller->t('请按营业执照填写')?>" />
                                        </div>
                                        <div class="pa f14 form-tip none"></div>
                                    </div>
                                    <div class="login-input-outer reg-input-outer reg-compinput-outer pr flex-box">
                                        <div class="label"><?=$controller->t('公司地址')?>：</div>
                                        <div class="flex">
                                            <input type="text" name="companyaddr" class="login-input" autocomplete="off" value="" placeholder="<?=$controller->t('请填写实际所在地')?>" />
                                        </div>
                                        <div class="pa f14 form-tip none"></div>
                                    </div>
                                    <div class="login-input-outer reg-input-outer reg-compinput-outer pr flex-box">
                                        <div class="label"><?=$controller->t('公司行业')?>：</div>
                                        <div class="flex pr js-select-panel">
                                            <div class="login-input" style="color:#b4b4b4;cursor:pointer"><?=$controller->t('请选择行业')?></div>
                                            <div class="pa select-panel js-select-list none">
                                                <dl class="select-list">
                                                    <dd data-value="1"><?=$controller->t('地产')?></dd>
                                                    <dd data-value="2"><?=$controller->t('酒店')?></dd>
                                                    <dd data-value="3"><?=$controller->t('建筑设计')?></dd>
                                                    <dd data-value="4"><?=$controller->t('室内设计')?></dd>
                                                    <dd data-value="5"><?=$controller->t('产品设计')?></dd>
                                                    <dd data-value="6"><?=$controller->t('卖场及代理')?></dd>
                                                    <dd data-value="7"><?=$controller->t('项目管控')?></dd>
                                                </dl>
                                            </div>
                                            <b class="pa icon-sprite select-arrow"></b>
                                        </div>
                                        <input type="hidden" value="-1" name="industry" />
                                        <div class="pa f14 form-tip none"></div>
                                    </div>
                                    <div class="login-input-outer reg-input-outer reg-compinput-outer pr flex-box">
                                        <div class="label"><?=$controller->t('公司人数')?>：</div>
                                        <div class="flex pr js-select-panel">
                                            <div class="login-input" style="color:#b4b4b4;cursor:pointer"><?=$controller->t('请选择人数')?></div>
                                            <div class="pa select-panel js-select-list none">
                                                <dl class="select-list">
                                                    <dd data-value="1"><?=$controller->t('20人以下')?></dd>
                                                    <dd data-value="2">20-50<?=$controller->t('人')?></dd>
                                                    <dd data-value="3">50-100<?=$controller->t('人')?></dd>
                                                    <dd data-value="4"><?=$controller->t('100人以上')?></dd>
                                                </dl>
                                            </div>
                                            <b class="pa icon-sprite select-arrow"></b>
                                        </div>
                                        <input type="hidden" value="-1" name="dim" />
                                        <div class="pa f14 form-tip none"></div>
                                    </div>
                                    <div class="login-input-outer reg-input-outer reg-compinput-outer pr flex-box">
                                        <div class="label"><?=$controller->t('联系人')?>：</div>
                                        <div class="flex">
                                            <input type="text" name="conact" class="login-input" autocomplete="off" value="" placeholder="<?=$controller->t('请填写联系人的姓名')?>" />
                                        </div>
                                        <div class="pa f14 form-tip none"></div>
                                    </div>
                                    <div class="login-input-outer reg-input-outer reg-compinput-outer pr flex-box">
                                        <div class="label"><?=$controller->t('公司网址')?>：</div>
                                        <div class="flex">
                                            <input type="text" name="website" class="login-input" autocomplete="off" value="" placeholder="<?=$controller->t('请填写公司网址(选填)')?>" />
                                        </div>
                                        <div class="pa f14 form-tip none"></div>
                                    </div>
                                    <input type="hidden" name = "member_id" value="<{$member_id}>">
                                    <div class="pr upload-blpic">
                                        <em class="dib pr vm">
                                            <input class="upload-blid" type="file" value="" />
                                            <s><?=$controller->t('上传有效证件')?></s>
                                        </em>
                                        <b class="dib vm"><?=$controller->t('营业执照、组织机构代码等，图片大小不超过10M')?></b>
                                        <input type="hidden" name="passport_photo" value="" />
                                        <div class="pa f14 form-tip none" style="padding-left:.62rem;"></div>
                                        <input type="hidden" name="data" value="<?=$data?>" />
                                    </div>
                                    <div class="reg-step-btn"><button type="submit" style="margin-left:-1rem;"><?=$controller->t('立即注册')?></button></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-sub">
        <div class="wrapper">
            <div class="container tc">
                <p>Copyright ©2019 珠海市棐舍科技有限公司 版权所有</p>
                <p>“Vanseo Fei”以及“Vanseo Fei”图形均为Vanseo Fei Technology Co.,Ltd的商标，且在世界多个地点均有注册</p>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/public/assets/js/jquery-min.js"></script>
    <script type="text/javascript" src="/public/assets/js/webfont.js"></script>
    <script type="text/javascript" src="/public/assets/js/main.js"></script>
</body>
</html>
