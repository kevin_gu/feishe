<!DOCTYPE html>
<html lang="zh-CN" fixed>
<head>
    <meta charset="utf-8">
    <title>Vanseo Fei</title>
    <link type="text/css" rel="stylesheet" href="/public/assets/css/main.css?t=<?=date("i")?>">
    <script type="text/javascript" src="/public/assets/js/response.js?t=<?=date("i")?>"></script>
    <style type="text/css">
    html,body{overflow:hidden;}
    @media screen and (max-width:1919px) {
        .wrapper {
            width: 15.5rem
        }
    }
    @media screen and (min-width:1920px) and (max-width:2500px) {
        .wrapper {
            width: 17rem
        }
    }
    @media screen and (min-width:2560px) {
        .wrapper {
            width: 18.7rem
        }
    }
    </style>
</head>
<body>
<div class="login-panel">
    <div class="pr wrapper" hPer100>
        <div class="container" style="padding:0 1rem">
            <div class="login-top">
                <a href="/"><img src="/public/assets/images/logo.svg" alt="" /></a>
                <span class="dib"><?=$controller->t('欢迎登录')?>！</span>
            </div>
        </div>
        <div class="login-area">
            <div class="login-title tc"><?=$controller->t('用户登录')?></div>
            <form action="/index.php/passport-post_login.html" method="post" id="user-login">
                <div class="login-input-outer pr flex-box">
                    <div class="icon-sprite icon-user"></div>
                    <div class="flex">
                        <input type="text" name="username" class="login-input" autocomplete="off" value="" placeholder="<?=$controller->t('请输入邮箱账号')?>" />
                    </div>
                    <div class="pa f14 form-tip none"></div>
                </div>
                <div class="login-input-outer pr flex-box">
                    <div class="icon-sprite icon-pwd"></div>
                    <div class="flex">
                        <input type="password" name="password" class="login-input" autocomplete="off" value="" placeholder="<?=$controller->t('请输入密码')?>" />
                    </div>
                    <div class="pa f14 form-tip none"></div>
                </div>
                <div class="login-vcode-outer pr tc">
                        <span class="dib">
                            <input type="text" name="vcode" class="vcode-input vm" placeholder="<?=$controller->t('请输入验证码')?>" />
                        </span>
                    <div class="pa f14 form-tip none"></div>
                    <img id="vcode" class="vm" data-src="/index.php/index-gen_vcode-b2c-4.html" src="/index.php/index-gen_vcode-b2c-4.html" alt="" />
                </div>
                <div class="login-submit-outer tc">
                    <button type="submit" class="vm"><?=$controller->t('登录')?></button>
                    <span class="change-vcode dib vm"><a href="javascript:;"><?=$controller->t('换一个')?></a></span>
                </div>
            </form>
            <div class="login-help-outer">
                <div class="flex-box">
                    <div class="flex" holdUser>
                            <span class="dib js-remember">
                                <i class="dib vm pr"></i>
                                <em class="dib"><?=$controller->t('记住用户名')?></em>
                            </span>
                    </div>
                    <div class="flex tr" findPwd>
                        <a href="/index.php/passport-lost.html"><?=$controller->t('忘记密码')?>？</a>
                    </div>
                </div>
            </div>
            <div class="login-help-line"></div>
            <div class="login-help-outer">
                <div class="flex-box">
                    <div class="flex" holdUser>
                        <a href="/index.php/passport-signup.html"><?=$controller->t('还不是会员')?>？</a>
                    </div>
                    <div class="flex tr" findPwd>
                        <a href="/index.php/passport-signup.html"><?=$controller->t('免费注册')?>></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer-sub">
    <div class="wrapper">
        <div class="container tc">
            <p>Copyright ©2019 珠海市棐舍科技有限公司 版权所有</p>
                <p>“Vanseo Fei”以及“Vanseo Fei”图形均为Vanseo Fei Technology Co.,Ltd的商标，且在世界多个地点均有注册</p>
        </div>
    </div>
</div>
<script type="text/javascript" src="/public/assets/js/jquery-min.js"></script>
<script type="text/javascript" src="/public/assets/js/webfont.js"></script>
<script type="text/javascript" src="/public/assets/js/main.js"></script>
</body>
</html>
