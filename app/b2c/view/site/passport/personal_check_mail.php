<!DOCTYPE html>
<html lang="zh-CN" fixed>
<head>
    <meta charset="utf-8">
    <title>Vanseo Fei</title>
    <link type="text/css" rel="stylesheet" href="/public/assets/css/main.css">
     <script type="text/javascript" src="/public/assets/js/response.js?t=<?=date("i")?>"></script>
     <style type="text/css">
    html,body{overflow:hidden;}
    </style>
</head>
<body>
    <div class="login-panel reg-panel">
        <div class="pr wrapper" hPer100>
            <div class="container">
                <div class="login-top clearfix">
                    <div class="fr" goLogin>
                        <a href="/index.php/passport-login.html"><?=$controller->t('已有账号？请登录')?></a>
                        <i>〉</i>
                    </div>
                    <a href="/"><img src="/public/assets/images/logo.svg" alt="" /></a>
                    <span class="dib"><?=$controller->t('欢迎注册')?>！</span>
                </div>
            </div>
            <div class="reg-area-outer js-verifi-email">
                <div class="reg-area-inner">
                   <div class="reg-step-panel clearfix">
                      <div class="fl reg-step-3 reg-step-3_1 tc current">
                          <div class="fl step-text">
                              <i></i>
                              <span><?=$controller->t('验证邮箱')?></span>
                          </div>
                          <div class="fl icon-sprite step-mark"></div>
                      </div>
                      <div class="fl reg-step-3 reg-step-3_2 tc">
                          <div class="fl step-text">
                              <i></i>
                              <span><?=$controller->t('填写账号信息')?></span>
                          </div>
                          <div class="fl icon-sprite step-mark"></div>
                      </div>
                      <div class="fl reg-step-3 reg-step-3_3 tc">
                          <div class="fl step-text">
                              <i></i>
                              <span><?=$controller->t('注册成功')?></span>
                          </div>
                      </div>
                   </div>
                   <div class="login-input-outer reg-input-outer pr flex-box">
                    <div class="icon-sprite icon-email"></div>
                    <div class="flex">
                        <input type="text" name="email" class="login-input" autocomplete="off" value="" placeholder="<?=$controller->t('请输入邮箱地址')?>" />
                    </div>
                    <div class="pa f14 form-tip none"></div>
                    <div class="send-email" use><?=$controller->t('发送验证邮件')?></div>
                    <div class="send-email none" unuse><?=$controller->t('60秒后重发')?></div>
                  </div>
                  <div class="reg-step-tip tc"><?=$controller->t('请发送验证邮件至您输入的邮箱，如果您已点击确认操作，请点击下一步')?></div>
                   <div class="reg-step-btn"><button type="button"><?=$controller->t('下一步')?></button></div>
                   <div class="tc reg-enp"><a href="/index.php/passport-company_signup.html"><?=$controller->t('企业用户注册')?></a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-sub">
        <div class="wrapper">
            <div class="container tc">
                <p>Copyright ©2019 珠海市棐舍科技有限公司 版权所有</p>
                <p>“Vanseo Fei”以及“Vanseo Fei”图形均为Vanseo Fei Technology Co.,Ltd的商标，且在世界多个地点均有注册</p>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/public/assets/js/jquery-min.js"></script>
    <script type="text/javascript" src="/public/assets/js/webfont.js"></script>
    <script type="text/javascript" src="/public/assets/js/main.js"></script>
</body>
</html>
