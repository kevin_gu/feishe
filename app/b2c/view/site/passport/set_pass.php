<!DOCTYPE html>
<html lang="zh-CN" fixed>
<head>
    <meta charset="utf-8">
    <title>Vanseo Fei</title>
    <link type="text/css" rel="stylesheet" href="/public/assets/css/main.css">
     <script type="text/javascript" src="/public/assets/js/response.js?t=<?=date("i")?>"></script>
     <style type="text/css">
    html,body{overflow:hidden;}
    </style>
</head>
<body>
    <div class="login-panel reg-panel">
        <div class="pr wrapper" hPer100>
            <div class="container">
                <div class="login-top clearfix">
                    <div class="fr" goLogin>
                        <a href="/index.php/passport-login.html"><?=$controller->t('已有账号？请登录')?></a>
                        <i>〉</i>
                    </div>
                    <a href="/"><img src="/public/assets/images/logo.svg" alt="" /></a>
                    <span class="dib"><?=$controller->t('忘记密码')?></span>
                </div>
            </div>
            <div class="reg-area-outer">
                <div class="reg-area-inner">
                   <div class="reg-step-panel clearfix">
                      <div class="fl reg-step-3 reg-step-3_1 tc current ok">
                          <div class="fl step-text">
                              <i></i>
                              <span><?=$controller->t('验证邮箱')?></span>
                          </div>
                          <div class="fl icon-sprite step-mark"></div>
                      </div>
                      <div class="fl reg-step-3 reg-step-3_2 tc current">
                          <div class="fl step-text">
                              <i></i>
                              <span><?=$controller->t('设置新密码')?></span>
                          </div>
                          <div class="fl icon-sprite step-mark"></div>
                      </div>
                      <div class="fl reg-step-3 reg-step-3_3 tc">
                          <div class="fl step-text">
                              <i></i>
                              <span><?=$controller->t('修改成功')?></span>
                          </div>
                      </div>
                   </div>
                   <form action="###" class="js-edit-pass">
                      <div class="login-input-outer reg-input-outer pr flex-box">
                        <div class="icon-sprite icon-pwd"></div>
                        <div class="flex">
                            <input type="password" id="pwd" name="pwd" class="login-input" autocomplete="off" value="" placeholder="<?=$controller->t('请输入新密码')?>" />
                        </div>
                        <div class="pa f14 form-tip none"></div>
                      </div>
                      <div class="login-input-outer reg-input-outer pr flex-box">
                        <div class="icon-sprite icon-pwd"></div>
                        <div class="flex">
                            <input type="password" id="repwd" name="repwd" class="login-input" autocomplete="off" value="" placeholder="<?=$controller->t('请再次输入新密码')?>" />
                        </div>
                          <input type="hidden" name = "p" value="<?=$email?>">
                        <div class="pa f14 form-tip none"></div>
                      </div>
                      <div class="reg-step-btn"><button type="submit"><?=$controller->t('立即重置')?></button></div>
                  </form>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-sub">
        <div class="wrapper">
            <div class="container tc">
                <p>Copyright ©2019 珠海市棐舍科技有限公司 版权所有</p>
                <p>“Vanseo Fei”以及“Vanseo Fei”图形均为Vanseo Fei Technology Co.,Ltd的商标，且在世界多个地点均有注册</p>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/public/assets/js/jquery-min.js"></script>
    <script type="text/javascript" src="/public/assets/js/webfont.js"></script>
    <script type="text/javascript" src="/public/assets/js/main.js"></script>
</body>
</html>
