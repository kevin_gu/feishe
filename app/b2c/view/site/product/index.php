<script>
    var specs = <?=json_encode($productBasic['spec'])?>;
    var annexList = <?=json_encode($goodsAdjunct)?>;
    var modleFiles = <?=json_encode($productBasic['files_id'])?>;
</script>
<script type="text/tpl" id="specs-tpl">
<div class="clearfix">
    <ul class="filter-tab f16 fl none">
        {{ for(var i = 0; i < it.specification.spec_name.length; i++){ }}
        <li>{{=it.specification.spec_name[i]}}</li>
        {{ } }}
    </ul>
    <div class="fr amount-panel">
        <div class="fl">
            <span class="dib vm">数量：</span>
            <span class="dib vm clearfix js-cartcount">
                <i class="fl pr tc reduce"></i>
                <input class="fl entry-data" type="text" value="1" />
                <i class="fl pr tc increase">+</i>
            </span>
        </div>
        <?php if($this->pagedata['is_login']){ ?>
        <div class="fl add-cart" data-goodsid="<?=$productBasic['goods_id']?>">
            <span class="dib vm"><?=$controller->t("加入心愿单")?></span>
        </div>
        <?php }else{ ?>
        <a href="/index.php/passport-login.html" class="fl add-cart">
            <span class="dib vm"><?=$controller->t("加入心愿单")?></span>
        </a>
        <?php } ?>
    </div>
 </div>
<div class="filter-bd none" style="width:99.99rem;">
    <div class="filter-wrapper flex-box">
        {{ for(var m = 0; m < it.specification.spec_type.length; m++ ){ }}
        {{ if(it.specification.spec_type[m] == 'text'){ }}
        <div class="filter-item scrollbar filter-style">
            <ul class="clearfix" style="{{! m == 0 ? 'margin-left:-.34rem;' : ''}}">
                {{ for(var p in it.goods[m]){ }}
                <li class="{{! it.goods[m][p].product_id ? '' : 'current'}}"><a title="{{=it.goods[m][p].spec_value}}" data-productid="{{! it.goods[m][p].product_id ? it.goods[m][p].product_id : 'no'}}" class="js-props" href="javascript:;">{{=it.goods[m][p].spec_value}}</a></li>
                {{ } }}
            </ul>
        </div>
        {{ }else if(it.specification.spec_type[m] == 'image'){ }}
        <div class="filter-item scrollbar filter-color">
            <ul class="clearfix" style="{{! m == 0 ? 'margin-left:-.34rem;' : ''}}">
                {{ for(var p in it.goods[m]){ }}
                <li class="{{! it.goods[m][p].product_id ? '' : 'current'}}"><i style="background:url({{=it.goods[m][p].spec_image}}) no-repeat;background-size:cover"></i><span class="pa zoom-effect" style="background:url({{=it.goods[m][p].spec_image}}) no-repeat;background-size:cover"></span><a title="{{=it.goods[m][p].spec_value}}" data-productid="{{! it.goods[m][p].product_id ? it.goods[m][p].product_id : 'no'}}" class="js-spec" href="javascript:;">{{=it.goods[m][p].spec_value}}</a></li>
                {{ } }}
            </ul>
        </div>
        {{ } }}
        {{ } }}
    </div>
</div>
</script>
<script type="text/tpl" id="model-tpl">
<ul class="pa model-list scrollbar none">
{{ if(modleFiles){ }}
    {{ for(var i = 0; i < it.length; i++){ }}
        <li><a href="{{=it[i].file_path}}" title="{{=it[i].file_name}}">{{=it[i].file_name}}</a></li>
    {{ } }}
{{ }else{ }}
    <li><a href="javascript:;"><?=$controller->t("暂无模型下载")?></a></li>
{{ } }}
</ul>
</script>
<script type="text/tpl" id="annexlist-tpl">
{{ if(it.length){ }}
<ul class="pa present-goods clearfix parts-checkbox">
    {{ for(var i = 0; i < it.length; i++){ }}
    <li title="{{=it[i].name}}">
        <div class="thumb"><img src="{{=it[i].image}}" alt=""></div>
        <p><a href="/index.php/product-{{=it[i].product_id}}.html">{{=it[i].name}}</a></p>
        <i class="checkbox{{=(it[i].checked ? ' checkbox-checked' : '')}}"></i>
    </li>
    {{ } }}
</ul>
{{ } }}
</script>
<script type="text/tpl" id="compare-tpl">
<div class="global-hd f16 tc">
    {{ if(it.length > 1){ }}
    <a href="javascript:;" class="compare-now"><?=$controller->t("查看对比详情")?></a>
    {{ }else{ }}
    <span><?=$controller->t("查看对比详情")?></span>
    {{ } }}
</div>
<div class="pa compare-panel" style="left:1.8rem;top:0;width:9.25rem;height:1.42rem;">
    {{ if(it.length){}}
    <ul class="pa compare-list clearfix" style="width:12rem">
        {{ for(var i = 0; i < it.length; i++){ }}
        <li data-typeid="{{=it[i].typeId}}" data-goodsid="{{=it[i].goodsId}}">
            <a href="{{=it[i].goodsUrl}}" class="db">
                <div class="thumb"><img src="{{=it[i].goodsImg}}" alt="" /></div>
                <p>{{=it[i].goodsTitle}}</p>
            </a>
            <span class="icon-sprite remove"></span>
        </li>
        {{ } }}
   </ul>
   {{ }else{ }}
   <div class="tc" style="padding-top:.2rem;">
        <img style="width:2rem;opacity:.5" src="/public/assets/images/del_ico.png" alt="" />
        <div class="f14" style="margin-top:.1rem;color:#ccc;"><?=$controller->t("还没有商品哦")?></div>
   </div>
   {{ } }}
</div>
<a class="remove-panel" href="javascript:;"></a>
</script>
<div class="header header-hasfix">
    <div class="wrapper">
        <div class="header-wrapper">
            <div class="pr clearfix">

                <?= $this->renderPartial('/top.php'); ?>

            </div>
            <div class="header-sub-menu">
                <?= $this->renderPartial('/menu.php'); ?>
            </div>
        </div>
    </div>
</div>
<div class="final-slider">
    <div class="wrapper">
        <div class="pr container" h690>
            <div id="detail-slider" class="pr slider-container">
                <div class="pr oh slider-wrapper swiper-wrapper">
                    <?php
                    foreach ($productBasic['images'] as $v)
                    {
                        ?>
                    <div class="swiper-slide slider-item" h460>
                        <img src="<?=$img_model->get_url($v['image_id'])?>" alt="" />
                    </div>
                    <?php }
                    ?>
                </div>
                <div class="pa tc slider-pagination"></div>
                <a href="javascript:;" class="pa slider-btn slider-prev-btn"></a>
                <a href="javascript:;" class="pa slider-btn slider-next-btn"></a>
            </div>
            <div class="pa slider-title"><?=$productBasic['title']?></div>
            <div data-typeid="<?=$productBasic['type_id']?>" data-goodsid="<?=$productBasic['goods_id']?>" class="pa slider-annex">
                <?php if($this->pagedata['is_login']){ ?>
                    <?php if($productBasic['is_fav'] == true){ ?>
                    <span class="flex js-textChange favor favored">
                        <img class="vm" src="/public/assets/images/favoring.gif?v=<?=$productBasic['goods_id']?>" alt="" />
                        <span class="dib vm"><?=$controller->t("已收藏")?></span>
                    </span>
                    <?php }else{ ?>
                    <span class="flex js-textChange favor">
                        <img class="vm" src="/public/assets/images/favoring.gif?v=<?=$productBasic['goods_id']?>" alt="" />
                        <span class="dib vm"><?=$controller->t("收藏")?></span>
                    </span>
                    <?php } ?>
                <?php }else{ ?>
                    <a class="flex favor" href="/index.php/passport-login.html">
                        <span class="dib vm"><?=$controller->t("收藏")?></span>
                    </a>
                <?php } ?>

            </div>
            <div class="pa slider-model">
                <div class="model-hd hide"><span class="dib vm"><?=$controller->t("模型下载")?></span></div>
            </div>
        </div>
    </div>
</div>
<div class="oh main">
    <div class="wrapper">
        <div class="container clearfix" goodsList>
            <div id="detail-filter-panel" class="pr filter-panel">
                <div class="filter-hd f18 none"><?=$controller->t("选择商品样式")?></div>
                <div class="clearfix filter-outer">
                    <div class="fr amount-panel">
                        <div class="fl">
                            <span class="dib vm"><?=$controller->t("数量")?>：</span>
                            <span class="dib vm clearfix js-cartcount">
                                <i class="fl pr tc reduce"></i>
                                <input class="fl entry-data" type="text" value="1" />
                                <i class="fl pr tc increase">+</i>
                            </span>
                        </div>
                        <?php if($this->pagedata['is_login']){ ?>
                        <div class="fl add-cart" data-goodsid="<?=$productBasic['goods_id']?>">
                            <span class="dib vm"><?=$controller->t("加入心愿单")?></span>
                        </div>
                        <?php }else{ ?>
                        <a href="/index.php/passport-login.html" class="fl add-cart">
                            <span class="dib vm"><?=$controller->t("加入心愿单")?></span>
                        </a>
                        <?php } ?>
                    </div>
                 </div>
            </div>
        </div>
        <div class="container clearfix" goodsList>
            <div class="assort-panel none">
                <ul class="assort-tab f16 clearfix">
                    <?php
                    foreach ($goodsAdjunct as $k=>$v)
                    {
                        ?>
                        <li class="<?php if ($k == 0) echo 'current';  ?>"><?=$v['name']?></li>
                    <?php }
                    ?>
                </ul>
                <div class="assort-bd">
                    <div class="assort-item flex-box">
                        <ul class="present-goods clearfix">
                            <li title="<?=$productBasic['title']?>">
                                <a href="" class="db">
                                    <div class="thumb"><img src="<?=$img_model->get_url($productBasic['image_default_id'])?>" alt=""></div>
                                    <p><?=$productBasic['title']?></p>
                                </a>
                            </li>
                        </ul>
                        <div class="assort-bol pr"></div>
                        <a href="javascript:;" class="pr assort-exec prev"></a>
                        <div class="assort-wrap pr flex">
                            <div class="pa oh assort-inner">
                                <div class="pa assort-list" style="width:99.99rem;"></div>
                            </div>
                            <div class="pa assort-annex flex-box">
                                <a href="javascript:;" class="pr assort-exec next"></a>
                                <span class="db icon-sprite assort-eq"></span>
                                <div class="flex tr">
                                    <p><?=$controller->t("已选择<em>{count}</em>个配件",['count'=>0])?></p>
                                    <input id="annex-product" type="hidden" value="" />
                                    <a href="javascript:;" class="dib add-cart"><span class="dib vm"><?=$controller->t("加入心愿单")?></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container clearfix" goodsList>
            <div class="goods-detail-panel">
                <div class="goods-detail-hd f16"><?=$controller->t("商品介绍")?></div>
                <div class="goods-detail-bd">
                    <ul class="clearfix">
                        <li>
                            <div class="goods-key"><?=$controller->t("商品名称")?></div>
                            <div class="goods-value"><?=$productBasic['title']?></div>
                        </li>
                        <li>
                            <div class="goods-key"><?=$controller->t("品牌")?></div>
                            <div class="goods-value"><?=$productBasic['brand']['brand_name']?></div>
                        </li>
                        <?php
                        foreach ($productBasic['props'] as $k=>$v)
                        {?>
                        <li>
                            <div class="goods-key"><?=$v['name']?></div>
                            <div class="goods-value"><?=$v['value']?></div>
                        </li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container clearfix" goodsList>
            <div class="goods-desc-panel">
                <div class="goods-desc-bd clearfix">
                    <div class="fl goods-desc-textwrap f14">
                        <div class="goods-desc-hd f18 clearfix"><span class="fl"><?=$productBasic['brief']?></span></div>
                        <div class="goods-desc-tp">
                        <?php
                        $i=0;
                        foreach ($productBasic['detail_ext_imgdesc'] as $v)
                        {
                            ?>
                            <div class="goods-desc-txt <?php if ($i!==0) echo 'none'?>"><p><?=$v?></p></div>
                            <?php
                            $i++;
                        }
                        ?>
                        </div>
                    </div>
                    <div class="fr pr goods-desc-pics">
                        <div class="oh goods-slider-container" id="desc-slider">
                            <div class="goods-slider-wrapper swiper-wrapper">
                                <?php
                                foreach ($productBasic['detail_ext_img'] as $v)
                                {
                                    ?>
                                    <div class="goods-slider-item swiper-slide"><img src="/<?=$v['file_path']?>" alt="" /></div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <a href="javascript:;" class="pa slider-btn slider-prev-btn" id="desc-prev-btn"></a>
                        <a href="javascript:;" class="pa slider-btn slider-next-btn" id="desc-next-btn"></a>
                    </div>
                </div>
                <div class="goods-desc-bd flex-box">
                    <div class="goods-scene-pics flex">
                        <div class="pr goods-slider-container">
                            <div class="pa goods-slider-wrapper" style="width:999.99rem;overflow:hidden">
                                <div class="goods-slider-item"><img src="/<?=$productBasic['detail_ext2_img'][0]['file_path']?>" alt="" /></div>
                                <ul class="pa goods-slider-list" style="left:10.4rem;">
                                 <?php
                                 $first = 0;
                                foreach ($productBasic['detail_ext2_img'] as $v)
                                {
                                    if($first==0){$first++;continue;}
                                   ?>
                                    <li><img src="/<?=$v['file_path']?>" alt="" /></li>
                                    <?php
                                }
                                ?>
                                </ul>
                            </div>
                            <a href="javascript:;" class="pa slider-btn slider-prev-btn" id="scene-prev-btn"></a>
                            <a href="javascript:;" class="pa slider-btn slider-next-btn" id="scene-next-btn"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if($goodsLink){ ?>
        <div class="container clearfix" goodsList>
            <div class="goods-panel">
                <div class="goods-hd f16"><?=$controller->t("相关商品")?></div>
                <ul class="goods-list clearfix">
                    <?php
                    foreach ($goodsLink as $v)
                    {
                        ?>
                        <li data-productid="<?=$productBasic['product_id']?>" data-typeid="<?=$v['type_id']?>" data-goodsid="<?=$v['goods_id']?>" data-goodstitle="<?=$v['name'] ?>" data-goodsurl="<?=$v['product_url']?>" data-goodsimg="<?=$v['image_default_src']?>">
                            <div class="pr thumb-item">
                                <a href="<?=$v['product_url']?>" class="db" target="_blank">
                                    <div class="thumb"><img src="<?=$v['image_default_src']?>" alt="" /></div>
                                    <p><?=$v['name']?></p>
                                </a>
                                <span class="pa icon-sprite add-compare"></span>
                                <?php if($this->pagedata['is_login']){ ?>
                                <span class="pa icon-sprite add-cart" title="加入心愿单"></span>
                                <?php }else{ ?>
                                <a href="/index.php/passport-login.html" class="pa icon-sprite add-cart" title="加入心愿单"></a>
                                <?php } ?>
                            </div>
                            <?php if($this->pagedata['is_login']){ ?>
                                <?php if($v['is_fav'] == true){ ?>
                                    <span class="pa icon-sprite favor favored"></span>
                                    <img class="pa favoring none" src="/public/assets/images/favoring.gif?v=<?=$v['goods_id']?>" />
                                <?php }else{ ?>
                                    <span class="pa icon-sprite favor"></span>
                                    <img class="pa favoring none" src="/public/assets/images/favoring.gif?v=<?=$v['goods_id']?>" />
                                <?php } ?>
                            <?php }else{ ?>
                                <a href="/index.php/passport-login.html" class="pa icon-sprite favor"></a>
                            <?php } ?>
                        </li>
                        <?php
                    }
                    ?>

                </ul>
            </div>
        </div>
        <?php } ?>
    </div>
    <div class="global-nav cp">
        <img src="/public/assets/images/mico_st.gif" alt="" />
        <img class="mico " src="/public/assets/images/mico.png" alt="" />
        <span><?=$controller->t("添加成功")?></span>
    </div>
    <div class="global-panel"></div>
</div>
<?= $this->renderPartial('/footer.php'); ?>
