<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */

/**
 * Class base_controller
 * @property boolean $transaction_start
 * @property array|boolean $transaction_status
 * @property string $_action_url
 * @property mixed $_end_status
 */
class base_controller extends base_render{

    var $pagedata = array();
    var $force_compile = 0;
    var $_tag_stack = array();
    public $_end_message = null;

    function begin($url_params=null){
        set_error_handler(array(&$this,'_errorHandler'), E_USER_ERROR | E_ERROR);
        if($this->transaction_start) trigger_error('The transaction has been started',E_USER_ERROR);
        $db = kernel::database();
        $this->transaction_status = $db->beginTransaction();
        $this->transaction_start = true;
        if(is_array($url_params)){
            $this->_action_url = $this->app->router()->gen_url($url_params);
        }else{
            $this->_action_url = $url_params;
        }
    }
    
    function endonly($result=true){
        if(!$this->transaction_start) trigger_error('The transaction has not started yet',E_USER_ERROR);
        $this->transaction_start = false;
        $db = kernel::database();
        restore_error_handler();
        if($result){
            $db->commit($this->transaction_status);
        }else{
            $db->rollback();
        }
    }

    function end($result=true,$message=null,$url_params=null,$params=array()){
        if(!$this->transaction_start) trigger_error('The transaction has not started yet',E_USER_ERROR);
        $this->transaction_start = false;
        $db = kernel::database();
        restore_error_handler();
        if(is_null($url_params)){
            $url = $this->_action_url;
        }elseif(is_array($url_params)){
            $url = $this->app->router()->gen_url($url_params);
        }else{
            $url = $url_params;
        }
        if($result){
            $db->commit($this->transaction_status);
            $status = 'success';
            $message = ($message=='' ? app::get('base')->_('操作成功！') : app::get('base')->_('成功：').$message);
        }else{
            $db->rollback();
            $status = 'error';
            $message = $message?$message:app::get('base')->_("操作失败: 对不起,无法执行您要求的操作");
        }
        $this->_end_message = $message;
        $this->_end_status = $status;
        $this->splash($status,$url,$message,'redirect',$params);
    }
    
    function splash($status='success',$url=null,$msg=null,$method='redirect',$params=array()){
        header("Cache-Control:no-store, no-cache, must-revalidate"); // HTTP/1.1
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");// 强制查询etag
        header('Progma: no-cache');
        header('Location: '.$url);
    }

    function page($detail){
        header('Content-type: text/html; charset=utf-8');
        $object = kernel::service('theme');
        if($object){
            $object->display($detail);
        }else{
            $this->display($detail);
        }
    }

    function _errorHandler($errno, $errstr, $errfile, $errline){
        if($errno==E_ERROR){
            $errstr = basename($errfile).':'.$errline.'&nbsp;'.$errstr;
        }elseif($errno == E_USER_ERROR){
            $errstr = $errstr;
        }else{
            return;    
        }

        $this->splash('error',$this->_action_url,$errstr);
        header('Location: '.$this->_action_url);
        return true;
    }




    public $layout = "main.php";
    public $head_js = [];
    public $footer_js = [];
    public $pre_footer_js = [];
    public $head_css = ['main.css'];
    public $footer_css = [];
    public $raw_header;
    public $raw_footer;
    public $append_header;
    public $append_footer;
    public $title;
    public $keyword;
    public $description;
    public $bodyClass = '';

    /**
     * 获取模板文件的真实路径
     * @param string $view
     * @return string
     */
    public function  getViewPath($view){
        if($view[0] == '/'){
            return ROOT_DIR.'/layout/common/'.$view;
        }else{
            return  realpath(APP_DIR.'/'.($this->app->app_id).'/view/'.$view);
        }
    }
    /**
     * 获取布局文件的真实路径
     * @param string $view
     * @return string
     */
    public function  getLayoutPath(){
        return ROOT_DIR.'/layout/'.$this->layout;
    }

    /*app访问模板*/
    public function beforeRender($view){
        return $view;
    }
    public function beforeOutput($content)
    {
        return $content;
    }
    /**
     * 渲染模板文件，可以设置缓存，并输出
     * @param string $view
     * @param mixed $data
     */
    public function render($view, $data){
        $view = $this->beforeRender($view);
        $output = $this->_render($view,$data);
        $output = $this->beforeOutput($output);
        echo $output;
    }

    public function renderPartial($view,$data=null)
    {
        $layout = $this->layout;
        $this->layout = null;
        $content = $this->_render($view,$data);
        $this->layout = $layout;
        return $content;
    }

    /**
     * 渲染模板文件不输出，返回渲染后的内容
     * @param string $view
     * @param array $data
     * @return string
     */
    private function _render($view, $data=null){
        ob_start();
        if(is_array($data) && $data)
            extract($data,EXTR_OVERWRITE);
        $controller = $this;
        $__controller =$this;
        if($this->layout){
            ob_start();
            include $this->getViewPath($view);
            $__head_js__ = $this->head_js;
            $__head_css__= $this->head_css;
            $__footer_css__= $this->footer_css;
            $__footer_js__= $this->footer_js;
            $__pre_footer_js__ = $this->pre_footer_js;
            $__raw_header = $this->raw_header;
            $__raw_footer = $this->raw_footer;
            $__append_header=$this->append_header;
            $__append_footer=$this->append_footer;
            $title=$this->title;
            $keyword=$this->keyword;
            $decription=$this->description;
            $__content__ = ob_get_contents();
            $__bodyClass = $this->bodyClass;
            ob_end_clean();
            include $this->getLayoutPath();
        }else{
            include $this->getViewPath($view);
        }
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    /**
     * 生成url
     * @param $params
     * @return mixed
     */
    public function link($params)
    {
        return kernel::router()->gen_url($params);
    }

    /**
     * @param $image_id
     * @param string $size
     * @return string
     */
    public function image_path($image_id,$size='')
    {
        return base_storager::image_path($image_id,$size);
    }

    /**
     * 多语言
     * @param $words
     * @param array $params
     * @return string
     */
    public function t($words,$params=[])
    {
        return i18n_mdl_words::translate($words,$params);
    }

    /**
     * @return int
     */
    public function getMemberId()
    {
        if(!$this->app->member_id){
            $userObject = kernel::single('b2c_user_object');
            return $userObject->get_member_id();
        }
        return $this->app->member_id;
    }

    /**
     * 是否为白名单
     * @return bool
     */
    public function getMemberIsWhite()
    {
        if(!isset($_SESSION['is_white'])){
            $mdl_member = $this->app->model('members');
            $result = $mdl_member->getRow('is_white',['member_id'=>$this->getMemberId()]);
            $_SESSION['is_white'] = $result['is_white'];
        }
        return $_SESSION['is_white'] == 1;
    }


    /**
     * 分页
     * @param $pager
     * @return string
     */
    public function pager($pager)
    {
        $prev = '###';
        $next = '###';
        if ($pager['current'] != 1) {
            $prev = str_replace($pager['token'], ($pager['current'] - 1), $pager['link']);
        }
        if ($pager['current'] != $pager['total']) {
            $next = str_replace($pager['token'], ($pager['current'] + 1), $pager['link']);
        }
        $pageHtml = '';
        for ($i = 1; $i <= $pager['total']; $i++) {
            if ($i == $pager['current']) {
                $pageHtml .= '<b>' . $i . '</b>';
            } else {
                $url = str_replace($pager['token'], $i, $pager['link']);
                $pageHtml .= '<a href="' . $url . '">' . $i . '</a>';
            }
        }
        $html = '';
        if ($pager['total']) {
            $html = <<< EOD
<div class="pagination tc">
    <span total>{$pager['total']}页</span>
    <a href="{$prev}">上一页</a>
    <a href="{$prev}" prev></a>
    <span pageNum>{$pageHtml} </span>
    <a href="{$next}" next> </a>
    <a href="{$next}" >下一页</a>
</div>
EOD;
        }
        return $html;
    }

    public function isGoodsFav($goods_id)
    {
        static  $favs = null;
        if($favs === null){
            $favs  = explode(',',$_COOKIE['S']['GFAV'][$this->getMemberId()]);
        }
        return in_array($goods_id,$favs);
    }
}
