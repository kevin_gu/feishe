 <div class="header header-hasfix">
        <div class="wrapper">
             <div class="header-wrapper">
                 <div class="pr clearfix">
                     <?= $this->renderPartial('/top.php'); ?>
                 </div>
                 <div class="header-sub-menu">
                     <?= $this->renderPartial('/menu.php'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="oh main">
        <div class="oh wrapper">
           <div class="container" goodsList>
                <div class="crumbs-panel">
                    <a href="/" class="vm">首页</a>
                    <i class="icon-sprite vm"></i>
                    <span class="vm">详情</span>
                </div>
            </div>
           <div class="container" goodsList>
                <div class="news-panel clearfix">
                    <!--<h1><?=$article['detail']['indexs']['title']?></h1>-->
                    <dl class="f14">
                    	<dt>尊敬的各位商城用户：</dt>
                    	<dd>
                            <?=$article['content']?>
                        </dd>
                        <dd>
                            Vanseo Fei商城管理员
                        </dd>
                        <dd class="date tr">发布时间：<?=date('Y-m-d',$article['detail']['indexs']['pubtime'])?></dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
 <?= $this->renderPartial('/footer.php'); ?>