<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */
 
class desktop_mdl_tag_rel extends dbeav_model{

    function save( &$item,$mustUpdate = null,$mustInsert = false){
        $list = parent::getList('*',array('tag_id'=>$item['tag']['tag_id'],'rel_id'=>$item['rel_id']));
        if($list && count($list)>0){
            $item = $list[0];
        }else{
            parent::save($item);
        }
    }


    function getGoodsIdsByTagId($tag_id)
    {
        $tag_id = intval($tag_id);
        if (!$tag_id)
            return false;

        $data =  parent::getList('rel_id',['tag_type'=>'goods','tag_id'=>$tag_id]);
        $rtn = [];
        foreach ($data as $v)
        {
            $rtn[] = $v['rel_id'];
        }
        return $rtn;
    }
}
