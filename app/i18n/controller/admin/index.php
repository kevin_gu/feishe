<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/3/7
 * Time: 16:40
 */
class i18n_ctl_admin_index extends desktop_controller
{
    function index(){
        $this->finder('i18n_mdl_words',
            array('title'=>'翻译词列表',
                'actions' =>
                    array(
                        array(
                            'label' => app::get('notebook')->_('添加翻译'),
                            'icon' => 'add.gif',
                            'href' => 'index.php?app=i18n&ctl=admin_index&act=add',
                            'target'=>'dialog::{ title:\''.app::get('b2c')->_('添加翻译').'\', width:600, height:120}'
                        ),
                    ),
                //'use_view_tab'=>true,
            ));


    }
    function add(){
        $this->page('admin/edit.html');
    }

    function edit(){
        header("cache-control:no-store,no-cache,must-revalidate");
        $id = $_GET["id"];
        $oItem = kernel::single('i18n_mdl_words');
        $row = $oItem->getList('*',array('id'=>$id),0,1);
        $this->pagedata['item'] = $row[0];
        $this->pagedata['page'] = $_GET['page'];
        $this->page('admin/edit.html');
    }

    function toEdit(){
        /**
         * @var $oItem i18n_mdl_words
         */
        $oItem = kernel::single("i18n_mdl_words");
        $arr = $_POST['item'];
        $arr['created_at'] = time();
        $arr['source_md5'] = md5($arr['source']);
        $this->begin('index.php?app=i18n&ctl=admin_index&act=index&page='.$_GET['page']);
        if($oItem->save($arr)){
            $this->end(true, "添加成功！");
        }else{
            $this->end(false,'添加失败,不能添加相同的中文词');
        }
    }
}