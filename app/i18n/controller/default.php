<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.com/license/gpl GPL License
 */

/**
 * Class orders_ctl_default
 *
 */
class orders_ctl_default extends base_controller{


    function index(){
        $this->pagedata['items'] = $this->app->model('item')->getList('*');
        $gravatar = new orders_gravatar();
        foreach($this->pagedata['items'] as $k=>$item){
            $this->pagedata['items'][$k]['avatar'] = $gravatar->get_avatar($item['item_email']);
        }

        foreach (kernel::servicelist('notebook_addon') as $object){
            foreach($this->pagedata['items'] as $k=>$item){
                $this->pagedata['items'][$k]['addon'][] = $object->get_output($item);
            }
        }
        //$this->display('default.html');
        $this->head_js[] = '/app.js';
        $this->title = "abc";
        $this->render('default.php',$this->pagedata);
    }

    public function addnew()
    {
        $this->begin(kernel::router()->gen_url(['ctl'=>'default','act'=>'index','app'=>'orders']));
        $data =  [
            'item_subject'=>$_POST['subject'],
            'item_content'=>$_POST['content'],
            'item_email'=>$_POST['email'],
            'item_posttime'=>time()
        ];
        $result = $this->app->model('item')->insert($data);
        $this->end($result);
    }
}