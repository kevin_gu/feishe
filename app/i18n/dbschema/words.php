<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/3/7
 * Time: 14:31
 */
$db['words'] = [
    'columns'=>[
        'id' => [
            'type'=>'number',
            'required'=>true,
            'extra'=>'auto_increment',
            'pkey'=>true,
        ],
        'source_md5'=>[
            'type' => 'char(32)',
            'label' => 'md5',
            'in_list' => false,
            'default_in_list' => false,
        ],
        'source'=>[
            'type'=>'text',
            'in_list'=>true,
            'is_title'=>true,
            'default_in_list'=>true,
            'label'=>'中文',
            'filtertype'=>true,
            'searchtype' => 'has',
            'order'=>10,
        ],
        'translate'=>[
            'label' => '英文',
            'in_list'=>true,
            'type'=>'text',
            'default_in_list'=>true,
            'order'=>20,
        ],

        'created_at'=>[
            'type'=>'time',
            'in_list'=>false,
            'default_in_list' => false,
            'label' => '时间',
        ]
    ],
    'index'=>[
        'idx_source_md5' =>
            array (
                'prefix'=>'UNIQUE',
                'columns' =>
                    array (
                        0 => 'source_md5',
                    ),
            ),
    ],
];