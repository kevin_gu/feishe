<?php


class i18n_mdl_words extends dbeav_model
{
    public static function add($word)
    {
        /**
         * @var $oItem i18n_mdl_words
         */
        $oItem = kernel::single("i18n_mdl_words");
        $arr = ['source'=>$word,'source_md5'=>md5($word)];
        $arr['created_at'] = time();
       return $oItem->save($arr);
    }

    public static function translate($word,$params=[])
    {
        if($_COOKIE['lang'] != 'en'){
            $t = $word;
            if($params){
                foreach ($params as $k=>$v){
                    $params['{'.$k.'}']=$v;
                }
                $t = strtr($t,$params);
            }
            return $t;
        }
        static $cache = null;
        $md5 = md5($word);
        if($cache === null){
            /**
             * @var $oItem i18n_mdl_words
             */
            $oItem = kernel::single("i18n_mdl_words");
            $list = $oItem->getList('source_md5,translate');
            $cache = [];
            foreach ($list as $item) {
                $cache[$item['source_md5']] = $item['translate'];
            }
        }
        if(!isset($cache[$md5])){
            static::add($word);
        }
        $t = $cache[$md5]?:$word;
        if($params){
            foreach ($params as $k=>$v){
                $params['{'.$k.'}']=$v;
            }
            $t = strtr($t,$params);
        }
        return $t;
    }

    public static function translateArray($data)
    {
        if(is_array($data)){
            foreach ($data as $k=>$v){
                $data[$k] = static::translateArray($v);
            }
        }else{
            if(trim($data) && preg_match('/[\x{4e00}-\x{9fa5}]+/u',$data)) {
                $data = static::translate($data);
            }
        }
        return $data;
    }
}