<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */

class logisticstrack_ctl_admin_tracker extends desktop_controller{

    public function __construct($app) {
        parent::__construct($app);
    }

    public function index(){
        base_kvstore::instance('ome/bind/hqepay')->fetch('ome_bind_hqepay', $is_ome_bind_hqepay);
        if($_POST){
            $system_order_tracking = $_POST['system_order_tracking'];
            app::get('b2c')->setConf('system.order.tracking',$system_order_tracking);
            $this->pagedata['system_order_tracking'] = $system_order_tracking;
            app::get('b2c')->setConf('system.order.hqepay',$_POST['hqepay']);
            $this->pagedata['hqepay'] = $_POST['hqepay'];
        }else{
            $this->pagedata['system_order_tracking'] = app::get('b2c')->getConf('system.order.tracking');
            $this->pagedata['hqepay'] = app::get('b2c')->getConf('system.order.hqepay');
        }
        //不走矩阵 改成直连形式
        // if($_POST['system_order_tracking']==='true'){
        //     $logi = kernel::single('logisticstrack_service_hqepay');
        //     $logi->bind();
        // }
        $this->page('admin/setting.html');
    }

    public function pull($deliveryid) {
        header("cache-control: no-store, no-cache, must-revalidate");
        header('Expires: Fri, 16 Dec 2000 10:38:27 GMT');
        header('Content-Type: text/html; charset=UTF-8');

        if ( logisticstrack_puller::pull_logi($deliveryid, $data) ) {
            $this->pagedata['logi'] = $data['Traces'];
        } else {
            $this->pagedata['logi_error'] = $data['msg'];
        }
        $this->display('admin/logistic_detail.html');
    }
}
