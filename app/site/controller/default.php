<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */

ini_set('display_errors',1);
error_reporting('E_ALL');
class site_ctl_default extends site_controller{
    function index(){
        $userObject = kernel::single('b2c_user_object');
        if(!$userObject->is_login()){
            $this->redirect("/index.php/passport-login.html");
        }
        if(defined('APP_SITE_INDEX_MAXAGE') && APP_SITE_INDEX_MAXAGE > 1){
            $this->set_max_age(APP_SITE_INDEX_MAXAGE);
        }//todo: 首页max-age设定

        if(kernel::single('site_theme_base')->theme_exists()){

            $obj = kernel::service('site_index_seo');

            if(is_object($obj) && method_exists($obj, 'title')){
                $title = $obj->title();
            }else{
                $title = (app::get('site')->getConf('site.name')) ? app::get('site')->getConf('site.name') : app::get('site')->getConf('page.default_title');
            }

            if(is_object($obj) && method_exists($obj, 'keywords')){
                $keywords = $obj->keywords();
            }else{
                $keywords = (app::get('site')->getConf('page.default_keywords')) ? app::get('site')->getConf('page.default_keywords') : $title;
            }

            if(is_object($obj) && method_exists($obj, 'description')){
                $description = $obj->description();
            }else{
                $description = (app::get('site')->getConf('page.default_description')) ? app::get('site')->getConf('page.default_description') : $title;
            }

            $this->pagedata['headers'][] = '<title>' . htmlspecialchars($title) . '</title>';
            $this->pagedata['headers'][] = '<meta name="keywords" content="' . htmlspecialchars($keywords). '" />';
            $this->pagedata['headers'][] = '<meta name="description" content="' . htmlspecialchars($description) . '" />';
            $this->pagedata['headers'][] = "<link rel='icon' href='{$this->app->res_url}/favicon.ico' type='image/x-icon' />";
            $this->pagedata['headers'][] = "<link rel='shortcut icon' href='{$this->app->res_url}/favicon.ico' type='image/x-icon' />";
            $GLOBALS['runtime']['path'][] = array('title'=>app::get('b2c')->_('首页'),'link'=>kernel::base_url(1));



            $tag_model = app::get('desktop')->model('tag_rel');


            /**
             * 首页banner
             */
            $where = [
                'selector'=>'select',
                'pricefrom'=>'',
                'priceto'=>'',
                'searchname'=>'',
                'type_id'=>'_ANY_',
                'cat_id'=>['_ANY_'],
                'brand_id'=>['_ANY_'],
                'tag'=>['_ANY_'],
                'goods_filter'=>'pricefrom=&priceto=&searchname=&type_id=_ANY_&cat_id[]=_ANY_&brand_id[]=_ANY_&tag[]=_ANY_',
                'goods_order_by'=>'1',
                'goods_select'=>'',
                'goods_select_linkobj'=>'',
            ];
            $goods_ids = $tag_model->getGoodsIdsByTagId(13);
            $where['goods_select'] = implode(',',$goods_ids);
            $banner = $this->index_tab_goods($where);


            /**
             * 首页热门商品
             */
            $where = [
                'selector'=>'select',
                'pricefrom'=>'',
                'priceto'=>'',
                'searchname'=>'',
                'type_id'=>'_ANY_',
                'cat_id'=>['_ANY_'],
                'brand_id'=>['_ANY_'],
                'tag'=>['_ANY_'],
                'goods_filter'=>'pricefrom=&priceto=&searchname=&type_id=_ANY_&cat_id[]=_ANY_&brand_id[]=_ANY_&tag[]=_ANY_',
                'goods_order_by'=>'1',
                'goods_select'=>'',
                'goods_select_linkobj'=>'',
            ];
            $goods_ids = $tag_model->getGoodsIdsByTagId(10);
            $where['goods_select'] = implode(',',$goods_ids);
            $hot_list = $this->index_tab_goods($where);



            /**
             * 首页推荐商品
             */
            $where = [
                'selector'=>'select',
                'pricefrom'=>'',
                'priceto'=>'',
                'searchname'=>'',
                'type_id'=>'_ANY_',
                'cat_id'=>['_ANY_'],
                'brand_id'=>['_ANY_'],
                'tag'=>['_ANY_'],
                'goods_filter'=>'pricefrom=&priceto=&searchname=&type_id=_ANY_&cat_id[]=_ANY_&brand_id[]=_ANY_&tag[]=_ANY_',
                'goods_order_by'=>'1',
                'goods_select'=>'',
                'goods_select_linkobj'=>'',
            ];

            $goods_ids = $tag_model->getGoodsIdsByTagId(11);
            $where['goods_select'] = implode(',',$goods_ids);
            $tj_list = $this->index_tab_goods($where);


            /**
             * 首页新品上市
             */
            $where = [
                'selector'=>'select',
                'pricefrom'=>'',
                'priceto'=>'',
                'searchname'=>'',
                'type_id'=>'_ANY_',
                'cat_id'=>['_ANY_'],
                'brand_id'=>['_ANY_'],
                'tag'=>['_ANY_'],
                'goods_filter'=>'pricefrom=&priceto=&searchname=&type_id=_ANY_&cat_id[]=_ANY_&brand_id[]=_ANY_&tag[]=_ANY_',
                'goods_order_by'=>'1',
                'goods_select'=>'',
                'goods_select_linkobj'=>'',
            ];

            $goods_ids = $tag_model->getGoodsIdsByTagId(12);
            $where['goods_select'] = implode(',',$goods_ids);
            $new_list = $this->index_tab_goods($where);




            $img_model = app::get('image')->model('image');

            $banner = $this->theme_widget_friend_links(5);

            $this->render('index.php',[
                'banner'=>$banner,
                'hot_list'=>i18n_mdl_words::translateArray($hot_list),
                'tj_list'=>i18n_mdl_words::translateArray($tj_list),
                'new_list'=>i18n_mdl_words::translateArray($new_list),
                'img_model'=>$img_model,
                'pagedata'=>$this->pagedata,
            ]);

        }else{

            $this->display('splash/install_template.html');
        }
    }

    function old(){
        if(defined('APP_SITE_INDEX_MAXAGE') && APP_SITE_INDEX_MAXAGE > 1){
            $this->set_max_age(APP_SITE_INDEX_MAXAGE);
        }//todo: 首页max-age设定

        if(kernel::single('site_theme_base')->theme_exists()){


            $obj = kernel::service('site_index_seo');

            if(is_object($obj) && method_exists($obj, 'title')){
                $title = $obj->title();
            }else{
                $title = (app::get('site')->getConf('site.name')) ? app::get('site')->getConf('site.name') : app::get('site')->getConf('page.default_title');
            }

            if(is_object($obj) && method_exists($obj, 'keywords')){
                $keywords = $obj->keywords();
            }else{
                $keywords = (app::get('site')->getConf('page.default_keywords')) ? app::get('site')->getConf('page.default_keywords') : $title;
            }

            if(is_object($obj) && method_exists($obj, 'description')){
                $description = $obj->description();
            }else{
                $description = (app::get('site')->getConf('page.default_description')) ? app::get('site')->getConf('page.default_description') : $title;
            }

            $this->pagedata['headers'][] = '<title>' . htmlspecialchars($title) . '</title>';
            $this->pagedata['headers'][] = '<meta name="keywords" content="' . htmlspecialchars($keywords). '" />';
            $this->pagedata['headers'][] = '<meta name="description" content="' . htmlspecialchars($description) . '" />';
            $this->pagedata['headers'][] = "<link rel='icon' href='{$this->app->res_url}/favicon.ico' type='image/x-icon' />";
            $this->pagedata['headers'][] = "<link rel='shortcut icon' href='{$this->app->res_url}/favicon.ico' type='image/x-icon' />";
            $GLOBALS['runtime']['path'][] = array('title'=>app::get('b2c')->_('首页'),'link'=>kernel::base_url(1));




                  $this->set_tmpl('index');
                  $this->page('index.html');
        }else{

            $this->display('splash/install_template.html');
        }
    }

    //验证码组件调用
    function gen_vcode($key='vcode',$len=4){
        $vcode = kernel::single('base_vcode');
        $vcode->length($len);
        $vcode->verify_key($key);
        $vcode->display();
    }

    public function activeCallback()
    {
        echo kernel::single('base_license_active')->callBack();
        exit;
    }


}
