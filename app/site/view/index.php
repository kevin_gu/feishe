<div class="header">
    <div class="wrapper">
        <div class="header-wrapper">
            <div class="pr clearfix">

                <?= $this->renderPartial('/top.php'); ?>

            </div>
            <div class="header-sub-menu">
                <?= $this->renderPartial('/menu.php'); ?>
            </div>
        </div>
    </div>
</div>
<div id="index-slider" class="pr slider-container swiper-container">
    <div class="pr slider-wrapper swiper-wrapper">
        <?php
        foreach ($banner as $v)
        { ?>
            <div class="swiper-slide slider-item">
                <a href="<?=$v['href'] ?>"><img src="<?=$v['image_url']?>" alt="" /></a>
            </div>

        <?php }
        ?>
    </div>
    <div class="pa tc slider-pagination"></div>
</div>
<div class="oh main">
    <div class="oh wrapper">
        <div class="container clearfix" hot>
            <div class="fl hot-area">
                <div class="hot-modify">
                    <div class="area-title"><?=$controller->t("品牌推荐")?></div>
                    <div class="tl">
                        <p class="area-desc">理性设计与建筑奇思同时存在，将温暖而经久耐用的材料变成易于诠释的元素。</p>
                        <span class="dib area-line"></span>
                    </div>
                </div>
            </div>

            <?php
            $data = $hot_list['goodsRows'];
            foreach ($data as $v)
            { ?>

                <div class="fl hot-item goods-intro">
                    <a href="<?=$v['goodsLink'] ?>">
                        <img src="<?=$img_model->get_url($v['goodsPicS'])?>" alt="" />
                        <div class="goods-intro-layer">
                            <div class="goods-intro-inner">
                                <strong><?=$v['goodsName'] ?></strong>
                                <!--<p>
                                    <?=$v['goodsIntro']?>
                                </p>-->
                                <span><?=$controller->t("查看详情")?></span>
                            </div>
                        </div>
                    </a>
                </div>

            <?php }
            ?>

        </div>
        <div class="container clearfix" goods>
            <?php
            $data = $tj_list['goodsRows'];
            $tj = [];
            foreach ($data as $v)
            {
                $tj[] = $v;
            }
            ?>
            <div class="fl pr goods-major goods-intro">
                <a href="<?=$tj[0]['goodsLink'] ?>">
                    <img src="<?=$img_model->get_url($tj[0]['goodsPicS'])?>" alt="" />
                    <div class="goods-intro-layer" major>
                        <div class="goods-intro-inner">
                            <strong><?=$tj[0]['goodsName'] ?></strong>
                            <!--<p>
                                <?=$tj[0]['goodsIntro']?>
                            </p>-->
                            <span><?=$controller->t("查看详情")?></span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="fl goods-minor">
                <div class="fl goods-minor-infor">
                    <div class="hot-modify">
                        <div class="area-title tr"><?=$controller->t("逛好物")?></div>
                        <div class="tr">
                            <p class="area-desc">充满大胆创想的家具，挑战静态法则，在展现纯正性的同时，充分尊重建造模型。</p>
                            <span class="dib area-line"></span>
                        </div>
                    </div>
                </div>
                <div class="fl goods-minor-item goods-intro">
                    <a href="<?=$tj[1]['goodsLink'] ?>">
                        <img src="<?=$img_model->get_url($tj[1]['goodsPicS'])?>" alt="" />
                        <div class="goods-intro-layer">
                            <div class="goods-intro-inner">
                                <strong><?=$tj[1]['goodsName'] ?></strong>
                                <!--<p>
                                    <?=$tj[1]['goodsIntro']?>
                                </p>-->
                                <span><?=$controller->t("查看详情")?></span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="fl goods-minor-item goods-intro">
                    <a href="<?=$tj[2]['goodsLink'] ?>">
                        <img src="<?=$img_model->get_url($tj[2]['goodsPicS'])?>" alt="" />
                        <div class="goods-intro-layer">
                            <div class="goods-intro-inner">
                                <strong><?=$tj[2]['goodsName'] ?></strong>
                                <!--<p>
                                    <?=$tj[2]['goodsIntro']?>
                                </p>-->
                                <span><?=$controller->t("查看详情")?></span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="container clearfix" goods>
            <div class="fl goods-minor">
                <div class="fl goods-minor-new">
                    <?php
                    $data = $new_list['goodsRows'];
                    $new = [];
                    foreach ($data as $v)
                    {
                        $new[] = $v;
                    }
                    ?>
                    <div class="goods-minor-item goods-intro">
                        <a href="<?=$new[0]['goodsLink'] ?>">
                            <img src="<?=$img_model->get_url($new[0]['goodsPicS'])?>" alt="" />
                            <div class="goods-intro-layer">
                                <div class="goods-intro-inner">
                                    <strong><?=$new[0]['goodsName'] ?></strong>
                                    <!--<p>
                                        <?=$new[0]['goodsIntro']?>
                                    </p>-->
                                    <span><?=$controller->t("查看详情")?></span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="hot-modify">
                        <div class="area-title tr"><?=$controller->t("人气新品")?></div>
                        <div class="tr">
                            <p class="area-desc">能工巧匠和工业的创新交织，带来细腻而独特的产品系列。</p>
                            <span class="dib area-line"></span>
                        </div>
                    </div>
                </div>
                <div class="fl goods-minor-new">
                    <div class="pr new">
                        <span class="pa">NEW</span>
                    </div>
                    <div class="goods-minor-item goods-intro">
                        <a href="<?=$new[1]['goodsLink'] ?>">
                            <img src="<?=$img_model->get_url($new[1]['goodsPicS'])?>" alt="" />
                            <div class="goods-intro-layer">
                                <div class="goods-intro-inner">
                                    <strong><?=$new[1]['goodsName'] ?></strong>
                                    <!--<p>
                                        <?=$new[1]['goodsIntro']?>
                                    </p>-->
                                    <span><?=$controller->t("查看详情")?></span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="fl pr goods-major goods-intro">
                <a href="<?=$new[2]['goodsLink'] ?>">
                    <img src="<?=$img_model->get_url($new[2]['goodsPicS'])?>" alt="" />
                    <div class="goods-intro-layer" major>
                        <div class="goods-intro-inner">
                            <strong><?=$new[2]['goodsName'] ?></strong>
                            <!--<p>
                                <?=$new[2]['goodsIntro']?>
                            </p>-->
                            <span><?=$controller->t("查看详情")?></span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<?= $this->renderPartial('/footer.php'); ?>

