<?php

class system_commerce{

    public function get_commerce_version(){
        $xml = file_get_contents(ROOT_DIR.'/config/deploy.xml');
        $deploy = kernel::single('base_xml')->xml2array($xml,'base_deploy');
        if($deploy['publish_version'] == 'commerce'){
            return true;
        }
        return false;
    }

}

