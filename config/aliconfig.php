<?php
$notify_url = kernel::openapi_url('openapi.payment_notify', 'alipay');
return [
    'use_sandbox'               => false,// 是否使用沙盒模式
    'app_id'                    => '2019080666164095',
    'sign_type'                 => 'RSA2',// RSA  RSA2
    'ali_public_key'            => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiybcwM+XqLStOuOr97AEnzVwT2Pd6ZKkZLHP18FFwIHbK1Kqegd+IFN9LHZ3vryacfEOCt8gsFLrb5xWZMVxwd6T8ZzMtIx7J4bJbcxLXLW9/HVp1d5vAwKCH1dlTiTSCfU8gzZzlbCjl5LUbEpW0PPsN6SrTbZZWJpGecodURqFuDgbBMvkwUSMPqHoT9A9Ug89B1CDNCz19k8hCIHoVXHfROo/qPV0LlLQ6eH/LCZyAdNEJax4jNs9LmssFNMKcfUq+ogVh8w2e182vJU1+q3pwRabJFSW6aUqFsIPUbtCjSqhRH2TtUolsBR1ZfYgDY2jDtnBpU2wQWeOdrlrDQIDAQAB',
    'rsa_private_key'           => 'MIIEogIBAAKCAQEArW3LAy4tWJYDMO5iYU5XtbG5ZYn0Qv1DAgymMQFIFCX0n6fg
cEOjrZxhX/cXaVM8OfGxFnPEZcQN73QK023sMMHnbuVZJYcmCE8Qu83kOZ3o9UDn
BpypzHX2gB0vyts05syEn3ZdXoB19co4rYrKWlPujFnmBMXTEPRdo5Qpq1PODnzy
BToSbBJack8VdZU7b9zm1z7TCp/7xdvrjJCzY7aRvTj3k4BrT6UHeiXkyd1U1YKS
y69iOZhpC3NLjIfL73chZWbz6RnKBH/dfQRpijxyeFEbKlEumQf4GYYAJEXGzrYc
tAHLj1CCKIN8IJtHqfa6UD3VvwYG8GBDQBzTxwIDAQABAoIBADU+5TpnvgJZpEcs
mUXYwLMHCq2zCixbB2ykp42aICAXeyBdcVnmWmVeAsNs4GI07OO5Ya5MArtf0wt2
bDB3qjFaBw0UGyebc8DPTYrrbquF4NUThRqRlMwyV2kEDP1SCc7mcb1noMj31b+N
w5L8bp4G8NFkdGt4tRGahPlI7vYrdiimNiRSkbwkU5kvVPleyXF6VOawsss8WwIZ
7UaNW1zvKqymZvUIDRIzKW8fVchvyuvA49FhKOQGsoogZ2ijFKRKSp1fsVnsrGnh
5WDelZeMK+j/Gv74ZmiDIvF7tqhrV9bqJHzqRmGzX1qgNRQfs+ozWJxnR9Pc4nYA
W+q+ExkCgYEA3VgI6F0J5kU1NUgnBMMzV1KxC1dz+ubOjKjkz5LwcVTrJrI6mAGV
V/cDGEH+f46zuQkzbKXzi68y3bGXbuhYFc/u0TC5RLs7rnNbUTChelAKCeLZmLR7
gS/SPgEgVi36Ov0gS0SLwqoClb5uVGQhCCaE4WNc3j0tUNGGkYcitoMCgYEAyJU3
TPe8d/JRJPEevLqHNceZX1W8MaWxXnCvAMrpZ17nngoGlazt3FDhmum4KMLvcnQ6
5j+6J7lPyyI/SReG79sFE5W2iwMxu1ku1nA9jGFmSYZaB8vwZ1VrK6p2Oj1QpRIU
FsmyHy/QRSuUenHFTP0FywjyeIRRhm+hkvG8Cm0CgYAe79cO4zInBTVSpSsO91mN
svkb5LVJNElaT6DrglYpAP19Axw9crs0+ElucornuUJVcnvFi8xmka5CXjgUQFVP
dyiiBHVgrerBj04MIwfu2JEOoK5ZMaCxhcWU9QNYCEUZSZfh7Uq7gpep5lv1X6le
QkQAXKr2RQa4bRCp9oFbkQKBgF5wIsFSQqIWrK2/+MDS8aYYe9WvABQ/9uqIE11E
K/BHdrWp8r+106fPTOkq8VH/tc0lfDW7+2GIllEFptY7BYUYkaNO8csxjdCvhKXh
4pidoxMq9miFVILSbMr6Tv/MrpOJl3vDKuIUGPL4OcgMmOBOoNOfOsoTQm8RT/ln
ZylhAoGACsBPyiHXMh1uTVBsvfXDqQ0FPjKmGwkEjrXhqJibKrApkL8/EA+EwNE/
ydO2nugQ9DZTWGkXqMrQOfMsg+Pw2PF6TTjnQpf5wd+fDd+a/6je2cA8pFubZxEI
MPRRQ0j+Abf6Z4KPOF8IS7AVwqrDpMyzPnLf/hkNhLQCnfv4oYk=',
    'limit_pay'                 => [
        //'balance',// 余额
        //'moneyFund',// 余额宝
        //'debitCardExpress',// 	借记卡快捷
        //'creditCard',//信用卡
        //'creditCardExpress',// 信用卡快捷
        //'creditCardCartoon',//信用卡卡通
        //'credit_group',// 信用支付类型（包含信用卡卡通、信用卡快捷、花呗、花呗分期）
    ],// 用户不可用指定渠道支付当有多个渠道时用“,”分隔

    // 与业务相关参数
    'notify_url'                => $notify_url,
    'return_url'                => '',
    'return_raw'                => false,// 在处理回调时，是否直接返回原始数据，默认为 true
];
