<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */
$db['email_check'] = array(
    'columns'=>
    array(
        'id'=>
        array(
            'type'=>'number',
            'extra' => 'auto_increment',
            'pkey'=>true,
            'comment' => app::get('b2c')->_('自增id'),
        ),
        'email'=>
        array(
            'type'=>'varchar(50)',
            'required' => true,
            'comment' => app::get('b2c')->_('邮箱'),
        ),
        'sign'=>
        array(
            'type'=>'varchar(50)',
            'required' => true,
            'comment' => app::get('b2c')->_('验证sign'),
        ),
        'email_check'=>
            array(
                'type'=>'bool',
                'default'=>'false',
            ),
        'createtime'=>
        array(
            'type'=>'time',
            'comment' => app::get('b2c')->_('创建时间'),
        ),
    ),
    'index' =>
    array (
        'ind_email_sign' =>
        array (
            'columns' =>
            array (
                0 => 'email',
                1 => 'sign',
            ),
        ),
    ),
    'engine' => 'innodb',
    'comment' => app::get('b2c')->_('企业用户邮箱验证表'),
);
