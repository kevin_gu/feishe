<?php
/**
 * Created by PhpStorm.
 * User: daiyangqiang
 * Date: 2018/3/19
 * Time: 14:37
 */

class b2c_helper
{
    const MAX_LEN = 512000;
    private static $char= '0123456789';
    private static $offset = 10000;
    private static $maxLength = 10;

    private static $couponChars = "23456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
    private static $placeholder = '0';
    private static $limit = 100000;

    public static function validateMobile($mobile){
        if(!preg_match("/^1[23456789]{1}\d{9}$/",$mobile)){
            return false;
        }
        return true;
    }

    //获取用户id
    public static function getMemberId($invitationCode){
        $mod = strlen(self::$char);
        $number = 0;
        $verify = 0;
        if(!$invitationCode || strlen($invitationCode) > self::$maxLength){
            return 0;
        }
        for($i = strlen($invitationCode) - 2; $i >= 0; $i--){
            $index = strpos(self::$char,$invitationCode[$i]);
            $verify = $verify + ord($invitationCode[$i]);
            if($index < 0){
                return 0;
            }else{
                $number = $number * $mod + $index;
            }
        }
        if(strpos(self::$char,$invitationCode[strlen($invitationCode) - 1]) != ($verify % $mod)){
            return 0;
        }
        $number = $number - self::$offset;
        return $number;
    }

    //获取邀请码
    public static function getInvitationCode($memberId){
        $mod = strlen(self::$char);
        $memberId = $memberId + self::$offset;
        while($memberId > 0){
            $arr[] = $memberId % $mod;
            $memberId = floor($memberId / $mod);
        }
        $verify = 0;
        $code = '';
        foreach ($arr as $value){
            $code .= self::$char[$value];
            $verify = $verify + ord(self::$char[$value]);
        }
        $code .= self::$char[$verify % $mod];
        return $code;
    }

    //获取优惠券兑换码
    public static function getCouponCode($couponId,$index,$count){
        if($index <= 0 || $index > $count || $count > self::$limit){
            return false;
        }
        $step = floor(self::$limit / $count);
        $noInt = ($index - 1) * $step + $index % $step;
        $keyInt = $couponId + $noInt;
        $no = Helper::convertToString($noInt);
        $key = Helper::convertToString($keyInt);
        $random = Helper::createRandom(6);
        $check = Helper::getCheck($random,$no,$key);
        $code = $check.$random;
        for($i = 0;$i < 3 - strlen($no);$i++){
            $code = $code.self::$placeholder;
        }
        $code = $code.$no.$key;
        for($i = 0; $i < 5 -strlen($key);$i++){
            $code = $code.self::$placeholder;
        }
        return $code;
    }

    //根据兑换码获取优惠券id
    public static function getKey($code){
        $key = Helper::convertToInt(substr($code,11));
        $no = Helper::convertToInt(substr($code,8,3));
        return $key - $no;
    }

    private static function convertToString($value){
        $mod = strlen(self::$couponChars);
        $str = '';
        do{
            $index = $value % $mod;
            $str = $str.self::$couponChars[$index];
            $value = floor($value/$mod);
        }while($value > 0);
        return $str;
    }

    private static function convertToInt($str){
        if(!$str || strlen($str) > 6){
            return 0;
        }
        $mod = strlen(self::$couponChars);
        $number = 0;
        for($i = strlen($str) - 1; $i >= 0;$i--){
            $idx = strpos(self::$couponChars,$str[$i]);
            if(!$idx){
                continue;
            }
            $number = $number * $mod + $idx;
        }
        return $number;
    }

    public static function createRandom($len){
        $mod = strlen(self::$couponChars);
        $str = '';
        for($i = 0; $i < $len; $i++){
            $str = $str.self::$couponChars[rand(0,$mod - 1)];
        }
        return $str;
    }

    private static function getCheck($random,$no,$key){
        $mod = strlen(self::$couponChars);
        $str = $random.$no.$key;
        $verify = 0;
        for($i = 0; $i < strlen($str);$i++){
            $verify = $verify + ord($str[$i]);
        }
        $strOne = self::$couponChars[$verify % $mod];
        $strTwo = self::$couponChars[floor($verify / $mod) % $mod];
        return $strOne.$strTwo;
    }

    public static function check($code){
        if(!$code || strlen($code) < 16){
            return false;
        }
        $mod = strlen(self::$couponChars);
        $verify = 0;
        for($i = 2; $i < strlen($code);$i++){
            if($code[$i] == self::$placeholder){
                continue;
            }
            if(strpos(self::$couponChars,$code[$i]) < 0){
                return false;
            }
            $verify = $verify + ord($code[$i]);
        }
        if(strpos(self::$couponChars,$code[0]) != ($verify % $mod) || strpos(self::$couponChars,$code[1]) != (floor($verify / $mod) % $mod)){
            return false;
        }
        return true;
    }

    public static function cutStr($string,$length,$more='...')
    {
        if(mb_strlen($string,'utf-8')<=$length){
            return $string;
        }
        return mb_substr($string,0,$length,'utf-8').$more;
    }

    public static function htmlOutPut($str)
    {
        return htmlspecialchars($str);
    }

    //上传头像
    public static function uploadImg($file,$saveDir){
        $result = ['result' => 0, 'path' => '','name' => '', 'error' => ''];
        $imageType = ['gif','jpg','bmp','png'];
        $name = $file['name'];
        $type = strtolower(substr($name,strrpos($name,'.')+1));
        if(!in_array($type,$imageType)){
            $result['error'] = "对不起,只能上传gif,jpg,bmp,png格式的图片！";
            return $result;
        }
        if($file['size'] > self::MAX_LEN){
            $result['error'] = "对不起,图片大小不能超过500k！";
            return $result;
        }
        if(!is_uploaded_file($file['tmp_name'])){
            $result['error'] = "非法上传！";
            return $result;
        }
        if($file['error'] > 0){
            $result['error'] = "上传错误,error下标：".$file['error'];
            return $result;
        }
        if(self::mkDir($saveDir) == false){
            $result['error'] = "创建目录失败！";
            return $result;
        }
        $newFileName = date('ymdHis',time()).'_'.uniqid().'.'.$type;
        $newPath = $saveDir.$newFileName;

        if(!move_uploaded_file($file['tmp_name'],$newPath)){
            $result['error'] = '文件上传错误！';
            return $result;
        }
        $result['result'] = 1;
        $result['path'] = $newPath;
        $result['name'] = $newFileName;
        return $result;
    }

    public static function mkDir($dir){
        if(!file_exists($dir)){
            if(false == mkdir($dir,0777,true)){
               return false;
            }
        }
        return true;
    }

    public static function helpCurl($url){
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //运行curl，结果以json形式返回
        $res = curl_exec($ch);
        curl_close($ch);
        //取出openid
        $data = json_decode($res,true);
        return $data;
    }

    //登录时生成 token
    public static function getToken($memberId,$length = 10){
        /*$char = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLOMNOPQRSTUVWXYZ';
        $key = '';
        for($i=0; $i < $length; $i++) {
            $key .= $char[mt_rand(0,strlen($char)-1)];
        }*/
        $str = "tdB0JMW5KFp7CIcd";
        return md5(md5($memberId.$str));
    }

    //统一设置用户 cookie
    public static function setCookie($name,$value,$domain){
        setcookie($name,$value,0,'/',$domain);
    }

}