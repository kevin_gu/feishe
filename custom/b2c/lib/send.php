<?php
class b2c_send{
    /**
     * @param $email
     * @param $subject
     * @param $content
     * @param $config
     * @param  $error
     * @return bool
     */
    public static function sendByEmail($email,$subject,$content,$config=array(),&$error='')
    {
        $mail = new b2c_mail();
        if($config) {
            $mail_config = $config;
        }else{
            $mail_config = include ROOT_DIR.'/config/email.php';
        }
        if(!$mail_config){
            return false;
        }
        $mail_username = $mail_config['username'];
        $mail_password = $mail_config['password'];
        $mail_server = $mail_config['server'];
        $mail_port = $mail_config['port'];
        $mail_ssl = $mail_config['ssl'];
        $mail->setServer($mail_server, $mail_username, $mail_password, $mail_port, $mail_ssl); //到服务器的SSL连接
        $mail->setFrom($mail_username);
        $mail->setReceiver($email);
        $mail->setMail($subject, $content);
        $flag = $mail->sendMail();
        if(!$flag){
            $error = $mail->error();
        }
        return $flag;
    }

}