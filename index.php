<?php
#include("app/serveradm/xhprof.php");
#
/*register_shutdown_function(function(){
   $error = error_get_last();
   print_r($error);
});*/
include __DIR__.'/vendor/autoload.php';

define('HOST_STATIC', '/public/assets/');
define('HOST_CSS', HOST_STATIC.'css/');
define('HOST_JS', HOST_STATIC.'js/');
define('HOST_IMG', HOST_STATIC.'images/');
define('HOST_FONT', HOST_STATIC.'font/');


define('ROOT_DIR',realpath(dirname(__FILE__)));
require(ROOT_DIR.'/app/base/kernel.php');
kernel::boot();

if(defined("STRESS_TESTING")){
    b2c_forStressTest::logSqlAmount();
}
