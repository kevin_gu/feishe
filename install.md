# ECStore电子商务软件 安装说明

| 版本 | 更新日期 | 修改人 |
|----- |----- |----- |
| v5.0 | 201806 | summer |

## 使用vagrant box快速安装
我们的官方vagrant box为：`shopex/ecstore50`

支持的操作系统

- windows
- mac
- linux

1. 通过vagrant官网安装vagrant应用：https://www.vagrantup.com/
2. tar xvf ECStore-xxx.tar (这里的.tar文件是从商派获取到的代码包)：   
3. cd ecstore
4. vagrant up

执行结果如下：
```
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'shopex/ecstore50'...
==> default: Matching MAC address for NAT networking...
==> default: Checking if box 'shopex/ecstore50' is up to date...
==> default: Setting the name of the VM: ecstore_default_1530062175568_46261
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 80 (guest) => 8088 (host) (adapter 1)
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: The guest additions on this VM do not match the installed version of
    default: VirtualBox! In most cases this is fine, but in rare cases it can
    default: prevent things such as shared folders from working properly. If you see
    default: shared folder errors, please make sure the guest additions within the
    default: virtual machine match the version of VirtualBox you have installed on
    default: your host and reload your VM.
    default:
    default: Guest Additions Version: 5.0.20
    default: VirtualBox Version: 5.2
==> default: Mounting shared folders...
    default: /vagrant => D:/sites/ecstore
```
默认配置如下（Vagrantfile)：
1. 端口转发：
     ```
         default: 80 (guest) => 8088 (host) (adapter 1)
         default: 22 (guest) => 2222 (host) (adapter 1)
    ```
2. 目录挂载：  

    当前的ecstore目录会被自动挂在到虚拟机的/vagrant目录下，方便进行代码调试。
   
3. mysql数据库账号：
 
    虚拟机中自带了mysql 5.6应用，默认数据库账号为root,默认密码为空，已经默认创建了一个name为ecstore的空数据库。
 
 
## 使用Docker部署：
我们的官方docker镜像为：`ecstore/nginx-php56`

**docker镜像预计在7月上旬更新至hub.docker.com**

## 使用yum源方式安装：
这是一个使用yum源安装shopex ECSTORE应用的简单教程。这套教程可以大大减轻搭建以上应用的php环境的工作量。

### 关闭selinux
```bash
#Disable SeLinux
setenforce 0
if [ -s /etc/selinux/config ]; then
    sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
fi
```

### 初始化yum源
1. 下载shopex-lnmp源
     - CentOS6:
         ```bash
        curl -o /etc/yum.repos.d/shopex-lnmp.repo http://mirrors.shopex.cn/shopex/shopex-lnmp/shopex-lnmp.repo
        ```
     - CentOS7:
         ```bash
        curl -o /etc/yum.repos.d/shopex-lnmp.el7.repo http://mirrors.shopex.cn/shopex/shopex-lnmp/shopex-lnmp.el7.repo
        ```
    
2. 安装epel扩展源

    ```bash
    yum install epel-release -y
    ```
   
3. install PHP5.6 mysql nginx 
    ```bash
    yum install php-fpm56 ngx_openresty mysql.x86_64  php-memcache56  memcached -y
    ```
 
4. 安装Swoole Loader扩展（必要扩展）
    
    为了方便自行编译安装php，我们在代码包中提供了php所需的扩展文件。在[代码包]/deploy文件夹中，存放有编译好的windows平台和linux平台php所需的Swoole loader扩展文件，直接将文件引入到php.ini中即可。
    
    **文件名称对应关系如下**
    
    | 扩展文件名 | 适用平台 | 适用php版本 | 说明 |    
    |----- |----- |-----  |----- |    
    | php_swoole_loader_php56_nzts_x64.dll| windows | php5.6.x_x64_nzts| - |
    | php_swoole_loader_php56_zts_x64.dll | windows | php5.6.x_x64_zts | - |
    | swoole_loader56.so | linux | php5.6 | 需要将libsodium.so.24复制到/usr/lib64/下/
    | swoole_loader56_zts.so | linux | php5.6_zts | 需要将libsodium.so.24复制到/usr/lib64/下/
    
    通过如下命令，找到当前php默认的扩展目录：
    
    ```bash
    # shell命令： 
    /usr/local/php56/bin/php -i|grep extension_dir
    #执行结果：
    #extension_dir => /usr/local/php56/lib/php/extensions/no-debug-non-zts-20131226 => /usr/local/php56/lib/php/extensions/no-debug-non-zts-20131226
       
    ```
    将扩展文件放置到`/usr/local/php56/lib/php/extensions/no-debug-non-zts-20131226`
    
    在/usr/local/php56/etc/php.d/下新增文件swoole_loader.ini，文件内容如下：
    
    ```ini
    [swoole]
    ;注意这里的.so如果不是在php的默认扩展目录中，需要适用全局文件路径。
    extension = swoole_loader56.so
    ```

4. 关于配置文件以及相关目录说明
 
    |名称|	本次版本(2018.06)	|相关说明|
    |----- |----- |----- |
    |nginx	|openresty/1.9.7.4|	配置文件 /usr/local/nginx/conf/nginx.conf <br>vhost配置文件 /usr/local/nginx/conf/vhosts/*.conf
    |php	|5.6.19|	配置文件 /usr/local/php56/etc/php.ini <br>php 扩展配置文件目录 /usr/local/php56/etc/php.d/ <br>memcached 配置文件 /usr/local/php56/etc/php.d/memcached.ini|
    |mysql |	5.6.22 |	basedir /usr/local/mysql <br>datadir /data/mysql/3306 <br> 配置文件 /usr/local/mysql/my.cnf|
    | redis |	3.0.3|	配置文件 /etc/redis.conf |
    |memcached  (选装,单机部署可不装) |	1.4.4	| 配置文件 /etc/sysconfig/memcached|
    | 代码目录	| -	|/data/httpd|
5. 启动环境的脚本
     ```bash
    /etc/init.d/php-fpm56 start
    /etc/init.d/nginx start
    /etc/init.d/mysqld start
    /etc/init.d/redis start
    #如果安装了memcached，则需要启动，一般单机下BBC无需安装memcached，安装了性能也不高，用默认的本地缓存即可
    /etc/init.d/memcached start
    ```
6. 确认目录权限

    /data/httpd目录的所有者和组都设置为`www`
    
    ```bash
    chown -R www:www /data/httpd
    ```
    
7. 配置定时任务
    ```shell
    #检查 /home/目录下是否有www目录，没有则创建一个
    mkdir /home/www
    #配置定时任务，必须配置的是www权限的crontab，不要在root或者其他用户下配置
    crontab -uwww -e
    #将下面的配置复制进去
    ```
    ```shell
    * * * * * /data/httpd/ecstore/script/queue/queue.sh /usr/local/php56/bin/php >/dev/null
    * * * * * /usr/local/php56/bin/php /data/httpd/ecstore/script/crontab/crontab.php >/dev/null
    ```
8. 配置Rewrite - 修改完重启nginx(根据实际需求，不影响功能)
 
     `vim /usr/local/nginx/conf/vhosts/default.conf`
     
     ```
     location / {
        if (!-e $request_filename) {
        rewrite ^/(.*)$ /index.php/$1 last;
        }
        index index.php index.html index.htm;
     }
     
     location ~ ^/shopadmin {
        rewrite ^/(.*)$ /index.php/$1 last;
        break;
     }