<?php
$data = app::get('content')->model("article_indexs")->getArticleList();
?>
<div class="footer">
    <div class="wrapper">
        <div class="container clearfix">
            <dl class="fl footer-nav">
                <dt>使用指南</dt>
                <?php foreach($data[0] as $key => $article){ ?>
                 <dd><a href="/index.php/article-gouwuzhinan-<?=$article['article_id']?>.html"><?=$article['title']?></a></dd>
                <?php }?>
            </dl>
            <dl class="fl footer-nav">
                <dt>配送方式</dt>
                <?php foreach($data[1] as $key => $article){ ?>
                    <dd><a href="/index.php/article-peisongfangshi-<?=$article['article_id']?>.html"><?=$article['title']?></a></dd>
                <?php }?>
            </dl>
            <dl class="fl footer-nav">
                <dt>支付方式</dt>
                <?php foreach($data[2] as $key => $article){ ?>
                    <dd><a href="/index.php/article-zhifufangshi-<?=$article['article_id']?>.html"><?=$article['title']?></a></dd>
                <?php }?>
            </dl>
            <dl class="fl footer-nav">
                <dt>售后服务</dt>
                <?php foreach($data[3] as $key => $article){ ?>
                    <dd><a href="/index.php/article-shouhousuwu-<?=$article['article_id']?>.html"><?=$article['title']?></a></dd>
                <?php }?>
            </dl>
            <dl class="fl footer-nav">
                <dt>特色服务</dt>
                <?php foreach($data[4] as $key => $article){ ?>
                    <dd><a href="/index.php/article-tesesuwu-<?=$article['article_id']?>.html"><?=$article['title']?></a></dd>
                <?php }?>
            </dl>
        </div>
        <div class="container">
            <div class="tc footer-copyright">
                <p>Copyright ©2019 珠海市棐舍科技有限公司 版权所有</p>
                <p>“Vanseo Fei”以及“Vanseo Fei”图形均为Vanseo Fei Technology Co.,Ltd的商标，且在世界多个地点均有注册</p>
            </div>
        </div>
    </div>
</div>
<div class="search-panel none">
    <form action="/index.php/gallery.html" method="get">
        <div class="flex-box pr search-input">
            <div class="flex">
                <input type="text" name="scontent" value="" placeholder="你可以在这里搜索你想要的商品..." />
            </div>
            <button type="submit" class="pa icon-sprite search-btn"></button>
        </div>
        <div class="keyword clearfix">
            <div class="fl"><a href="/index.php/gallery.html?scontent=%E6%B2%99%E5%8F%91">沙发</a></div>
            <div class="fl"><a href="/index.php/gallery.html?scontent=%E5%A4%A7%E7%90%86%E7%9F%B3">大理石</a></div>
            <div class="fl"><a href="/index.php/gallery.html?scontent=%E5%8F%8C%E4%BA%BA%E5%BA%8A">双人床</a></div>
            <div class="fl"><a href="/index.php/gallery.html?scontent=%E9%87%91%E5%B1%9E">金属</a></div>
            <div class="fl"><a href="/index.php/gallery.html?scontent=%E7%8E%84%E5%85%B3%E6%A1%8C">玄关桌</a></div>
            <div class="fl"><a href="/index.php/gallery.html?scontent=%E6%89%B6%E6%89%8B%E6%A4%85">扶手椅</a></div>
            <div class="fl"><a href="/index.php/gallery.html?scontent=%E8%8C%B6%E5%87%A0">茶几</a></div>
            <div class="fl"><a href="/index.php/gallery.html?scontent=%E6%9C%A8%E8%B4%A8">木质</a></div>
        </div>
    </form>
</div>
<div class="icon-sprite close-panel none"></div>
<div class="mask none" menuMask></div>
<div id="search-mark" class="mask none" searchMask></div>