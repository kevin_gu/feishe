<?php
/**
 * @var base_controller $controller
 */
/** @var $headerClass */
?>
<div class="header <?=$headerClass?>">
    <div class="wrapper">
        <div class="header-wrapper">
            <div class="pr clearfix">
                <?=$controller->renderPartial('/top.php')?>
            </div>
            <div class="header-sub-menu">
                <?=$controller->renderPartial('/menu.php')?>
            </div>
        </div>
    </div>
</div>
