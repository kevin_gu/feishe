<?php
if($this->pagedata['is_login']){
    $arr = app::get('b2c')->model("cart")->get_objects();  //获取购物车数据
    $count = b2c_arrayhelper::getValue($arr,'_cookie.CART_NUMBER',0);
}else{
    $count = 0;
}
?>
<div class="fl logo">
    <a class="db" href="/"><img src="../public/assets/images/logo.svg" alt="" white /></a>
    <a class="db" href="/"><img src="../public/assets/images/black_logo.svg" black alt="" /></a>
</div>
<div class="fr header-menu">
    <a href="javascript:;" class="fl icon-sprite search"></a>
    <ul class="fl f14">
        <li><a href="/index.php/members-orders.html">我的订单</a></li>
        <li><a href="">会员中心</a></li>
        <li>心愿单中有<em><?=$count?></em>件商品<a href="/index.php/cart.html" class="place-order">心愿单</a></li>
    </ul>
    <?php if($this->pagedata['is_login']){ ?>
        <a href="/index.php/passport-logout.html" class="fr f14 login">退出登录</a>
    <?php }else{ ?>
        <a href="/index.php/passport-login.html" class="fr f14 login">登录</a>
    <?php } ?>
</div>