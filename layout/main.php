<?php
/**
 * @var $__content__
 * @var $__head_js__;
 * @var $__head_css__;
 * @var $__footer_js__;
 * @var $__footer_css__;
 * @var $__pre_footer_js__
 * @var $__raw_header
 * @var $__raw_footer
 * @var $__append_header;
 * @var $__append_footer;
 * @var $title;
 * @var $keyword
 * @var $decription
 * @var $__bodyClass
 * @var base_controller $controller
 */
?>
<!DOCTYPE html>
<html>
<head>
    <?php if($__raw_header){?>
        <?=$__raw_header?>
    <?php }else{?>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title><?=$title?></title>
       <?=$__append_header?>
        <?php foreach($__head_css__  as $css){?>
        <link rel="stylesheet" type="text/css"  href="<?=$css[0] == '/'?$css:HOST_CSS.$css?>" media="screen" />
        <?php }?>
        <?php foreach($__head_js__  as $js){?>
        <script type="text/javascript" src="<?=$js?>"></script>
        <?php } ?>
    <?php }?>
    <script type="text/javascript" src="<?=HOST_JS?>response.js?t=<?=date("i")?>"></script>
    <script>
      var cateJSON = <?=$this->pagedata['cat_info']?>;
    </script>
</head>
<body <?=$__bodyClass?>>
<script type="text/tpl" id="cate-level-tpl"> <!--头部分类模板-->
<ul class="sub-menu-item pr">
    {{ for(var i = 0, len = it.lv1.slice(0, 3).length; i < len; i++){ }}
    <li data-type="{{=it.lv1[i].type}}" class="item">{{=it.lv1[i].cat_name}}</li>
    {{ } }}
    <li class="slider none"></li>
</ul>
<div class="pr cate-item-wrap clearfix none">
    <div class="cate-level-1 pr fl">
        {{ for(var i = 0, len = it.lv1.slice(0, 3).length; i < len; i++){ }}
         <ul class="fl{{! i == 0 ? '' : ' none'}}">
            {{ for(var j = 0, jLen = it.lv2.length; j < jLen; j++){ }}
              {{ if(it.lv2[j].pid == it.lv1[i].cat_id){ }}
                <li data-type="{{=it.lv2[j].type}}" data-title="{{=(it.lv2[j].cover_goods_info.goodsName || '')}}" data-url="{{=(it.lv2[j].cover_goods_info.goodsLink || '')}}" data-src="{{=(it.lv2[j].cover_goods_info.goodsPicS || '')}}"><a style="cursor:default" href="javascript:;">{{=it.lv2[j].cat_name}}</a></li>
              {{ } }}
            {{ } }}
        </ul>
        {{ } }}
    </div> 
    <div class="pa cate-level-2">
        {{ for(var i = 0, len = it.lv1.slice(0, 3).length; i < len; i++){ }}
        <div class="clearfix{{! i == 0 ? '' : ' none'}}">
            {{ for(var j = 0, jLen = it.lv2.length; j < jLen; j++){ }}
            {{ if(it.lv2[j].pid == it.lv1[i].cat_id){ }}
            <ul data-type="{{=it.lv2[j].type}}" class="fl{{! j == 0 ? '' : ' none'}}">
                {{ for(var n = 0, nLen = it.lv3.length; n < nLen; n++){ }}
                  {{ if(it.lv2[j].cat_id == it.lv3[n].pid){ }}
                    <li data-type="{{=it.lv3[n].type}}" data-title="{{=(it.lv3[n].cover_goods_info.goodsName || '')}}" data-url="{{=(it.lv3[n].cover_goods_info.goodsLink || '')}}" data-src="{{=(it.lv3[n].cover_goods_info.goodsPicS || '')}}"><a href="{{=it.lv3[n].url}}">{{=it.lv3[n].cat_name}}</a></li>
                  {{ } }}
                {{ } }}
            </ul>
            {{ } }}
            {{ } }}
        </div>
        {{ } }}
    </div>
    <div class="pa categoods-scene none">
        <a class="db" href="javascript:;">
            <div class="scene"><img src="" alt="" /></div>
            <p>-</p>
            <span>点击查看</span>
        </a>
    </div>
</div>
</script>
<?=$__content__?>

</body>
<?php if($__raw_footer){?>
    <?=$__raw_footer?>
<?php }else{?>
    <?php foreach($__pre_footer_js__  as $js){?>
    <script type="text/javascript" src="<?=$js[0] == '/'?$js:HOST_JS.$js?>"></script>
    <?php } ?>
    <script type="text/javascript" src="<?=HOST_JS?>jquery-min.js?t=<?=date("i")?>"></script>
    <script type="text/javascript" src="<?=HOST_JS?>doT.min.js?t=<?=date("i")?>"></script>
    <script type="text/javascript" src="<?=HOST_JS?>swiper.min.js?t=<?=date("i")?>"></script>
    <script type="text/javascript" src="<?=HOST_JS?>webfont.js?t=<?=date("i")?>"></script>
    <script type="text/javascript" src="<?=HOST_JS?>main.js?t=<?=date("i")?>"></script>
    <?php foreach($__footer_js__  as $js){?>
    <script type="text/javascript" src="<?=$js[0] == '/'?$js:HOST_JS.$js?>"></script>
    <?php } ?>
    <?=$__append_footer?>
<?php }?>
</html>
