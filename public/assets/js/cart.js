(function ($){
    var toQueryString = function(obj){
        var queryString = [];
        obj.find('input').each(function(index,el){
            var type = el.type;
            if (!el.name || el.disabled || type == 'submit' || type == 'reset' || type == 'file' || type == 'image') return;
            queryString.push(encodeURIComponent(el.name) + '=' + encodeURIComponent(el.value));
        });
        return queryString.join('&');
    };

    $('.remove').click(function () {
        var container = $(this).parents('li'),
            dialog = container.find('.remove-layer');
        dialog.show();
        dialog.find('.cancel').one('click',function () {
            dialog.hide();
        });
        dialog.find('.confirm').one('click',function () {

            dialog.hide();
        });
    });
    $('.entry-data').on('quantity_change',function () {
        console.log($(this).val());
    })
    {
        'click:relay(.btn-delete)' :function(e) {
            Dialog.tips(this, '确定将商品从购物车中移除吗？', function(e){
                if(e) {
                    var p = this.getParent('tr.p-adjunct') || this.getParent('.cart-item') || this.getParent('tr.item');
                    new Request({
                        url:'<{link app=b2c ctl=site_cart act=remove}>',
                        data: p.toQueryString()+"&response_json=true",
                        onRequest: function() {
                            p.mask({html:'处理中...','class':'cover',injectTo:document.body,position:true,effect:false, resize: false});
                        },
                        onComplete:function(rs) {
                            rs = JSON.decode(rs);
                            if(rs.is_empty) return location.reload();
                            p.unmask();
                            if(rs.error) return Message.error(rs.error);
                            if(p.hasClass('item') && $('cart_exchange').getChildren().length == 2) $('cart_exchange').destroy();
                            else p.destroy();
                            updateCart(rs, p);
                        }
                    }).post(p.get('data-product'));
                }
            }.bind(this));
            document.body.store('dialog-tip_show', this);
        },
        'click:relay(.action-clean)': function(e) {
        Dialog.confirm('确定清空购物车吗？', function(e){
            if(e) {
                new Request({
                    url:'<{link app=b2c ctl=site_cart act=remove}>',
                    data:'obj_type=all&response_json=true',
                    onRequest: function() {
                        container.mask({html:'处理中...','class':'cover',position:true,effect:false, resize: false});
                    },
                    onSuccess:function(rs) {
                        rs = JSON.decode(rs);
                        container.unmask();
                        if(rs.error) return Message.error(rs.error);
                        location.reload();
                    }
                }).post();
            }
        });
    },
    'setQuantity':    function (input, value) {
            var type = input.getParent('.p-packs') ? 'packs' : input.getParent('.cart-exchange') ? 'exchange' : 'product';
            var typeName = typeJSON[type];
            var p = input.getParent('tr');
            if(!inputCheck(input, {min: getQuantity(p, 'min'), max: getQuantity(p, 'max'), 'default': value, type: type, store: getQuantity(p, 'stock')}) && input.value == value) {
                return;
            }
            //更新商品数量
            new Request({
                url: '<{link app=b2c act=update ctl=site_cart}>',
                data: p.toQueryString()+"&response_json=true",
                onComplete: function(rs){
                    rs = JSON.decode(rs);
                    if(rs.success) {
                        updateCart(rs, p);
                    }
                }
            }).post();
        }
    }

})(jQuery);