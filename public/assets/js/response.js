function throttle(fn, wait, options) {
    var options = options || {};
    var timer;
    var previous = 0;
    var throttled = function () {
        var now = +new Date();
        if (!previous && options.leading === false) previous = now;
        var remaining = wait - (now - previous);
        if (remaining < 0) {
            if (timer) {
                clearTimeout(timer);
                timer = null;
            }
            previous = now;
            fn.apply(this, arguments)
        } else if (!timer && options.trailing !== false) {
            timer = setTimeout(function(){
                previous = options.leading === false ? 0 : new Date().getTime();
                timer = null;
                fn.apply(this, arguments);
            }, remaining);
        }
    }
    return throttled;
}
function UIResponse(callback){
    var docEl = document.documentElement;
    var clientWidth = docEl.clientWidth;
    if(!clientWidth) return;
    if(clientWidth > 1900){
        docEl.style.fontSize = '100px';
    }else{
        docEl.style.fontSize = 100 * (clientWidth / 1900) + 'px';
    }
    typeof callback == 'function' ? callback.call(this) : null;
};
UIResponse();