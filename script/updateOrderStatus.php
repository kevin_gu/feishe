#!/usr/bin/env php
<?php

ob_implicit_flush(1);
$root_dir = realpath(dirname(__FILE__).'/../');

require($root_dir."/config/config.php");

require(APP_DIR.'/base/kernel.php');
@require(APP_DIR.'/base/defined.php');

if(!kernel::register_autoload()) {
    require(APP_DIR . '/base/autoload.php');
}
$lockFile = ROOT_DIR."/data/check_order_status";
$canExecute = true;
$date = date("Y-m-d H:i:s");
if(file_exists($lockFile)){
    $canExecute = false;
    $time = file_get_contents($lockFile);
    if($time < time()){
        unlink($lockFile);
        $canExecute = true;
    }
}
if(!$canExecute){
    exit($date."\t".__FILE__.' is running');
}
file_put_contents($lockFile,time()+3600*5);
try{
    b2c_mdl_orders::checkOrderStatus();
}catch (Exception $e){
    echo $date.PHP_EOL;
    echo $e->getMessage().PHP_EOL;
    echo $e->getTraceAsString().PHP_EOL;
}finally{
    unlink($lockFile);
}